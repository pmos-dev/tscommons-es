import { commonsBase62HasPropertyId, commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TCommonsSocketIoPseudoResponse = {
		clientId: string;
		queryUid: string;
		result: string;
};

export function isTCommonsSocketIoPseudoResponse(test: unknown): test is TCommonsSocketIoPseudoResponse {
	if (!commonsBase62HasPropertyId(test, 'clientId')) return false;
	if (!commonsBase62HasPropertyId(test, 'queryUid')) return false;
	if (!commonsTypeHasPropertyString(test, 'result')) return false;

	return true;
}
