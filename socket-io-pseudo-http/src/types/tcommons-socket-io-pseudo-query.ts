import {
		TEncodedObject,
		TKeyObject,
		TPrimativeObject,
		commonsBase62HasPropertyId,
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyObjectOrUndefined,
		commonsTypeHasPropertyString,
		commonsTypeIsEncodedObject,
		commonsTypeIsStringKeyObject
} from 'tscommons-es-core';

export type TCommonsSocketIoPseudoQuery = {
		pseudoDomain: string;
		clientId: string;
		queryUid: string;
		url: string;
		query: TPrimativeObject;
		headers: TKeyObject<string>;
		body?: TEncodedObject;
};

export function isTCommonsSocketIoPseudoQuery(test: unknown): test is TCommonsSocketIoPseudoQuery {
	if (!commonsTypeHasPropertyString(test, 'pseudoDomain')) return false;
	if (!commonsBase62HasPropertyId(test, 'clientId')) return false;
	if (!commonsBase62HasPropertyId(test, 'queryUid')) return false;
	if (!commonsTypeHasPropertyString(test, 'url')) return false;

	if (!commonsTypeHasPropertyObject(test, 'query') || !commonsTypeIsEncodedObject(test.query)) return false;
	if (!commonsTypeHasPropertyObject(test, 'headers') || !commonsTypeIsStringKeyObject(test.headers)) return false;
	if (!commonsTypeHasPropertyObjectOrUndefined(test, 'body')) return false;
	if (commonsTypeHasPropertyObject(test, 'body') && !commonsTypeIsEncodedObject(test.body)) return false;

	return true;
}
