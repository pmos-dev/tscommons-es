import {
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyStringOrUndefined,
		TKeyObject
} from 'tscommons-es-core';

export type TCommonsSocketIoPseudoDecodedResponse = {
		isEmpty: true;
		headers: TKeyObject<string>;
		statusCode: number;
} | {
		headers: TKeyObject<string>;
		statusCode: number;
		body: string;
};

export function isTCommonsSocketIoPseudoDecodedResponse(test: unknown): test is TCommonsSocketIoPseudoDecodedResponse {
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'empty')) return false;
	if (test.empty === false) return false;

	if (!commonsTypeHasPropertyObject(test, 'headers')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'statusCode')) return false;
	
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'body')) return false;
	if (test.empty === true && test.body !== undefined) return false;

	return true;
}
