import {
		CommonsGenericSecondClassFieldModelCache,
		CommonsSecondClassCache,
		CommonsUniquelyIdentifiedCache,
		ICommonsFirstClass,
		ICommonsSecondClass,
		ICommonsUniquelyIdentified
} from 'tscommons-es-models';

import { ICommonsManaged } from '../interfaces/icommons-managed';

export class NamedSecondClassCache<
		ModelT extends ICommonsManaged & ICommonsSecondClass<ParentT>,
		ParentT extends ICommonsFirstClass
> extends CommonsGenericSecondClassFieldModelCache<
		ModelT,
		ParentT,
		string
> {
	constructor(
			getRemote: (parent: ParentT, name: string) => Promise<ModelT|undefined>,
			maxAgeInMillis?: number
	) {
		super(
				'name',
				getRemote,
				(): Promise<string[]> => { throw new Error('listRemoteIds is not applicable for NamedFirstClassCache'); },	// eslint-disable-line brace-style
				(): Promise<ModelT[]> => { throw new Error('listRemotes is not applicable for NamedFirstClassCache'); },	// eslint-disable-line brace-style
				maxAgeInMillis
		);
	}
}

export class ManagedSecondClassCache<
		ModelT extends ICommonsManaged & ICommonsSecondClass<ParentT>,
		ParentT extends ICommonsFirstClass
> {
	protected idCache: CommonsSecondClassCache<ModelT, ParentT>;
	protected nameCache: NamedSecondClassCache<ModelT, ParentT>;

	constructor(
			getRemoteById: (parent: ParentT, id: number) => Promise<ModelT|undefined>,
			getRemoteByName: (parent: ParentT, name: string) => Promise<ModelT|undefined>,
			listRemoteIds?: (parent: ParentT) => Promise<number[]>,
			listRemotes?: (parent: ParentT) => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		this.idCache = new CommonsSecondClassCache<ModelT, ParentT>(
				getRemoteById,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);
		this.nameCache = new NamedSecondClassCache<ModelT, ParentT>(
				getRemoteByName,
				maxAgeInMillis
		);
	}

	public async getById(parent: ParentT, id: number): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.idCache.get(parent, id);

		// sync
		if (item) this.nameCache.appendToCacheOnly(parent, item, false);

		return item;
	}

	public async getByName(parent: ParentT, name: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.nameCache.get(parent, name);

		// sync
		if (item) this.idCache.appendToCacheOnly(parent, item, false);

		return item;
	}

	public async list(parent: ParentT): Promise<ModelT[]> {
		const items: ModelT[] = await this.idCache.list(parent);

		// sync
		for (const item of items) this.nameCache.appendToCacheOnly(parent, item, false);

		return items;
	}

	public clearById(parent: ParentT, id: number): void {
		const existing: ModelT|undefined = this.idCache.getFromCacheOnly(parent, id);

		if (existing) this.nameCache.clear(parent, existing.name);

		this.idCache.clear(parent, id);
	}

	public clearByName(parent: ParentT, name: string): void {
		const existing: ModelT|undefined = this.nameCache.getFromCacheOnly(parent, name);

		if (existing) this.idCache.clear(parent, existing.id);

		this.nameCache.clear(parent, name);
	}

	public wipe(parent: ParentT): void {
		this.idCache.wipe(parent);
		this.nameCache.wipe(parent);
	}

	public wipeAll(): void {
		this.idCache.wipeAll();
		this.nameCache.wipeAll();
	}

	// not intended for use outside of other models
	public appendToCacheOnly(
			parent: ParentT,
			data: ModelT,
			resetListIfNew: boolean = false
	): void {
		this.idCache.appendToCacheOnly(parent, data, resetListIfNew);
		this.nameCache.appendToCacheOnly(parent, data, resetListIfNew);
	}
}

export class ManagedUniquelyIdentifiedSecondClassCache<
		ModelT extends ICommonsManaged & ICommonsSecondClass<ParentT> & ICommonsUniquelyIdentified,
		ParentT extends ICommonsFirstClass
> extends ManagedSecondClassCache<
		ModelT,
		ParentT
> {
	private uidCache: CommonsUniquelyIdentifiedCache<ModelT>;

	constructor(
			private parentField: string,
			getRemoteById: (parent: ParentT, id: number) => Promise<ModelT|undefined>,
			getRemoteByName: (parent: ParentT, name: string) => Promise<ModelT|undefined>,
			getRemoteByUid: (uid: string) => Promise<ModelT|undefined>,
			listRemoteIds?: (parent: ParentT) => Promise<number[]>,
			listRemotes?: (parent: ParentT) => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		super(
				getRemoteById,
				getRemoteByName,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);

		this.uidCache = new CommonsUniquelyIdentifiedCache<ModelT>(
				getRemoteByUid,
				maxAgeInMillis
		);
	}

	public async getById(parent: ParentT, id: number): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await super.getById(parent, id);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByName(parent: ParentT, name: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await super.getByName(parent, name);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByUid(uid: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.uidCache.get(uid);

		// sync
		if (item) {
			const parentHack: ParentT = { id: item[this.parentField ] as number } as ParentT;	// eslint-disable-line @typescript-eslint/consistent-type-assertions
			super.appendToCacheOnly(parentHack, item, false);
		}

		return item;
	}

	public async list(parent: ParentT): Promise<ModelT[]> {
		const items: ModelT[] = await super.list(parent);

		// sync
		for (const item of items) this.uidCache.appendToCacheOnly(item, false);

		return items;
	}

	public clearById(parent: ParentT, id: number): void {
		const existing: ModelT|undefined = super.idCache.getFromCacheOnly(parent, id);

		super.clearById(parent, id);

		if (existing) this.uidCache.clear(existing.uid);
	}

	public clearByName(parent: ParentT, name: string): void {
		const existing: ModelT|undefined = super.nameCache.getFromCacheOnly(parent, name);
		
		super.clearByName(parent, name);

		if (existing) this.uidCache.clear(existing.uid);
	}

	public clearByUid(uid: string): void {
		const existing: ModelT|undefined = this.uidCache.getFromCacheOnly(uid);
		if (!existing) return;

		this.uidCache.clear(existing.uid);

		const parentHack: ParentT = { id: existing[this.parentField ] as number } as ParentT;	// eslint-disable-line @typescript-eslint/consistent-type-assertions

		super.clearById(parentHack, existing.id);
	}

	public wipe(parent: ParentT): void {
		super.wipe(parent);

		// can't do this selectively, so have to wipe the lot sadly
		this.uidCache.wipe();
	}

	public wipeAll(): void {
		super.wipeAll();
		this.uidCache.wipe();
	}
}
