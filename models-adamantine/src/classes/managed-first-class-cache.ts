import {
		CommonsFirstClassCache,
		CommonsGenericFieldModelCache,
		CommonsUniquelyIdentifiedCache,
		ICommonsFirstClass,
		ICommonsUniquelyIdentified
} from 'tscommons-es-models';

import { ICommonsManaged } from '../interfaces/icommons-managed';

export class NamedFirstClassCache<
		ModelT extends ICommonsManaged & ICommonsFirstClass
> extends CommonsGenericFieldModelCache<ModelT, string> {
	constructor(
			getRemote: (name: string) => Promise<ModelT|undefined>,
			maxAgeInMillis?: number
	) {
		super(
				'name',
				getRemote,
				(): Promise<string[]> => { throw new Error('listRemoteIds is not applicable for NamedFirstClassCache'); },	// eslint-disable-line brace-style
				(): Promise<ModelT[]> => { throw new Error('listRemotes is not applicable for NamedFirstClassCache'); },	// eslint-disable-line brace-style
				maxAgeInMillis
		);
	}
}

export class ManagedFirstClassCache<
		ModelT extends ICommonsFirstClass & ICommonsManaged
> {
	protected idCache: CommonsFirstClassCache<ModelT>;
	protected nameCache: NamedFirstClassCache<ModelT>;

	constructor(
			getRemoteById: (id: number) => Promise<ModelT|undefined>,
			getRemoteByName: (name: string) => Promise<ModelT|undefined>,
			listRemoteIds?: () => Promise<number[]>,
			listRemotes?: () => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		this.idCache = new CommonsFirstClassCache<ModelT>(
				getRemoteById,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);
		this.nameCache = new NamedFirstClassCache<ModelT>(
				getRemoteByName,
				maxAgeInMillis
		);
	}

	public async getById(id: number): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.idCache.get(id);

		// sync
		if (item) this.nameCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByName(name: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.nameCache.get(name);

		// sync
		if (item) this.idCache.appendToCacheOnly(item, false);

		return item;
	}

	public async list(): Promise<ModelT[]> {
		const items: ModelT[] = await this.idCache.list();

		// sync
		for (const item of items) this.nameCache.appendToCacheOnly(item, false);

		return items;
	}

	public clearById(id: number): void {
		const existing: ModelT|undefined = this.idCache.getFromCacheOnly(id);

		if (existing) this.nameCache.clear(existing.name);

		this.idCache.clear(id);
	}

	public clearByName(name: string): void {
		const existing: ModelT|undefined = this.nameCache.getFromCacheOnly(name);

		if (existing) this.idCache.clear(existing.id);

		this.nameCache.clear(name);
	}

	public wipe(): void {
		this.idCache.wipe();
		this.nameCache.wipe();
	}

	// not intended for use outside of other models
	public appendToCacheOnly(
			data: ModelT,
			resetListIfNew: boolean = false
	): void {
		this.idCache.appendToCacheOnly(data, resetListIfNew);
		this.nameCache.appendToCacheOnly(data, resetListIfNew);
	}
}

export class ManagedUniquelyIdentifiedFirstClassCache<
		ModelT extends ICommonsFirstClass & ICommonsManaged & ICommonsUniquelyIdentified
> extends ManagedFirstClassCache<
		ModelT
> {
	private uidCache: CommonsUniquelyIdentifiedCache<ModelT>;

	constructor(
			getRemoteById: (id: number) => Promise<ModelT|undefined>,
			getRemoteByName: (name: string) => Promise<ModelT|undefined>,
			getRemoteByUid: (uid: string) => Promise<ModelT|undefined>,
			listRemoteIds?: () => Promise<number[]>,
			listRemotes?: () => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		super(
				getRemoteById,
				getRemoteByName,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);

		this.uidCache = new CommonsUniquelyIdentifiedCache<ModelT>(
				getRemoteByUid,
				maxAgeInMillis
		);
	}

	public async getById(id: number): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await super.getById(id);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByName(name: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await super.getByName(name);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByUid(uid: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.uidCache.get(uid);

		// sync
		if (item) super.appendToCacheOnly(item, false);

		return item;
	}

	public async list(): Promise<ModelT[]> {
		const items: ModelT[] = await super.list();

		// sync
		for (const item of items) this.uidCache.appendToCacheOnly(item, false);

		return items;
	}

	public clearById(id: number): void {
		const existing: ModelT|undefined = super.idCache.getFromCacheOnly(id);

		super.clearById(id);

		if (existing) this.uidCache.clear(existing.uid);
	}

	public clearByName(name: string): void {
		const existing: ModelT|undefined = super.nameCache.getFromCacheOnly(name);
		
		super.clearByName(name);

		if (existing) this.uidCache.clear(existing.uid);
	}

	public clearByUid(uid: string): void {
		const existing: ModelT|undefined = this.uidCache.getFromCacheOnly(uid);
		if (!existing) return;

		this.uidCache.clear(existing.uid);

		super.clearById(existing.id);
	}

	public wipe(): void {
		super.idCache.wipe();
		super.nameCache.wipe();
		this.uidCache.wipe();
	}
}
