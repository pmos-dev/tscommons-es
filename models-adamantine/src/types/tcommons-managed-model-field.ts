import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyStringArrayOrUndefined,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyNumberOrUndefined,
		TEncodedObject
} from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models';

import { ICommonsManagedModelField } from '../interfaces/icommons-managed-model-field';

import { ECommonsAdamantineManagedModelFieldType, isECommonsAdamantineManagedModelFieldType } from '../enums/ecommons-adamantine-managed-model-field-type';

// This is about which fields of adamantine models can be managed, and various getSuffix() methods etc.
// It is nothing to do with models-field and Field

// Omit can't be used any more
export type TCommonsManagedModelField = ICommonsModel & {
		name: string;
		type: ECommonsAdamantineManagedModelFieldType;
		optional: boolean;
} & TEncodedObject;

type TCommonsManagedModelFieldWithDescription = TCommonsManagedModelField & {
		description: string;
};
type TCommonsManagedModelFieldWithOptions = TCommonsManagedModelField & {
		options: string[];
};
type TCommonsManagedModelFieldWithSuffix = TCommonsManagedModelField & {
		suffix: string;
};
type TCommonsManagedModelFieldWithHelper = TCommonsManagedModelField & {
		helper: string;
};
type TCommonsManagedModelFieldWithAlpha = TCommonsManagedModelField & {
		alpha: boolean;
};
type TCommonsManagedModelFieldWithDefaultValue = TCommonsManagedModelField & {
		defaultValue: string|number|boolean;
};

export function isTCommonsManagedModelField(
		test: unknown
): test is TCommonsManagedModelField {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineManagedModelFieldType>(test, 'type', isECommonsAdamantineManagedModelFieldType)) return false;
	if (!commonsTypeHasPropertyBoolean(test, 'optional')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'description')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'suffix')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'helper')) return false;
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, 'options')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'alpha')) return false;
	
	if (
		!commonsTypeHasPropertyNumberOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyStringOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyBooleanOrUndefined(test, 'defaultValue')
	) return false;
	
	return true;
}

export function encodeICommonsManagedModelField(data: ICommonsManagedModelField): TCommonsManagedModelField {
	const encoded: TCommonsManagedModelField = {
			name: data.name,
			type: data.type,
			optional: data.optional
	};
	
	if (data.description !== undefined) {
		(encoded as TCommonsManagedModelFieldWithDescription).description = data.description;
	}
	
	if (data.suffix !== undefined) {
		(encoded as TCommonsManagedModelFieldWithSuffix).suffix = data.suffix;
	}
	
	if (data.helper !== undefined) {
		(encoded as TCommonsManagedModelFieldWithHelper).helper = data.helper;
	}
	
	if (data.options !== undefined) {
		(encoded as TCommonsManagedModelFieldWithOptions).options = data.options;
	}
	
	if (data.alpha !== undefined) {
		(encoded as TCommonsManagedModelFieldWithAlpha).alpha = data.alpha;
	}
	
	if (data.defaultValue !== undefined) {
		(encoded as TCommonsManagedModelFieldWithDefaultValue).defaultValue = data.defaultValue;
	}
	
	return encoded;
}

export function decodeICommonsManagedModelField(encoded: TCommonsManagedModelField): ICommonsManagedModelField {
	const decoded: ICommonsManagedModelField = {
			name: encoded.name,
			type: encoded.type,
			optional: encoded.optional
	};
	
	if ((encoded as TCommonsManagedModelFieldWithDescription).description !== undefined) decoded.description = (encoded as TCommonsManagedModelFieldWithDescription).description;
	if ((encoded as TCommonsManagedModelFieldWithSuffix).suffix) decoded.suffix = (encoded as TCommonsManagedModelFieldWithSuffix).suffix;
	if ((encoded as TCommonsManagedModelFieldWithHelper).helper) decoded.helper = (encoded as TCommonsManagedModelFieldWithHelper).helper;
	if ((encoded as TCommonsManagedModelFieldWithOptions).options !== undefined) decoded.options = (encoded as TCommonsManagedModelFieldWithOptions).options;
	if ((encoded as TCommonsManagedModelFieldWithAlpha).alpha !== undefined) decoded.alpha = (encoded as TCommonsManagedModelFieldWithAlpha).alpha;
	if ((encoded as TCommonsManagedModelFieldWithDefaultValue).defaultValue !== undefined) decoded.defaultValue = (encoded as TCommonsManagedModelFieldWithDefaultValue).defaultValue;
	
	return decoded;
}
