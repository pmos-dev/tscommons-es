import {
		commonsTypeHasPropertyTArray,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyBooleanOrUndefined
} from 'tscommons-es-core';

import { ICommonsManagedModelMetadata } from '../interfaces/icommons-managed-model-metadata';
import { ICommonsManagedModelField } from '../interfaces/icommons-managed-model-field';

import { ECommonsAdamantineAccess, isECommonsAdamantineAccess } from '../enums/ecommons-adamantine-access';

import { TCommonsManagedModelField, isTCommonsManagedModelField, encodeICommonsManagedModelField, decodeICommonsManagedModelField } from './tcommons-managed-model-field';

// Omit can't be used any more
export type TCommonsManagedModelMetadata = {
		accessRequiredToManage: ECommonsAdamantineAccess;
		manageableFields: TCommonsManagedModelField[];
};

type TCommonsManagedModelMetadataWithIsAliasingSupported = TCommonsManagedModelMetadata & {
		isAliasingSupported: boolean;
};

export function isTCommonsManagedModelMetadata(test: unknown): test is TCommonsManagedModelMetadata {
	if (!commonsTypeHasPropertyTArray<TCommonsManagedModelField>(test, 'manageableFields', isTCommonsManagedModelField)) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'isAliasingSupported')) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineAccess>(test, 'accessRequiredToManage', isECommonsAdamantineAccess)) return false;
	
	return true;
}

export function encodeICommonsManagedModelMetadata(data: ICommonsManagedModelMetadata): TCommonsManagedModelMetadata {
	const encoded: TCommonsManagedModelMetadata = {
			// tslint:disable:object-literal-sort-keys
			manageableFields: data.manageableFields
					.map((field: ICommonsManagedModelField): TCommonsManagedModelField => encodeICommonsManagedModelField(field)),
			accessRequiredToManage: data.accessRequiredToManage
			// tslint:enable:object-literal-sort-keys
	};
	
	if (data.isAliasingSupported !== undefined) {
		(encoded as TCommonsManagedModelMetadataWithIsAliasingSupported).isAliasingSupported = data.isAliasingSupported;
	}
	
	return encoded;
}

export function decodeICommonsManagedModelMetadata(encoded: TCommonsManagedModelMetadata): ICommonsManagedModelMetadata {
	const decoded: ICommonsManagedModelMetadata = {
			// tslint:disable:object-literal-sort-keys
			manageableFields: encoded.manageableFields
					.map((field: TCommonsManagedModelField): ICommonsManagedModelField => decodeICommonsManagedModelField(field)),
			accessRequiredToManage: encoded.accessRequiredToManage
			// tslint:enable:object-literal-sort-keys
	};

	if (encoded['isAliasingSupported'] !== undefined) {
		decoded.isAliasingSupported = (encoded as TCommonsManagedModelMetadataWithIsAliasingSupported).isAliasingSupported;
	}
	
	return decoded;
}
