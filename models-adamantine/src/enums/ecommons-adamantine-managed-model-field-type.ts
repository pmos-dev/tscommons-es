import { commonsTypeIsString } from 'tscommons-es-core';

// NB, this is about possible model field types, i.e. the fields of models
// It is not about ModelField.type, i.e. the types of fields [data field rows] that can be created 
// The main difference is that STRINGARRAY exists here, as it is needed for things like field options
// It is confusing as there is considerable overlap between the values, even though they mean two different things!

export enum ECommonsAdamantineManagedModelFieldType {
		BOOLEAN = 'boolean',
		DATETIME = 'datetime',
		DATE = 'date',
		EMAIL = 'email',
		ENUM = 'enum',
		FLOAT = 'float',
		HEXRGB = 'hexrgb',
		INT = 'int',
		SMALLINT = 'smallint',
		STRING = 'string',
		TEXT = 'text',
		TIME = 'time',
		TINYINT = 'tinyint',
		URL = 'url',
		STRINGARRAY = 'stringarray',
		DURATION = 'duration'
}

export function toECommonsAdamantineManagedModelFieldType(type: string): ECommonsAdamantineManagedModelFieldType|undefined {
	switch (type) {
		case ECommonsAdamantineManagedModelFieldType.BOOLEAN.toString():
			return ECommonsAdamantineManagedModelFieldType.BOOLEAN;
		case ECommonsAdamantineManagedModelFieldType.DATETIME.toString():
			return ECommonsAdamantineManagedModelFieldType.DATETIME;
		case ECommonsAdamantineManagedModelFieldType.DATE.toString():
			return ECommonsAdamantineManagedModelFieldType.DATE;
		case ECommonsAdamantineManagedModelFieldType.EMAIL.toString():
			return ECommonsAdamantineManagedModelFieldType.EMAIL;
		case ECommonsAdamantineManagedModelFieldType.ENUM.toString():
			return ECommonsAdamantineManagedModelFieldType.ENUM;
		case ECommonsAdamantineManagedModelFieldType.FLOAT.toString():
			return ECommonsAdamantineManagedModelFieldType.FLOAT;
		case ECommonsAdamantineManagedModelFieldType.HEXRGB.toString():
			return ECommonsAdamantineManagedModelFieldType.HEXRGB;
		case ECommonsAdamantineManagedModelFieldType.INT.toString():
			return ECommonsAdamantineManagedModelFieldType.INT;
		case ECommonsAdamantineManagedModelFieldType.SMALLINT.toString():
			return ECommonsAdamantineManagedModelFieldType.SMALLINT;
		case ECommonsAdamantineManagedModelFieldType.STRING.toString():
			return ECommonsAdamantineManagedModelFieldType.STRING;
		case ECommonsAdamantineManagedModelFieldType.TEXT.toString():
			return ECommonsAdamantineManagedModelFieldType.TEXT;
		case ECommonsAdamantineManagedModelFieldType.TIME.toString():
			return ECommonsAdamantineManagedModelFieldType.TIME;
		case ECommonsAdamantineManagedModelFieldType.TINYINT.toString():
			return ECommonsAdamantineManagedModelFieldType.TINYINT;
		case ECommonsAdamantineManagedModelFieldType.URL.toString():
			return ECommonsAdamantineManagedModelFieldType.URL;
		case ECommonsAdamantineManagedModelFieldType.STRINGARRAY.toString():
			return ECommonsAdamantineManagedModelFieldType.STRINGARRAY;
		case ECommonsAdamantineManagedModelFieldType.DURATION.toString():
			return ECommonsAdamantineManagedModelFieldType.DURATION;
	}
	return undefined;
}

export function fromECommonsAdamantineManagedModelFieldType(type: ECommonsAdamantineManagedModelFieldType): string {
	switch (type) {
		case ECommonsAdamantineManagedModelFieldType.BOOLEAN:
			return ECommonsAdamantineManagedModelFieldType.BOOLEAN.toString();
		case ECommonsAdamantineManagedModelFieldType.DATETIME:
			return ECommonsAdamantineManagedModelFieldType.DATETIME.toString();
		case ECommonsAdamantineManagedModelFieldType.DATE:
			return ECommonsAdamantineManagedModelFieldType.DATE.toString();
		case ECommonsAdamantineManagedModelFieldType.EMAIL:
			return ECommonsAdamantineManagedModelFieldType.EMAIL.toString();
		case ECommonsAdamantineManagedModelFieldType.ENUM:
			return ECommonsAdamantineManagedModelFieldType.ENUM.toString();
		case ECommonsAdamantineManagedModelFieldType.FLOAT:
			return ECommonsAdamantineManagedModelFieldType.FLOAT.toString();
		case ECommonsAdamantineManagedModelFieldType.HEXRGB:
			return ECommonsAdamantineManagedModelFieldType.HEXRGB.toString();
		case ECommonsAdamantineManagedModelFieldType.INT:
			return ECommonsAdamantineManagedModelFieldType.INT.toString();
		case ECommonsAdamantineManagedModelFieldType.SMALLINT:
			return ECommonsAdamantineManagedModelFieldType.SMALLINT.toString();
		case ECommonsAdamantineManagedModelFieldType.STRING:
			return ECommonsAdamantineManagedModelFieldType.STRING.toString();
		case ECommonsAdamantineManagedModelFieldType.TEXT:
			return ECommonsAdamantineManagedModelFieldType.TEXT.toString();
		case ECommonsAdamantineManagedModelFieldType.TIME:
			return ECommonsAdamantineManagedModelFieldType.TIME.toString();
		case ECommonsAdamantineManagedModelFieldType.TINYINT:
			return ECommonsAdamantineManagedModelFieldType.TINYINT.toString();
		case ECommonsAdamantineManagedModelFieldType.URL:
			return ECommonsAdamantineManagedModelFieldType.URL.toString();
		case ECommonsAdamantineManagedModelFieldType.STRINGARRAY:
			return ECommonsAdamantineManagedModelFieldType.STRINGARRAY.toString();
		case ECommonsAdamantineManagedModelFieldType.DURATION:
			return ECommonsAdamantineManagedModelFieldType.DURATION.toString();
	}
	
	throw new Error('Unknown ECommonsAdamantineManagedModelFieldType');
}

export function isECommonsAdamantineManagedModelFieldType(test: unknown): test is ECommonsAdamantineManagedModelFieldType {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsAdamantineManagedModelFieldType(test) !== undefined;
}

export function keyToECommonsAdamantineManagedModelFieldType(key: string): ECommonsAdamantineManagedModelFieldType {
	switch (key) {
		case 'BOOLEAN':
			return ECommonsAdamantineManagedModelFieldType.BOOLEAN;
		case 'DATETIME':
			return ECommonsAdamantineManagedModelFieldType.DATETIME;
		case 'DATE':
			return ECommonsAdamantineManagedModelFieldType.DATE;
		case 'EMAIL':
			return ECommonsAdamantineManagedModelFieldType.EMAIL;
		case 'ENUM':
			return ECommonsAdamantineManagedModelFieldType.ENUM;
		case 'FLOAT':
			return ECommonsAdamantineManagedModelFieldType.FLOAT;
		case 'HEXRGB':
			return ECommonsAdamantineManagedModelFieldType.HEXRGB;
		case 'INT':
			return ECommonsAdamantineManagedModelFieldType.INT;
		case 'SMALLINT':
			return ECommonsAdamantineManagedModelFieldType.SMALLINT;
		case 'STRING':
			return ECommonsAdamantineManagedModelFieldType.STRING;
		case 'TEXT':
			return ECommonsAdamantineManagedModelFieldType.TEXT;
		case 'TIME':
			return ECommonsAdamantineManagedModelFieldType.TIME;
		case 'TINYINT':
			return ECommonsAdamantineManagedModelFieldType.TINYINT;
		case 'URL':
			return ECommonsAdamantineManagedModelFieldType.URL;
		case 'STRINGARRAY':
			return ECommonsAdamantineManagedModelFieldType.STRINGARRAY;
		case 'DURATION':
			return ECommonsAdamantineManagedModelFieldType.DURATION;
	}
	
	throw new Error(`Unable to obtain ECommonsAdamantineManagedModelFieldType for key: ${key}`);
}

export const ECOMMONS_ADAMANTINE_MANAGED_MODEL_FIELD_TYPES: ECommonsAdamantineManagedModelFieldType[] = Object.keys(ECommonsAdamantineManagedModelFieldType)
		.map((e: string): ECommonsAdamantineManagedModelFieldType => keyToECommonsAdamantineManagedModelFieldType(e));
