import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsAdamantineAccess {
		NONE = 'none',
		READ = 'read',
		USER = 'user',
		EDITOR = 'editor',
		FULL = 'full'
}

export function toECommonsAdamantineAccess(type: string): ECommonsAdamantineAccess|undefined {
	switch (type) {
		case ECommonsAdamantineAccess.NONE.toString():
			return ECommonsAdamantineAccess.NONE;
		case ECommonsAdamantineAccess.READ.toString():
			return ECommonsAdamantineAccess.READ;
		case ECommonsAdamantineAccess.USER.toString():
			return ECommonsAdamantineAccess.USER;
		case ECommonsAdamantineAccess.EDITOR.toString():
			return ECommonsAdamantineAccess.EDITOR;
		case ECommonsAdamantineAccess.FULL.toString():
			return ECommonsAdamantineAccess.FULL;
	}
	return undefined;
}

export function fromECommonsAdamantineAccess(type: ECommonsAdamantineAccess): string {
	switch (type) {
		case ECommonsAdamantineAccess.NONE:
			return ECommonsAdamantineAccess.NONE.toString();
		case ECommonsAdamantineAccess.READ:
			return ECommonsAdamantineAccess.READ.toString();
		case ECommonsAdamantineAccess.USER:
			return ECommonsAdamantineAccess.USER.toString();
		case ECommonsAdamantineAccess.EDITOR:
			return ECommonsAdamantineAccess.EDITOR.toString();
		case ECommonsAdamantineAccess.FULL:
			return ECommonsAdamantineAccess.FULL.toString();
	}
	
	throw new Error('Unknown ECommonsAdamantineAccess');
}

export function isECommonsAdamantineAccess(test: unknown): test is ECommonsAdamantineAccess {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsAdamantineAccess(test) !== undefined;
}

export function keyToECommonsAdamantineAccess(key: string): ECommonsAdamantineAccess {
	switch (key) {
		case 'NONE':
			return ECommonsAdamantineAccess.NONE;
		case 'READ':
			return ECommonsAdamantineAccess.READ;
		case 'USER':
			return ECommonsAdamantineAccess.USER;
		case 'EDITOR':
			return ECommonsAdamantineAccess.EDITOR;
		case 'FULL':
			return ECommonsAdamantineAccess.FULL;
	}
	
	throw new Error(`Unable to obtain ECommonsAdamantineAccess for key: ${key}`);
}

export const ECOMMONS_ADAMANTINE_ACCESSS: ECommonsAdamantineAccess[] = Object.keys(ECommonsAdamantineAccess)
		.map((e: string): ECommonsAdamantineAccess => keyToECommonsAdamantineAccess(e));

export function computeECommonsAdamantineAccess(has: ECommonsAdamantineAccess, required: ECommonsAdamantineAccess): boolean {
	switch (required) {
		case ECommonsAdamantineAccess.NONE:
			// i.e. guest access allowed
			return true;
		case ECommonsAdamantineAccess.READ:
			switch (has) {
				case ECommonsAdamantineAccess.READ:
				case ECommonsAdamantineAccess.USER:
				case ECommonsAdamantineAccess.EDITOR:
				case ECommonsAdamantineAccess.FULL:
					return true;
				default:
					return false;
			}
		case ECommonsAdamantineAccess.USER:
			switch (has) {
				case ECommonsAdamantineAccess.USER:
				case ECommonsAdamantineAccess.EDITOR:
				case ECommonsAdamantineAccess.FULL:
					return true;
				default:
					return false;
			}
		case ECommonsAdamantineAccess.EDITOR:
			switch (has) {
				case ECommonsAdamantineAccess.EDITOR:
				case ECommonsAdamantineAccess.FULL:
					return true;
				default:
					return false;
			}
		case ECommonsAdamantineAccess.FULL:
			switch (has) {
				case ECommonsAdamantineAccess.FULL:
					return true;
				default:
					return false;
			}
	}
	
	throw new Error('Unknown access');
}
