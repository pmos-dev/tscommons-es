import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUser, isICommonsUser } from 'tscommons-es-session';

import { ICommonsManaged, isICommonsManaged } from './icommons-managed';
import { ICommonsAccess, isICommonsAccess } from './icommons-access';

export interface ICommonsAdamantineManagedSecondClassUser<
		P extends ICommonsFirstClass
> extends ICommonsManaged, ICommonsSecondClass<P>, ICommonsUser, ICommonsAccess {}

export function isICommonsAdamantineManagedSecondClassUser<
		P extends ICommonsFirstClass
>(
		test: unknown,
		firstClassField: string
): test is ICommonsAdamantineManagedSecondClassUser<P> {
	if (!isICommonsSecondClass<P>(test, firstClassField)) return false;
	if (!isICommonsManaged(test)) return false;
	if (!isICommonsUser(test)) return false;
	if (!isICommonsAccess(test)) return false;
	
	return true;
}
