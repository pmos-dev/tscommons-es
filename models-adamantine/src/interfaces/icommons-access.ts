// This could conceivably be in tscommons-session, but since the enum is here we might as well put it here too

import { commonsTypeHasPropertyEnum } from 'tscommons-es-core';

import { ECommonsAdamantineAccess, isECommonsAdamantineAccess } from '../enums/ecommons-adamantine-access';

export interface ICommonsAccess {
		access: ECommonsAdamantineAccess;
}

export function isICommonsAccess(
		test: unknown
): test is ICommonsAccess {
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineAccess>(test, 'access', isECommonsAdamantineAccess)) return false;
	
	return true;
}
