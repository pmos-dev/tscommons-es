import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyStringArrayOrUndefined,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models/dist';

import { ECommonsAdamantineManagedModelFieldType, isECommonsAdamantineManagedModelFieldType } from '../enums/ecommons-adamantine-managed-model-field-type';

// This is about which fields of adamantine models can be managed, and various getSuffix() methods etc.
// It is nothing to do with models-field and Field

export interface ICommonsManagedModelField extends ICommonsModel {
		name: string;
		type: ECommonsAdamantineManagedModelFieldType;
		optional: boolean;
		description?: string;
		suffix?: string;
		helper?: string;
		options?: string[];
		alpha?: boolean;
		defaultValue?: number|string|boolean|undefined;
}

export function isICommonsManagedModelField(
		test: unknown
): test is ICommonsManagedModelField {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineManagedModelFieldType>(test, 'type', isECommonsAdamantineManagedModelFieldType)) return false;
	if (!commonsTypeHasPropertyBoolean(test, 'optional')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'description')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'suffix')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'helper')) return false;
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, 'options')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'alpha')) return false;

	if (
		!commonsTypeHasPropertyNumberOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyStringOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyBooleanOrUndefined(test, 'defaultValue')
	) return false;

	return true;
}

// Omit can't be used any more
export interface ICommonsEncodedManagedModelField extends ICommonsModel {
		name: string;
		type: ECommonsAdamantineManagedModelFieldType;
		optional: boolean;
		description?: string;
		suffix?: string;
		helper?: string;
		options?: string[];
		alpha?: boolean;
		defaultValue?: string;
}
