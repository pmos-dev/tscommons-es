import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsFirstClass, isICommonsFirstClass } from 'tscommons-es-models';

import { ICommonsUniqueNamed } from './icommons-unique-named';

export interface ICommonsManaged extends ICommonsFirstClass, ICommonsUniqueNamed {
		name: string;
}

export function isICommonsManaged(test: unknown): test is ICommonsManaged {
	if (!isICommonsFirstClass(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'name')) return false;

	return true;
}
