import {
		commonsTypeHasPropertyTArray,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyBooleanOrUndefined
} from 'tscommons-es-core';

import { ECommonsAdamantineAccess, isECommonsAdamantineAccess } from '../enums/ecommons-adamantine-access';

import { ICommonsManagedModelField, isICommonsManagedModelField } from './icommons-managed-model-field';

export interface ICommonsManagedModelMetadata {
		manageableFields: ICommonsManagedModelField[];
		accessRequiredToManage: ECommonsAdamantineAccess;
		isAliasingSupported?: boolean;
}

export function isICommonsManagedModelMetadata(test: unknown): test is ICommonsManagedModelMetadata {
	if (!commonsTypeHasPropertyTArray<ICommonsManagedModelField>(test, 'manageableFields', isICommonsManagedModelField)) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineAccess>(test, 'accessRequiredToManage', isECommonsAdamantineAccess)) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'isAliasingSupported')) return false;

	return true;
}
