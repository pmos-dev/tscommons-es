import { ICommonsModel } from 'tscommons-es-models';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsUniqueNamed extends ICommonsModel {}
