import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsHttpMethod {
		GET = 'get',
		POST = 'post',
		PUT = 'put',
		PATCH = 'patch',
		DELETE = 'delete',
		HEAD = 'head',
		OPTIONS = 'options'
}

export function toECommonsHttpMethod(type: string): ECommonsHttpMethod|undefined {
	switch (type) {
		case ECommonsHttpMethod.GET.toString():
			return ECommonsHttpMethod.GET;
		case ECommonsHttpMethod.POST.toString():
			return ECommonsHttpMethod.POST;
		case ECommonsHttpMethod.PUT.toString():
			return ECommonsHttpMethod.PUT;
		case ECommonsHttpMethod.PATCH.toString():
			return ECommonsHttpMethod.PATCH;
		case ECommonsHttpMethod.DELETE.toString():
			return ECommonsHttpMethod.DELETE;
		case ECommonsHttpMethod.HEAD.toString():
			return ECommonsHttpMethod.HEAD;
		case ECommonsHttpMethod.OPTIONS.toString():
			return ECommonsHttpMethod.OPTIONS;
	}
	return undefined;
}

export function fromECommonsHttpMethod(type: ECommonsHttpMethod): string {
	switch (type) {
		case ECommonsHttpMethod.GET:
			return ECommonsHttpMethod.GET.toString();
		case ECommonsHttpMethod.POST:
			return ECommonsHttpMethod.POST.toString();
		case ECommonsHttpMethod.PUT:
			return ECommonsHttpMethod.PUT.toString();
		case ECommonsHttpMethod.PATCH:
			return ECommonsHttpMethod.PATCH.toString();
		case ECommonsHttpMethod.DELETE:
			return ECommonsHttpMethod.DELETE.toString();
		case ECommonsHttpMethod.HEAD:
			return ECommonsHttpMethod.HEAD.toString();
		case ECommonsHttpMethod.OPTIONS:
			return ECommonsHttpMethod.OPTIONS.toString();
	}
	
	throw new Error('Unknown ECommonsHttpMethod');
}

export function isECommonsHttpMethod(test: unknown): test is ECommonsHttpMethod {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsHttpMethod(test) !== undefined;
}

export function keyToECommonsHttpMethod(key: string): ECommonsHttpMethod {
	switch (key) {
		case 'GET':
			return ECommonsHttpMethod.GET;
		case 'POST':
			return ECommonsHttpMethod.POST;
		case 'PUT':
			return ECommonsHttpMethod.PUT;
		case 'PATCH':
			return ECommonsHttpMethod.PATCH;
		case 'DELETE':
			return ECommonsHttpMethod.DELETE;
		case 'HEAD':
			return ECommonsHttpMethod.HEAD;
		case 'OPTIONS':
			return ECommonsHttpMethod.OPTIONS;
	}
	
	throw new Error(`Unable to obtain ECommonsHttpMethod for key: ${key}`);
}

export const ECOMMONS_HTTP_METHODS: ECommonsHttpMethod[] = Object.keys(ECommonsHttpMethod)
		.map((e: string): ECommonsHttpMethod => keyToECommonsHttpMethod(e));
