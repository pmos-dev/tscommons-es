import { Observable } from 'rxjs';

import { CommonsHttpError } from '../classes/commons-http-error.class';

export type TCommonsStreamableHttpObservable = {
		dataStream: Observable<string|Uint8Array>;
		outcome: Promise<true|CommonsHttpError>;
};
