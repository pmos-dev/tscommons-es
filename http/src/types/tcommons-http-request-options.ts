import {
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyEnumOrUndefined
} from 'tscommons-es-core';

import { ECommonsHttpContentType, isECommonsHttpContentType } from '../enums/ecommons-http-content-type';
import { ECommonsHttpResponseDataType, isECommonsHttpResponseDataType } from '../enums/ecommons-http-response-data-type';

export type TCommonsHttpRequestOptions = {
		timeout?: number;
		bodyDataEncoding?: ECommonsHttpContentType;
		responseDataType?: ECommonsHttpResponseDataType;
		maxReattempts?: number;
};

export type TCommonsHttpInternalRequestOptions = Omit<
		TCommonsHttpRequestOptions,
		'maxReattempts' | 'bodyDataEncoding' | 'responseDataType'
>;

export function isTCommonsHttpRequestOptions(test: unknown): test is TCommonsHttpRequestOptions {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'timeout')) return false;
	if (!commonsTypeHasPropertyEnumOrUndefined<ECommonsHttpContentType>(test, 'bodyDataEncoding', isECommonsHttpContentType)) return false;
	if (!commonsTypeHasPropertyEnumOrUndefined<ECommonsHttpResponseDataType>(test, 'responseDataType', isECommonsHttpResponseDataType)) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'maxReattempts')) return false;
	
	return true;
}
