import { commonsTypeAttemptNumber, commonsTypeHasPropertyNumberOrUndefined, commonsTypeHasPropertyString, commonsTypeHasPropertyStringOrUndefined } from 'tscommons-es-core';

// This is largely just a clone of Node's own URL, so that it can be used in browsers etc.

export type TCommonsHttpUrlSegments = {
		protocol: string;
		hostname: string;
		pathname: string;
		port?: number;
		username?: string;
		password?: string;
		query?: string;
		anchor?: string;
};

export function isTCommonsHttpUrlSegments(test: unknown): test is TCommonsHttpUrlSegments {
	if (!commonsTypeHasPropertyString(test, 'protocol')) return false;
	if (!commonsTypeHasPropertyString(test, 'hostname')) return false;
	if (!commonsTypeHasPropertyString(test, 'pathname')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'port')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'username')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'password')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'query')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'anchor')) return false;

	return true;
}

const URL_PATTERN: RegExp = /^([a-z]+):\/\/(?:([^@:/]*):([^@:/]*)@)?([a-z0-9][-a-z0-9]{0,62}(?:\.[a-z0-9][-a-z0-9]{0,62}){0,32})(?::([0-9]{1,5}))?(\/?[-a-z0-9\/._&=!~*'():\[\]@$+,;%]{0,940})(?:[?]([^#]*))?(?:#(.*))?$/i;

export function parseTCommonsHttpUrlSegments(url: string): TCommonsHttpUrlSegments {
	const match: RegExpExecArray|null = URL_PATTERN.exec(url);
	if (!match) throw new Error('Unable to parse URL to segments');

	const base: TCommonsHttpUrlSegments = {
			protocol: match[1],
			hostname: match[4],
			pathname: match[6].trim()
	};

	if (match[5] && match[5].trim() !== '') base.port = commonsTypeAttemptNumber(match[5]);
	if (match[2] && match[2].trim() !== '') base.username = match[2].trim();
	if (match[3] && match[3].trim() !== '') base.password = match[3].trim();
	if (match[7] && match[7].trim() !== '') base.query = match[7].trim();
	if (match[8] && match[8].trim() !== '') base.anchor = match[8].trim();

	return base;
}
