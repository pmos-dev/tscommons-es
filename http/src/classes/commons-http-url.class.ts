import { commonsTypeIsString } from 'tscommons-es-core/dist';
import { isTCommonsHttpUrlSegments, parseTCommonsHttpUrlSegments, TCommonsHttpUrlSegments } from '../types/tcommons-http-url-segments';

export class CommonsHttpUrl {
	private internal: TCommonsHttpUrlSegments;

	constructor(url: string|TCommonsHttpUrlSegments) {
		if (isTCommonsHttpUrlSegments(url)) {
			this.internal = url;
		} else {
			if (!commonsTypeIsString(url)) throw new Error('Cannot parse non-string and non-TCommonsHttpUrlSegments url');

			this.internal = parseTCommonsHttpUrlSegments(url);
		}
	}

	public get protocol(): string {
		return this.internal.protocol;
	}
	public set protocol(protocol: string) {
		this.internal.protocol = protocol;
	}
	
	public get hostname(): string {
		return this.internal.hostname;
	}
	public set hostname(hostname: string) {
		this.internal.hostname = hostname;
	}
	
	public get pathname(): string {
		return this.internal.pathname;
	}
	public set pathname(pathname: string) {
		this.internal.pathname = pathname;
	}
	
	public get port(): number|undefined {
		return this.internal.port;
	}
	public set port(port: number|undefined) {
		this.internal.port = port;
	}
	
	public get username(): string|undefined {
		return this.internal.username;
	}
	public set username(username: string|undefined) {
		this.internal.username = username;
	}
	
	public get password(): string|undefined {
		return this.internal.password;
	}
	public set password(password: string|undefined) {
		this.internal.password = password;
	}
	
	public get query(): string|undefined {
		return this.internal.query;
	}
	public set query(query: string|undefined) {
		this.internal.query = query;
	}
	
	public get anchor(): string|undefined {
		return this.internal.anchor;
	}
	public set anchor(anchor: string|undefined) {
		this.internal.anchor = anchor;
	}

	public get host(): string {
		return `${this.hostname}${this.port === undefined ? '' : `:${this.port.toString(10)}`}`;
	}

	public get origin(): string {
		return `${this.protocol}://${this.host}`;
	}

	public get href(): string {
		const parts: string[] = [
				`${this.protocol}://`
		];

		if (this.username && this.password) parts.push(`${this.username}:${this.password}@`);

		parts.push(this.host);
		if (!this.pathname.startsWith('/')) parts.push('/');
		parts.push(this.pathname);

		if (this.query) parts.push(`?${this.query}`);
		if (this.anchor) parts.push(`#${this.anchor}`);

		return parts.join('');
	}
}
