import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

export class CommonsHttpError extends Error {
	constructor(
			m: string,
			public readonly httpResponseCode: ECommonsHttpResponseCode|number	// eslint-disable-line @typescript-eslint/no-parameter-properties
	) {
		super(m);
	}
}
