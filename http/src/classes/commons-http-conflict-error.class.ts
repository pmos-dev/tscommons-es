import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpConflictError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.CONFLICT);
		Object.setPrototypeOf(this, CommonsHttpConflictError.prototype);
	}
}
