import { CommonsHttpUrl } from '../classes/commons-http-url.class';

export function commonsHttpUrlNormalizePath(path: string): string {
	if (path === '') return '';

	const firstSlash: boolean = path.startsWith('/');

	const segments: string[] = path.split('/');
	
	const rebuild: string[] = [];
	for (const segment of segments) {
		if (segment === '') continue;
		if (segment === '.') continue;
		if (segment === '..') {
			if (rebuild.length > 0) rebuild.pop();
			continue;
		}

		rebuild.push(segment);
	}
	if (firstSlash) rebuild.unshift('');

	return rebuild.join('/');
}

export function commonsHttpUrlNormalizeRelativeToAbsolute(relative: string, reference: string): string {
	const referenceUrl: CommonsHttpUrl = new CommonsHttpUrl(reference);
	referenceUrl.pathname = commonsHttpUrlNormalizePath(referenceUrl.pathname);

	if (/^[a-z]+:\/\//i.test(relative)) {
		// absolute path

		const relativeUrl: CommonsHttpUrl = new CommonsHttpUrl(relative);
		relativeUrl.pathname = commonsHttpUrlNormalizePath(relativeUrl.pathname);

		return relativeUrl.href;
	}

	if (/^\/\//.test(relative)) {
		// auto-http/s urls

		const relativeUrl: CommonsHttpUrl = new CommonsHttpUrl(`${referenceUrl.protocol}:${relative}`);
		relativeUrl.pathname = commonsHttpUrlNormalizePath(relativeUrl.pathname);

		return relativeUrl.href;
	}

	if (/^\//.test(relative)) {
		// root urls

		const relativeUrl: CommonsHttpUrl = new CommonsHttpUrl(`${referenceUrl.origin}${relative}`);
		relativeUrl.pathname = commonsHttpUrlNormalizePath(relativeUrl.pathname);

		return relativeUrl.href;
	}

	let existing: string = referenceUrl.pathname;
	if (
		existing !== '' && !existing.endsWith('/')
		&& relative !== '' && !/^[?#]/.test(relative)
	) {
		// assume file, and strip it off, if there is a replacement path file

		const split: string[] = existing.split('/');
		split.pop();
		existing = split.join('/');
	}

	{	// scope
		const relativeUrl: CommonsHttpUrl = new CommonsHttpUrl(`${referenceUrl.origin}${existing}${/^[?#]/.test(relative) ? '' : '/'}${relative}`);	// queries and anchors aren't counted for relative
		relativeUrl.pathname = commonsHttpUrlNormalizePath(relativeUrl.pathname);

		return relativeUrl.href;
	}
}
