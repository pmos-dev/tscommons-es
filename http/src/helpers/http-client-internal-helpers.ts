import { commonsObjectMapObject, commonsTypeIsObject, TEncodedObject } from 'tscommons-es-core';

import { THttpHeaderOrParamObject } from '../types/thttp-header-or-param-object';

import { ECommonsHttpContentType } from '../enums/ecommons-http-content-type';

export function commonsHttpClientInternalBuildHeadersOrParams(data: TEncodedObject|undefined): THttpHeaderOrParamObject {
	if (!data) return {};
	if (!commonsTypeIsObject(data)) throw new Error('REST data is not an object');
	
	return commonsObjectMapObject(
			data,
			(value: any, _key: string): string => {
				if (value === undefined || value === null) return '';
				return value.toString();	// eslint-disable-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
			}
	);
}

export function commonsHttpClientInternalAppendContentType(
		contentType: ECommonsHttpContentType,
		headers: THttpHeaderOrParamObject|undefined
): THttpHeaderOrParamObject {
	// immutable; deep doesn't matter as it isn't valid for http headers anyway
	const clone: THttpHeaderOrParamObject = headers === undefined ? {} : { ...headers };
	
	switch (contentType) {
		case ECommonsHttpContentType.FORM_URL:
			clone['Content-Type'] = 'application/x-www-form-urlencoded';
			break;
		case ECommonsHttpContentType.JSON:
			clone['Content-Type'] = 'application/json; charset=utf-8';
			break;
	}

	return clone;
}
