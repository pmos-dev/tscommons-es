import { ECommonsCsvColumnType } from '../enums/ecsv-column-type';

export type TCommonsCsvColumn<N = string> = {
		name: N;
		type: ECommonsCsvColumnType;
};
