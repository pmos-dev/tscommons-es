export enum ECommonsCsvColumnType {
		STRING = 'string',
		NUMBER = 'number',
		BOOLEAN = 'boolean',
		DATE = 'date',
		TIME = 'time',
		DATETIME = 'datetime'
}
