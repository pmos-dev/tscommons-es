import {
		commonsTypeIsBlank,
		commonsTypeIsDate,
		commonsTypeHasProperty,
		commonsDateIsYmd,
		commonsDateIsdmY,
		commonsDateIsHis,
		commonsDateIsHi,
		commonsDateIsYmdHis,
		commonsDateIsdmYHi,
		commonsDateDateTodmY,
		commonsDateDateToYmd,
		commonsDateDateToHi,
		commonsDateDateToHis,
		commonsDateDateTodmYHi,
		commonsDateDateToYmdHis,
		commonsTypeIsString,
		commonsTypeAttemptString,
		commonsTypeAttemptBoolean,
		commonsTypeAttemptNumber,
		commonsTypeAttemptDate,
		CommonsFixedDate,
		commonsTypeAttemptFixedDate,
		commonsTypeIsFixedDate
} from 'tscommons-es-core';

import { TCommonsCsvColumn } from '../types/tcsv-column';

import { ECommonsCsvColumnType } from '../enums/ecsv-column-type';
import { ECommonsCsvDateFormat } from '../enums/ecsv-date-format';
import { ECommonsCsvBooleanFormat } from '../enums/ecommons-csv-boolean-format';

type TAcceptableValues = string|number|boolean|Date|CommonsFixedDate|undefined;

function parseValue(
		data: unknown,
		type: ECommonsCsvColumnType,
		iso: boolean,
		fixed: boolean = false
): string|number|boolean|Date|CommonsFixedDate|undefined {
	if (commonsTypeIsBlank(data)) return undefined;

	try {
		switch (type) {
			case ECommonsCsvColumnType.STRING:
				return commonsTypeAttemptString(data);
			case ECommonsCsvColumnType.NUMBER: {
				const match: number|undefined = commonsTypeAttemptNumber(data);
				if (match !== undefined) return match;
				throw new Error('Unable to handle number column type for supplied data');
			}
			case ECommonsCsvColumnType.BOOLEAN: {
				const match: boolean|undefined = commonsTypeAttemptBoolean(data);
				if (match !== undefined) return match;
				throw new Error('Unable to handle boolean column type for supplied data');
			}
			case ECommonsCsvColumnType.DATE: {
				let match: Date|CommonsFixedDate|undefined;
				if (fixed) {
					match = commonsTypeAttemptFixedDate(data);
				} else {
					match = commonsTypeAttemptDate(data, iso);
				}

				// only accept dates, even if there was a match for a time/datetime
				if (commonsTypeIsString(data) && (commonsDateIsYmd(data) || commonsDateIsdmY(data))) return match;
		
				throw new Error('Unable to handle date column type for supplied data');
			}
			case ECommonsCsvColumnType.TIME: {
				let match: Date|CommonsFixedDate|undefined;
				if (fixed) {
					match = commonsTypeAttemptFixedDate(data);
				} else {
					match = commonsTypeAttemptDate(data, iso);
				}

				// only accept times, even if there was a match for a date/datetime
				if (commonsTypeIsString(data) && (commonsDateIsHis(data) || commonsDateIsHi(data))) return match;

				throw new Error('Unable to handle time column type for supplied data');
			}
			case ECommonsCsvColumnType.DATETIME: {
				let match: Date|CommonsFixedDate|undefined;
				if (fixed) {
					match = commonsTypeAttemptFixedDate(data);
				} else {
					match = commonsTypeAttemptDate(data, iso);
				}

				// only accept datetimes, even if there was a match for a date/time
				if (commonsTypeIsString(data) && (commonsDateIsYmdHis(data) || commonsDateIsdmYHi(data))) return match;

				throw new Error('Unable to handle datetime column type for supplied data');
			}
			default:
				throw new Error('Unknown data type');
		}
	} catch (e) {
		throw new Error(`Unable to parse CSV data: ${(e as Error).message}`);
	}
}

export class CommonsCsv<NS extends string = string> {
	public static buildGenericStringColumns<NS extends string = string>(
			source: string,
			separator: string = ',',
			textDelimiter: string = '"'
	): Record<NS, TAcceptableValues>[] {
		const dummy: CommonsCsv<NS> = new CommonsCsv<NS>(
				[],
				separator,
				textDelimiter
		);
		
		const header: TCommonsCsvColumn<NS>[] = dummy.readHeader(source);
		
		const generic: CommonsCsv<NS> = new CommonsCsv<NS>(
				header,
				separator,
				textDelimiter
		);
		
		return generic
				.parse(source)
				.slice(1);
	}
	
	constructor(
			private headers: TCommonsCsvColumn<NS>[],
			private separator: string = ',',
			private textDelimiter: string = '"',
			private dateFormat: ECommonsCsvDateFormat = ECommonsCsvDateFormat.YMDHIS,
			private booleanFormat: ECommonsCsvBooleanFormat = ECommonsCsvBooleanFormat.NORMAL,
			private isNS: (test: unknown) => test is NS = (_: unknown): _ is NS => commonsTypeIsString(_),
			private iso: boolean = false,
			private fixed: boolean = false
	) {}

	protected readHeader(source: string): TCommonsCsvColumn<NS>[] {
		const consumed: string[] = this.consumeNextLine(source);
		
		return this.splitLine(consumed[0].trim())
				.map((column: string): TCommonsCsvColumn<NS> => {
					if (!this.isNS(column)) throw new Error('Column name does not match permitted options');
					return {
							name: column,
							type: ECommonsCsvColumnType.STRING
					};
				});
	}

	private consumeNextLine(source: string): string[] {
		source = source.trim();
	
		let quoted: boolean = false;
	
		let line: string = '';
		while (source.length > 0) {
			const c: string = source.substr(0, 1);
			source = source.substr(1);
			
			if (c === this.textDelimiter) {
				if (!quoted) {
					quoted = true;
					line = `${line}${c}`;
					continue;
				}
	
				if (quoted) {
					if (source !== '' && source.substr(0, 1) === this.textDelimiter) {
						line = `${line}${c}${c}`;
						source = source.substr(1);
						continue;
					}
					
					quoted = false;
					line = `${line}${c}`;
					continue;
				}
				
				throw new Error('Unable to parse CSV line');
			}
			
			if (c === '\n' && !quoted) break;
			
			line = `${line}${c}`;	// use this rather than + so that it is always a string append
		}
	
		return [ line, source ];
	}

	protected splitLine(source: string): string[] {
		source = source.trim();
	
		const parts: string[] = [];
		let part: string = '';
		let quoted: boolean = false;
		let terminal: boolean = true;
	
		while (source.length > 0) {
			terminal = false;
			
			const c: string = source.substr(0, 1);
			source = source.substr(1);
			
			if (c === this.separator && !quoted) {
				parts.push(part.trim());
				part = '';
				source = source.trim();
				terminal = true;
				continue;
			}

			if (c === this.textDelimiter) {
				if (!quoted && part === '') {
					quoted = true;
					continue;
				}
	
				if (quoted) {
					if (source !== '' && source.substr(0, 1) === this.textDelimiter) {
						part = `${part}${c}`;
						source = source.substr(1);
						continue;
					}
					
					parts.push(part);
					part = '';
					terminal = true;
					
					quoted = false;
					source = source.trim();
					if (source === '') continue;
					
					if (source.substr(0, 1) !== this.separator) throw new Error('Unable to parse CSV line');
					source = source.substr(1).trim();
					continue;
				}
				
				throw new Error('Unable to parse CSV line');
			}
			
			part = `${part}${c}`;	// use this rather than + so that it is always a string append
		}
	
		if (!terminal) parts.push(part.trim());
	
		return parts;
	}

	private parseLine(parts: string[]): Record<NS, TAcceptableValues> {
		// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
		const values: Record<NS, TAcceptableValues> = {} as Record<NS, TAcceptableValues>;

		for (let i = this.headers.length; i-- > 0;) {
			const value: (string|number|boolean|Date|CommonsFixedDate|undefined) = parseValue(parts[i], this.headers[i].type, this.iso, this.fixed);
			values[this.headers[i].name] = value;
		}
	
		return values;
	}

	public parse(source: string): Record<NS, TAcceptableValues>[] {
		source = source.replace('\r\n', '\n').replace('\r', '\n').trim();
		
		const parsed: Record<NS, TAcceptableValues>[] = [];

		while (source.length > 0) {
			const consumed: string[] = this.consumeNextLine(source);
			source = consumed[1];
			
			const line: string = consumed[0].trim();
			if (line === '') continue;

			const parts: string[] = this.splitLine(line);
			while (parts.length < this.headers.length) parts.push('');
			if (parts.length > this.headers.length) throw new Error('CSV line is longer than header');

			parsed.push(this.parseLine(parts));
		}
	
		return parsed;
	}

	private encodeValue(data: (string|number|boolean|Date|CommonsFixedDate|undefined), type: ECommonsCsvColumnType): string {
		if (data === undefined) return '';

		try {
			switch (type) {
				case ECommonsCsvColumnType.STRING:
					const dual: string = `${this.textDelimiter}${this.textDelimiter}`;
					return `${this.textDelimiter}${data.toString().replace(this.textDelimiter, dual)}${this.textDelimiter}`;
				case ECommonsCsvColumnType.NUMBER:
					if ('number' === typeof data) return data.toString();
					throw new Error('Unable to handle number column type for supplied data');
				case ECommonsCsvColumnType.BOOLEAN:
					if ('boolean' === typeof data) {
						if (this.booleanFormat === ECommonsCsvBooleanFormat.NORMAL) return data ? 'true' : 'false';
						if (this.booleanFormat === ECommonsCsvBooleanFormat.UPPERCASE) return data ? 'TRUE' : 'FALSE';
						if (this.booleanFormat === ECommonsCsvBooleanFormat.DIGIT) return data ? '1' : '0';
					}
					throw new Error('Unable to handle boolean column type for supplied data');
				case ECommonsCsvColumnType.DATE:
					if (this.fixed) {
						if (commonsTypeIsFixedDate(data)) {
							if (this.dateFormat === ECommonsCsvDateFormat.DMYHI) return data.dmY;
							if (this.dateFormat === ECommonsCsvDateFormat.YMDHIS) return data.Ymd;
						}
						throw new Error('Unable to handle date column type for supplied data');
					} else {
						if (commonsTypeIsDate(data)) {
							if (this.dateFormat === ECommonsCsvDateFormat.DMYHI) return commonsDateDateTodmY(data, this.iso);
							if (this.dateFormat === ECommonsCsvDateFormat.YMDHIS) return commonsDateDateToYmd(data, this.iso);
						}
						throw new Error('Unable to handle date column type for supplied data');
					}
				case ECommonsCsvColumnType.TIME:
					if (this.fixed) {
						if (commonsTypeIsFixedDate(data)) {
							if (this.dateFormat === ECommonsCsvDateFormat.DMYHI) return data.Hi;
							if (this.dateFormat === ECommonsCsvDateFormat.YMDHIS) return data.His;
						}
						throw new Error('Unable to handle time column type for supplied data');
					} else {
						if (commonsTypeIsDate(data)) {
							if (this.dateFormat === ECommonsCsvDateFormat.DMYHI) return commonsDateDateToHi(data, this.iso);
							if (this.dateFormat === ECommonsCsvDateFormat.YMDHIS) return commonsDateDateToHis(data, this.iso);
						}
						throw new Error('Unable to handle time column type for supplied data');
					}
				case ECommonsCsvColumnType.DATETIME:
					if (this.fixed) {
						if (commonsTypeIsFixedDate(data)) {
							if (this.dateFormat === ECommonsCsvDateFormat.DMYHI) return data.dmYHi;
							if (this.dateFormat === ECommonsCsvDateFormat.YMDHIS) return data.YmdHis;
						}
						throw new Error('Unable to handle datetime column type for supplied data');
					} else {
						if (commonsTypeIsDate(data)) {
							if (this.dateFormat === ECommonsCsvDateFormat.DMYHI) return commonsDateDateTodmYHi(data, this.iso);
							if (this.dateFormat === ECommonsCsvDateFormat.YMDHIS) return commonsDateDateToYmdHis(data, this.iso);
						}
						throw new Error('Unable to handle datetime column type for supplied data');
					}
				default:
					throw new Error('Unknown data type');
			}
		} catch (e) {
			throw new Error(`Unable to encode value to CSV data: ${(e as Error).message}`);
		}
	}

	public encodeLine(values: Record<NS, TAcceptableValues>): string[] {
		return this.headers
				.map((header: TCommonsCsvColumn<NS>): string => {
					if (!commonsTypeHasProperty(values, header.name)) return '';
					return this.encodeValue(values[header.name], header.type);
				});
	}

	public encodeHeaderRow(): string {
		const names: string[] = this.headers
				.map((header: TCommonsCsvColumn<NS>): string => header.name);
		
		return names.join(this.separator);
	}

	public encode(values: Record<NS, TAcceptableValues>[], withHeaderRow: boolean = false): string {
		const rows: string = values
				.map((vs: Record<NS, TAcceptableValues>): string[] => this.encodeLine(vs))
				.map((vs: string[]): string => vs.join(this.separator))
				.join('\n');
		
		if (withHeaderRow) return `${this.encodeHeaderRow()}\n${rows}`;
		return rows;
	}
}
