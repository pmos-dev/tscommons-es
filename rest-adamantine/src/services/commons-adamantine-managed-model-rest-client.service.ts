import { TEncodedObject } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ICommonsManagedModelMetadata } from 'tscommons-es-models-adamantine';
import { TCommonsManagedModelMetadata, isTCommonsManagedModelMetadata, decodeICommonsManagedModelMetadata } from 'tscommons-es-models-adamantine';
import { CommonsRestClientService } from 'tscommons-es-rest';

export abstract class CommonsAdamantineManagedModelRestClientService<
		_M extends (	// eslint-disable-line @typescript-eslint/no-unused-vars
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		_T extends TEncodedObject	// eslint-disable-line @typescript-eslint/no-unused-vars
> {
	constructor(
			protected restClientService: CommonsRestClientService,
			protected path: string
	) {}

	public async getMetadata(
			options: TCommonsHttpRequestOptions = {}
	): Promise<ICommonsManagedModelMetadata> {
		const metadata: unknown = await this.restClientService.getRest<TCommonsManagedModelMetadata>(
				`${this.path}metadata`,
				undefined,
				undefined,
				options
		);
		if (!isTCommonsManagedModelMetadata(metadata)) throw new Error('Invalid metadata returned');
		
		return decodeICommonsManagedModelMetadata(metadata);
	}
	
}
