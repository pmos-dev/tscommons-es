import {
		commonsTypeIsBoolean,
		commonsTypeIsNumberArray,
		commonsTypeIsStringArray,
		commonsBase62IsIdArray,
		commonsTypeEncodePropertyObject,
		commonsTypeIsTArray
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { CommonsAdamantineManagedModelRestClientService } from './commons-adamantine-managed-model-rest-client.service';

export abstract class CommonsAdamantineManagedSecondClassModelRestClientService<
		M extends (
				(ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)) & ICommonsSecondClass<P>
		),
		P extends ICommonsFirstClass,
		T extends TEncodedObject
> extends CommonsAdamantineManagedModelRestClientService<M, T> {
	constructor(
			restClientService: CommonsRestClientService,
			path: string,
			protected isT: (test: unknown) => test is T,
			protected decodeM: (data: T) => M,
			protected isUidSupported: boolean = false
	) {
		super(restClientService, path);
	}

	public async listByFirstClass(
			firstClass: P,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M[]> {
		const encodeds: unknown = await this.restClientService.getRest<T[]>(
				`${this.path}data/${firstClass.id}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsTArray<T>(encodeds, this.isT)) throw new Error('Invalid ids returned');
		
		return encodeds
				.map((encoded: T): M => this.decodeM(encoded));
	}

	public async listIdsByFirstClass(
			firstClass: P,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}data/${firstClass.id}/ids`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid ids returned');
		
		return ids;
	}
	
	public async listNamesByFirstClass(
			firstClass: P,
			options: TCommonsHttpRequestOptions = {}
	): Promise<string[]> {
		const names: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}data/${firstClass.id}/names`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsStringArray(names)) throw new Error('Invalid names returned');
		
		return names;
	}
	
	public async listUidsByFirstClass(
			firstClass: P,
			options: TCommonsHttpRequestOptions = {}
	): Promise<string[]> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');
		
		const uids: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}data/${firstClass.id}/uids`,
				undefined,
				undefined,
				options
		);
		if (!commonsBase62IsIdArray(uids)) throw new Error('Invalid uids returned');
		
		return uids;
	}

	public async getByFirstClassAndId(
			firstClass: P,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}data/${firstClass.id}/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid model item');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async getByFirstClassAndName(
			firstClass: P,
			name: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}data/${firstClass.id}/names/${name}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid model item');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async getByUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}data/uids/${uid}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid model item');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async createForFirstClass(
			firstClass: P,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.postRest<T, TEncodedObject>(
				`${this.path}data/${firstClass.id}`,
				commonsTypeEncodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Creation failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async updateForFirstClassAndId(
			firstClass: P,
			id: number,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}data/${firstClass.id}/ids/${id}`,
				commonsTypeEncodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async updateForFirstClassAndName(
			firstClass: P,
			name: string,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}data/${firstClass.id}/names/${name}`,
				commonsTypeEncodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async updateForUid(
			uid: string,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}data/uids/${uid}`,
				commonsTypeEncodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async deleteForFirstClassAndId(
			firstClass: P,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		const result: unknown = await this.restClientService.deleteRest<boolean>(
				`${this.path}data/${firstClass.id}/ids/${id}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsBoolean(result)) throw new Error('Delete failure: non-boolean returned');
		
		return result;
	}

	public async deleteForFirstClassAndName(
			firstClass: P,
			name: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		const result: unknown = await this.restClientService.deleteRest<boolean>(
				`${this.path}data/${firstClass.id}/names/${name}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsBoolean(result)) throw new Error('Delete failure: non-boolean returned');
		
		return result;
	}

	public async deleteForUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		const result: unknown = await this.restClientService.deleteRest<boolean>(
				`${this.path}data/uids/${uid}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsBoolean(result)) throw new Error('Delete failure: non-boolean returned');
		
		return result;
	}
	
}
