import {
		commonsTypeIsNumberArray,
		commonsTypeIsBoolean,
		commonsTypeEncodePropertyObject
} from 'tscommons-es-core';
import { commonsStringPluralise } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsManagedM2MLinkTableModelMetadata } from 'tscommons-es-models-adamantine';
import { TCommonsManagedM2MLinkTableModelMetadata, isTCommonsManagedM2MLinkTableModelMetadata, decodeICommonsManagedM2MLinkTableModelMetadata } from 'tscommons-es-models-adamantine';
import { CommonsRestClientService } from 'tscommons-es-rest';

export abstract class CommonsAdamantineManagedM2MModelRestClientService<
		A extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		B extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		M extends ICommonsM2MLink<A, B>,
		T extends TEncodedObject
> {
	private aPlural: string;
	private bPlural: string;

	constructor(
			protected restClientService: CommonsRestClientService,
			protected path: string,
			aField: string,
			bField: string,
			protected isT?: (test: unknown) => test is T,
			protected decodeM?: (data: T) => M
	) {
		this.aPlural = commonsStringPluralise(aField, 2);
		this.bPlural = commonsStringPluralise(bField, 2);
	}

	public async getMetadata(
			options: TCommonsHttpRequestOptions = {}
	): Promise<ICommonsManagedM2MLinkTableModelMetadata> {
		const metadata: unknown = await this.restClientService.getRest<TCommonsManagedM2MLinkTableModelMetadata>(
				`${this.path}metadata`,
				undefined,
				undefined,
				options
		);
		if (!isTCommonsManagedM2MLinkTableModelMetadata(metadata)) throw new Error('Invalid metadata returned');
		
		return decodeICommonsManagedM2MLinkTableModelMetadata(metadata);
	}
	
	public async listBsByA(
			a: A,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}${this.aPlural}/${a.id}/${this.bPlural}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid B ids returned');
		
		return ids;
	}

	public async listAsByB(
			b: B,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}${this.bPlural}/${b.id}/${this.aPlural}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid A ids returned');
		
		return ids;
	}
	
	public async isLinked(
			a: A,
			b: B,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		try {
			await this.restClientService.getRest<T>(
					`${this.path}${this.aPlural}/${a.id}/${this.bPlural}/${b.id}`,
					undefined,
					undefined,
					options
			);

			return true;
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) return false;
			throw e;
		}
	}

	public async getLinkRow(
			a: A,
			b: B,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		if (!this.isT || !this.decodeM) throw new Error('Link row data is not supported for this model');
		
		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}${this.aPlural}/${a.id}/${this.bPlural}/${b.id}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid M2M link row');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async link(
			a: A,
			b: B,
			data: TPropertyObject = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.putRest<T, TEncodedObject>(
				`${this.path}${this.aPlural}/${a.id}/${this.bPlural}/${b.id}`,
				commonsTypeEncodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		
		if (!this.isT || !this.decodeM) return encoded as M;
		
		if (!this.isT(encoded)) throw new Error('Link failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async update(
			a: A,
			b: B,
			data: TPropertyObject = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}${this.aPlural}/${a.id}/${this.bPlural}/${b.id}`,
				commonsTypeEncodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		
		if (!this.isT || !this.decodeM) return encoded as M;
		
		if (!this.isT(encoded)) throw new Error('Link update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async unlink(
			a: A,
			b: B,
			ignoreNotLinked: boolean = false,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		try {
			const result: unknown = await this.restClientService.deleteRest<boolean>(
					`${this.path}${this.aPlural}/${a.id}/${this.bPlural}/${b.id}`,
					undefined,
					undefined,
					options
			);
			if (!commonsTypeIsBoolean(result)) throw new Error('Delete failure: non-boolean returned');
			
			return true;
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				if (ignoreNotLinked) return false;
			}
			
			throw e;
		}
	}
}
