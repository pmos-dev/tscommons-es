import { TPrimative } from 'tscommons-es-core';

type TActionAndReward<ActionT> = {
		action: ActionT;
		reward: number;
}

export abstract class CommonsAlgorithmAiQLearningTransitionModel<
		StateT,
		ActionT
> {
	private q: Map<TPrimative, number>;

	constructor(
			private alpha: number,
			private epsilon: number = 0
	) {
		this.q = new Map<TPrimative, number>();
	}

	protected abstract stateActionToIndex(state: StateT, action: ActionT): TPrimative;
	protected abstract listAvailableActions(state: StateT): ActionT[];

	private getStateQ(state: StateT, action: ActionT): number {
		const index: TPrimative = this.stateActionToIndex(state, action);
		
		const q: number|undefined = this.q.get(index);
		if (q === undefined) return 0;

		return q;
	}

	private listActionsAndRewards(state: StateT): TActionAndReward<ActionT>[] {
		const actions: ActionT[] = this.listAvailableActions(state);

		return actions
				.map((action: ActionT): TActionAndReward<ActionT> => ({
						action: action,
						reward: this.getStateQ(state, action)
				}));
	}

	private bestFutureReward(state: StateT): number {
		const actionsAndRewards: TActionAndReward<ActionT>[] = this.listActionsAndRewards(state);
		if (actionsAndRewards.length === 0) return 0;

		return Math.max(
				...actionsAndRewards
						.map((ar: TActionAndReward<ActionT>): number => ar.reward)
		);
	}

	public chooseAction(state: StateT): ActionT|undefined {
		const actionsAndRewards: TActionAndReward<ActionT>[] = this.listActionsAndRewards(state);
		if (actionsAndRewards.length === 0) return undefined;

		const maxReward: number = Math.max(
				...actionsAndRewards
						.map((ar: TActionAndReward<ActionT>): number => ar.reward)
		);

		const bestActions: ActionT[] = actionsAndRewards
				.filter((ar: TActionAndReward<ActionT>): boolean => ar.reward === maxReward)
				.map((ar: TActionAndReward<ActionT>): ActionT => ar.action);
		if (bestActions.length === 0) return undefined;

		if (Math.random() < this.epsilon) {
			return actionsAndRewards[Math.floor(actionsAndRewards.length * Math.random())].action;
		}

		return bestActions[Math.floor(bestActions.length * Math.random())];
	}

	private updateQ(
			state: StateT,
			action: ActionT,
			oldQ: number,
			reward: number,
			futureRewards: number
	): void {
		const index: TPrimative = this.stateActionToIndex(state, action);

		this.q.set(index, oldQ + (this.alpha * ((reward + futureRewards) - oldQ)));
	}

	public update(
			oldState: StateT,
			action: ActionT,
			newState: StateT,
			reward: number
	): void {
		const oldQ: number = this.getStateQ(oldState, action);
		const bestFutureReward: number = this.bestFutureReward(newState);

		this.updateQ(oldState, action, oldQ, reward, bestFutureReward);
	}

	public reward(
			oldState: StateT,
			action: ActionT,
			newState: StateT,
			magnitude: number = 1
	): void {
		if (magnitude < 0) throw new Error('Reward cannot be negative. Use update for a generic any-magnitude');

		this.update(oldState, action, newState, magnitude);
	}

	public punish(
			oldState: StateT,
			action: ActionT,
			newState: StateT,
			magnitude: number = 1
	): void {
		if (magnitude < 0) throw new Error('Punishment cannot be negative. Use update for a generic any-magnitude');
		
		this.update(oldState, action, newState, -magnitude);
	}

	public touch(
			oldState: StateT,
			action: ActionT,
			newState: StateT
	): void {
		this.update(oldState, action, newState, 0);
	}
}
