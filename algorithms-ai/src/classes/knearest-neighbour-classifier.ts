/* eslint-disable @typescript-eslint/require-await */

import { commonsArrayEmpty, commonsArrayRapidSortHead } from 'tscommons-es-core';
import {
		ICommonsAiModelPredictable,
		ICommonsAiModelTrainable,
		TCommonsAiGenericTrainingData,
		TCommonsAiOutputWithCertainty,
		TCommonsAiQuality,
		TCommonsAiTrainingStats
} from 'tscommons-es-ai';

import { commonsAlgorithmAiNormaliseInput } from '../helpers/normalise-input';

type TDataWithDistance<I, O> = TCommonsAiGenericTrainingData<I, O> & {
		distance: number;
}
export type TCommonsAlgorithmAiOutputWithDistance<O> = Pick<TDataWithDistance<unknown, O>, 'output' | 'distance'>;

export abstract class CommonsAlgorithmAiKNearestNeighbourClassifier<I, O> implements ICommonsAiModelTrainable<
		I,
		O,
		TCommonsAiTrainingStats
>, ICommonsAiModelPredictable<I, O> {
	private data: TCommonsAiGenericTrainingData<I, O>[] = [];

	constructor(
			private k: number = 1,
			private expandKToIdenticalDistances: boolean = false
	) {
		if (k < 1) throw new Error('k cannot be less than 1');
	}

	public load(data: TCommonsAiGenericTrainingData<I, O>[]): void {
		this.data = data.slice();
	}

	public async build(): Promise<void> {
		// do nothing
	}

	public async train(): Promise<TCommonsAiTrainingStats|undefined> {
		// do nothing

		return undefined;
	}

	protected abstract calculateDistance(a: I, b: I): number;

	protected abstract getConsensus(neighbours: TCommonsAlgorithmAiOutputWithDistance<O>[]): TCommonsAiOutputWithCertainty<O>|undefined;

	public async predict(input: I): Promise<TCommonsAiOutputWithCertainty<O>|undefined> {
		if (this.data.length === 0) return undefined;

		// efficiency hack
		const typecast: TDataWithDistance<I, O>[] = this.data as unknown as TDataWithDistance<I, O>[];
		for (const d of typecast) {
			d.distance = this.calculateDistance(input, d.input);
		}
		try {
			const nearests: TDataWithDistance<I, O>[] = commonsArrayRapidSortHead(
					typecast,
					this.k,
					(a: TDataWithDistance<I, O>, b: TDataWithDistance<I, O>): boolean => a.distance < b.distance
			);

			if (nearests.length === 0) return undefined;

			if (this.expandKToIdenticalDistances) {
				const equals: TDataWithDistance<I, O>[] = [];
				for (const nearest of nearests) {
					equals.push(...typecast
							.filter((d: TDataWithDistance<I, O>): boolean => d.distance === nearest.distance));
				}
				equals
						.sort((a: TDataWithDistance<I, O>, b: TDataWithDistance<I, O>): number => {
							if (a.distance < b.distance) return -1;
							if (a.distance > b.distance) return 1;
							return 0;
						});
			
				let cutoff: number = Math.min(this.k, equals.length);
				while (cutoff < equals.length && equals[cutoff].distance === equals[0].distance) cutoff++;

				commonsArrayEmpty(nearests);
				nearests.push(...equals.slice(0, cutoff));

				if (nearests.length === 0) throw new Error('Post expand nearest size was 0. This should not be possible');
			}

			return this.getConsensus(nearests);
		} finally {
			// efficiency hack cleanup
			for (const d of typecast) {
				delete (d as { distance?: number }).distance;
			}
		}
	}

	protected abstract calculateQuality(actual: TCommonsAiOutputWithCertainty<O>, expected: O): [ number, number ];

	public async test(testData: TCommonsAiGenericTrainingData<I, O>[]): Promise<TCommonsAiQuality> {
		if (testData.length === 0) throw new Error('No data to test on');
		
		let positiveQuality: number = 0;
		let negativeQuality: number = 0;
		for (const test of testData) {
			const prediction: TCommonsAiOutputWithCertainty<O>|undefined = await this.predict(test.input);

			if (!prediction) {
				negativeQuality++;
			} else {
				const [ positive, negative ]: [ number, number ] = this.calculateQuality(prediction, test.output);

				positiveQuality += positive;
				negativeQuality += negative;
			}
		}

		return {
				positive: positiveQuality / testData.length,
				negative: negativeQuality / testData.length
		};
	}
}

export abstract class CommonsAlgorithmAiKNearestNeighbourEnumClassifier<I, E extends string> extends CommonsAlgorithmAiKNearestNeighbourClassifier<I, E> {
	constructor(
			protected values: E[],
			k?: number,
			expandKToIdenticalDistances?: boolean
	) {
		super(k, expandKToIdenticalDistances);
	}

	public loadMap(data: Map<E, I[]>): void {
		const rebuild: TCommonsAiGenericTrainingData<I, E>[] = [];

		for (const value of this.values) {
			rebuild.push(...(data.get(value) || [])
					.map((input: I): TCommonsAiGenericTrainingData<I, E> => ({
							input: input,
							output: value
					}))
			);
		}

		this.load(rebuild);
	}
}

class NumericClassifier<E extends string> extends CommonsAlgorithmAiKNearestNeighbourEnumClassifier<number[], E> implements ICommonsAiModelTrainable<
		number[],
		E,
TCommonsAiTrainingStats
>, ICommonsAiModelPredictable<
		number[],
		E
> {
	constructor(
			private getConsensusCallback: (neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[]) => TCommonsAiOutputWithCertainty<E>|undefined,
			values: E[],
			k?: number,
			expandKToIdenticalDistances?: boolean
	) {
		super(values, k, expandKToIdenticalDistances);
	}
	
	protected override calculateDistance(a: number[], b: number[]): number {
		if (a.length !== b.length) throw new Error('a and b are not equal length');

		let squares: number = 0;
		for (let i = a.length; i-- > 0;) squares += Math.pow(a[i] - b[i], 2);

		return Math.sqrt(squares);
	}

	protected override getConsensus(neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[]): TCommonsAiOutputWithCertainty<E>|undefined {
		return this.getConsensusCallback(neighbours);
	}

	public override calculateQuality(actual: TCommonsAiOutputWithCertainty<E>, expected: E): [number, number] {
		if (actual.output === expected) return [ 1, 0 ];
		return [ 0, 1 ];
	}
}

export abstract class CommonsAlgorithmAiKNearestNeighbourNormalisedNumericInputEnumClassifier<
		I,
		E extends string
> implements ICommonsAiModelTrainable<
		I,
		E,
		TCommonsAiTrainingStats
>, ICommonsAiModelPredictable<
		I,
		E
> {
	private sourceData: Map<E, I[]>|undefined;

	private numericClassifier: NumericClassifier<E>|undefined;

	constructor(
			private values: E[],
			private k?: number,
			private normaliseInput: boolean = false,
			private expandKToIdenticalDistances: boolean = false
	) {}

	protected abstract numericiseInput(input: I): number[];

	protected abstract getConsensus(neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[]): TCommonsAiOutputWithCertainty<E>|undefined;

	public loadMap(data: Map<E, I[]>): void {
		this.sourceData = data;
	}

	public async build(): Promise<void> {
		if (!this.sourceData) throw new Error('No source data. Use loadMap first');

		const flattened: TCommonsAiGenericTrainingData<number[], E>[] = [];

		for (const value of this.values) {
			flattened.push(...(this.sourceData.get(value) || [])
					.map((input: I): TCommonsAiGenericTrainingData<number[], E> => ({
							input: this.numericiseInput(input),
							output: value
					}))
			);
		}

		if (this.normaliseInput) commonsAlgorithmAiNormaliseInput<number[]>(flattened);

		this.numericClassifier = new NumericClassifier<E>(
				(neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[]): TCommonsAiOutputWithCertainty<E>|undefined => this.getConsensus(neighbours),
				this.values,
				this.k,
				this.expandKToIdenticalDistances
		);
		this.numericClassifier.load(flattened);
	}

	public async train(): Promise<TCommonsAiTrainingStats | undefined> {
		if (!this.numericClassifier) throw new Error('Model has not been built yet');

		// do nothing

		return undefined;
	}

	public async predict(value: I): Promise<TCommonsAiOutputWithCertainty<E> | undefined> {
		if (!this.numericClassifier) throw new Error('Model has not been built yet');

		const numericised: number[] = this.numericiseInput(value);

		return this.numericClassifier.predict(numericised);
	}

	public async test(testData: TCommonsAiGenericTrainingData<I, E>[]): Promise<TCommonsAiQuality> {
		if (!this.numericClassifier) throw new Error('Model has not been built yet');

		if (testData.length === 0) throw new Error('No data to test on');
		
		let positiveQuality: number = 0;
		let negativeQuality: number = 0;
		for (const test of testData) {
			const prediction: TCommonsAiOutputWithCertainty<E>|undefined = await this.predict(test.input);

			if (!prediction) {
				negativeQuality++;
			} else {
				const [ positive, negative ]: [ number, number ] = this.numericClassifier.calculateQuality(prediction, test.output);

				positiveQuality += positive;
				negativeQuality += negative;
			}
		}

		return {
				positive: positiveQuality / testData.length,
				negative: negativeQuality / testData.length
		};
	}
}
