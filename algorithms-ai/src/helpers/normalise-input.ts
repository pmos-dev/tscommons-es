import { TCommonsAiGenericTrainingData } from 'tscommons-es-ai';

export function commonsAlgorithmAiNormaliseInput<I extends number[]>(data: TCommonsAiGenericTrainingData<I, unknown>[]): void {
	if (data.length === 0) return;

	const inputLength: number = data[0].input.length;

	type TMinMax = {
			min: number;
			max: number;
			delta: number;
	};
	const minMaxs: TMinMax[] = [];

	for (let i = 0; i < inputLength; i++) {
		const values: number[] = data
				.map((value: TCommonsAiGenericTrainingData<I, unknown>): number => value.input[i]);
		
		const min: number = Math.min(...values);
		const max: number = Math.max(...values);
		minMaxs.push({
				min: min,
				max: max,
				delta: min === max ? 1 : (max - min)
		});
	}

	for (const item of data) {
		if (item.input.length !== inputLength) throw new Error('Inconsistent input lengths');

		for (let i = inputLength; i-- > 0;) item.input[i] = (item.input[i] - minMaxs[i].min) / minMaxs[i].delta;
	}
}
