import { CommonsTally, commonsNumberAverage } from 'tscommons-es-core';
import { TCommonsAiOutputWithCertainty } from 'tscommons-es-ai';

import { TCommonsAlgorithmAiOutputWithDistance } from '../classes/knearest-neighbour-classifier';

function genericConsensus<E extends string>(
		values: E[],
		neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[],
		handle: (
				tally: CommonsTally<E>,
				neighbour: TCommonsAlgorithmAiOutputWithDistance<E>
		) => void
): TCommonsAiOutputWithCertainty<E>|undefined {
	if (neighbours.length === 0) return undefined;

	const tally: CommonsTally<E> = new CommonsTally<E>(values);
	for (const neighbour of neighbours) {
		handle(tally, neighbour);
	}

	const best: E|undefined = tally.winner;
	if (best === undefined) return undefined;

	const distance: number|undefined = commonsNumberAverage(
			neighbours
					.filter((neighbour: TCommonsAlgorithmAiOutputWithDistance<E>): boolean => neighbour.output === best)
					.map((neighbour: TCommonsAlgorithmAiOutputWithDistance<E>): number => neighbour.distance)
	);
	if (distance === undefined) throw new Error('Unable to determine distance for consensus');

	return {
			output: best,
			certainty: distance
	};
}

export function commonsAlgorithmAiNearestNeighbourSimpleConsensus<E extends string>(
		values: E[],
		neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[]
): TCommonsAiOutputWithCertainty<E>|undefined {
	return genericConsensus<E>(
			values,
			neighbours,
			(
					tally: CommonsTally<E>,
					neighbour: TCommonsAlgorithmAiOutputWithDistance<E>
			): void => {
				tally.increment(neighbour.output);
			}
	);
}

export function commonsAlgorithmAiNearestNeighbourWeightedConsensus<E extends string>(
		values: E[],
		neighbours: TCommonsAlgorithmAiOutputWithDistance<E>[]
): TCommonsAiOutputWithCertainty<E>|undefined {
	return genericConsensus<E>(
			values,
			neighbours,
			(
					tally: CommonsTally<E>,
					neighbour: TCommonsAlgorithmAiOutputWithDistance<E>
			): void => {
				tally.add(neighbour.output, -neighbour.distance);	// negative so we give bias towards the closest
			}
	);
}
