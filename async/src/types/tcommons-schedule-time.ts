import {
		commonsTypeIsObject,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyArrayOrUndefined,
		commonsTypeIsNumberArray,
		commonsTypeHasPropertyArray
} from 'tscommons-es-core';

export type TCommonsScheduleTime = {
		year?: number|number[];
		month?: number|number[];
		day?: number|number[];
		dow?: number|number[];
		hour?: number|number[];
		minute?: number|number[];
		second?: number|number[];
};

export function isTCommonsScheduleTime(test: unknown): test is TCommonsScheduleTime {
	if (!commonsTypeIsObject(test)) return false;

	for (const key of 'year,month,day,dow,hour,minute,second'.split(',')) {
		if (!commonsTypeHasPropertyNumberOrUndefined(test, key) && !commonsTypeHasPropertyArrayOrUndefined(test, key)) return false;
		if (commonsTypeHasPropertyArray(test, key) && !commonsTypeIsNumberArray(test[key])) return false;
	}
	
	return true;
}
