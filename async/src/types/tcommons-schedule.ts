import { commonsTypeHasPropertyT, commonsTypeHasPropertyString } from 'tscommons-es-core';

import { TCommonsScheduleTime, isTCommonsScheduleTime } from './tcommons-schedule-time';

export type TCommonsSchedule = {
		at: TCommonsScheduleTime;
		action: string;
};

export function isTCommonsSchedule(test: unknown): test is TCommonsSchedule {
	if (!commonsTypeHasPropertyT<TCommonsScheduleTime>(test, 'at', isTCommonsScheduleTime)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'action')) return false;
	
	return true;
}
