export type TCommonsFinalValue<T> = {
		value: T;
		timestamp: Date;
};
