export enum ECommonsSynchronisedTimeoutAction {
	PROCEED = 'proceed',
	ABORT = 'abort'
}
