import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsRunState {
		PRE = 'pre',
		RUNNING = 'running',
		COMPLETED = 'completed',
		TIMEDOUT = 'timedout',
		ERROR = 'error',
		ABORTED = 'aborted'
}

export function fromECommonsRunState(value: ECommonsRunState): string {
	switch (value) {
		case ECommonsRunState.PRE:
			return ECommonsRunState.PRE.toString();
		case ECommonsRunState.RUNNING:
			return ECommonsRunState.RUNNING.toString();
		case ECommonsRunState.COMPLETED:
			return ECommonsRunState.COMPLETED.toString();
		case ECommonsRunState.TIMEDOUT:
			return ECommonsRunState.TIMEDOUT.toString();
		case ECommonsRunState.ERROR:
			return ECommonsRunState.ERROR.toString();
		case ECommonsRunState.ABORTED:
			return ECommonsRunState.ABORTED.toString();
		default:
			throw new Error('Unknown ECommonsRunState');
	}
}

export function toECommonsRunState(value: string): ECommonsRunState|undefined {
	switch (value.toLowerCase()) {
		case ECommonsRunState.PRE.toString():
			return ECommonsRunState.PRE;
		case ECommonsRunState.RUNNING.toString():
			return ECommonsRunState.RUNNING;
		case ECommonsRunState.COMPLETED.toString():
			return ECommonsRunState.COMPLETED;
		case ECommonsRunState.TIMEDOUT.toString():
			return ECommonsRunState.TIMEDOUT;
		case ECommonsRunState.ERROR.toString():
			return ECommonsRunState.ERROR;
		case ECommonsRunState.ABORTED.toString():
			return ECommonsRunState.ABORTED;
		default:
			return undefined;
	}
}

export function isECommonsRunState(test: unknown): test is ECommonsRunState {
	if (!commonsTypeIsString(test)) return false;

	return toECommonsRunState(test) !== undefined;
}
