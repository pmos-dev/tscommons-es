import { commonsTypeIsNumberArray } from 'tscommons-es-core';
import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import {
		commonsAsyncInterval,
		commonsAsyncAbortInterval
} from '../helpers/commons-async';

import { TCommonsScheduleTime } from '../types/tcommons-schedule-time';
import { isTCommonsSchedule } from '../types/tcommons-schedule';

import { ECommonsInvocation } from '../enums/ecommons-invocation';

type TScheduled = {
		at: TCommonsScheduleTime;
		callback: () => Promise<void>;
};

export class CommonsSchedule {
	private static doesMatchSchedule(test: Date, at: TCommonsScheduleTime) {
		const years: number[] = [];
		if (at.year !== undefined) {
			if (commonsTypeIsNumberArray(at.year)) years.push(...at.year);
			else years.push(at.year);
		}
		if (years.length > 0 && !years.includes(test.getFullYear())) return false;

		const months: number[] = [];
		if (at.month !== undefined) {
			if (commonsTypeIsNumberArray(at.month)) months.push(...at.month);
			else months.push(at.month);
		}
		if (months.length > 0 && !months.includes(test.getMonth())) return false;

		const days: number[] = [];
		if (at.day !== undefined) {
			if (commonsTypeIsNumberArray(at.day)) days.push(...at.day);
			else days.push(at.day);
		}
		if (days.length > 0 && !days.includes(test.getDate())) return false;

		const dows: number[] = [];
		if (at.dow !== undefined) {
			if (commonsTypeIsNumberArray(at.dow)) dows.push(...at.dow);
			else dows.push(at.dow);
		}
		if (dows.length > 0 && !dows.includes(test.getDay())) return false;

		const hours: number[] = [];
		if (at.hour !== undefined) {
			if (commonsTypeIsNumberArray(at.hour)) hours.push(...at.hour);
			else hours.push(at.hour);
		}
		if (hours.length > 0 && !hours.includes(test.getHours())) return false;

		const minutes: number[] = [];
		if (at.minute !== undefined) {
			if (commonsTypeIsNumberArray(at.minute)) minutes.push(...at.minute);
			else minutes.push(at.minute);
		}
		if (minutes.length > 0 && !minutes.includes(test.getMinutes())) return false;

		const seconds: number[] = [];
		if (at.second !== undefined) {
			if (commonsTypeIsNumberArray(at.second)) seconds.push(...at.second);
			else seconds.push(at.second);
		}
		if (seconds.length > 0 && !seconds.includes(test.getSeconds())) return false;

		return true;
	}

	private schedules: Map<string, TScheduled> = new Map<string, TScheduled>();

	private isRunning: boolean = false;

	constructor(
		private id: string
	) {}
	
	public schedule(
			at: TCommonsScheduleTime,
			callback: () => Promise<void>,
			id?: string
	): void {
		if (id && this.schedules.has(id)) this.clearSchedule(id);
	
		if (!id) id = commonsBase62GenerateRandomId();

		const schedule: TScheduled = {
				at: at,
				callback: callback
		};
	
		this.schedules.set(id, schedule);
	}

	public clearSchedule(id: string): void {
		this.schedules.delete(id);
	}

	private dispatch(): void {
		const now: Date = new Date();

		for (const schedule of Array.from(this.schedules.values())) {
			if (!CommonsSchedule.doesMatchSchedule(now, schedule.at)) continue;

			void (async (): Promise<void> => {
				await schedule.callback();
			})();
		}
	}

	public start(): boolean {
		if (this.isRunning) return false;
	
		commonsAsyncInterval(
				1000,
				ECommonsInvocation.FIXED,
				async (): Promise<void> => {	// eslint-disable-line @typescript-eslint/require-await
					this.dispatch();
				},
				`schedule_${this.id}`
		);
	
		return true;
	}

	public stop(): boolean {
		if (!this.isRunning) return false;
	
		commonsAsyncAbortInterval(`schedule_${this.id}`);
	
		return true;
	}
	
	public parse<T>(json: unknown[], toAction: (_: string) => T|undefined, callback: (_: T) => Promise<void>): void {
		for (const schedule of json) {
			if (!isTCommonsSchedule(schedule)) throw new Error('Schedule JSON contains an invalid entry');

			const action: T|undefined = toAction(schedule.action);
			if (action === undefined) throw new Error('Schedule JSON contains an invalid action');

			this.schedule(
					schedule.at,
					async (): Promise<void> => {
						await callback(action);
					}
			);
		}
	}
}
