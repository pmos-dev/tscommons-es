import { CommonsSemaphore } from '../helpers/commons-semaphore';

import { ECommonsSynchronisedTimeoutAction } from '../enums/ecommons-synchronised-timeout-action';

export class CommonsSynchronised {
	private semaphores: Map<string, CommonsSemaphore> = new Map<string, CommonsSemaphore>();

	public async synchronised<T>(
			context: string,
			timeout: number,
			timeoutAction: ECommonsSynchronisedTimeoutAction,
			callback: (claimed?: boolean) => Promise<T>	// the ? is for backwards compatibility
	): Promise<T> {
		if (!this.semaphores.has(context)) this.semaphores.set(context, new CommonsSemaphore());
		const semaphore: CommonsSemaphore = this.semaphores.get(context)!;

		let claimed: boolean = false;
		try {
			await semaphore.claim(timeout);
			claimed = true;
		} catch (e) {
			claimed = false;
		}

		if (!claimed && timeoutAction === ECommonsSynchronisedTimeoutAction.ABORT) throw new Error('Timeout on synchronised block');
		
		try {
			return await callback(claimed);
		} finally {
			if (claimed) semaphore.release();
		}
	}

	public attempt(
			context: string,
			timeout: number,
			timeoutAction: ECommonsSynchronisedTimeoutAction
	): Promise<boolean> {
		return new Promise((resolve: (claimed: boolean) => void, reject: (reason: Error) => void): void => {
			void (async (): Promise<void> => {
				try {
					await this.synchronised<void>(
							context,
							timeout,
							timeoutAction,
							async (claimed?: boolean): Promise<void> => {	// eslint-disable-line @typescript-eslint/require-await
								resolve(claimed || false);	// the || false is just for typescript, as synchronised always returns boolean
							}
					);
				} catch (e) {
					reject(e as Error);
				}
			})();
		});
	}
}
