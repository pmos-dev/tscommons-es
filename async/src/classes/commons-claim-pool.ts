import { commonsArrayRemove, commonsBase62GenerateLongRandomId } from 'tscommons-es-core';

import { commonsAsyncAbortTimeout, commonsAsyncTimeout } from '../helpers/commons-async';

export class CommonsClaimPool<ResourceT = undefined> {
	private active: string[] = [];
	private pendingTimeouts: string[] = [];

	private available: ResourceT[]|undefined;
	private allocated: Map<string, ResourceT> = new Map<string, ResourceT>();

	constructor(
			private maxClaimable: number,
			private timeoutToClaim: number,
			resources?: ResourceT[]
	) {
		if (resources) {
			if (resources.length !== maxClaimable) throw new Error('Available resources does not match the max claimable');
			this.available = [ ...resources ];
		}
	}

	public claim(): string|false {
		if (this.active.length >= this.maxClaimable) return false;

		const uid: string = commonsBase62GenerateLongRandomId();
		this.active.push(uid);

		if (this.available) {
			const resource: ResourceT|undefined = this.available.shift();
			if (!resource) throw new Error('Assertion failure. No resource avaliable');

			this.allocated.set(uid, resource);
		}

		return uid;
	}

	public getResource(uid: string): ResourceT {
		if (!this.available) throw new Error('Resources are not available');
		if (!this.allocated.has(uid)) throw new Error('Resource has not been allocated to this uid');

		return this.allocated.get(uid)!;
	}

	public claimOrPend(): Promise<string> {
		const internalTimeoutUid: string = commonsBase62GenerateLongRandomId();
		const internalSpinUid: string = commonsBase62GenerateLongRandomId();

		return new Promise<string>((resolve: (uid: string) => void, reject: (e: Error) => void) => {
			let timedout: boolean = false;

			void (async (): Promise<void> => {
				try {
					await commonsAsyncTimeout(this.timeoutToClaim, internalTimeoutUid);
				} catch (e) {
					// do nothing
				}
				
				timedout = true;
				commonsAsyncAbortTimeout(internalSpinUid);
			})();

			void (async (): Promise<void> => {
				while (!timedout) {
					const uid: string|false = this.claim();
					if (uid) {
						commonsAsyncAbortTimeout(internalTimeoutUid);
						commonsAsyncAbortTimeout(internalSpinUid);

						resolve(uid);
						return;
					}

					try {
						await commonsAsyncTimeout(100, internalSpinUid);	// 100ms spin
					} catch (e) {
						// do nothing
					}
				}

				reject(new Error('Timeout pending to claim a pool space'));
			})();
		});
	}

	public free(uid: string): void {
		commonsArrayRemove(this.active, uid);

		if (this.available) {
			try {
				const existing: ResourceT = this.getResource(uid);
				this.allocated.delete(uid);
				this.available.push(existing);
			} catch (e) {
				// ignore
			}
		}

		for (const pendingTimeout of this.pendingTimeouts) {
			commonsAsyncAbortTimeout(pendingTimeout);
		}
	}

	public async runWithinPool<ReturnT = void>(
			callback: (resource?: ResourceT) => Promise<ReturnT>
	): Promise<ReturnT> {
		const uid: string = await this.claimOrPend();
		try {
			if (this.available) {
				const resource: ResourceT = this.getResource(uid);
				return await callback(resource);
			} else {
				return await callback();
			}
		} finally {
			this.free(uid);
		}
	}
}
