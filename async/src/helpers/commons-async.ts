import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { ECommonsInvocation } from '../enums/ecommons-invocation';

type TTimeout = {
		thread: any;	// has to any, as node.js uses Timer and Angular uses number
		reject: () => void;
};

const timeouts: Map<string, TTimeout> = new Map<string, TTimeout>();

// has to any, as node.js uses Timer and Angular uses number
const intervals: Map<string, any> = new Map<string, any>();

const isInCycle: Map<string, boolean> = new Map<string, boolean>();
const idUid: Map<string, string> = new Map<string, string>();
const isAborted: Map<string, boolean> = new Map<string, boolean>();

function internalClearTimeout(id: string): void {
	clearTimeout(timeouts.get(id)!.thread);	// eslint-disable-line
	timeouts.delete(id);
}

export function commonsAsyncTimeout(ms: number, id?: string): Promise<void> {
	if (id && timeouts.has(id)) internalClearTimeout(id);
	
	return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
		const thread = setTimeout((): void => {
			if (id && timeouts.has(id)) internalClearTimeout(id);
			resolve();
		}, ms);
		
		if (id) {
			timeouts.set(id, {
					thread: thread,
					reject: (): void => {
						reject(new Error('abortTimeout called'));
					}
			});
		}
	});
}

export function commonsAsyncAbortTimeout(id: string): void {
	if (timeouts.has(id)) {
		const reject: () => void = timeouts.get(id)!.reject;
		internalClearTimeout(id);
		reject();
	}
}

function commonsAsyncClearInterval(id: string): void {
	clearInterval(intervals.get(id)!);	// eslint-disable-line
	intervals.delete(id);
	
	const internalId: string|undefined = idUid.get(id);
	if (internalId !== undefined) isInCycle.delete(internalId);
	
	idUid.delete(id);
}

export function commonsAsyncInterval(
		ms: number,
		approach: ECommonsInvocation,
		callback: () => Promise<void>,
		id?: string
): void {
	if (id && intervals.has(id)) commonsAsyncClearInterval(id);
	
	switch (approach) {
		case ECommonsInvocation.FIXED: {
			const thread = setInterval(async (): Promise<void> => {
				await callback();
			}, ms);
		
			if (id) intervals.set(id, thread);
			
			break;
		}
		case ECommonsInvocation.WHEN: {
			const cycle: () => Promise<void> = async (): Promise<void> => {
				if (id !== undefined) isAborted.set(id, false);
				
				const isAbortedCallback: () => boolean = () => {
					if (id === undefined) return false;
					return isAborted.get(id) || false;
				};
				
				let first: boolean = true;
				while (!isAbortedCallback()) {
					try {
						if (first) {
							first = false;
							continue;
						}
						await callback();
					} catch (ex) {
						// can't throw as it would abort the while loop
						console.log(ex);
					} finally {
						await commonsAsyncTimeout(ms);
					}
				}
				
				if (id !== undefined) isAborted.delete(id);
			};
			
			void cycle();
			
			break;
		}
		case ECommonsInvocation.IF: {
			const internalId = commonsBase62GenerateRandomId();
			if (id) idUid.set(id, internalId);
			
			isInCycle.set(internalId, false);
			
			commonsAsyncInterval(
					ms,
					ECommonsInvocation.FIXED,
					async (): Promise<void> => {
						if (isInCycle.get(internalId)) return;

						try {
							isInCycle.set(internalId, true);
							await callback();
						} finally {
							isInCycle.set(internalId, false);
						}
					},
					id
			);
			
			break;
		}
	}
}

export function commonsAsyncAbortInterval(id: string): void {
	if (isAborted.has(id)) isAborted.set(id, true);
	
	if (intervals.has(id)) {
		commonsAsyncClearInterval(id);
	}
}
