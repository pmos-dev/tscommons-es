import { commonsAsyncTimeout } from './commons-async';

export class CommonsSemaphore {
	private flag: boolean = false;

	public async claim(timeout: number): Promise<void> {
		return new Promise<void>((resolve: () => void, reject: (e: Error) => void) => {
			void (async (): Promise<void> => {
				let t: number = Math.floor(timeout / 50);
				
				while (this.flag) {
					// wait
					await commonsAsyncTimeout(50);
					t--;
					if (t < 0) {
						reject(new Error('Timeout whilst waiting to claim semaphore'));
						return;
					}
				}
				this.flag = true;
				resolve();
			})();
		});
	}

	public release(): void {
		this.flag = false;
	}
}
