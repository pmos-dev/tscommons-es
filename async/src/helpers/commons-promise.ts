import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { commonsAsyncAbortTimeout, commonsAsyncTimeout } from './commons-async';

export function commonsPromiseTimeout<T>(
		promise: () => Promise<T>,
		timeout: number
): Promise<T> {
	return new Promise((resolve: (result: T) => void, reject: (e: Error) => void): void => {
		const timeoutId: string = `commonsAsyncPromiseTimeout_${commonsBase62GenerateRandomId()}`;
		let completed: boolean = false;

		void Promise.all([
				(async (): Promise<void> => {
					try {
						const outcome: T = await promise();
						resolve(outcome);
					} catch (e) {
						reject(e as Error);
					} finally {
						completed = true;
						commonsAsyncAbortTimeout(timeoutId);
					}
				})(),
				(async (): Promise<void> => {
					try {
						await commonsAsyncTimeout(timeout, timeoutId);
					} catch (e) {
						// do nothing
					}

					if (!completed) reject(new Error('Timeout'));
					completed = true;
				})()
		]);

	});
}
