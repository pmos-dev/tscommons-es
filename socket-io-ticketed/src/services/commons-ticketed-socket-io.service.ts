import { Observable, Subject } from 'rxjs';

import { commonsTypeHasPropertyString } from 'tscommons-es-core';

import { ICommonsTicketedSocketIo } from '../interfaces/icommons-ticketed-socket-io';

type TSocketIoTicketData = {
		ticket: string;
};
function isTSocketIoTicketData(test: unknown): test is TSocketIoTicketData {
	if (!commonsTypeHasPropertyString(test, 'ticket')) return false;

	return true;
}

type TSocketIoDispatched = TSocketIoTicketData;
function isTSocketIoDispatched(test: unknown): test is TSocketIoDispatched {
	return isTSocketIoTicketData(test);
}

type TSocketIoCancelled = TSocketIoTicketData;
function isTSocketIoCancelled(test: unknown): test is TSocketIoCancelled {
	return isTSocketIoTicketData(test);
}

type TSocketIoTimedout = TSocketIoTicketData;
function isTSocketIoTimedout(test: unknown): test is TSocketIoTimedout {
	return isTSocketIoTicketData(test);
}

export type TCommonsTicketedSocketIoError = TSocketIoTicketData & {
		message: string;
};
function isTSocketIoError(test: unknown): test is TCommonsTicketedSocketIoError {
	if (!isTSocketIoTicketData(test)) return false;
	if (!commonsTypeHasPropertyString(test, 'message')) return false;

	return true;
}

export type TCommonsTicketedSocketIoProgress = TSocketIoTicketData & {
		src: 'stdout'|'stderr';
		data: string;
};
function isTSocketIoProgress(test: unknown): test is TCommonsTicketedSocketIoProgress {
	if (!isTSocketIoTicketData(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'src')) return false;
	if (![ 'stdout', 'stderr' ].includes((test as TCommonsTicketedSocketIoProgress).src as string)) return false;

	if (!commonsTypeHasPropertyString(test, 'data')) return false;

	return true;
}

export type TCommonsTicketedSocketIoSucceeded = TSocketIoTicketData & {
		data: string;
};
function isTSocketIoSucceeded(test: unknown): test is TCommonsTicketedSocketIoSucceeded {
	if (!isTSocketIoTicketData(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'data')) return false;

	return true;
}

export type TCommonsTicketedSocketIoFailed = TSocketIoTicketData & {
		data: string;
};
function isTSocketIoFailed(test: unknown): test is TCommonsTicketedSocketIoFailed {
	if (!isTSocketIoTicketData(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'data')) return false;

	return true;
}

export class CommonsTicketedSocketIoService implements ICommonsTicketedSocketIo {
	private internalSocketIoId: string|undefined;
	public get socketIoId(): string|undefined {
		return this.internalSocketIoId;
	}
	public set socketIoId(id: string|undefined) {
		this.internalSocketIoId = id;
	}

	private dispatchedEmitter: Subject<string> = new Subject<string>();
	public get dispatchedObservable(): Observable<string> {
		return this.dispatchedEmitter;
	}

	private timedoutEmitter: Subject<string> = new Subject<string>();
	public get timedoutObservable(): Observable<string> {
		return this.timedoutEmitter;
	}

	private cancelledEmitter: Subject<string> = new Subject<string>();
	public get cancelledObservable(): Observable<string> {
		return this.cancelledEmitter;
	}

	private errorEmitter: Subject<TCommonsTicketedSocketIoError> = new Subject<TCommonsTicketedSocketIoError>();
	public get errorObservable(): Observable<TCommonsTicketedSocketIoError> {
		return this.errorEmitter;
	}

	private progressEmitter: Subject<TCommonsTicketedSocketIoProgress> = new Subject<TCommonsTicketedSocketIoProgress>();
	public get progressObservable(): Observable<TCommonsTicketedSocketIoProgress> {
		return this.progressEmitter;
	}

	private succeededEmitter: Subject<TCommonsTicketedSocketIoSucceeded> = new Subject<TCommonsTicketedSocketIoSucceeded>();
	public get succeededObservable(): Observable<TCommonsTicketedSocketIoSucceeded> {
		return this.succeededEmitter;
	}

	private failedEmitter: Subject<TCommonsTicketedSocketIoFailed> = new Subject<TCommonsTicketedSocketIoFailed>();
	public get failedObservable(): Observable<TCommonsTicketedSocketIoFailed> {
		return this.failedEmitter;
	}

	constructor(private ns: string) {}

	public setupOns(
			onCallback: (
					command: string,
					callback: (data: any) => void
			) => void
	): void {
		onCallback(
				`${this.ns}-ticketed-error`,
				(data: unknown): void => {
					if (!isTSocketIoError(data)) throw new Error('Data is not TSocketIoError');
					this.errorEmitter.next(data);
				}
		);

		onCallback(
				`${this.ns}-ticketed-dispatched`,
				(data: unknown): void => {
					if (!isTSocketIoDispatched(data)) throw new Error('Data is not TSocketIoDispatched');
					this.dispatchedEmitter.next(data.ticket);
				}
		);

		onCallback(
				`${this.ns}-ticketed-timedout`,
				(data: unknown): void => {
					if (!isTSocketIoTimedout(data)) throw new Error('Data is not TSocketIoTimedout');
					this.timedoutEmitter.next(data.ticket);
				}
		);

		onCallback(
				`${this.ns}-ticketed-cancelled`,
				(data: unknown): void => {
					if (!isTSocketIoCancelled(data)) throw new Error('Data is not TSocketIoCancelled');
					this.cancelledEmitter.next(data.ticket);
				}
		);

		onCallback(
				`${this.ns}-ticketed-progress`,
				(data: unknown): void => {
					if (!isTSocketIoProgress(data)) throw new Error('Data is not TSocketIoProgress');
					this.progressEmitter.next(data);
				}
		);

		onCallback(
				`${this.ns}-ticketed-succeeded`,
				(data: unknown): void => {
					if (!isTSocketIoSucceeded(data)) throw new Error('Data is not TSocketIoSucceeded');
					this.succeededEmitter.next(data);
				}
		);

		onCallback(
				`${this.ns}-ticketed-failed`,
				(data: unknown): void => {
					if (!isTSocketIoFailed(data)) throw new Error('Data is not TSocketIoFailed');
					this.failedEmitter.next(data);
				}
		);
	}
}
