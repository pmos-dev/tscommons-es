// This is a true interface

import { Observable } from 'rxjs/internal/Observable';

import {
		TCommonsTicketedSocketIoError,
		TCommonsTicketedSocketIoProgress,
		TCommonsTicketedSocketIoSucceeded,
		TCommonsTicketedSocketIoFailed
} from '../services/commons-ticketed-socket-io.service';

export type TConnectionCallback = () => void;

export interface ICommonsTicketedSocketIo {
	get socketIoId(): string|undefined;
	set socketIoId(id: string|undefined);

	get dispatchedObservable(): Observable<string>;
	get timedoutObservable(): Observable<string>;
	get cancelledObservable(): Observable<string>;
	get errorObservable(): Observable<TCommonsTicketedSocketIoError>;
	get progressObservable(): Observable<TCommonsTicketedSocketIoProgress>;
	get succeededObservable(): Observable<TCommonsTicketedSocketIoSucceeded>;
	get failedObservable(): Observable<TCommonsTicketedSocketIoFailed>;
}

export interface ICommonsTicketedSocketIoAutoSetup {
	addConnectCallback(callback: TConnectionCallback): void;
	connect(params?: { [ key: string ]: string|number|boolean }|undefined): boolean;
	getId(): Promise<string|undefined>;
	disconnect(): void;
}

export interface ICommonsTicketedSocketIoWithAutoSetup extends ICommonsTicketedSocketIo, ICommonsTicketedSocketIoAutoSetup {}
