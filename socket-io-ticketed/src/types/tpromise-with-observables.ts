import { Observable } from 'rxjs';

export type TPromiseWithObservables<
		PromiseT,
		ProgressObservableT
> = {
		promise: Promise<PromiseT>;
		queuedObservable: Observable<string>;
		dispatchedObservable: Observable<void>;
		progressObservable: Observable<ProgressObservableT>;
};
