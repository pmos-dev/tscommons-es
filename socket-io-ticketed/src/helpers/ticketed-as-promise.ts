import { Subscription, Subject } from 'rxjs';

import { TEncoded, TEncodedObject, commonsBase62IsLongId, commonsTypeIsString } from 'tscommons-es-core';
import { commonsAsyncTimeout } from 'tscommons-es-async';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsHttpMethod, TCommonsHttpRequestOptions } from 'tscommons-es-http';

import {
		TCommonsTicketedSocketIoError,
		TCommonsTicketedSocketIoProgress,
		TCommonsTicketedSocketIoSucceeded,
		TCommonsTicketedSocketIoFailed
} from '../services/commons-ticketed-socket-io.service';

import { ICommonsTicketedSocketIo, ICommonsTicketedSocketIoWithAutoSetup } from '../interfaces/icommons-ticketed-socket-io';

import { TPromiseWithObservables } from '../types/tpromise-with-observables';

export function commonsTicketedSubmitAndWaitAsPromise<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		restService: CommonsRestClientService,
		socketIoService: ICommonsTicketedSocketIo,
		connectCallback: (callback: (socketIoId: string) => void) => void,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		queued: (ticketId: string) => void = (_ticketId: string): void => {
			// do nothing
		},
		dispatched: () => void = (): void => {
			// do nothing
		},
		progress: (data: Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>) => void = (_data: Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>): void => {
			// do nothing
		},
		debug: (message: string) => void = (_message: string): void => {
			// do nothing
		}
): Promise<ResultT> {
	return new Promise<ResultT>((resolve: (data: ResultT) => void, reject: (e: Error) => void): void => {
		const subscriptions: Subscription[] = [];

		const cleanup: () => void = (): void => {
			for (const subscription of subscriptions) subscription.unsubscribe();
		};

		connectCallback((socketIoId: string): void => {
			let ticket: string|undefined;
			socketIoService.socketIoId = socketIoId;

			subscriptions.push(socketIoService.dispatchedObservable
					.subscribe((t: string): void => {
						if (t !== ticket) return;
						debug('Ticketed dispatched');

						dispatched();
					})
			);
		
			subscriptions.push(socketIoService.timedoutObservable
					.subscribe((t: string): void => {
						if (t !== ticket) return;
						debug('Ticketed timedout');

						cleanup();
						reject(new Error('Ticketed timedout'));
					})
			);
		
			subscriptions.push(socketIoService.cancelledObservable
					.subscribe((t: string): void => {
						if (t !== ticket) return;
						debug('Ticketed cancelled');

						cleanup();
						reject(new Error('Ticketed cancelled'));
					})
			);
		
			subscriptions.push(socketIoService.errorObservable
					.subscribe((t: TCommonsTicketedSocketIoError): void => {
						if (t.ticket !== ticket) return;
						debug(`Ticketed error: ${t.message}`);

						cleanup();
						reject(new Error(`Ticketed error: ${t.message}`));
					})
			);
		
			subscriptions.push(socketIoService.progressObservable
					.subscribe((t: TCommonsTicketedSocketIoProgress): void => {
						if (t.ticket !== ticket) return;
						debug('Ticketed progressed');

						progress({
								src: t.src,
								data: t.data
						});
					})
			);
		
			subscriptions.push(socketIoService.succeededObservable
					.subscribe((t: TCommonsTicketedSocketIoSucceeded): void => {
						if (t.ticket !== ticket) return;
						debug('Ticketed succeeded');

						cleanup();

						if (!checker(t.data)) {
							reject(new Error('Returned data did not match expected checker value'));
							return;
						}

						resolve(t.data);
					})
			);
		
			subscriptions.push(socketIoService.failedObservable
					.subscribe((t: TCommonsTicketedSocketIoFailed): void => {
						if (t.ticket !== ticket) return;
						debug('Ticketed failed');

						cleanup();
						reject(new Error('Ticketed failed'));
					})
			);
		
			void (async (): Promise<void> => {
				await commonsAsyncTimeout(connectToSubmitDelay);

				body = {
						socketIoId: socketIoId,
						...body
				};
				
				try {
					let result: unknown|undefined;
					
					switch (verb) {
						case ECommonsHttpMethod.POST:
							result = await restService.postRest(
									script,
									body,
									params,
									headers,
									options
							);
							break;
						case ECommonsHttpMethod.PUT:
							result = await restService.putRest(
									script,
									body,
									params,
									headers,
									options
							);
							break;
						case ECommonsHttpMethod.PATCH:
							result = await restService.patchRest(
									script,
									body,
									params,
									headers,
									options
							);
							break;
						default:
							throw new Error('Unsupported verb for commonsTicketedSubmitAndWaitAsPromise');
					}

					if (!commonsTypeIsString(result)) {
						cleanup();
						reject(new Error('Result was not a string'));
						return;
					}

					if (!commonsBase62IsLongId(result)) {
						cleanup();
						reject(new Error('Result was not a base62 long ID'));
						return;
					}
		
					debug(`Ticket uid is ${result}`);
					ticket = result;

					queued(ticket);
				} catch (e) {
					reject(e as Error);	// cascade
				}
			})();
		});
	});
}

export async function commonsTicketedSubmitAndWaitAsPromiseWithAutoSocketIoSetupAndDisconnect<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		restService: CommonsRestClientService,
		socketIoService: ICommonsTicketedSocketIoWithAutoSetup,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		queued?: (ticketId: string) => void,
		dispatched?: () => void,
		progress?: (data: Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>) => void,
		debug: (message: string) => void = (_message: string): void => {
			// do nothing
		}
): Promise<ResultT> {
	try {
		return await commonsTicketedSubmitAndWaitAsPromise(
				restService,
				socketIoService,
				(callback: (socketIoId: string) => void): void => {
					socketIoService.addConnectCallback((): void => {
						void (async (): Promise<void> => {
							const socketIoId: string|undefined = await socketIoService.getId();
							if (!socketIoId) throw new Error('Unable to obtain socket.io ID');
							
							callback(socketIoId);
						})();
					});
					socketIoService.connect();
				},
				verb,
				script,
				body,
				checker,
				params,
				headers,
				options,
				connectToSubmitDelay,
				queued,
				dispatched,
				progress,
				debug
		);
	} finally {
		socketIoService.disconnect();
	}
}

export function commonsTicketedSubmitAndWaitAsPromiseWithObservables<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		restService: CommonsRestClientService,
		socketIoService: ICommonsTicketedSocketIo,
		connectCallback: (callback: (socketIoId: string) => void) => void,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		debug: (message: string) => void = (_message: string): void => {
			// do nothing
		}
): TPromiseWithObservables<
		ResultT,
		Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
> {
	const queuedSubject: Subject<string> = new Subject<string>();
	let hasQueuedSubjectCompleted: boolean = false;

	const dispatchedSubject: Subject<void> = new Subject<void>();
	let hasDispatchedSubjectCompleted: boolean = false;

	const progressSubject: Subject<Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>> = new Subject<Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>>();

	const promise: Promise<ResultT> = commonsTicketedSubmitAndWaitAsPromise<
			ResultT,
			BodyT,
			ParamsT,
			HeadersT
	>(
			restService,
			socketIoService,
			connectCallback,
			verb,
			script,
			body,
			checker,
			params,
			headers,
			options,
			connectToSubmitDelay,
			(ticketId: string): void => {
				queuedSubject.next(ticketId);
				queuedSubject.complete();
				hasQueuedSubjectCompleted = true;
			},
			(): void => {
				dispatchedSubject.next();
				dispatchedSubject.complete();
				hasDispatchedSubjectCompleted = true;
			},
			(data: Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>): void => {
				progressSubject.next(data);
			},
			debug
	);

	const promiseWrapper: Promise<ResultT> = (async (): Promise<ResultT> => {
		try {
			return await promise;
		} finally {
			progressSubject.complete();

			if (!hasQueuedSubjectCompleted) queuedSubject.complete();
			if (!hasDispatchedSubjectCompleted) dispatchedSubject.complete();
		}
	})();

	return {
			promise: promiseWrapper,
			queuedObservable: queuedSubject,
			dispatchedObservable: dispatchedSubject,
			progressObservable: progressSubject
	};
}

export function commonsTicketedSubmitAndWaitAsPromiseWithObservablesWithAutoSocketIoSetupAndDisconnect<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		restService: CommonsRestClientService,
		socketIoService: ICommonsTicketedSocketIoWithAutoSetup,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		debug: (message: string) => void = (_message: string): void => {
			// do nothing
		}
): TPromiseWithObservables<
		ResultT,
		Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
> {
	const promiseWithObservables: TPromiseWithObservables<
			ResultT,
			Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
	> = commonsTicketedSubmitAndWaitAsPromiseWithObservables(
			restService,
			socketIoService,
			(callback: (socketIoId: string) => void): void => {
				socketIoService.addConnectCallback((): void => {
					void (async (): Promise<void> => {
						const socketIoId: string|undefined = await socketIoService.getId();
						if (!socketIoId) throw new Error('Unable to obtain socket.io ID');
						
						callback(socketIoId);
					})();
				});
				socketIoService.connect();
			},
			verb,
			script,
			body,
			checker,
			params,
			headers,
			options,
			connectToSubmitDelay,
			debug
	);

	const promiseWrapper: Promise<ResultT> = (async (): Promise<ResultT> => {
		try {
			return await promiseWithObservables.promise;
		} finally {
			socketIoService.disconnect();
		}
	})();

	return {
			promise: promiseWrapper,
			queuedObservable: promiseWithObservables.queuedObservable,
			dispatchedObservable: promiseWithObservables.dispatchedObservable,
			progressObservable: promiseWithObservables.progressObservable
	};
}
