export function commonsAiDataHumanise(
		value: number,
		magnitude: number = 0.1
): number {
	const error: number = Math.random() - 0.5;
	value += error * magnitude;
	if (value < 0) value = 0;
	if (value > 1) value = 1;
	
	return value;
}

export function commonsAiDataHumaniseArray(
		values: number[],
		magnitude: number = 0.1
): void {
	for (let i = values.length; i-- > 0;) {
		values[i] = commonsAiDataHumanise(
				values[i],
				magnitude
		);
	}
}
