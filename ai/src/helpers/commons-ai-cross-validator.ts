import { commonsArrayShuffle } from 'tscommons-es-core';

import { TCommonsAiCrossValidationK } from '../types/tcross-validation-k';
import { TCommonsAiGenericTrainingData } from '../types/tgeneric-training-data';

export function commonsAiCrossValidationBuildKs<I, O>(
		trainingData: TCommonsAiGenericTrainingData<I, O>[],
		iterations: number,
		maxValidationRatio: number = 0.2
): TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<I, O>>[] {
	const validationPivot: number = Math.min(maxValidationRatio, 1 / iterations);

	const ks: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<I, O>>[] = [];

	const clone: TCommonsAiGenericTrainingData<I, O>[] = [ ...trainingData ];
	commonsArrayShuffle(clone);

	const validationLength: number = Math.floor(clone.length * validationPivot);

	for (let i = iterations; i-- > 0;) {
		const k: TCommonsAiGenericTrainingData<I, O>[] = [ ...clone ];

		const validationOffset: number = i * validationLength;

		const validation: TCommonsAiGenericTrainingData<I, O>[] = k.splice(validationOffset, validationLength);
		const training: TCommonsAiGenericTrainingData<I, O>[] = k;

		ks.push({
				training: training,
				validation: validation
		});
	}

	return ks;
}

export function commonsAiCrossValidationBuildEnumKs<I, E extends string>(
		keys: E[],
		trainingData: Map<E, I[]>,
		iterations: number,
		maxValidationRatio: number = 0.2
): TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<I, E>>[] {
	const datas: Map<E, TCommonsAiGenericTrainingData<I, E>[]> = new Map<E, TCommonsAiGenericTrainingData<I, E>[]>();
	for (const key of keys) {
		datas.set(
				key,
				(trainingData.get(key) || [])
						.map((d: I): TCommonsAiGenericTrainingData<I, E> => ({
								input: d,
								output: key
						}))
		);
	}

	const refactors: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<I, E>>[] = [];
	for (let i = iterations; i-- > 0;) {
		refactors.push({
				training: [],
				validation: []
		});
	}

	for (const key of keys) {
		const set: TCommonsAiGenericTrainingData<I, E>[] = datas.get(key) || [];

		const k: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<I, E>>[] = commonsAiCrossValidationBuildKs<I, E>(
				set,
				iterations,
				maxValidationRatio
		);
		
		for (let i = iterations; i-- > 0;) {
			const training: TCommonsAiGenericTrainingData<I, E>[] = k[i].training;
			const validation: TCommonsAiGenericTrainingData<I, E>[] = k[i].validation;

			refactors[i].training.push(...training);
			refactors[i].validation.push(...validation);
		}
	}

	return refactors;
}
