export enum ECommonsAiUnclassified {
		UNCLASSIFIED = 'unclassified'
}

export type TCommonsAiEOrUnclassified<E extends string> = E | ECommonsAiUnclassified;
