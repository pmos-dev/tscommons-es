import { TBoundary, TXy } from 'tscommons-es-graphics';

import { TCommonsAiEOrUnclassified } from '../enums/te-or-unclassified';

export type TCommonsAiPrediction<T> = {
		prediction: T;
		certainty: number;
};

export type TCommonsAiClassifiedPrediction<E extends string> = TCommonsAiPrediction<TCommonsAiEOrUnclassified<E>>;

export type TCommonsAiXyPrediction = TCommonsAiPrediction<TXy>;

export type TCommonsAiBoundaryPrediction = TCommonsAiPrediction<TBoundary>;
