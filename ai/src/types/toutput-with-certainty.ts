export type TCommonsAiOutputWithCertainty<O> = {
		output: O;
		certainty: number;
};
