import { ICommonsAiModelPredictable } from '../interfaces/imodel-predictable';

import { TCommonsAiQuality } from './tquality';

export type TCommonsAiModelWithQuality<I, O, M extends ICommonsAiModelPredictable<I, O>> = {
		model: M;
		quality: TCommonsAiQuality;
};
