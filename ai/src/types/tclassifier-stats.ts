export type TCommonsAiClassifierStats<E extends string> = {
		pool: number;
		classified: { [ key in E ]: number };
		bestLoss: number|null;
};
