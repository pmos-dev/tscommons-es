export type TCommonsAiCrossValidationK<T> = {
		training: T[];
		validation: T[];
};
