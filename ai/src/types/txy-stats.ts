export type TCommonsAiXyStats = {
		pool: number;
		done: number;
		bestLoss: number|null;
};
