import { TXy, TBoundary } from 'tscommons-es-graphics';

import { TCommonsAiEOrUnclassified } from '../enums/te-or-unclassified';

import { TCommonsAiPrediction } from './tprediction';

export type TCommonsAiFileAndPrediction<T> = {
		file: string;
		prediction: TCommonsAiPrediction<T>|null;
};

export type TCommonsAiClassifiedFileAndPrediction<E extends string> = TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>;
export type TCommonsAiXyFileAndPrediction = TCommonsAiFileAndPrediction<TXy>;
export type TCommonsAiBoundaryFileAndPrediction = TCommonsAiFileAndPrediction<TBoundary>;
