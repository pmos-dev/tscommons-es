export type TCommonsAiBoundaryStats = {
		pool: number;
		done: number;
		bestLoss: number|null;
};
