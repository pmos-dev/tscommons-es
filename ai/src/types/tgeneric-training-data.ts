export type TCommonsAiGenericTrainingData<I, O> = {
		input: I;
		output: O;
};
