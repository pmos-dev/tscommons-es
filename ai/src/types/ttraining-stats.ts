export type TCommonsAiTrainingStats = {
		[ metric: string ]: number;
};

export type TCommonsAiEpochTrainingStats = TCommonsAiTrainingStats & {
		epochs: number;
};
