// this is a true interface

import { TCommonsAiTrainingStats } from '../types/ttraining-stats';
import { TCommonsAiQuality } from '../types/tquality';
import { TCommonsAiGenericTrainingData } from '../types/tgeneric-training-data';

import { ICommonsAiModelPredictable } from './imodel-predictable';

export interface ICommonsAiModelTrainable<
		I,
		O,
		S extends TCommonsAiTrainingStats
> extends ICommonsAiModelPredictable<I, O> {
	build(): Promise<void>;

	train(): Promise<S|undefined>;

	test(testData: TCommonsAiGenericTrainingData<I, O>[]): Promise<TCommonsAiQuality>;
}
