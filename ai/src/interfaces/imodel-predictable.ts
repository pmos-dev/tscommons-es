// this is a true interface

import { TCommonsAiOutputWithCertainty } from '../types/toutput-with-certainty';

export interface ICommonsAiModelPredictable<I, O> {
	predict(value: I): Promise<TCommonsAiOutputWithCertainty<O>|undefined>;
}
