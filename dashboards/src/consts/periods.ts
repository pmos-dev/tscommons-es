import { CommonsFixedDate, CommonsFixedDateRange, TCommonsFixedDateRange } from 'tscommons-es-core';

import { ECommonsTimeRangePeriod } from '../enums/eperiod';

export const COMMONS_PERIOD_PIVOT_HOURS: Map<ECommonsTimeRangePeriod, number> = new Map<ECommonsTimeRangePeriod, number>();

COMMONS_PERIOD_PIVOT_HOURS.set(ECommonsTimeRangePeriod.DAY, 3);	// 3am
COMMONS_PERIOD_PIVOT_HOURS.set(ECommonsTimeRangePeriod.NIGHT, 12);	// 12pm
COMMONS_PERIOD_PIVOT_HOURS.set(ECommonsTimeRangePeriod.FULL, 6);	// 6pm (attempt to include day and night)
COMMONS_PERIOD_PIVOT_HOURS.set(ECommonsTimeRangePeriod.EAST, 12);	// 12pm

export function commonsDashboardsGetClampForPeriod(
		period: ECommonsTimeRangePeriod,
		base: CommonsFixedDate
): TCommonsFixedDateRange {
	const pivot: number = COMMONS_PERIOD_PIVOT_HOURS.get(period)!;

	const from: CommonsFixedDate = base.clone;
	from.hour = pivot;

	const to: CommonsFixedDate = from.clone;
	to.day = to.day++;
	to.second--;	// use 1 second rather than 1 milli, as most database times are to the second rather than the milli

	return new CommonsFixedDateRange(from, to).range;
}
