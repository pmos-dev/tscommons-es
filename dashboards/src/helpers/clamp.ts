import { CommonsFixedDate, TCommonsFixedDateRange, commonsArrayRemoveUndefineds } from 'tscommons-es-core';

import { ECommonsTimeRangePeriod } from '../enums/eperiod';

import { commonsDashboardsGetClampForPeriod } from '../consts/periods';

// NB: this expects the datas to be pivoted and sanitised already. See commonsDashboardsPivotAndFillAndSanitisePartialData
export function commonsDashboardsClampDateRanges<T extends TCommonsFixedDateRange = TCommonsFixedDateRange>(
		ranges: T[],
		clamp: TCommonsFixedDateRange
): T[] {
	return commonsArrayRemoveUndefineds<T>(
			ranges
					.map((range: T): T|undefined => {
						if (range.to.compareTo(clamp.from) === -1) return undefined;	// ends before clamp start
						if (range.from.compareTo(clamp.to) === 1) return undefined;	// starts after clamp end

						let from: CommonsFixedDate = range.from.clone;
						let to: CommonsFixedDate = range.to.clone;

						if (from.compareTo(clamp.from) === -1) from = clamp.from.clone;
						if (to.compareTo(clamp.from) === -1) {
							to = clamp.from.clone;
							to.milli--;
						}
						if (from.compareTo(clamp.to) === 1) from = clamp.to.clone;
						if (to.compareTo(clamp.to) === 1) {
							to = clamp.to.clone;
							to.milli--;
						}

						return {
								...range,
								from: from,
								to: to
						};
					})
	);
}

// NB: this expects the datas to be pivoted and sanitised already. See commonsDashboardsPivotAndFillAndSanitisePartialData
export function commonsDashboardsClampDateRangesForPeriod<T extends TCommonsFixedDateRange = TCommonsFixedDateRange>(
		ranges: T[],
		period: ECommonsTimeRangePeriod,
		base: CommonsFixedDate
): T[] {
	const clamp: TCommonsFixedDateRange = commonsDashboardsGetClampForPeriod(period, base);

	return commonsDashboardsClampDateRanges(ranges, clamp);
}
