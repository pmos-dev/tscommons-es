import { CommonsFixedDate, CommonsFixedDuration, TCommonsFixedDateRange, commonsArrayRemoveUndefineds } from 'tscommons-es-core';

import { ECommonsTimeRangePeriod } from '../enums/eperiod';

import { COMMONS_PERIOD_PIVOT_HOURS } from '../consts/periods';

function deriveDate(
		src: CommonsFixedDate,
		delta: CommonsFixedDuration,
		orientation: number = 1	// allows for subtraction from the to
): CommonsFixedDate {
	const clone: CommonsFixedDate = src.clone;
	clone.milli += delta.millis * orientation;

	return clone;
}

export function commonsDashboardsFillPartialDateRanges<T extends TCommonsFixedDateRange = TCommonsFixedDateRange>(
		datas: Partial<T>[],
		defaultDuration: CommonsFixedDuration
): T[] {
	return commonsArrayRemoveUndefineds<T>(
			[ ...datas ]
					.map((d: Partial<T>): T|undefined => {
						if (d.from && d.to) return d as T;

						if (!d.from && !d.to) return undefined;

						if (d.from && !d.to) {
							return {	// eslint-disable-line @typescript-eslint/consistent-type-assertions
									...d,
									to: deriveDate(d.from, defaultDuration)
							} as T;
						}

						if (!d.from && d.to) {
							return {	// eslint-disable-line @typescript-eslint/consistent-type-assertions
									...d,
									from: deriveDate(d.to, defaultDuration, -1)
							} as T;
						}

						throw new Error('Unable to derive in/out. This should not be possible.');
					})
	);
}

function pivotNextDayIfDefined(
		date: CommonsFixedDate|undefined,
		period: ECommonsTimeRangePeriod
): void {
	if (!date) return;

	const pivot: number = COMMONS_PERIOD_PIVOT_HOURS.get(period)!;
	date.pivotNextDay(pivot);
}

export function commonsDashboardsPivotAndFillAndSanitisePartialDateRanges<T extends TCommonsFixedDateRange = TCommonsFixedDateRange>(
		datas: Partial<T>[],
		period: ECommonsTimeRangePeriod,
		defaultDuration: CommonsFixedDuration,
		maxDuration: CommonsFixedDuration,
		baseDate?: CommonsFixedDate
): T[] {
	if (datas.length === 0) return [];

	const correctedForPivots: Partial<T>[] = datas
			.map((range: Partial<T>): Partial<T> => {
				const from: CommonsFixedDate|undefined = range.from ? range.from.clone : undefined;
				const to: CommonsFixedDate|undefined = range.to ? range.to.clone : undefined;

				if (from) {
					if (baseDate) from.Ymd = baseDate.Ymd;
					pivotNextDayIfDefined(from, period);
				}
				if (to) {
					if (baseDate) to.Ymd = baseDate.Ymd;
					pivotNextDayIfDefined(to, period);
				}

				return {
						...range,
						from: from,
						to: to
				};
			});

	const filleds: T[] = commonsDashboardsFillPartialDateRanges<T>(
			correctedForPivots,
			defaultDuration
	);

	for (const range of filleds) {
		while (range.from.compareTo(range.to) === 1) range.to.day++;
	}

	return filleds
			.filter((range: T): boolean => range.from.deltaTo(range.to) <= maxDuration.millis);
}
