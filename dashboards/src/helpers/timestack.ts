import { CommonsFixedDate, CommonsFixedDateRange, CommonsFixedDuration, TCommonsFixedDateRange, TPropertyObject, commonsArrayRemove } from 'tscommons-es-core';

import { ECommonsTimeRangePeriod } from '../enums/eperiod';

import { TCommonsFixedFromAndDuration } from '../types/tfixed-from-and-duration';

import { commonsDashboardsPivotAndFillAndSanitisePartialDateRanges } from './date-range';
import { commonsDashboardsClampDateRanges, commonsDashboardsClampDateRangesForPeriod } from './clamp';

export function commonsDashboardsComputeTimestack<T extends TCommonsFixedDateRange = TCommonsFixedDateRange>(
		quantiseds: T[],	// needs to be sanitised, quantised, filled etc.
		gap: CommonsFixedDuration
): TCommonsFixedFromAndDuration<T>[][] {
	if (quantiseds.length === 0) return [];

	const sorted: T[] = [ ...quantiseds ]
			.sort((a: TCommonsFixedDateRange, b: TCommonsFixedDateRange): number => {
				if (a.from.compareTo(b.from) === -1) return -1;
				if (a.from.compareTo(b.from) === 1) return 1;
				if (a.to.compareTo(b.to) === -1) return -1;
				if (a.to.compareTo(b.to) === 1) return 1;
				return 0;
			});

	const durationeds: TCommonsFixedFromAndDuration<T>[] = sorted
			.map((range: T): TCommonsFixedFromAndDuration<T> => {
				const clone: T = { ...range };
				delete (clone as TPropertyObject).to;

				const r: CommonsFixedDateRange = new CommonsFixedDateRange(range.from, range.to);

				return {
						...clone,
						// checks for negative durations are required to be filtered out already
						duration: r.isReversed ? CommonsFixedDuration.fromMillis(0) : r.durationAbs
				};
			});

	if (durationeds.length === 0) return [];

	const stack: TCommonsFixedFromAndDuration<T>[][] = [];

	let current: TCommonsFixedFromAndDuration<T> = durationeds.shift()!;
	let row: TCommonsFixedFromAndDuration<T>[] = [ current ];

	while (durationeds.length > 0) {
		const compare: CommonsFixedDate = current.from.clone;
		compare.second += current.duration.seconds + gap.seconds;

		const next: TCommonsFixedFromAndDuration<T>|undefined = durationeds
				.find((d: TCommonsFixedFromAndDuration<T>): boolean => d.from.YmdHis > compare.YmdHis);
		
		if (!next) {
			// can't fit any more on this row
			stack.push(row);
			
			row = [];
			const attempt: TCommonsFixedFromAndDuration<T>|undefined = durationeds.shift();
			if (attempt) {
				current = attempt;
				row.push(current);
			}
			continue;
		}

		row.push(next);
		current = next;
		commonsArrayRemove(durationeds, next);
	}

	if (row.length > 0) stack.push(row);

	return stack;
}

export function commonsDashboardsAutoComputeTimestack<T extends TCommonsFixedDateRange = TCommonsFixedDateRange>(
		partialUnsanitiseds: Partial<T>[],
		period: ECommonsTimeRangePeriod,
		defaultDuration: CommonsFixedDuration,
		maxDuration: CommonsFixedDuration,
		quantise: CommonsFixedDuration,
		gap: CommonsFixedDuration,
		clamp?: TCommonsFixedDateRange,
		baseDate?: CommonsFixedDate,
		_autoPadToClamp: boolean = false
): TCommonsFixedFromAndDuration<T>[][] {
	const sanitiseds: T[] = commonsDashboardsPivotAndFillAndSanitisePartialDateRanges<T>(
			partialUnsanitiseds,
			period,
			defaultDuration,
			maxDuration,
			baseDate
	);
	if (sanitiseds.length === 0) return [];

	const clampeds: T[] = [];
	if (clamp) {
		clampeds.push(...commonsDashboardsClampDateRanges<T>(sanitiseds, clamp));
	} else {
		// Clamp will need a baseDate, so if one isn't set, use the earliest from
		if (!baseDate) {
			baseDate = sanitiseds[0].from.clone;
			for (const s of sanitiseds) {
				if (s.from.compareTo(baseDate) === -1) baseDate = s.from.clone;
			}
		}

		clampeds.push(...commonsDashboardsClampDateRangesForPeriod<T>(
				sanitiseds,
				period,
				baseDate
		));
	}

	for (const range of clampeds) {
		const r: CommonsFixedDateRange = new CommonsFixedDateRange(range.from.clone, range.to.clone);
		r.quantiseToSeconds(quantise.seconds, 'outer');

		range.from = r.from.clone;
		range.to = r.to.clone;
	}

	return commonsDashboardsComputeTimestack<T>(clampeds, gap);
}
