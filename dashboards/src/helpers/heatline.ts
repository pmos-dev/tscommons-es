import { CommonsFixedDate, CommonsFixedDateRange, CommonsFixedDuration, TCommonsFixedDateRange, commonsFixedDateTallyQuantisedRanges } from 'tscommons-es-core';

import { TCommonsFixedHeat } from '../types/tfixed-heat';
import { TCommonsFixedDateTally } from '../types/tfixed-date-tally';
import { ECommonsTimeRangePeriod } from '../enums/eperiod';

import { commonsDashboardsPivotAndFillAndSanitisePartialDateRanges } from './date-range';
import { commonsDashboardsClampDateRanges, commonsDashboardsClampDateRangesForPeriod } from './clamp';

export function commonsDashboardsComputeHeatline(
		quantiseds: TCommonsFixedDateTally[],
		syncMax?: number
): TCommonsFixedHeat[] {
	if (quantiseds.length === 0) return [];

	const max: number = Math.max(...[
			syncMax || 1,
			...quantiseds
					.map((t: { tally: number }): number => t.tally)
	]);

	return quantiseds
			.map((t: TCommonsFixedDateTally): TCommonsFixedHeat => ({
					date: t.date,
					tally: t.tally,
					heat: t.tally / max
			}))
			.sort((a: TCommonsFixedHeat, b: TCommonsFixedHeat): number => a.date.compareTo(b.date));
}

export function commonsDashboardsAutoComputeHeatline(
		partialUnsanitiseds: Partial<TCommonsFixedDateRange>[],
		period: ECommonsTimeRangePeriod,
		defaultDuration: CommonsFixedDuration,
		maxDuration: CommonsFixedDuration,
		quantise: CommonsFixedDuration,
		clamp?: TCommonsFixedDateRange,
		baseDate?: CommonsFixedDate,
		syncMax?: number,
		autoPadToClamp: boolean = false
): TCommonsFixedHeat[] {
	const sanitiseds: TCommonsFixedDateRange[] = commonsDashboardsPivotAndFillAndSanitisePartialDateRanges(
			partialUnsanitiseds,
			period,
			defaultDuration,
			maxDuration,
			baseDate
	);
	if (sanitiseds.length === 0) return [];

	const clampeds: TCommonsFixedDateRange[] = [];
	if (clamp) {
		clampeds.push(...commonsDashboardsClampDateRanges(sanitiseds, clamp));
	} else {
		// Clamp will need a baseDate, so if one isn't set, use the earliest from
		if (!baseDate) {
			baseDate = sanitiseds[0].from.clone;
			for (const s of sanitiseds) {
				if (s.from.compareTo(baseDate) === -1) baseDate = s.from.clone;
			}
		}

		clampeds.push(...commonsDashboardsClampDateRangesForPeriod(
				sanitiseds,
				period,
				baseDate
		));
	}

	const quantised: TCommonsFixedDateTally[] = commonsFixedDateTallyQuantisedRanges(
			clampeds,
			quantise
	);
	if (clamp && autoPadToClamp) {
		const r: CommonsFixedDateRange = new CommonsFixedDateRange(clamp.from.clone, clamp.to.clone);
		r.quantiseToSeconds(quantise.seconds, 'outer');
		const padRange: CommonsFixedDate[] = r.quantisedFixedDatesBetween(quantise, true);

		// const padRange: CommonsFixedDate[] = new CommonsFixedDateRange(clamp.from, clamp.to).quantisedFixedDatesBetween(quantise);

		for (const t of padRange) {
			const existing: TCommonsFixedDateTally|undefined = quantised
					.find((q: TCommonsFixedDateTally): boolean => q.date.isEqual(t));
			if (existing) continue;

			quantised.push({
					date: t.clone,
					tally: 0
			});
		}

		quantised
				.sort((a: TCommonsFixedDateTally, b: TCommonsFixedDateTally): number => a.date.compareTo(b.date));
	}

	return commonsDashboardsComputeHeatline(
			quantised,
			syncMax
	);
}
