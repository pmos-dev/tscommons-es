import { CommonsFixedDate, CommonsFixedDateRange, CommonsFixedDuration, CommonsTally, TCommonsFixedDateRange } from 'tscommons-es-core';

import { ECommonsTimeRangePeriod } from '../enums/eperiod';

import { TCommonsFixedDurationTally } from '../types/tfixed-duration-tally';

import { commonsDashboardsPivotAndFillAndSanitisePartialDateRanges } from './date-range';

export function commonsDashboardsComputeDurations(
		quantisedDurations: CommonsFixedDuration[],
		quantise: CommonsFixedDuration
): TCommonsFixedDurationTally[] {
	if (quantisedDurations.length === 0) return [];

	const max: CommonsFixedDuration = CommonsFixedDuration.fromMillis(
			Math.max(...quantisedDurations
					.map((duration: CommonsFixedDuration): number => duration.millis)
			)
	);

	const tally: CommonsTally<string> = new CommonsTally<string>();
	for (const duration of quantisedDurations) {
		tally.increment(duration.dHis);
	}

	const map: Map<string, number> = tally.asMap();

	const values: TCommonsFixedDurationTally[] = [];

	const m: CommonsFixedDuration = CommonsFixedDuration.fromMillis(0);
	while (m.dHis <= max.dHis) {
		values.push({
				duration: m.clone,
				tally: map.get(m.dHis) || 0
		});

		m.millis += quantise.millis;
	}

	return values;
}

export function commonsDashboardsAutoComputeDurations(
		partialUnsanitiseds: Partial<TCommonsFixedDateRange>[],
		period: ECommonsTimeRangePeriod,
		quantise: CommonsFixedDuration,
		defaultDuration: CommonsFixedDuration,
		maxDuration: CommonsFixedDuration,
		baseDate?: CommonsFixedDate
): TCommonsFixedDurationTally[] {
	const sanitiseds: TCommonsFixedDateRange[] = commonsDashboardsPivotAndFillAndSanitisePartialDateRanges<TCommonsFixedDateRange>(
			partialUnsanitiseds,
			period,
			defaultDuration,
			maxDuration,
			baseDate
	);
	if (sanitiseds.length === 0) return [];

	const durations: CommonsFixedDuration[] = sanitiseds
			.map((range: TCommonsFixedDateRange): CommonsFixedDuration => {
				const r: CommonsFixedDateRange = new CommonsFixedDateRange(range.from, range.to);
				
				const duration: CommonsFixedDuration = r.durationAbs;
				duration.quantiseToSeconds(quantise.seconds, 'closest');

				return duration;
			});
	
	return commonsDashboardsComputeDurations(durations, quantise);
}
