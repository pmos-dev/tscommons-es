import { CommonsFixedDate, CommonsFixedDateRange, CommonsFixedDuration, CommonsTally, TCommonsFixedDateRange } from 'tscommons-es-core';

import { ECommonsTimeRangePeriod } from '../enums/eperiod';

import { TCommonsFixedDateTally } from '../types/tfixed-date-tally';

import { commonsDashboardsPivotAndFillAndSanitisePartialDateRanges } from './date-range';

export function commonsDashboardsComputeTimeGraph(
		quantiseds: CommonsFixedDate[],
		quantise: CommonsFixedDuration,
		clamp: TCommonsFixedDateRange
): TCommonsFixedDateTally[] {
	if (quantiseds.length === 0) return [];

	const min: CommonsFixedDate = quantiseds[0].clone;
	const max: CommonsFixedDate = quantiseds[0].clone;
	for (const q of quantiseds) {
		if (q.compareTo(min) === -1) min.YmdHisMs = q.YmdHisMs;
		if (q.compareTo(max) === 1) max.YmdHisMs = q.YmdHisMs;
	}

	const tally: CommonsTally<string> = new CommonsTally<string>();
	for (const date of quantiseds) {
		tally.increment(date.YmdHis);
	}

	const periods: CommonsFixedDate[] = new CommonsFixedDateRange(min, max).quantisedFixedDatesBetween(quantise, true);

	const values: TCommonsFixedDateTally[] = [];
	for (const p of periods) {
		values.push({
				date: p.clone,
				tally: tally.get(p.YmdHis) || 0
		});
	}

	return values
			.filter((tv: TCommonsFixedDateTally): boolean => {
				if (tv.date.compareTo(clamp.from) === -1) return false;
				if (tv.date.compareTo(clamp.to) === 1) return false;

				return true;
			});
}

export function commonsDashboardsAutoComputeTimeGraph(
		aspect: 'in' | 'out',
		partialUnsanitiseds: Partial<TCommonsFixedDateRange>[],
		period: ECommonsTimeRangePeriod,
		quantise: CommonsFixedDuration,
		defaultDuration: CommonsFixedDuration,
		maxDuration: CommonsFixedDuration,
		clamp: TCommonsFixedDateRange,
		baseDate?: CommonsFixedDate
): TCommonsFixedDateTally[] {
	const sanitiseds: TCommonsFixedDateRange[] = commonsDashboardsPivotAndFillAndSanitisePartialDateRanges<TCommonsFixedDateRange>(
			partialUnsanitiseds,
			period,
			defaultDuration,
			maxDuration,
			baseDate
	);
	if (sanitiseds.length === 0) return [];

	const dates: CommonsFixedDate[] = sanitiseds
			.map((range: TCommonsFixedDateRange): CommonsFixedDate => {
				switch (aspect) {
					case 'in':
						return range.from.clone;
					case 'out':
						return range.to.clone;
				}
				throw new Error('Unknown aspect');
			});
	dates
			.forEach((date: CommonsFixedDate): void => {
				date.quantiseToSeconds(quantise.seconds, 'closest');
			});
	
	return commonsDashboardsComputeTimeGraph(
			dates,
			quantise,
			clamp
	);
}
