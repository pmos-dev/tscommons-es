import { CommonsFixedDate } from 'tscommons-es-core';

export type TCommonsFixedHeat = {
		date: CommonsFixedDate;
		heat: number;
		tally: number;
};
