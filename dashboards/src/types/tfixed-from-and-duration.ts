import { CommonsFixedDuration, TCommonsFixedDateRange } from 'tscommons-es-core';

export type TCommonsFixedFromAndDuration<T extends TCommonsFixedDateRange = TCommonsFixedDateRange> = Omit<T, 'to'> & {
		duration: CommonsFixedDuration;
};
