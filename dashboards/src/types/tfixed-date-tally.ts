import { CommonsFixedDate } from 'tscommons-es-core';

export type TCommonsFixedDateTally = {
		date: CommonsFixedDate;
		tally: number;
};
