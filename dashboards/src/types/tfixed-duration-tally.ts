import { CommonsFixedDuration } from 'tscommons-es-core';

export type TCommonsFixedDurationTally = {
		duration: CommonsFixedDuration;
		tally: number;
};
