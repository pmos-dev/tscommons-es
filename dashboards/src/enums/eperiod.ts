import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsTimeRangePeriod {
		DAY = 'day',
		NIGHT = 'night',
		FULL = 'full',
		EAST = 'east'
}

export function toECommonsTimeRangePeriod(type: string): ECommonsTimeRangePeriod|undefined {
	switch (type) {
		case ECommonsTimeRangePeriod.DAY.toString():
			return ECommonsTimeRangePeriod.DAY;
		case ECommonsTimeRangePeriod.NIGHT.toString():
			return ECommonsTimeRangePeriod.NIGHT;
		case ECommonsTimeRangePeriod.FULL.toString():
			return ECommonsTimeRangePeriod.FULL;
		case ECommonsTimeRangePeriod.EAST.toString():
			return ECommonsTimeRangePeriod.EAST;
	}
	return undefined;
}

export function fromECommonsTimeRangePeriod(type: ECommonsTimeRangePeriod): string {
	switch (type) {
		case ECommonsTimeRangePeriod.DAY:
			return ECommonsTimeRangePeriod.DAY.toString();
		case ECommonsTimeRangePeriod.NIGHT:
			return ECommonsTimeRangePeriod.NIGHT.toString();
		case ECommonsTimeRangePeriod.FULL:
			return ECommonsTimeRangePeriod.FULL.toString();
		case ECommonsTimeRangePeriod.EAST:
			return ECommonsTimeRangePeriod.EAST.toString();
	}
	
	throw new Error('Unknown ECommonsTimeRangePeriod');
}

export function isECommonsTimeRangePeriod(test: unknown): test is ECommonsTimeRangePeriod {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsTimeRangePeriod(test) !== undefined;
}

export function keyToECommonsTimeRangePeriod(key: string): ECommonsTimeRangePeriod {
	switch (key) {
		case 'DAY':
			return ECommonsTimeRangePeriod.DAY;
		case 'NIGHT':
			return ECommonsTimeRangePeriod.NIGHT;
		case 'FULL':
			return ECommonsTimeRangePeriod.FULL;
		case 'EAST':
			return ECommonsTimeRangePeriod.EAST;
	}
	
	throw new Error(`Unable to obtain ECommonsTimeRangePeriod for key: ${key}`);
}

export const EPERIODS: ECommonsTimeRangePeriod[] = Object.keys(ECommonsTimeRangePeriod)
		.map((e: string): ECommonsTimeRangePeriod => keyToECommonsTimeRangePeriod(e));
