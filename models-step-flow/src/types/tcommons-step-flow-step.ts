import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyString } from 'tscommons-es-core';
import { isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { ICommonsStepFlowStep } from '../interfaces/icommons-step-flow-step';

import { isECommonsStepFlowStepType, toECommonsStepFlowStepType, ECommonsStepFlowStepType } from '../enums/ecommons-step-flow-step-type';

export type TCommonsStepFlowStep = ICommonsStepFlowStep;

export function isTCommonsStepFlowStep(test: unknown): test is TCommonsStepFlowStep {
	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'type')) return false;
	if (!isECommonsStepFlowStepType(test['type'])) return false;
	
	return true;
}

export function encodeICommonsStepFlowStep(step: ICommonsStepFlowStep): TCommonsStepFlowStep {
	return {
			id: step.id,
			uid: step.uid,
			type: step.type
	};
}

export function decodeICommonsStepFlowStep(encoded: TCommonsStepFlowStep): ICommonsStepFlowStep {
	const type: ECommonsStepFlowStepType|undefined = toECommonsStepFlowStepType(encoded.type);
	if (!type) throw new Error('Type cannot be decoded in decodeICommonsStepFlowStep');

	return {
			id: encoded.id,
			uid: encoded.uid,
			type: type
	};
}
