import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsStepFlowStep } from '../interfaces/icommons-step-flow-step';
import { ICommonsStepFlowFlow } from '../interfaces/icommons-step-flow-flow';

export type TCommonsStepFlowFlow = Readonly<ICommonsStepFlowFlow<
		ICommonsStepFlowStep
>>;

export function isTCommonsStepFlowFlow(test: unknown): test is TCommonsStepFlowFlow {
	if (!commonsTypeHasPropertyNumber(test, 'step')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'ordered')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'outstep')) return false;
	
	return true;
}

export function encodeICommonsStepFlowFlow(flow: ICommonsStepFlowFlow<ICommonsStepFlowStep>): TCommonsStepFlowFlow {
	return {
			step: flow.step,
			id: flow.id,
			ordered: flow.ordered,
			outstep: flow.outstep
	};
}

export function decodeICommonsStepFlowFlow(encoded: TCommonsStepFlowFlow): ICommonsStepFlowFlow<ICommonsStepFlowStep> {
	return {
			step: encoded.step,
			id: encoded.id,
			ordered: encoded.ordered,
			outstep: encoded.outstep
	};
}
