import { ICommonsSecondClass, isICommonsSecondClass } from 'tscommons-es-models';

import { ICommonsStepFlowStep } from './icommons-step-flow-step';
import { ICommonsStepFlowFlow } from './icommons-step-flow-flow';

export interface ICommonsStepFlowCondition<
		F extends ICommonsStepFlowFlow<S>,
		S extends ICommonsStepFlowStep
> extends ICommonsSecondClass<F> {
		flow: number;
		
		// definition of things like condition values as strings/enums/etc should be done in subinterfaces
		// also ordering, as it may not be relevant for all subinterfaces, so this is a secondclass that could be extended to orientatedordered further down
}

export function isICommonsStepFlowCondition<
		F extends ICommonsStepFlowFlow<S>,
		S extends ICommonsStepFlowStep
>(test: unknown): test is ICommonsStepFlowCondition<F, S> {
	if (!isICommonsSecondClass<ICommonsStepFlowFlow<S>>(test, 'flow')) return false;

	return true;
}
