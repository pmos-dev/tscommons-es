import { commonsTypeHasPropertyEnum } from 'tscommons-es-core';
import { ICommonsFirstClass, isICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsUniquelyIdentifiedBase62, isICommonsUniquelyIdentifiedBase62 } from 'tscommons-es-models';

import { ECommonsStepFlowStepType, isECommonsStepFlowStepType } from '../enums/ecommons-step-flow-step-type';

export interface ICommonsStepFlowStep extends ICommonsFirstClass, ICommonsUniquelyIdentifiedBase62 {
		type: ECommonsStepFlowStepType;
}

export function isICommonsStepFlowStep(test: unknown): test is ICommonsStepFlowStep {
	if (!isICommonsFirstClass(test)) return false;
	if (!isICommonsUniquelyIdentifiedBase62(test)) return false;
	
	if (!commonsTypeHasPropertyEnum<ECommonsStepFlowStepType>(test, 'type', isECommonsStepFlowStepType)) return false;
	
	return true;
}
