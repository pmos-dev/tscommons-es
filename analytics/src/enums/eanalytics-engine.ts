import { commonsTypeIsString } from 'tscommons-es-core';

export enum EAnalyticsEngine {
		AMAYA = 'amaya',
		BLINK = 'blink',
		EDGEHTML = 'edgehtml',
		GECKO = 'gecko',
		GOANNA = 'goanna',
		ICAB = 'icab',
		KHTML = 'khtml',
		LINKS = 'links',
		LYNX = 'lynx',
		NETFRONT = 'netfront',
		NETSURF = 'netsurf',
		PRESTO = 'presto',
		TASMAN = 'tasman',
		TRIDENT = 'trident',
		W3M = 'w3m',
		WEBKIT = 'webkit'
}

export function fromEAnalyticsEngine(value: EAnalyticsEngine): string {
	switch (value) {
		case EAnalyticsEngine.AMAYA:
			return EAnalyticsEngine.AMAYA.toString();
		case EAnalyticsEngine.BLINK:
			return EAnalyticsEngine.BLINK.toString();
		case EAnalyticsEngine.EDGEHTML:
			return EAnalyticsEngine.EDGEHTML.toString();
		case EAnalyticsEngine.GECKO:
			return EAnalyticsEngine.GECKO.toString();
		case EAnalyticsEngine.GOANNA:
			return EAnalyticsEngine.GOANNA.toString();
		case EAnalyticsEngine.ICAB:
			return EAnalyticsEngine.ICAB.toString();
		case EAnalyticsEngine.KHTML:
			return EAnalyticsEngine.KHTML.toString();
		case EAnalyticsEngine.LINKS:
			return EAnalyticsEngine.LINKS.toString();
		case EAnalyticsEngine.LYNX:
			return EAnalyticsEngine.LYNX.toString();
		case EAnalyticsEngine.NETFRONT:
			return EAnalyticsEngine.NETFRONT.toString();
		case EAnalyticsEngine.NETSURF:
			return EAnalyticsEngine.NETSURF.toString();
		case EAnalyticsEngine.PRESTO:
			return EAnalyticsEngine.PRESTO.toString();
		case EAnalyticsEngine.TASMAN:
			return EAnalyticsEngine.TASMAN.toString();
		case EAnalyticsEngine.TRIDENT:
			return EAnalyticsEngine.TRIDENT.toString();
		case EAnalyticsEngine.W3M:
			return EAnalyticsEngine.W3M.toString();
		case EAnalyticsEngine.WEBKIT:
			return EAnalyticsEngine.WEBKIT.toString();
		default:
			throw new Error('Unknown EAnalyticsEngine');
	}
}

export function toEAnalyticsEngine(value: string): EAnalyticsEngine|undefined {
	switch (value.toLowerCase()) {
		case EAnalyticsEngine.AMAYA.toString():
			return EAnalyticsEngine.AMAYA;
		case EAnalyticsEngine.BLINK.toString():
			return EAnalyticsEngine.BLINK;
		case EAnalyticsEngine.EDGEHTML.toString():
			return EAnalyticsEngine.EDGEHTML;
		case EAnalyticsEngine.GECKO.toString():
			return EAnalyticsEngine.GECKO;
		case EAnalyticsEngine.GOANNA.toString():
			return EAnalyticsEngine.GOANNA;
		case EAnalyticsEngine.ICAB.toString():
			return EAnalyticsEngine.ICAB;
		case EAnalyticsEngine.KHTML.toString():
			return EAnalyticsEngine.KHTML;
		case EAnalyticsEngine.LINKS.toString():
			return EAnalyticsEngine.LINKS;
		case EAnalyticsEngine.LYNX.toString():
			return EAnalyticsEngine.LYNX;
		case EAnalyticsEngine.NETFRONT.toString():
			return EAnalyticsEngine.NETFRONT;
		case EAnalyticsEngine.NETSURF.toString():
			return EAnalyticsEngine.NETSURF;
		case EAnalyticsEngine.PRESTO.toString():
			return EAnalyticsEngine.PRESTO;
		case EAnalyticsEngine.TASMAN.toString():
			return EAnalyticsEngine.TASMAN;
		case EAnalyticsEngine.TRIDENT.toString():
			return EAnalyticsEngine.TRIDENT;
		case EAnalyticsEngine.W3M.toString():
			return EAnalyticsEngine.W3M;
		case EAnalyticsEngine.WEBKIT.toString():
			return EAnalyticsEngine.WEBKIT;
		default:
			return undefined;
	}
}

export function isEAnalyticsEngine(test: any): test is EAnalyticsEngine {
	if (!commonsTypeIsString(test)) return false;

	return toEAnalyticsEngine(test) !== undefined;
}
