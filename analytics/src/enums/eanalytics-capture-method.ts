import { commonsTypeIsString } from 'tscommons-es-core';

export enum EAnalyticsCaptureMethod {
		JPEG = 'jpeg',
		PNG = 'png',
		JSON = 'json'
}

export function toEAnalyticsCaptureMethod(type: string): EAnalyticsCaptureMethod|undefined {
	switch (type) {
		case EAnalyticsCaptureMethod.JPEG.toString():
			return EAnalyticsCaptureMethod.JPEG;
		case EAnalyticsCaptureMethod.PNG.toString():
			return EAnalyticsCaptureMethod.PNG;
		case EAnalyticsCaptureMethod.JSON.toString():
			return EAnalyticsCaptureMethod.JSON;
	}
	return undefined;
}

export function fromEAnalyticsCaptureMethod(type: EAnalyticsCaptureMethod): string {
	switch (type) {
		case EAnalyticsCaptureMethod.JPEG:
			return EAnalyticsCaptureMethod.JPEG.toString();
		case EAnalyticsCaptureMethod.PNG:
			return EAnalyticsCaptureMethod.PNG.toString();
		case EAnalyticsCaptureMethod.JSON:
			return EAnalyticsCaptureMethod.JSON.toString();
	}
	
	throw new Error('Unknown EAnalyticsCaptureMethod');
}

export function isEAnalyticsCaptureMethod(test: unknown): test is EAnalyticsCaptureMethod {
	if (!commonsTypeIsString(test)) return false;
	
	return toEAnalyticsCaptureMethod(test) !== undefined;
}

export function keyToEAnalyticsCaptureMethod(key: string): EAnalyticsCaptureMethod {
	switch (key) {
		case 'JPEG':
			return EAnalyticsCaptureMethod.JPEG;
		case 'PNG':
			return EAnalyticsCaptureMethod.PNG;
		case 'JSON':
			return EAnalyticsCaptureMethod.JSON;
	}
	
	throw new Error(`Unable to obtain EAnalyticsCaptureMethod for key: ${key}`);
}

export const EANALYTICS_CAPTURE_METHODS: EAnalyticsCaptureMethod[] = Object.keys(EAnalyticsCaptureMethod)
		.map((e: string): EAnalyticsCaptureMethod => keyToEAnalyticsCaptureMethod(e));
