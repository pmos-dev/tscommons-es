import {
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

import { EAnalyticsCpu, isEAnalyticsCpu } from '../enums/eanalytics-cpu';

export type TAnalyticsDeviceCpuHits = {
		cpu: EAnalyticsCpu;
		tally: number;
};

export function isTAnalyticsDeviceCpuHits(test: any): test is TAnalyticsDeviceCpuHits {
	if (!commonsTypeHasPropertyEnum<EAnalyticsCpu>(test, 'cpu', isEAnalyticsCpu)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
