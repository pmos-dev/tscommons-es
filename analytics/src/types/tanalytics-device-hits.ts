import {
		commonsTypeIsObject,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

export type TAnalyticsDeviceHits = {
		vendor: string|null;
		model: string|null;
		tally: number;
};

export function isTAnalyticsDeviceHits(test: unknown): test is TAnalyticsDeviceHits {
	if (!commonsTypeIsObject(test)) return false;	// needed for the !== null checks below
	
	if (!commonsTypeHasPropertyString(test, 'vendor') && test['vendor'] !== null) return false;
	if (!commonsTypeHasPropertyString(test, 'model') && test['model'] !== null) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	if (!test['vendor'] && !test['model']) return false;
	
	return true;
}
