import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

export type TAnalyticsGeoHitCountry = {
		country: string;
		tally: number;
};

export function isTAnalyticsGeoHitCountry(test: any): test is TAnalyticsGeoHitCountry {
	if (!commonsTypeHasPropertyString(test, 'country')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
