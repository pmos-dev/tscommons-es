import {
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

export type TAnalyticsGeoAggregate = {
		longitude: number;
		latitude: number;
		t: number;
};

export function isTAnalyticsGeoAggregate(test: any): test is TAnalyticsGeoAggregate {
	if (!commonsTypeHasPropertyNumber(test, 'longitude')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'latitude')) return false;
	if (!commonsTypeHasPropertyNumber(test, 't')) return false;
	
	return true;
}
