import {
		commonsTypeIsObject,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

export type TAnalyticsDeviceVersionedHits = {
		name: string;
		version: number|null;
		tally: number;
};

export function isTAnalyticsDeviceVersionedHits(test: unknown): test is TAnalyticsDeviceVersionedHits {
	if (!commonsTypeIsObject(test)) return false;	// needed for the !== null checks below
	
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'version') && test['version'] !== null) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
