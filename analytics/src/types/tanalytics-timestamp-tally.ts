import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyDate
} from 'tscommons-es-core';

export type TAnalyticsTimestampTally = {
		timestamp: Date;
		tally: number;
};

export function isTAnalyticsTimestampTally(test: any): test is TAnalyticsTimestampTally {
	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
