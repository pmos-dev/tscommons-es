import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

export type TAnalyticsGeoHitCity = {
		city: string;
		tally: number;
};

export function isTAnalyticsGeoHitCity(test: any): test is TAnalyticsGeoHitCity {
	if (!commonsTypeHasPropertyString(test, 'city')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
