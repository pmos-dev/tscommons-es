import {
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';

import { EAnalyticsDeviceType, isEAnalyticsDeviceType } from '../enums/eanalytics-device-type';

export type TAnalyticsDeviceTypeHits = {
		type: EAnalyticsDeviceType;
		tally: number;
};

export function isTAnalyticsDeviceTypeHits(test: any): test is TAnalyticsDeviceTypeHits {
	if (!commonsTypeHasPropertyEnum<EAnalyticsDeviceType>(test, 'type', isEAnalyticsDeviceType)) return false;
	if (!commonsTypeHasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
