import {
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyDateOrUndefined,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyTOrUndefined,
		commonsBase62HasPropertyId,
		commonsBase62HasPropertyIdOrUndefined
} from 'tscommons-es-core';

import { EAnalyticsCaptureMethod, isEAnalyticsCaptureMethod } from '../enums/eanalytics-capture-method';

import { IAnalyticsGeo, isIAnalyticsGeo } from './ianalytics-geo';
import { IAnalyticsDevice, isIAnalyticsDevice } from './ianalytics-device';

export interface IAnalyticsHit {
		timestamp: Date;
		day: Date;
		hour: Date;
		captureMethod: EAnalyticsCaptureMethod;
		uid: string;
		session?: string;
		unload?: Date;
		geo?: IAnalyticsGeo;
		device?: IAnalyticsDevice;
}

export function isIAnalyticsHit(test: any): test is IAnalyticsHit {
	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;
	if (!commonsTypeHasPropertyDate(test, 'day')) return false;
	if (!commonsTypeHasPropertyDate(test, 'hour')) return false;
	if (!commonsTypeHasPropertyEnum<EAnalyticsCaptureMethod>(test, 'captureMethod', isEAnalyticsCaptureMethod)) return false;
	if (!commonsBase62HasPropertyId(test, 'uid')) return false;
	if (!commonsBase62HasPropertyIdOrUndefined(test, 'session')) return false;
	if (!commonsTypeHasPropertyDateOrUndefined(test, 'unload')) return false;
	if (!commonsTypeHasPropertyTOrUndefined<IAnalyticsGeo>(test, 'geo', isIAnalyticsGeo)) return false;
	if (!commonsTypeHasPropertyTOrUndefined<IAnalyticsDevice>(test, 'device', isIAnalyticsDevice)) return false;

	return true;
}
