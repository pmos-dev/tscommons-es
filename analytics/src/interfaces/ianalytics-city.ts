import {
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyTOrUndefined
} from 'tscommons-es-core';
import { ICommonsGeographic, isICommonsGeographic } from 'tscommons-es-geographics';

export interface IAnalyticsCity {
		city?: string;
		country?: string;
		continent?: string;
		postCode?: string;
		location?: ICommonsGeographic;
}

export function isIAnalyticsCity(test: any): test is IAnalyticsCity {
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'city')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'country')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'continent')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'postCode')) return false;
	if (!commonsTypeHasPropertyTOrUndefined<ICommonsGeographic>(test, 'location', isICommonsGeographic)) return false;
	
	return true;
}
