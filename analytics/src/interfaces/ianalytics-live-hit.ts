import {
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyT
} from 'tscommons-es-core';

import { IAnalyticsGeo, isIAnalyticsGeo } from './ianalytics-geo';

export interface IAnalyticsLiveHit {
		timestamp: Date;
		session?: string;
		geo?: IAnalyticsGeo;
}

export function isIAnalyticsLiveHit(test: any): test is IAnalyticsLiveHit {
	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'session')) return false;
	if (!commonsTypeHasPropertyT<IAnalyticsGeo>(test, 'geo', isIAnalyticsGeo)) return false;
	
	return true;
}
