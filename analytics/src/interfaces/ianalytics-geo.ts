import { IAnalyticsCity, isIAnalyticsCity } from './ianalytics-city';
import { IAnalyticsCountry, isIAnalyticsCountry } from './ianalytics-country';

export interface IAnalyticsGeo extends IAnalyticsCountry, IAnalyticsCity {}

export function isIAnalyticsGeo(test: any): test is IAnalyticsGeo {
	return isIAnalyticsCity(test) || isIAnalyticsCountry(test);
}
