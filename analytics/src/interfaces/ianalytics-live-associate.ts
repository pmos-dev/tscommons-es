import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export interface IAnalyticsLiveAssociate {
		uid: string;
		session: string;
}

export function isIAnalyticsLiveAssociate(test: any): test is IAnalyticsLiveAssociate {
	if (!commonsTypeHasPropertyString(test, 'uid')) return false;
	if (!commonsTypeHasPropertyString(test, 'session')) return false;
	
	return true;
}
