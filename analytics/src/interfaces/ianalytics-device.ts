import {
		commonsTypeIsObject,
		commonsTypeHasPropertyEnumOrUndefined,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

import { EAnalyticsDeviceType, isEAnalyticsDeviceType } from '../enums/eanalytics-device-type';
import { EAnalyticsCpu, isEAnalyticsCpu } from '../enums/eanalytics-cpu';
import { EAnalyticsEngine, isEAnalyticsEngine } from '../enums/eanalytics-engine';

export interface IAnalyticsDevice {
		type?: EAnalyticsDeviceType;
		cpu?: EAnalyticsCpu;
		vendor?: string;
		model?: string;
		os?: string;
		osVersion?: number;
		engine?: EAnalyticsEngine;
		engineVersion?: number;
		browser?: string;
		browserVersion?: number;
}

export function isIAnalyticsDevice(test: any): test is IAnalyticsDevice {
	if (!commonsTypeIsObject(test)) return false;
	
	if (!commonsTypeHasPropertyEnumOrUndefined<EAnalyticsDeviceType>(test, 'type', isEAnalyticsDeviceType)) return false;
	if (!commonsTypeHasPropertyEnumOrUndefined<EAnalyticsCpu>(test, 'cpu', isEAnalyticsCpu)) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'vendor')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'model')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'os')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'osVersion')) return false;
	if (!commonsTypeHasPropertyEnumOrUndefined<EAnalyticsEngine>(test, 'engine', isEAnalyticsEngine)) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'engineVersion')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'browser')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'browserVersion')) return false;

	return true;
}
