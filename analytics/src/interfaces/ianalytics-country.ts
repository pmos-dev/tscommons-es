import { commonsTypeHasPropertyStringOrUndefined } from 'tscommons-es-core';

export interface IAnalyticsCountry {
		country?: string;
		continent?: string;
}

export function isIAnalyticsCountry(test: any): test is IAnalyticsCountry {
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'country')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'continent')) return false;
	
	return true;
}
