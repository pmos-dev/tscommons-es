import {
		commonsTypeIsEncodedObject,
		commonsTypeIsObjectArray,
		commonsTypeDecodePropertyObject,
		commonsTypeIsNumber,
		commonsTypeIsTArray,
		TPropertyObject,
		commonsDateDateToYmdHis,
		TDateRange,
		TEmptyObject
} from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { IAnalyticsHit } from '../interfaces/ianalytics-hit';

import { TAnalyticsTimestampTally, isTAnalyticsTimestampTally } from '../types/tanalytics-timestamp-tally';
import { TAnalyticsGeoAggregate, isTAnalyticsGeoAggregate } from '../types/tanalytics-geo-aggregate';
import { TAnalyticsGeoHitCity, isTAnalyticsGeoHitCity } from '../types/tanalytics-geo-hit-city';
import { TAnalyticsGeoHitCountry, isTAnalyticsGeoHitCountry } from '../types/tanalytics-geo-hit-country';
import { TAnalyticsDeviceTypeHits, isTAnalyticsDeviceTypeHits } from '../types/tanalytics-device-type-hits';
import { TAnalyticsDeviceCpuHits, isTAnalyticsDeviceCpuHits } from '../types/tanalytics-device-cpu-hits';
import { TAnalyticsDeviceHits, isTAnalyticsDeviceHits } from '../types/tanalytics-device-hits';
import { TAnalyticsDeviceVersionedHits, isTAnalyticsDeviceVersionedHits } from '../types/tanalytics-device-versioned-hits';

type TWithRange = {
		from: string;
		to: string;
};
type TOptionalRange = TEmptyObject | TWithRange;

type TGeoParams = { limit: number };
type TGeoParamsWithRange = TGeoParams & TWithRange;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export class AnalyticsRestClientService<_H extends IAnalyticsHit> extends CommonsRestClientService {
	protected static buildOptionalRange(range?: TDateRange): TOptionalRange {
		if (!range) return {};
		
		return {
				from: commonsDateDateToYmdHis(range.from),
				to: commonsDateDateToYmdHis(range.to)
		};
	}
	
	protected static buildGeoParams(
			limit: number,
			range?: TDateRange
	): TGeoParams | TGeoParamsWithRange {
		return {
				limit: limit,
				...AnalyticsRestClientService.buildOptionalRange(range)
		};
	}
	
	// this has to be public as it is used elsewhere
	public static parseTimestampTally(rest: unknown): TAnalyticsTimestampTally {
		if (!commonsTypeIsEncodedObject(rest)) throw new Error('Unable to parse course');
		const object: TPropertyObject = commonsTypeDecodePropertyObject(rest);
		
		if (!isTAnalyticsTimestampTally(object)) throw new Error('Object is not an instance of TAnalyticsTimestampTally');

		return object;
	}

	public async getTotalHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number> {
		const result: unknown = await this.getRest<
				number,
				TOptionalRange
		>(
				'/statistics/hits/total',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsNumber(result)) throw new Error('Returned data from getInternalHits is not a number');

		return result;
	}

	public async getUniqueHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number> {
		const result: unknown = await this.getRest<
				number,
				TOptionalRange
		>(
				'/statistics/hits/unique',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsNumber(result)) throw new Error('Returned data from getInternalHits is not a number');

		return result;
	}

	public async getUnknownHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number> {
		const result: unknown = await this.getRest<
				number,
				TOptionalRange
		>(
				'/statistics/hits/unknown',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsNumber(result)) throw new Error('Returned data from getInternalHits is not a number');

		return result;
	}
	
	public async listHourlyTotalHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsTimestampTally[]> {
		const result: unknown = await this.getRest<
				number,
				TOptionalRange
		>(
				'/statistics/hits/hourly/total',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(result)) throw new Error('Returned data from listHourlyTotalHits is not an array');
		
		const typecast: TAnalyticsTimestampTally[] = result
				.map((object: TPropertyObject): TAnalyticsTimestampTally => AnalyticsRestClientService.parseTimestampTally(object));

		return typecast;
	}
	
	public async listDailyTotalHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsTimestampTally[]> {
		const result: unknown = await this.getRest<
				TPropertyObject,
				TOptionalRange
		>(
				'/statistics/hits/daily/total',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(result)) throw new Error('Returned data from listDailyInternalHits is not an array');

		const typecast: TAnalyticsTimestampTally[] = result
				.map((object: TPropertyObject): TAnalyticsTimestampTally => AnalyticsRestClientService.parseTimestampTally(object));

		return typecast;
	}
	
	public async listHourlyUniqueHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsTimestampTally[]> {
		const result: unknown = await this.getRest<
				TPropertyObject,
				TOptionalRange
		>(
				'/statistics/hits/hourly/unique',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(result)) throw new Error('Returned data from listDailyInternalHits is not an array');

		const typecast: TAnalyticsTimestampTally[] = result
				.map((object: TPropertyObject): TAnalyticsTimestampTally => AnalyticsRestClientService.parseTimestampTally(object));

		return typecast;
	}
	
	public async listDailyUniqueHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsTimestampTally[]> {
		const result: unknown = await this.getRest<
				TPropertyObject,
				TOptionalRange
		>(
				'/statistics/hits/daily/unique',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(result)) throw new Error('Returned data from listDailyInternalHits is not an array');

		const typecast: TAnalyticsTimestampTally[] = result
				.map((object: TPropertyObject): TAnalyticsTimestampTally => AnalyticsRestClientService.parseTimestampTally(object));

		return typecast;
	}
	
	public async listHourlyUnknownHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsTimestampTally[]> {
		const result: unknown = await this.getRest<
				TPropertyObject,
				TOptionalRange
		>(
				'/statistics/hits/hourly/unknown',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(result)) throw new Error('Returned data from listDailyInternalHits is not an array');

		const typecast: TAnalyticsTimestampTally[] = result
				.map((object: TPropertyObject): TAnalyticsTimestampTally => AnalyticsRestClientService.parseTimestampTally(object));

		return typecast;
	}
	
	public async listDailyUnknownHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsTimestampTally[]> {
		const result: unknown = await this.getRest<
				TPropertyObject,
				TOptionalRange
		>(
				'/statistics/hits/daily/unknown',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(result)) throw new Error('Returned data from listDailyInternalHits is not an array');

		const typecast: TAnalyticsTimestampTally[] = result
				.map((object: TPropertyObject): TAnalyticsTimestampTally => AnalyticsRestClientService.parseTimestampTally(object));

		return typecast;
	}
	
	//----------------------------------------
	
	public async listGeoAggregatesUk(
			granularity: number,
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsGeoAggregate[]> {
		const result: unknown = await this.getRest<
				TAnalyticsGeoAggregate[],
				TOptionalRange
		>(
				`/statistics/geo/map/uk/${granularity}`,
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsGeoAggregate>(result, isTAnalyticsGeoAggregate)) throw new Error('Returned data is not an array');

		return result;
	}
	
	public async listGeoAggregatesWorld(
			granularity: number,
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsGeoAggregate[]> {
		const result: unknown = await this.getRest<
				TAnalyticsGeoAggregate[],
				TOptionalRange
		>(
				`/statistics/geo/map/world/${granularity}`,
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsGeoAggregate>(result, isTAnalyticsGeoAggregate)) throw new Error('Returned data is not an array');

		return result;
	}
	
	//----------------------------------------
	
	public async listGeoHitsCityUk(
			limit: number,
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsGeoHitCity[]> {
		const result: unknown = await this.getRest<TAnalyticsGeoHitCity[]>(
				'/statistics/geo/hits/city/uk',
				AnalyticsRestClientService.buildGeoParams(limit, range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsGeoHitCity>(result, isTAnalyticsGeoHitCity)) throw new Error('Returned data is not an array');

		return result;
	}
	
	public async listGeoHitsCityWorld(
			limit: number,
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsGeoHitCity[]> {
		const result: unknown = await this.getRest<TAnalyticsGeoHitCity[]>(
				'/statistics/geo/hits/city/world',
				AnalyticsRestClientService.buildGeoParams(limit, range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsGeoHitCity>(result, isTAnalyticsGeoHitCity)) throw new Error('Returned data is not an array');

		return result;
	}
	
	public async listGeoHitsCountryUk(
			limit: number,
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsGeoHitCountry[]> {
		const result: unknown = await this.getRest<TAnalyticsGeoHitCountry[]>(
				'/statistics/geo/hits/country/uk',
				AnalyticsRestClientService.buildGeoParams(limit, range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsGeoHitCountry>(result, isTAnalyticsGeoHitCountry)) throw new Error('Returned data is not an array');

		return result;
	}
	
	public async listGeoHitsCountryWorld(
			limit: number,
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsGeoHitCountry[]> {
		const result: unknown = await this.getRest<TAnalyticsGeoHitCountry[]>(
				'/statistics/geo/hits/country/world',
				AnalyticsRestClientService.buildGeoParams(limit, range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsGeoHitCountry>(result, isTAnalyticsGeoHitCountry)) throw new Error('Returned data is not an array');

		return result;
	}
	
	//----------------------------------------
	
	public async listDeviceTypeHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsDeviceTypeHits[]> {
		const result: unknown = await this.getRest<
				TAnalyticsDeviceTypeHits[],
				TOptionalRange
		>(
				'/statistics/devices/types',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsDeviceTypeHits>(result, isTAnalyticsDeviceTypeHits)) throw new Error('Returned data is not an array');

		return result;
		//return commonsObjectStripNulls(commonsTypeDecode(result));
	}
	
	public async listDeviceCpuHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsDeviceCpuHits[]> {
		const result: unknown = await this.getRest<
				TAnalyticsDeviceCpuHits[],
				TOptionalRange
		>(
				'/statistics/devices/cpus',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsDeviceCpuHits>(result, isTAnalyticsDeviceCpuHits)) throw new Error('Returned data is not an array');

		return result;
		//return commonsObjectStripNulls(commonsTypeDecode(result));
	}
	
	public async listDeviceHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsDeviceHits[]> {
		const result: unknown = await this.getRest<
				TAnalyticsDeviceHits[],
				TOptionalRange
		>(
				'/statistics/devices/devices',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsDeviceHits>(result, isTAnalyticsDeviceHits)) throw new Error('Returned data is not an array');
		
		return result;
		//return commonsObjectStripNulls(commonsTypeDecode(result));
	}
	
	public async listDeviceBrowserHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsDeviceVersionedHits[]> {
		const result: unknown = await this.getRest<
				TAnalyticsDeviceVersionedHits[],
				TOptionalRange
		>(
				'/statistics/devices/browsers',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsDeviceVersionedHits>(result, isTAnalyticsDeviceVersionedHits)) throw new Error('Returned data is not an array');
		
		return result;
		//return commonsObjectStripNulls(commonsTypeDecode(result));
	}
	
	public async listDeviceOsHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsDeviceVersionedHits[]> {
		const result: unknown = await this.getRest<
				TAnalyticsDeviceVersionedHits[],
				TOptionalRange
		>(
				'/statistics/devices/oss',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsDeviceVersionedHits>(result, isTAnalyticsDeviceVersionedHits)) throw new Error('Returned data is not an array');
		
		return result;
		//return commonsObjectStripNulls(commonsTypeDecode(result));
	}
	
	public async listDeviceEngineHits(
			range?: TDateRange,
			options: TCommonsHttpRequestOptions = {}
	): Promise<TAnalyticsDeviceVersionedHits[]> {
		const result: unknown = await this.getRest<
				TAnalyticsDeviceVersionedHits[],
				TOptionalRange
		>(
				'/statistics/devices/engines',
				AnalyticsRestClientService.buildOptionalRange(range),
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TAnalyticsDeviceVersionedHits>(result, isTAnalyticsDeviceVersionedHits)) throw new Error('Returned data is not an array');
		
		return result;
		//return commonsObjectStripNulls(commonsTypeDecode(result));
	}

	public async xffPatchIp(
			uid: string,
			ip: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		await this.patchRest(
				`/xff-patch/${uid}`,
				{
						ip: ip
				},
				undefined,
				undefined,
				options
		);
	}
}
