import { CommonsFixedDate } from '../classes/commons-fixed-date';
import { CommonsFixedDuration } from '../classes/commons-fixed-duration';

import { TDefined } from '../types/tdefined';
import { TPrimative } from '../types/tprimative';
import { TPropertyObject } from '../types/tproperty-object';
import { TEncoded, TEncodedArray, TEncodedObject } from '../types/tencoded';
import { TKeyObject } from '../types/tkey-object';
import { TEnumObject } from '../types/tenum-object';

import { COMMONS_REGEX_PATTERN_DATE_ECMA } from '../consts/commons-regex';

import {
		commonsDateIsYmdHis,
		commonsDateIsdmYHi,
		commonsDateIsYmd,
		commonsDateIsdmY,
		commonsDateIsHis,
		commonsDateIsHi,
		commonsDateYmdHisToDate,
		commonsDatedmYHiToDate,
		commonsDateYmdToDate,
		commonsDatedmYToDate,
		commonsDateHisToDate,
		commonsDateHiToDate,
		commonsDateDateToYmdHis
} from './commons-date';
import {
		commonsFixedDurationIsHisMs,
		commonsFixedDurationIsdHis,
		commonsFixedDurationIsdHisMs
} from './commons-fixed-duration';

export enum ECommonsVariableType {
		STRING = 'string',
		NUMBER = 'number',
		BOOLEAN = 'boolean',
		OBJECT = 'object',
		DATE = 'date',
		FIXEDDATE = 'fixeddate',
		FIXEDDURATION = 'fixedduration'
}

export function commonsTypeIsDefined(test: unknown): test is TDefined {
	return test !== undefined;
}

export function commonsTypeAssertDefined(test: unknown): TDefined {
	if (!commonsTypeIsDefined(test)) throw new Error('Assertion fail: variable is undefined');
	return test;
}

export function commonsTypeIsPrimative(test: unknown): test is TPrimative {
	if (!commonsTypeIsDefined(test)) return false;

	if (test === null) return false;
	
	switch (typeof test) {
		case 'string':
		case 'number':
		case 'boolean':
			return true;
		default:
			return false;
	}
}

export function commonsTypeAssertPrimative(test: unknown): TPrimative {
	if (!commonsTypeIsPrimative(test)) throw new Error('Assertion fail: variable is not a primative');
	return test;
}

export function commonsTypeIsString(test: unknown): test is string {
	return commonsTypeIsPrimative(test) && 'string' === typeof test;
}

export function commonsTypeAssertString(test: unknown): string {
	if (!commonsTypeIsString(test)) throw new Error('Assertion fail: variable is not a string');
	return test;
}

export function commonsTypeIsNumber(test: unknown): test is number {
	return commonsTypeIsPrimative(test) && 'number' === typeof test;
}

export function commonsTypeAssertNumber(test: unknown): number {
	if (!commonsTypeIsNumber(test)) throw new Error('Assertion fail: variable is not a number');
	return test;
}

export function commonsTypeIsBoolean(test: unknown): test is boolean {
	return commonsTypeIsPrimative(test) && 'boolean' === typeof test;
}

export function commonsTypeAssertBoolean(test: unknown): boolean {
	if (!commonsTypeIsBoolean(test)) throw new Error('Assertion fail: variable is not a boolean');
	return test;
}

export function commonsTypeIsObject(test: unknown): test is { [ key: string ]: unknown } {
	if (!commonsTypeIsDefined(test)) return false;

	if (test === null) return false;

	return 'object' === typeof test;
}

export function commonsTypeAssertObject(test: unknown): { [ key: string ]: unknown } {
	if (!commonsTypeIsObject(test)) throw new Error('Assertion fail: variable is not an object');
	return test;
}

export function commonsTypeIsDate(test: unknown): test is Date {
	return commonsTypeIsObject(test) && test instanceof Date;
}

export function commonsTypeIsFixedDate(test: unknown): test is CommonsFixedDate {
	return CommonsFixedDate.is(test);
}

export function commonsTypeIsFixedDuration(test: unknown): test is CommonsFixedDuration {
	return CommonsFixedDuration.is(test);
}

export function commonsTypeAssertDate(test: unknown): Date {
	if (!commonsTypeIsDate(test)) throw new Error('Assertion fail: variable is not a Date object');
	return test;
}

export function commonsTypeAssertFixedDate(test: unknown): CommonsFixedDate {
	if (!commonsTypeIsFixedDate(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDate object');
	return test;
}

export function commonsTypeAssertFixedDuration(test: unknown): CommonsFixedDuration {
	if (!commonsTypeIsFixedDuration(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration object');
	return test;
}

export function commonsTypeIsError(test: unknown): test is Error {
	return commonsTypeIsObject(test) && test instanceof Error;
}

export function commonsTypeAssertError(test: unknown): Error {
	if (!commonsTypeIsError(test)) throw new Error('Assertion fail: variable is not an Error object');
	return test;
}

export function commonsTypeIsArray(test: unknown): test is unknown[] {
	return commonsTypeIsObject(test) && Array.isArray(test);
}

export function commonsTypeAssertArray(test: unknown): unknown[] {
	if (!commonsTypeIsArray(test)) throw new Error('Assertion fail: variable is not an array');
	return test;
}

function validateTKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T> {
	if (!commonsTypeIsObject(test)) return false;
	
	for (const key of Object.keys(test)) {
		if (!checker(test[key])) return false;
	}
	
	return true;
}

function validateTOrUndefinedKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T|undefined> {
	if (!commonsTypeIsObject(test)) return false;
	
	for (const key of Object.keys(test)) {
		// NB can't use hasPropertyTOrUndefined, as that requires T to be an object, and we also use this method for primatives
		if (!commonsTypeHasProperty(test, key) || test[key] === undefined) continue;	// eslint-disable-line @typescript-eslint/no-use-before-define

		if (!checker(test[key])) return false;
	}
	
	return true;
}

function validateTEnumObject<E extends string, T>(
		test: unknown,
		valueChecker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, T> {
	if (!commonsTypeIsObject(test)) return false;
	
	for (const key of Object.keys(test)) {
		if (!keyChecker(key)) return false;
		if (!valueChecker(test[key as string])) return false;
	}
	
	return true;
}

function validateTOrUndefinedEnumObject<E extends string, T>(
		test: unknown,
		valueChecker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, T|undefined> {
	if (!commonsTypeIsObject(test)) return false;
	
	for (const key of Object.keys(test)) {
		if (!keyChecker(key)) return false;

		// NB can't use hasPropertyTOrUndefined, as that requires T to be an object, and we also use this method for primatives
		if (!commonsTypeHasProperty(test, key) || test[key] === undefined) continue;	// eslint-disable-line @typescript-eslint/no-use-before-define

		if (!valueChecker(test[key as string])) return false;
	}
	
	return true;
}

export function commonsTypeIsDefinedArray(test: unknown): test is TDefined[] {
	if (!commonsTypeIsArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsDefined(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertDefinedArray(test: unknown): TDefined[] {
	if (!commonsTypeIsDefinedArray(test)) throw new Error('Assertion fail: variable is not a defined array');
	return test;
}

export function commonsTypeIsDefinedKeyObject(test: unknown): test is TKeyObject<TDefined> {
	if (!commonsTypeIsObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (!commonsTypeIsDefined(test[key])) return false;
	}

	return true;
}

export function commonsTypeAssertDefinedKeyObject(test: unknown): TKeyObject<TDefined> {
	if (!commonsTypeIsDefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a defined key object');
	return test;
}

export function commonsTypeIsDefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, TDefined> {
	if (!commonsTypeIsObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (!keyChecker(key)) return false;
		if (!commonsTypeIsDefined(test[key as string])) return false;
	}

	return true;
}

export function commonsTypeAssertDefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, TDefined> {
	if (!commonsTypeIsDefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a defined enum object');
	
	return test;
}

export function commonsTypeIsPrimativeArray(test: unknown): test is TPrimative[] {
	if (!commonsTypeIsDefinedArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsPrimative(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertPrimativeArray(test: unknown): TPrimative[] {
	if (!commonsTypeIsPrimativeArray(test)) throw new Error('Assertion fail: variable is not a primative array');
	return test;
}

export function commonsTypeIsPrimativeKeyObject(test: unknown): test is TKeyObject<TPrimative> {
	if (!commonsTypeIsObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (!commonsTypeIsPrimative(test[key])) return false;
	}

	return true;
}

export function commonsTypeAssertPrimativeKeyObject(test: unknown): TKeyObject<TPrimative> {
	if (!commonsTypeIsPrimativeKeyObject(test)) throw new Error('Assertion fail: variable is not a primative key object');
	return test;
}

export function commonsTypeIsPrimativeEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, TPrimative> {
	if (!commonsTypeIsObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (!keyChecker(key)) return false;
		if (!commonsTypeIsPrimative(test[key as string])) return false;
	}

	return true;
}

export function commonsTypeAssertPrimativeEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, TPrimative> {
	if (!commonsTypeIsPrimativeEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a primative enum object');
	
	return test;
}

export function commonsTypeIsStringArray(test: unknown): test is string[] {
	if (!commonsTypeIsPrimativeArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsString(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertStringArray(test: unknown): string[] {
	if (!commonsTypeIsStringArray(test)) throw new Error('Assertion fail: variable is not a string array');
	return test;
}

export function commonsTypeIsStringKeyObject(test: unknown): test is TKeyObject<string> {
	return validateTKeyObject<string>(test, commonsTypeIsString);
}

export function commonsTypeAssertStringKeyObject(test: unknown): TKeyObject<string> {
	if (!commonsTypeIsStringKeyObject(test)) throw new Error('Assertion fail: variable is not a string key object');
	return test;
}

export function commonsTypeIsStringEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, string> {
	return validateTEnumObject<E, string>(
			test,
			commonsTypeIsString,
			keyChecker
	);
}

export function commonsTypeAssertStringEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, string> {
	if (!commonsTypeIsStringEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a string enum object');
	
	return test;
}

export function commonsTypeIsStringOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
	return validateTOrUndefinedKeyObject<string>(test, commonsTypeIsString);
}

export function commonsTypeAssertStringOrUndefinedKeyObject(test: unknown): TKeyObject<string|undefined> {
	if (!commonsTypeIsStringOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a string or undefined key object');
	return test;
}

export function commonsTypeIsStringOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, string|undefined> {
	return validateTOrUndefinedEnumObject<E, string>(
			test,
			commonsTypeIsString,
			keyChecker
	);
}

export function commonsTypeAssertStringOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, string|undefined> {
	if (!commonsTypeIsStringOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a string or undefined key object');
	
	return test;
}

export function commonsTypeIsStringArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
	return validateTKeyObject<string[]>(test, commonsTypeIsStringArray);
}

export function commonsTypeAssertStringArrayKeyObject(test: unknown): TKeyObject<string[]> {
	if (!commonsTypeIsStringArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a string array key object');
	return test;
}

export function commonsTypeIsStringArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, string[]> {
	return validateTEnumObject<E, string[]>(
			test,
			commonsTypeIsStringArray,
			keyChecker
	);
}

export function commonsTypeAssertStringArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, string[]> {
	if (!commonsTypeIsStringArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a string array key object');
	
	return test;
}

export function commonsTypeIsNumberArray(test: unknown): test is number[] {
	if (!commonsTypeIsPrimativeArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsNumber(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertNumberArray(test: unknown): number[] {
	if (!commonsTypeIsNumberArray(test)) throw new Error('Assertion fail: variable is not a number array');
	return test;
}

export function commonsTypeIsNumberKeyObject(test: unknown): test is TKeyObject<number> {
	return validateTKeyObject<number>(test, commonsTypeIsNumber);
}

export function commonsTypeAssertNumberKeyObject(test: unknown): TKeyObject<number> {
	if (!commonsTypeIsNumberKeyObject(test)) throw new Error('Assertion fail: variable is not a number key object');
	return test;
}

export function commonsTypeIsNumberEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, number> {
	return validateTEnumObject<E, number>(
			test,
			commonsTypeIsNumber,
			keyChecker
	);
}

export function commonsTypeAssertNumberEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, number> {
	if (!commonsTypeIsNumberEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a number key object');
	return test;
}

export function commonsTypeIsNumberOrUndefinedKeyObject(test: unknown): test is TKeyObject<number|undefined> {
	return validateTOrUndefinedKeyObject<number>(test, commonsTypeIsNumber);
}

export function commonsTypeAssertNumberOrUndefinedKeyObject(test: unknown): TKeyObject<number|undefined> {
	if (!commonsTypeIsNumberOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a number or undefined key object');
	return test;
}

export function commonsTypeIsNumberOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, number|undefined> {
	return validateTOrUndefinedEnumObject<E, number>(
			test,
			commonsTypeIsNumber,
			keyChecker
	);
}

export function commonsTypeAssertNumberOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, number|undefined> {
	if (!commonsTypeIsNumberOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a number or undefined key object');
	
	return test;
}

export function commonsTypeIsNumberArrayKeyObject(test: unknown): test is TKeyObject<number[]> {
	return validateTKeyObject<number[]>(test, commonsTypeIsNumberArray);
}

export function commonsTypeAssertNumberArrayKeyObject(test: unknown): TKeyObject<number[]> {
	if (!commonsTypeIsNumberArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a number array key object');
	return test;
}

export function commonsTypeIsNumberArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, number[]> {
	return validateTEnumObject<E, number[]>(
			test,
			commonsTypeIsNumberArray,
			keyChecker
	);
}

export function commonsTypeAssertNumberArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, number[]> {
	if (!commonsTypeIsNumberArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a number array key object');
	
	return test;
}

export function commonsTypeIsBooleanArray(test: unknown): test is boolean[] {
	if (!commonsTypeIsPrimativeArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsBoolean(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertBooleanArray(test: unknown): boolean[] {
	if (!commonsTypeIsBooleanArray(test)) throw new Error('Assertion fail: variable is not a boolean array');
	return test;
}

export function commonsTypeIsBooleanKeyObject(test: unknown): test is TKeyObject<boolean> {
	return validateTKeyObject<boolean>(test, commonsTypeIsBoolean);
}

export function commonsTypeAssertBooleanKeyObject(test: unknown): TKeyObject<boolean> {
	if (!commonsTypeIsBooleanKeyObject(test)) throw new Error('Assertion fail: variable is not a boolean key object');
	return test;
}

export function commonsTypeIsBooleanEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, boolean> {
	return validateTEnumObject<E, boolean>(
			test,
			commonsTypeIsBoolean,
			keyChecker
	);
}

export function commonsTypeAssertBooleanEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, boolean> {
	if (!commonsTypeIsBooleanEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a boolean key object');
	
	return test;
}

export function commonsTypeIsBooleanOrUndefinedKeyObject(test: unknown): test is TKeyObject<boolean|undefined> {
	return validateTOrUndefinedKeyObject<boolean>(test, commonsTypeIsBoolean);
}

export function commonsTypeAssertBooleanOrUndefinedKeyObject(test: unknown): TKeyObject<boolean|undefined> {
	if (!commonsTypeIsBooleanOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a boolean or undefined key object');
	return test;
}

export function commonsTypeIsBooleanOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, boolean|undefined> {
	return validateTOrUndefinedEnumObject<E, boolean>(
			test,
			commonsTypeIsBoolean,
			keyChecker
	);
}

export function commonsTypeAssertBooleanOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, boolean|undefined> {
	if (!commonsTypeIsBooleanOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a boolean or undefined key object');
	
	return test;
}

export function commonsTypeIsBooleanArrayKeyObject(test: unknown): test is TKeyObject<boolean[]> {
	return validateTKeyObject<boolean[]>(test, commonsTypeIsBooleanArray);
}

export function commonsTypeAssertBooleanArrayKeyObject(test: unknown): TKeyObject<boolean[]> {
	if (!commonsTypeIsBooleanArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a boolean array key object');
	return test;
}

export function commonsTypeIsBooleanArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, boolean[]> {
	return validateTEnumObject<E, boolean[]>(
			test,
			commonsTypeIsBooleanArray,
			keyChecker
	);
}

export function commonsTypeAssertBooleanArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, boolean[]> {
	if (!commonsTypeIsBooleanArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a boolean array key object');
	
	return test;
}

export function commonsTypeIsObjectArray(test: unknown): test is { [ key: string ]: unknown }[] {
	if (!commonsTypeIsDefinedArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsObject(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertObjectArray(test: unknown): { [ key: string ]: unknown }[] {
	if (!commonsTypeIsObjectArray(test)) throw new Error('Assertion fail: variable is not an object array');
	return test;
}

export function commonsTypeIsObjectKeyObject(test: unknown): test is TKeyObject<{ [ key: string ]: unknown }> {
	return validateTKeyObject<{ [ key: string ]: unknown }>(test, commonsTypeIsObject);
}

export function commonsTypeAssertObjectKeyObject(test: unknown): TKeyObject<{ [ key: string ]: unknown }> {
	if (!commonsTypeIsObjectKeyObject(test)) throw new Error('Assertion fail: variable is not a object key object');
	return test;
}

export function commonsTypeIsObjectEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, { [ key: string ]: unknown }> {
	return validateTEnumObject<E, { [ key: string ]: unknown }>(
			test,
			commonsTypeIsObject,
			keyChecker
	);
}

export function commonsTypeAssertObjectEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, { [ key: string ]: unknown }> {
	if (!commonsTypeIsObjectEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a object key object');
	
	return test;
}

export function commonsTypeIsObjectOrUndefinedKeyObject(test: unknown): test is TKeyObject<{ [ key: string ]: unknown }|undefined> {
	return validateTOrUndefinedKeyObject<{ [ key: string ]: unknown }>(test, commonsTypeIsObject);
}

export function commonsTypeAssertObjectOrUndefinedKeyObject(test: unknown): TKeyObject<{ [ key: string ]: unknown }|undefined> {
	if (!commonsTypeIsObjectOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a object or undefined key object');
	return test;
}

export function commonsTypeIsObjectOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, { [ key: string ]: unknown }|undefined> {
	return validateTOrUndefinedEnumObject<E, { [ key: string ]: unknown }>(
			test,
			commonsTypeIsObject,
			keyChecker
	);
}

export function commonsTypeAssertObjectOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, { [ key: string ]: unknown }|undefined> {
	if (!commonsTypeIsObjectOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a object or undefined key object');
	
	return test;
}

export function commonsTypeIsObjectArrayKeyObject(test: unknown): test is TKeyObject<{ [ key: string ]: unknown }[]> {
	return validateTKeyObject<{ [ key: string ]: unknown }[]>(test, commonsTypeIsObjectArray);
}

export function commonsTypeAssertObjectArrayKeyObject(test: unknown): TKeyObject<{ [ key: string ]: unknown }[]> {
	if (!commonsTypeIsObjectArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a object array key object');
	return test;
}

export function commonsTypeIsObjectArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, { [ key: string ]: unknown }[]> {
	return validateTEnumObject<E, { [ key: string ]: unknown }[]>(
			test,
			commonsTypeIsObjectArray,
			keyChecker
	);
}

export function commonsTypeAssertObjectArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, { [ key: string ]: unknown }[]> {
	if (!commonsTypeIsObjectArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a object array key object');
	
	return test;
}

export function commonsTypeIsDateArray(test: unknown): test is Date[] {
	if (!commonsTypeIsObjectArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsDate(subtest)) return false;
	}

	return true;
}

export function commonsTypeIsFixedDateArray(test: unknown): test is CommonsFixedDate[] {
	if (!commonsTypeIsObjectArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsFixedDate(subtest)) return false;
	}

	return true;
}

export function commonsTypeIsFixedDurationArray(test: unknown): test is CommonsFixedDuration[] {
	if (!commonsTypeIsObjectArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsFixedDuration(subtest)) return false;
	}

	return true;
}

export function commonsTypeAssertDateArray(test: unknown): Date[] {
	if (!commonsTypeIsDateArray(test)) throw new Error('Assertion fail: variable is not a Date array');
	return test;
}

export function commonsTypeAssertFixedDateArray(test: unknown): CommonsFixedDate[] {
	if (!commonsTypeIsFixedDateArray(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDate array');
	return test;
}

export function commonsTypeAssertFixedDurationArray(test: unknown): CommonsFixedDuration[] {
	if (!commonsTypeIsFixedDurationArray(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration array');
	return test;
}

export function commonsTypeIsDateKeyObject(test: unknown): test is TKeyObject<Date> {
	return validateTKeyObject<Date>(test, commonsTypeIsDate);
}

export function commonsTypeIsFixedDateKeyObject(test: unknown): test is TKeyObject<CommonsFixedDate> {
	return validateTKeyObject<CommonsFixedDate>(test, commonsTypeIsFixedDate);
}

export function commonsTypeIsFixedDurationKeyObject(test: unknown): test is TKeyObject<CommonsFixedDuration> {
	return validateTKeyObject<CommonsFixedDuration>(test, commonsTypeIsFixedDuration);
}

export function commonsTypeAssertDateKeyObject(test: unknown): TKeyObject<Date> {
	if (!commonsTypeIsDateKeyObject(test)) throw new Error('Assertion fail: variable is not a Date key object');
	return test;
}

export function commonsTypeAssertFixedDateKeyObject(test: unknown): TKeyObject<CommonsFixedDate> {
	if (!commonsTypeIsFixedDateKeyObject(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDate key object');
	return test;
}

export function commonsTypeAssertFixedDurationKeyObject(test: unknown): TKeyObject<CommonsFixedDuration> {
	if (!commonsTypeIsFixedDurationKeyObject(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration key object');
	return test;
}

export function commonsTypeIsDateEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, Date> {
	return validateTEnumObject<E, Date>(
			test,
			commonsTypeIsDate,
			keyChecker
	);
}

export function commonsTypeIsFixedDateEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, CommonsFixedDate> {
	return validateTEnumObject<E, CommonsFixedDate>(
			test,
			commonsTypeIsFixedDate,
			keyChecker
	);
}

export function commonsTypeIsFixedDurationEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, CommonsFixedDuration> {
	return validateTEnumObject<E, CommonsFixedDuration>(
			test,
			commonsTypeIsFixedDuration,
			keyChecker
	);
}

export function commonsTypeAssertDateEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, Date> {
	if (!commonsTypeIsDateEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a Date key object');
	
	return test;
}

export function commonsTypeAssertFixedDateEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, CommonsFixedDate> {
	if (!commonsTypeIsFixedDateEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a CommonsFixedDate key object');
	
	return test;
}

export function commonsTypeAssertFixedDurationEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, CommonsFixedDuration> {
	if (!commonsTypeIsFixedDurationEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a CommonsFixedDate key object');

	return test;
}

export function commonsTypeIsDateOrUndefinedKeyObject(test: unknown): test is TKeyObject<Date|undefined> {
	return validateTOrUndefinedKeyObject<Date>(test, commonsTypeIsDate);
}

export function commonsTypeIsFixedDateOrUndefinedKeyObject(test: unknown): test is TKeyObject<CommonsFixedDate|undefined> {
	return validateTOrUndefinedKeyObject<CommonsFixedDate>(test, commonsTypeIsFixedDate);
}

export function commonsTypeIsFixedDurationOrUndefinedKeyObject(test: unknown): test is TKeyObject<CommonsFixedDuration|undefined> {
	return validateTOrUndefinedKeyObject<CommonsFixedDuration>(test, commonsTypeIsFixedDuration);
}

export function commonsTypeAssertDateOrUndefinedKeyObject(test: unknown): TKeyObject<Date|undefined> {
	if (!commonsTypeIsDateOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a Date or undefined key object');
	return test;
}

export function commonsTypeAssertFixedDateOrUndefinedKeyObject(test: unknown): TKeyObject<CommonsFixedDate|undefined> {
	if (!commonsTypeIsFixedDateOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDate or undefined key object');
	return test;
}

export function commonsTypeAssertFixedDurationOrUndefinedKeyObject(test: unknown): TKeyObject<CommonsFixedDuration|undefined> {
	if (!commonsTypeIsFixedDurationOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration or undefined key object');
	return test;
}

export function commonsTypeIsDateOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, Date|undefined> {
	return validateTOrUndefinedEnumObject<E, Date>(
			test,
			commonsTypeIsDate,
			keyChecker
	);
}

export function commonsTypeIsFixedDateOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, CommonsFixedDate|undefined> {
	return validateTOrUndefinedEnumObject<E, CommonsFixedDate>(
			test,
			commonsTypeIsFixedDate,
			keyChecker
	);
}

export function commonsTypeIsFixedDurationOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, CommonsFixedDuration|undefined> {
	return validateTOrUndefinedEnumObject<E, CommonsFixedDuration>(
			test,
			commonsTypeIsFixedDuration,
			keyChecker
	);
}

export function commonsTypeAssertDateOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, Date|undefined> {
	if (!commonsTypeIsDateOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a Date or undefined key object');
	
	return test;
}

export function commonsTypeAssertFixedDateOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, CommonsFixedDate|undefined> {
	if (!commonsTypeIsFixedDateOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a CommonsFixedDate or undefined key object');
	
	return test;
}

export function commonsTypeAssertFixedDurationOrUndefinedEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, CommonsFixedDuration|undefined> {
	if (!commonsTypeIsFixedDurationOrUndefinedEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration or undefined key object');
	
	return test;
}

export function commonsTypeIsDateArrayKeyObject(test: unknown): test is TKeyObject<Date[]> {
	return validateTKeyObject<Date[]>(test, commonsTypeIsDateArray);
}

export function commonsTypeIsFixedDateArrayKeyObject(test: unknown): test is TKeyObject<CommonsFixedDate[]> {
	return validateTKeyObject<CommonsFixedDate[]>(test, commonsTypeIsFixedDateArray);
}

export function commonsTypeIsFixedDurationArrayKeyObject(test: unknown): test is TKeyObject<CommonsFixedDuration[]> {
	return validateTKeyObject<CommonsFixedDuration[]>(test, commonsTypeIsFixedDurationArray);
}

export function commonsTypeAssertDateArrayKeyObject(test: unknown): TKeyObject<Date[]> {
	if (!commonsTypeIsDateArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a Date array key object');
	return test;
}

export function commonsTypeAssertFixedDateArrayKeyObject(test: unknown): TKeyObject<CommonsFixedDate[]> {
	if (!commonsTypeIsFixedDateArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDate array key object');
	return test;
}

export function commonsTypeAssertFixedDurationArrayKeyObject(test: unknown): TKeyObject<CommonsFixedDuration[]> {
	if (!commonsTypeIsFixedDurationArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration array key object');
	return test;
}

export function commonsTypeIsDateArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, Date[]> {
	return validateTEnumObject<E, Date[]>(
			test,
			commonsTypeIsDateArray,
			keyChecker
	);
}

export function commonsTypeIsFixedDateArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, CommonsFixedDate[]> {
	return validateTEnumObject<E, CommonsFixedDate[]>(
			test,
			commonsTypeIsFixedDateArray,
			keyChecker
	);
}

export function commonsTypeIsFixedDurationArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, CommonsFixedDuration[]> {
	return validateTEnumObject<E, CommonsFixedDuration[]>(
			test,
			commonsTypeIsFixedDurationArray,
			keyChecker
	);
}

export function commonsTypeAssertDateArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, Date[]> {
	if (!commonsTypeIsDateArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a Date array key object');
	
	return test;
}

export function commonsTypeAssertFixedDateArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, CommonsFixedDate[]> {
	if (!commonsTypeIsFixedDateArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a CommonsFixedDate array key object');
	
	return test;
}

export function commonsTypeAssertFixedDurationArrayEnumObject<E extends string>(
		test: unknown,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, CommonsFixedDuration[]> {
	if (!commonsTypeIsFixedDurationArrayEnumObject<E>(
			test,
			keyChecker
	)) throw new Error('Assertion fail: variable is not a CommonsFixedDuration array key object');

	return test;
}

// Object checkers and assertions

export function commonsTypeIsPropertyObject(test: unknown): test is TPropertyObject {
	if (!commonsTypeIsObject(test)) return false;
	if (commonsTypeIsArray(test) || commonsTypeIsDate(test) || commonsTypeIsFixedDate(test) || commonsTypeIsFixedDuration(test)) return false;
	
	return commonsTypeIsStringArray(Object.keys(test));
}

export function commonsTypeAssertPropertyObject(test: unknown): TPropertyObject {
	if (!commonsTypeIsPropertyObject(test)) throw new Error('Assertion fail: variable is not a property object');
	return test;
}

export function commonsTypeIsEncoded(test: unknown): test is TEncoded {
	if (!commonsTypeIsDefined(test)) return false;
	if (test === null) return true;
	
	if (commonsTypeIsPrimative(test)) return true;
	
	if (commonsTypeIsDate(test)) return false;
	if (commonsTypeIsFixedDate(test)) return false;
	if (commonsTypeIsFixedDuration(test)) return false;

	if (commonsTypeIsArray(test)) {
		for (const item of test) {
			if (!commonsTypeIsEncoded(item)) return false;
		}
		return true;
	}
	
	if (commonsTypeIsObject(test)) {
		for (const property of Object.keys(test)) {
			if (!commonsTypeIsDefined(test[property])) return false;
			if (!commonsTypeIsEncoded(test[property])) return false;
		}
		
		return true;
	}
	
	// unknown
	return false;
}

export function commonsTypeIsEncodedObject(test: unknown): test is TEncodedObject {
	if (!commonsTypeIsPropertyObject(test)) return false;
	return commonsTypeIsEncoded(test);
}

export function commonsTypeAssertEncodedObject(test: unknown): TEncodedObject {
	if (!commonsTypeIsEncodedObject(test)) throw new Error('Assertion fail: variable is not an encoded object');
	return test;
}

// Complex type guards and assertions

export function commonsTypeIsT<T>(test: unknown, checker: (t: unknown) => t is T): test is T {
	if (!commonsTypeIsObject(test)) return false;
	return checker(test);
}

export function commonsTypeAssertT<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): T {
	if (!commonsTypeIsT<T>(test, checker)) throw new Error(`Assertion fail: variable is not an object of type ${typeName}`);
	return test;
}

export function commonsTypeIsTArray<T>(test: unknown, checker: (t: unknown) => t is T): test is T[] {
	if (!commonsTypeIsObjectArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsT<T>(subtest, checker)) return false;
	}

	return true;
}

export function commonsTypeAssertTArray<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): T[] {
	if (!commonsTypeIsTArray<T>(test, checker)) throw new Error(`Assertion fail: variable is not an array of objects of type ${typeName}`);
	return test;
}

export function commonsTypeIsTKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T> {
	return validateTKeyObject<T>(
			test,
			(t: unknown): t is T => commonsTypeIsT<T>(t, checker)
	);
}

export function commonsTypeAssertTKeyObject<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): TKeyObject<T> {
	if (!commonsTypeIsTKeyObject<T>(test, checker)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);
	return test;
}

export function commonsTypeIsTEnumObject<E extends string, T>(
		test: unknown,
		checker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, T> {
	return validateTEnumObject<E, T>(
			test,
			(t: unknown): t is T => commonsTypeIsT<T>(t, checker),
			keyChecker
	);
}

export function commonsTypeAssertTEnumObject<E extends string, T>(
		test: unknown,
		checker: (t: unknown) => t is T,
		typeName: string,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, T> {
	if (!commonsTypeIsTEnumObject<E, T>(
			test,
			checker,
			keyChecker
	)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);
	return test;
}

export function commonsTypeIsTOrUndefinedKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T|undefined> {
	return validateTOrUndefinedKeyObject<T>(
			test,
			(t: unknown): t is T => commonsTypeIsT<T>(t, checker)
	);
}

export function commonsTypeAssertTOrUndefinedKeyObject<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): TKeyObject<T|undefined> {
	if (!commonsTypeIsTKeyObject<T>(test, checker)) throw new Error(`Assertion fail: variable is not an key object of objects or undefineds of type ${typeName}`);
	return test;
}

export function commonsTypeIsTOrUndefinedEnumObject<E extends string, T>(
		test: unknown,
		checker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, T|undefined> {
	return validateTOrUndefinedEnumObject<E, T>(
			test,
			(t: unknown): t is T => commonsTypeIsT<T>(t, checker),
			keyChecker
	);
}

export function commonsTypeAssertTOrUndefinedEnumObject<E extends string, T>(
		test: unknown,
		checker: (t: unknown) => t is T,
		typeName: string,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, T|undefined> {
	if (!commonsTypeIsTEnumObject<E, T>(
			test,
			checker,
			keyChecker
	)) throw new Error(`Assertion fail: variable is not an key object of objects or undefineds of type ${typeName}`);
	return test;
}

export function commonsTypeIsTArrayKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T[]> {
	return validateTKeyObject<T[]>(
			test,
			(t: unknown): t is T[] => commonsTypeIsTArray<T>(t, checker)
	);
}

export function commonsTypeAssertTArrayKeyObject<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): TKeyObject<T[]> {
	if (!commonsTypeIsTArrayKeyObject<T>(test, checker)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);
	return test;
}

export function commonsTypeIsTArrayEnumObject<E extends string, T>(
		test: unknown,
		checker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TEnumObject<E, T[]> {
	return validateTEnumObject<E, T[]>(
			test,
			(t: unknown): t is T[] => commonsTypeIsTArray<T>(t, checker),
			keyChecker
	);
}

export function commonsTypeAssertTArrayEnumObject<E extends string, T>(
		test: unknown,
		checker: (t: unknown) => t is T,
		typeName: string,
		keyChecker: (k: unknown) => k is E
): TEnumObject<E, T[]> {
	if (!commonsTypeIsTArrayEnumObject<E, T>(
			test,
			checker,
			keyChecker
	)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);

	return test;
}

export function commonsTypeIsEnum<E>(test: unknown, resolver: (_: string) => E|undefined): test is E {
	if (!commonsTypeIsString(test)) return false;
	return resolver(test) !== undefined;
}

export function commonsTypeAssertEnum<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): E {
	if (!commonsTypeIsEnum<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an enum of type ${enumName}`);
	return test;
}

export function commonsTypeIsEnumArray<E>(test: unknown, resolver: (_: string) => E|undefined): test is E[] {
	if (!commonsTypeIsStringArray(test)) return false;

	for (const subtest of test) {
		if (!commonsTypeIsEnum<E>(subtest, resolver)) return false;
	}

	return true;
}

export function commonsTypeAssertEnumArray<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): E[] {
	if (!commonsTypeIsEnumArray<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an array of enums of type ${enumName}`);
	return test;
}

export function commonsTypeIsEnumKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined): test is TKeyObject<E> {
	return validateTKeyObject<E>(
			test,
			(e: unknown): e is E => commonsTypeIsEnum<E>(e, resolver)
	);
}

export function commonsTypeAssertEnumKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): TKeyObject<E> {
	if (!commonsTypeIsEnumKeyObject<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
	return test;
}

// NB, K and E are the other way round for this one
export function commonsTypeIsEnumEnumObject<K extends string, E>(
		test: unknown,
		resolver: (_: string) => E|undefined,
		keyChecker: (k: unknown) => k is K
): test is TEnumObject<K, E> {
	return validateTEnumObject<K, E>(
			test,
			(e: unknown): e is E => commonsTypeIsEnum<E>(e, resolver),
			keyChecker
	);
}

// NB, K and E are the other way round for this one
export function commonsTypeAssertEnumEnumObject<K extends string, E>(
		test: unknown,
		resolver: (_: string) => E|undefined,
		enumName: string,
		keyChecker: (k: unknown) => k is K
): TEnumObject<K, E> {
	if (!commonsTypeIsEnumEnumObject<K, E>(
			test,
			resolver,
			keyChecker
	)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
	return test;
}

export function commonsTypeIsEnumOrUndefinedKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined): test is TKeyObject<E|undefined> {
	return validateTOrUndefinedKeyObject<E>(
			test,
			(e: unknown): e is E => commonsTypeIsEnum<E>(e, resolver)
	);
}

export function commonsTypeAssertEnumOrUndefinedKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): TKeyObject<E|undefined> {
	if (!commonsTypeIsEnumKeyObject<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
	return test;
}

// NB, K and E are the other way round for this one
export function commonsTypeIsEnumOrUndefinedEnumObject<K extends string, E>(
		test: unknown,
		resolver: (_: string) => E|undefined,
		keyChecker: (k: unknown) => k is K
): test is TEnumObject<K, E|undefined> {
	return validateTOrUndefinedEnumObject<K, E>(
			test,
			(e: unknown): e is E => commonsTypeIsEnum<E>(e, resolver),
			keyChecker
	);
}

// NB, K and E are the other way round for this one
export function commonsTypeAssertEnumOrUndefinedEnumObject<K extends string, E>(
		test: unknown,
		resolver: (_: string) => E|undefined,
		enumName: string,
		keyChecker: (k: unknown) => k is K
): TEnumObject<K, E|undefined> {
	if (!commonsTypeIsEnumEnumObject<K, E>(
			test,
			resolver,
			keyChecker
	)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
	return test;
}

export function commonsTypeIsEnumArrayKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined): test is TKeyObject<E[]> {
	return validateTKeyObject<E[]>(
			test,
			(e: unknown): e is E[] => commonsTypeIsEnumArray<E>(e, resolver)
	);
}

export function commonsTypeAssertEnumArrayKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): TKeyObject<E[]> {
	if (!commonsTypeIsEnumArrayKeyObject<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${enumName}`);
	return test;
}

// NB, K and E are the other way round for this one
export function commonsTypeIsEnumArrayEnumObject<K extends string, E>(
		test: unknown,
		resolver: (_: string) => E|undefined,
		keyChecker: (k: unknown) => k is K
): test is TEnumObject<K, E[]> {
	return validateTEnumObject<K, E[]>(
			test,
			(e: unknown): e is E[] => commonsTypeIsEnumArray<E>(e, resolver),
			keyChecker
	);
}

// NB, K and E are the other way round for this one
export function commonsTypeAssertEnumArrayEnumObject<K extends string, E>(
		test: unknown,
		resolver: (_: string) => E|undefined,
		enumName: string,
		keyChecker: (k: unknown) => k is K
): TEnumObject<K, E[]> {
	if (!commonsTypeIsEnumArrayEnumObject<K, E>(
			test,
			resolver,
			keyChecker
	)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${enumName}`);
	return test;
}

// Object property checkers and assertions

export function commonsTypeHasProperty(test: unknown, property: string, type?: ECommonsVariableType): test is TPropertyObject {
	if (type !== undefined && 'string' !== typeof type) throw new Error('Type is invalid');
	
	if (!commonsTypeIsPropertyObject(test)) return false;
	
	if (-1 === Object.keys(test).indexOf(property)) return false;

	if (type === undefined) return true;
	
	switch (type) {
		case ECommonsVariableType.STRING:
			return commonsTypeIsString(test[property]);
		case ECommonsVariableType.NUMBER:
			return commonsTypeIsNumber(test[property]);
		case ECommonsVariableType.BOOLEAN:
			return commonsTypeIsBoolean(test[property]);
		case ECommonsVariableType.OBJECT:
			return commonsTypeIsObject(test[property]);
		case ECommonsVariableType.DATE:
			return commonsTypeIsDate(test[property]);
		case ECommonsVariableType.FIXEDDATE:
			return commonsTypeIsFixedDate(test[property]);
		case ECommonsVariableType.FIXEDDURATION:
			return commonsTypeIsFixedDuration(test[property]);
		default:
			throw new Error('Unknown variable type');
	}
}

// The test is TPropertyObject is right, as we are testing the object's having the property and typecasting as a TPropertyObject, not typecasting the type of that property

export function commonsTypeHasPropertyNumber(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.NUMBER);
}

export function commonsTypeHasPropertyString(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.STRING);
}

export function commonsTypeHasPropertyBoolean(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.BOOLEAN);
}

export function commonsTypeHasPropertyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.OBJECT);
}

export function commonsTypeHasPropertyDate(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.DATE);
}

export function commonsTypeHasPropertyFixedDate(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.FIXEDDATE);
}

export function commonsTypeHasPropertyFixedDuration(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasProperty(test, property, ECommonsVariableType.FIXEDDURATION);
}

export function commonsTypeHasPropertyArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property) && commonsTypeIsArray(test[property]);
}

export function commonsTypeHasPropertyEnum<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
	return commonsTypeHasPropertyString(test, property) && checker(test[property]);
}

export function commonsTypeHasPropertyEnumArray<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
	if (!commonsTypeHasPropertyStringArray(test, property)) return false;	// eslint-disable-line @typescript-eslint/no-use-before-define

	const array: string[] = test[property] as string[];
	for (const item of array) if (!checker(item)) return false;
	
	return true;
}

export function commonsTypeHasPropertyT<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property) && commonsTypeIsT<T>(test[property], checker);
}

export function commonsTypeHasPropertyTArray<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property) && commonsTypeIsTArray<T>(test[property], checker);
}

export function commonsTypeHasPropertyTKeyObject<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property) && commonsTypeIsTKeyObject<T>(test[property], checker);
}

export function commonsTypeHasPropertyTEnumObject<E extends string, T>(
		test: unknown,
		property: string,
		checker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsTEnumObject<E, T>(
					test[property],
					checker,
					keyChecker
			);
}

export function commonsTypeHasPropertyNumberArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyArray(test, property)
			&& commonsTypeIsNumberArray(test[property]);
}

export function commonsTypeHasPropertyNumberKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsNumberKeyObject(test[property]);
}

export function commonsTypeHasPropertyNumberEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsNumberEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyStringArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyArray(test, property)
			&& commonsTypeIsStringArray(test[property]);
}

export function commonsTypeHasPropertyStringKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsStringKeyObject(test[property]);
}

export function commonsTypeHasPropertyStringEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsStringEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyBooleanArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyArray(test, property)
			&& commonsTypeIsBooleanArray(test[property]);
}

export function commonsTypeHasPropertyBooleanKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsBooleanKeyObject(test[property]);
}

export function commonsTypeHasPropertyBooleanEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsBooleanEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyDateArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArray<Date>(test, property, commonsTypeIsDate);
}

export function commonsTypeHasPropertyFixedDateArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArray<CommonsFixedDate>(test, property, commonsTypeIsFixedDate);
}

export function commonsTypeHasPropertyFixedDurationArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArray<CommonsFixedDuration>(test, property, commonsTypeIsFixedDuration);
}

export function commonsTypeHasPropertyDateKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsDateKeyObject(test[property]);
}

export function commonsTypeHasPropertyFixedDateKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsFixedDateKeyObject(test[property]);
}

export function commonsTypeHasPropertyFixedDurationKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsFixedDurationKeyObject(test[property]);
}

export function commonsTypeHasPropertyDateEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsDateEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyFixedDateEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsFixedDateEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyFixedDurationEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsFixedDurationEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyObjectArray(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArray<{ [ key: string ]: unknown }>(test, property, commonsTypeIsObject);
}

export function commonsTypeHasPropertyObjectKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsObjectKeyObject(test[property]);
}

export function commonsTypeHasPropertyObjectEnumObject<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	return commonsTypeHasPropertyObject(test, property)
			&& commonsTypeIsObjectEnumObject<E>(
					test[property],
					keyChecker
			);
}

export function commonsTypeHasPropertyNumberOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyNumber(test, property);
}

export function commonsTypeHasPropertyStringOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyString(test, property);
}

export function commonsTypeHasPropertyBooleanOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyBoolean(test, property);
}

export function commonsTypeHasPropertyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyObject(test, property);
}

export function commonsTypeHasPropertyDateOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyDate(test, property);
}

export function commonsTypeHasPropertyFixedDateOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyFixedDate(test, property);
}

export function commonsTypeHasPropertyFixedDurationOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyFixedDuration(test, property);
}

export function commonsTypeHasPropertyArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyArray(test, property);
}

export function commonsTypeHasPropertyEnumOrUndefined<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyEnum<E>(test, property, checker);
}

export function commonsTypeHasPropertyEnumArrayOrUndefined<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyEnumArray<E>(test, property, checker);
}

export function commonsTypeHasPropertyTOrUndefined<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyT<T>(test, property, checker);
}

export function commonsTypeHasPropertyTArrayOrUndefined<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyTArray<T>(test, property, checker);
}

export function commonsTypeHasPropertyTKeyObjectOrUndefined<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyTKeyObject<T>(test, property, checker);
}

export function commonsTypeHasPropertyTEnumObjectOrUndefined<E extends string, T>(
		test: unknown,
		property: string,
		checker: (t: unknown) => t is T,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyTEnumObject<E, T>(
			test,
			property,
			checker,
			keyChecker
	);
}

export function commonsTypeHasPropertyNumberArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyNumberArray(test, property);
}

export function commonsTypeHasPropertyNumberKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyNumberKeyObject(test, property);
}

export function commonsTypeHasPropertyNumberEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyNumberEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

export function commonsTypeHasPropertyStringArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyStringArray(test, property);
}

export function commonsTypeHasPropertyStringKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyStringKeyObject(test, property);
}

export function commonsTypeHasPropertyStringEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyStringEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

export function commonsTypeHasPropertyBooleanArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyBooleanArray(test, property);
}

export function commonsTypeHasPropertyBooleanKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyBooleanKeyObject(test, property);
}

export function commonsTypeHasPropertyBooleanEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyBooleanEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

export function commonsTypeHasPropertyDateArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArrayOrUndefined<Date>(test, property, commonsTypeIsDate);
}

export function commonsTypeHasPropertyFixedDateArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArrayOrUndefined<CommonsFixedDate>(test, property, commonsTypeIsFixedDate);
}

export function commonsTypeHasPropertyFixedDurationArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArrayOrUndefined<CommonsFixedDuration>(test, property, commonsTypeIsFixedDuration);
}

export function commonsTypeHasPropertyDateKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyDateKeyObject(test, property);
}

export function commonsTypeHasPropertyFixedDateKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyFixedDateKeyObject(test, property);
}

export function commonsTypeHasPropertyFixedDurationKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyFixedDurationKeyObject(test, property);
}

export function commonsTypeHasPropertyDateEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyDateEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

export function commonsTypeHasPropertyFixedDateEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyFixedDateEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

export function commonsTypeHasPropertyFixedDurationEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyFixedDurationEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

export function commonsTypeHasPropertyObjectArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsTypeHasPropertyTArrayOrUndefined<{ [ key: string ]: unknown }>(test, property, commonsTypeIsObject);
}

export function commonsTypeHasPropertyObjectKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyObjectKeyObject(test, property);
}

export function commonsTypeHasPropertyObjectEnumObjectOrUndefined<E extends string>(
		test: unknown,
		property: string,
		keyChecker: (k: unknown) => k is E
): test is TPropertyObject {
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	return commonsTypeHasPropertyObjectEnumObject<E>(
			test,
			property,
			keyChecker
	);
}

// Loosely typed checkers, converters, guards and assertions

export function commonsTypeAttemptString(value: unknown): string|undefined {
	if (commonsTypeIsString(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;

	if (commonsTypeIsArray(value)) {
		return value
				.map((item: unknown): string|undefined => commonsTypeAttemptString(item))
				.filter((item: string|undefined): boolean => item !== undefined)
				.join(', ');
	}
	
	if ('object' === typeof value) return JSON.stringify(value);
	
	if ('number' === typeof value) return value.toString();
	
	if ('boolean' === typeof value) return value ? 'true' : 'false';
	
	return String(value);
}

export function commonsTypeAttemptNumber(value: unknown): number|undefined {
	if (commonsTypeIsNumber(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;
	
	if ('string' === typeof value) {
		const attempt: number = value.indexOf('.') > -1 ? parseFloat(value) : parseInt(value, 10);
		if (Number.isNaN(attempt)) return undefined;
		
		return attempt;
	}
	
	if ('boolean' === typeof value) {
		return value ? 1 : 0;
	}

	return Number(value);
}

export function commonsTypeAttemptBoolean(value: unknown): boolean|undefined {
	if (commonsTypeIsBoolean(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;
	
	if (!commonsTypeIsPrimative(value)) return undefined;

	return commonsTypeCheckboxBoolean(value);	// eslint-disable-line @typescript-eslint/no-use-before-define
}

export function commonsTypeAttemptObject(value: unknown): { [ key: string ]: unknown }|undefined {
	if (commonsTypeIsObject(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;

	if ('string' === typeof value) {
		try {
			const attempt: unknown = JSON.parse(value);
			
			if ('object' === typeof attempt) return attempt as { [ key: string ]: unknown };

			return undefined;
		} catch (e) {
			return undefined;
		}
	}
	
	if ('object' !== typeof value || commonsTypeIsArray(value)) return undefined;
	
	return value as { [ key: string ]: unknown };
}

export function commonsTypeAttemptDate(value: unknown, iso: boolean = false): Date|undefined {
	if (commonsTypeIsDate(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;
	
	if ('string' === typeof value) {
		if (commonsDateIsYmdHis(value)) return commonsDateYmdHisToDate(value, iso);
		if (commonsDateIsdmYHi(value)) return commonsDatedmYHiToDate(value, iso);
		if (commonsDateIsYmd(value)) return commonsDateYmdToDate(value, iso);
		if (commonsDateIsdmY(value)) return commonsDatedmYToDate(value, iso);
		if (commonsDateIsHis(value)) return commonsDateHisToDate(value, iso);
		if (commonsDateIsHi(value)) return commonsDateHiToDate(value, iso);
		
		const attempt: number = Date.parse(value);
		if (Number.isNaN(attempt)) return undefined;
		
		return new Date(attempt);
	}

	return undefined;
}

export function commonsTypeAttemptFixedDate(value: unknown): CommonsFixedDate|undefined {
	if (commonsTypeIsFixedDate(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;
	
	if ('string' === typeof value) {
		if (commonsDateIsYmdHis(value)) return CommonsFixedDate.fromYmdHis(value);

		if (commonsDateIsdmYHi(value)) {
			const fixed: CommonsFixedDate = new CommonsFixedDate();
			fixed.dmYHi = value;
			return fixed;
		}

		if (commonsDateIsYmd(value)) return CommonsFixedDate.fromYmd(value);

		if (commonsDateIsdmY(value)) {
			const fixed: CommonsFixedDate = new CommonsFixedDate();
			fixed.dmY = value;
			return fixed;
		}

		if (commonsDateIsHis(value)) return CommonsFixedDate.fromHis(value);

		if (commonsDateIsHi(value)) return CommonsFixedDate.fromHi(value);
		
		const attempt: number = Date.parse(value);
		if (Number.isNaN(attempt)) return undefined;
		
		return CommonsFixedDate.fromUTCDate(new Date(attempt));
	}

	return undefined;
}

export function commonsTypeAttemptFixedDuration(value: unknown): CommonsFixedDuration|undefined {
	if (commonsTypeIsFixedDuration(value)) return value;
	
	if (value === undefined) return undefined;
	if (value === null) return undefined;
	if ('undefined' === typeof value) return undefined;
	
	if ('string' === typeof value) {
		if (commonsFixedDurationIsdHisMs(value)) return CommonsFixedDuration.fromdHisMs(value);
		if (commonsFixedDurationIsdHis(value)) return CommonsFixedDuration.fromdHis(value);
		if (commonsFixedDurationIsHisMs(value)) return CommonsFixedDuration.fromHisMs(value);
		if (commonsDateIsHi(value)) return CommonsFixedDuration.fromHi(value);

		const attempt: number = Date.parse(value);
		if (Number.isNaN(attempt)) return undefined;
		
		return CommonsFixedDuration.fromUTCDate(new Date(attempt));
	}

	return undefined;
}

export function commonsTypeValueOrDefault<T>(value: T|undefined, defaultValue: T): T {
	return value === undefined ? defaultValue : value;
}

export function commonsTypeTrimStringOrUndefined(value: string|number|boolean|Date|null|undefined): string|undefined {
	if (commonsTypeIsBlank(value)) return undefined;	// eslint-disable-line @typescript-eslint/no-use-before-define
	
	if ('' === (value = value.toString().trim())) return undefined;
	
	return value;
}

export function commonsTypeIsBlank(test: unknown): test is undefined|null|string {
	return test === undefined || test === null || test === '';
}

export function commonsTypeIsLooselyEqual(
		a: TPrimative|Date|CommonsFixedDate|CommonsFixedDuration|undefined,
		b: TPrimative|Date|CommonsFixedDate|CommonsFixedDuration|undefined,
		caseInsensitive: boolean = false,
		iso: boolean = false
): boolean {
	if (commonsTypeIsBlank(a) && commonsTypeIsBlank(b)) return true;
	
	if (commonsTypeIsDate(a) && commonsTypeIsDate(b) && a.getTime() === b.getTime()) return true;
	if (commonsTypeIsDate(a) && ('string' === typeof b) && commonsDateDateToYmdHis(a, iso) === b) return true;
	if (commonsTypeIsDate(b) && ('string' === typeof a) && commonsDateDateToYmdHis(b, iso) === a) return true;

	if (commonsTypeIsFixedDate(a) && commonsTypeIsFixedDate(b) && a.isEqual(b)) return true;
	try {
		if (commonsTypeIsFixedDate(a) && ('string' === typeof b) && a.isEqual(CommonsFixedDate.fromYmdHisMs(b))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDate(a) && ('string' === typeof b) && a.isEqual(CommonsFixedDate.fromYmdHis(b))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDate(b) && ('string' === typeof a) && b.isEqual(CommonsFixedDate.fromYmdHisMs(a))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDate(b) && ('string' === typeof a) && b.isEqual(CommonsFixedDate.fromYmdHis(a))) return true;
	} catch (e) {
		// ignore
	}

	if (commonsTypeIsFixedDuration(a) && commonsTypeIsFixedDuration(b) && a.isEqual(b)) return true;
	try {
		if (commonsTypeIsFixedDuration(a) && ('string' === typeof b) && a.isEqual(CommonsFixedDuration.fromdHisMs(b))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(a) && ('string' === typeof b) && a.isEqual(CommonsFixedDuration.fromdHis(b))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(a) && ('string' === typeof b) && a.isEqual(CommonsFixedDuration.fromHisMs(b))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(a) && ('string' === typeof b) && a.isEqual(CommonsFixedDuration.fromHis(b))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(b) && ('string' === typeof a) && b.isEqual(CommonsFixedDuration.fromdHisMs(a))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(b) && ('string' === typeof a) && b.isEqual(CommonsFixedDuration.fromdHis(a))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(b) && ('string' === typeof a) && b.isEqual(CommonsFixedDuration.fromHisMs(a))) return true;
	} catch (e) {
		// ignore
	}
	try {
		if (commonsTypeIsFixedDuration(b) && ('string' === typeof a) && b.isEqual(CommonsFixedDuration.fromHis(a))) return true;
	} catch (e) {
		// ignore
	}

	if (('string' === typeof a) && ('string' === typeof b)) {
		if (a.trim() === b.trim()) return true;
		if (caseInsensitive && a.toLowerCase().trim() === b.toLowerCase().trim()) return true;
	}
	
	if (('number' === typeof a) && ('string' === typeof b) && `${a}` === b) return true;
	if (('number' === typeof b) && ('string' === typeof a) && `${b}` === a) return true;

	if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? 'true' : 'false'}` === b.toLowerCase().trim()) return true;
	if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? 'true' : 'false'}` === a.toLowerCase().trim()) return true;
	if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? '1' : '0'}` === b.toLowerCase().trim()) return true;
	if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? '1' : '0'}` === a.toLowerCase().trim()) return true;
	if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? 'yes' : 'no'}` === b.toLowerCase().trim()) return true;
	if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? 'yes' : 'no'}` === a.toLowerCase().trim()) return true;
	if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? 'on' : 'off'}` === b.toLowerCase().trim()) return true;
	if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? 'on' : 'off'}` === a.toLowerCase().trim()) return true;
	if (('boolean' === typeof a) && ('number' === typeof b) && (a ? 1 : 0) === b) return true;
	if (('boolean' === typeof b) && ('number' === typeof a) && (b ? 1 : 0) === a) return true;

	return a === b;
}

export function commonsTypeCheckboxBoolean(a: TPrimative): boolean {
	return commonsTypeIsLooselyEqual(true, a);
}

export function commonsTypeEncode(value: any|null, iso: boolean = false): TEncoded {
	if (value === undefined) return null;	// this isn't ideal as we can't then differentiate between null and undefined during decode, but no other way to deal with undefined
	if (value === null) return null;
	
	if (commonsTypeIsPrimative(value)) return value;
	
	if (commonsTypeIsDate(value)) return commonsDateDateToYmdHis(value, iso);
	if (commonsTypeIsFixedDate(value)) return value.YmdHis;
	if (commonsTypeIsFixedDuration(value)) return value.dHis;

	if (Array.isArray(value)) {
		const encoded: TEncodedArray = [];
		
		for (const item of value) encoded.push(commonsTypeEncode(item, iso));
		
		return encoded;
	}
	
	if (commonsTypeIsObject(value)) {
		const encoded: TEncodedObject = {};
		
		for (const property of Object.keys(value)) {
			if (value[property] === undefined) continue;
			encoded[property] = commonsTypeEncode(value[property], iso);
		}
		
		return encoded;
	}
	
	// unknown
	throw new Error('Unable to automatically encode an unknown value');
}

export function commonsTypeEncodePropertyObject(object: TPropertyObject, iso: boolean = false): TEncodedObject {
	return commonsTypeEncode(object, iso) as TEncodedObject;
}

export function commonsTypeDecode(value: TEncoded, iso: boolean = false, fixedDate: boolean = false): any|null {
	// unfortunately, undefined is lost in the process of encoding and decoding. It will have been stripped from objects, but array values and literals will be turned into null. No easy fix.
	if (value === null) return null;

	if (commonsTypeIsPrimative(value)) {
		if (commonsTypeIsString(value)) {
			if (commonsDateIsYmdHis(value)) {
				if (fixedDate) {
					return CommonsFixedDate.fromYmdHis(value);
				} else {
					return commonsDateYmdHisToDate(value, iso);
				}
			}

			if (commonsDateIsYmd(value)) {
				if (fixedDate) {
					return CommonsFixedDate.fromYmd(value);
				} else {
					return commonsDateYmdToDate(value, iso);
				}
			}

			if (commonsFixedDurationIsdHis(value)) {
				return CommonsFixedDuration.fromdHis(value);
			}

			if (COMMONS_REGEX_PATTERN_DATE_ECMA.test(value)) return new Date(value);
		}
		return value as TPrimative;
	}
	
	if (Array.isArray(value)) {
		const decoded: any[] = [];
		const array: TEncodedArray = value;
		
		for (const item of array) decoded.push(commonsTypeDecode(item, iso, fixedDate));
		
		return decoded;	// eslint-disable-line @typescript-eslint/no-unsafe-return
	}
	
	if (commonsTypeIsObject(value)) {
		const decoded: TPropertyObject = {};
		const object: TEncodedObject = value;
		
		for (const property of Object.keys(object)) {
			if (object[property] === undefined) continue;
			decoded[property] = commonsTypeDecode(object[property], iso, fixedDate) as unknown;
		}
		
		return decoded;
	}
	
	// unknown
	return value;
}

export function commonsTypeDecodePropertyObject(object: TEncodedObject, iso: boolean = false, fixedDate: boolean = false): TPropertyObject {
	return commonsTypeDecode(object, iso, fixedDate) as TPropertyObject;
}

// other assertions and checkers

export function commonsTypeAssertArrayLengths(a: unknown, b: unknown): void|never {
	const assertedA: unknown[] = commonsTypeAssertArray(a);
	const assertedB: unknown[] = commonsTypeAssertArray(b);

	if (assertedA.length !== assertedB.length) throw new Error('Assertion fail: two arrays are not the same length');
}

// hack method to prevent typescript and eslint complaining about unused variables

export function commonsConsumeUnusedVariable(..._args: any): void {
	// do nothing
}
