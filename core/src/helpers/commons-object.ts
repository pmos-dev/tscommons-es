import { TPropertyObject } from '../types/tproperty-object';

import {
		commonsTypeIsObject,
		commonsTypeIsDate,
		commonsTypeIsArray
} from './commons-type';

export function commonsObjectMapObject<T>(src: TPropertyObject, mapper: (_value: any, _key: string) => T): TPropertyObject {
	if (!commonsTypeIsObject(src)) throw new Error('Trying to map a non-object');

	const dest: TPropertyObject = {};
	for (const key of Object.keys(src)) dest[key] = mapper(src[key], key);

	return dest;
}

export function commonsObjectIsEmpty(src: TPropertyObject): boolean {
	return Object.keys(src).length === 0;
}

function stripNulls(
		value: unknown,
		modifyArrays: boolean
): any {
	if (value === undefined || value === null) return undefined;

	if (commonsTypeIsDate(value)) return value;
	if (commonsTypeIsArray(value)) {
		return value
				.map((item: unknown): unknown => stripNulls(item, modifyArrays))
				.filter((item: unknown): boolean => !modifyArrays || (item !== undefined && item !== null));
	}
	
	if (commonsTypeIsObject(value)) {
		const rebuild: TPropertyObject = {};
		for (const key of Object.keys(value)) {
			const v: unknown = value[key];
			if (v === undefined || v === null) continue;
			
			rebuild[key] = stripNulls(v, modifyArrays);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
		}
		
		return rebuild;
	}

	return value;
}

export function commonsObjectStripNulls(
		value: TPropertyObject,
		modifyArrays: boolean = false
): TPropertyObject {
	if (!commonsTypeIsObject(value)) throw new Error('Supplied value to strip nulls is not an object');

	return stripNulls(value, modifyArrays);	// eslint-disable-line @typescript-eslint/no-unsafe-return
}
