import { TKeyObject } from '../types/tkey-object';
import { TPropertyObject } from '../types/tproperty-object';

import {
		COMMONS_REGEX_PATTERN_BASE36,
		COMMONS_REGEX_PATTERN_BASE36_ID,
		COMMONS_REGEX_PATTERN_BASE41,
		COMMONS_REGEX_PATTERN_BASE41_ID,
		COMMONS_REGEX_PATTERN_BASE62,
		COMMONS_REGEX_PATTERN_BASE62_ID,
		COMMONS_REGEX_PATTERN_BASE62_LONG_ID
} from '../consts/commons-regex';

import {
		commonsTypeIsStringArray,
		commonsTypeIsStringKeyObject,
		commonsTypeIsStringOrUndefinedKeyObject,
		commonsTypeHasProperty,
		commonsTypeIsStringArrayKeyObject,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringArray,
		commonsTypeHasPropertyStringKeyObject,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyStringArrayOrUndefined,
		commonsTypeHasPropertyStringKeyObjectOrUndefined,
		commonsTypeIsDefined
} from './commons-type';

export function commonsBaseDecToBaseNChunks(dec: number, base: number): number[] {
	if (dec === 0) return [ 0 ];
	
	const size = Math.floor(Math.log(dec) / Math.log(base)) + 1;

	const chunks: number[] = [];
	for (let i = size, j = 0; i-- > 0; j++) {
		const dividor = Math.pow(base, i);
		const div = Math.floor(dec / dividor);
		chunks.push(div);
			
		dec -= div * dividor;
	}

	return chunks;
}

export function commonsBaseDigitToBaseN(digit: number, base: number): string {
	if (base < 1 || base > 62) throw new Error('Base is less than 1 or greater than 62. This cannot be represented using 0-9A-Za-z notation');
	if (digit < 0 || digit > (base - 1)) throw new Error(`Digit out of range of base ${digit} vs base ${base}`);

	if (digit < 10) return digit.toString();

	digit -= 10;
	if (digit < 26) return String.fromCharCode(65 + digit);
	
	digit -= 26;
	return String.fromCharCode(97 + digit);
}

export function commonsBaseBaseNToDigit(digit: string, base: number): number {
	if (base < 1 || base > 62) throw new Error('Base is less than 1 or greater than 62. This cannot be represented using 0-9A-Za-z notation');

	const b = digit.charCodeAt(0);
	if (b < 48) throw new Error('Character is outside scope of given base');
	if (b < 58) {
		const d = b - 48;
		if (d >= base) throw new Error('Character is outside scope of given base');
		return d;
	}
	if (b < 91) {
		const d = (b - 65) + 10;
		if (d >= base) throw new Error('Character is outside scope of given base');
		return d;
	}
	if (b < 123) {
		const d = (b - 97) + 10 + 26;
		if (d >= base) throw new Error('Character is outside scope of given base');
		return d;
	}

	throw new Error('Character is outside scope of given base');
}

export function commonsBaseDecToBaseN(dec: number, base: number): string {
	const chunks: number[] = commonsBaseDecToBaseNChunks(dec, base);

	let result = '';
	for (const chunk of chunks) result = `${result}${commonsBaseDigitToBaseN(chunk, base)}`;

	return result;
}

export function commonsBaseBaseNToDec(baseN: string, base: number): number {
	let dec = 0;
	while (baseN !== '') {
		const digit = commonsBaseBaseNToDigit(baseN.substring(0, 1), base);
		baseN = baseN.substring(1);
			
		dec *= base;
		dec += digit;
	}

	return dec;
}

export function commonsBaseGenerateRandomBaseNId(length: number, base: number): string {
	let sb = '';
	for (let i = length; i-- > 0;) {
		const digit = Math.floor(Math.random() * base);
		sb = `${sb}${commonsBaseDigitToBaseN(digit, base)}`;
	}

	return sb;
}

export function commonsBaseIsBaseNIdArray(
		test: unknown,
		checker: (t: string) => boolean
): test is string[] {
	if (!commonsTypeIsStringArray(test)) return false;

	for (const subtest of test) {
		if (!checker(subtest)) return false;
	}

	return true;
}

export function commonsBaseIsBaseNIdKeyObject(
		test: unknown,
		checker: (t: string) => boolean
): test is TKeyObject<string> {
	if (!commonsTypeIsStringKeyObject(test)) return false;
	
	for (const key of Object.keys(test)) {
		if (!checker(test[key])) return false;
	}
	
	return true;
}

export function commonsBaseIsBaseNIdOrUndefinedKeyObject(
		test: unknown,
		checker: (t: string) => boolean
): test is TKeyObject<string|undefined> {
	if (!commonsTypeIsStringOrUndefinedKeyObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (!commonsTypeHasProperty(test, key) || test[key] === undefined) continue;

		if (!checker(test[key]!)) return false;
	}
	
	return true;
}

export function commonsBaseIsBaseNIdArrayKeyObject(
		test: unknown,
		checker: (t: string) => boolean
): test is TKeyObject<string[]> {
	if (!commonsTypeIsStringArrayKeyObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (!commonsBaseIsBaseNIdArray(test[key], checker)) return false;
	}

	return true;
}

export function commonsBaseHasPropertyBaseNId(
		test: unknown,
		property: string,
		checker: (t: string) => boolean
): test is TPropertyObject {
	if (!commonsTypeHasPropertyString(test, property)) return false;
	
	const value: string = test[property] as string;
	return checker(value);
}

export function commonsBaseHasPropertyBaseNIdArray(
		test: unknown,
		property: string,
		checker: (t: string) => boolean
): test is TPropertyObject {
	if (!commonsTypeHasPropertyStringArray(test, property)) return false;
	
	return commonsBaseIsBaseNIdArray(test[property], checker);
}

export function commonsBaseHasPropertyBaseNIdKeyObject(
		test: unknown,
		property: string,
		checker: (t: string) => boolean
): test is TPropertyObject {
	if (!commonsTypeHasPropertyStringKeyObject(test, property)) return false;

	return commonsBaseIsBaseNIdArrayKeyObject(test[property], checker);
}

export function commonsBaseHasPropertyBaseNIdOrUndefined(
		test: unknown,
		property: string,
		checker: (t: string) => boolean
): test is TPropertyObject {
	if (!commonsTypeHasPropertyStringOrUndefined(test, property)) return false;
	
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	
	return commonsBaseHasPropertyBaseNId(test, property, checker);
}

export function commonsBaseHasPropertyBaseNIdArrayOrUndefined(
		test: unknown,
		property: string,
		checker: (t: string) => boolean
): test is TPropertyObject {
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, property)) return false;
	
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	
	return commonsBaseHasPropertyBaseNIdArray(test, property, checker);
}

export function commonsBaseHasPropertyBaseNIdKeyObjectOrUndefined(
		test: unknown,
		property: string,
		checker: (t: string) => boolean
): test is TPropertyObject {
	if (!commonsTypeHasPropertyStringKeyObjectOrUndefined(test, property)) return false;
	
	if (!commonsTypeHasProperty(test, property) || !commonsTypeIsDefined(test[property])) return true;
	
	return commonsBaseHasPropertyBaseNIdKeyObject(test, property, checker);
}

// base 36

export function commonsBase36DecToChunks(dec: number): number[] {
	return commonsBaseDecToBaseNChunks(dec, 36);
}

export function commonsBase36DigitToBase(digit: number): string {
	return commonsBaseDigitToBaseN(digit, 36);
}

export function commonsBase36BaseToDigit(digit: string): number {
	return commonsBaseBaseNToDigit(digit.toUpperCase(), 36);
}

export function commonsBase36DecToBase(dec: number): string {
	return commonsBaseDecToBaseN(dec, 36);
}

export function commonsBase36BaseToDec(base: string): number {
	return commonsBaseBaseNToDec(base.toUpperCase(), 36);
}

export function commonsBase36GenerateRandomId(): string {
	return commonsBaseGenerateRandomBaseNId(10, 36);
}

export function commonsBase36Is(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE36);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase36IsId(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE36_ID);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase36IsIdArray(test: unknown): test is string[] {
	return commonsBaseIsBaseNIdArray(test, commonsBase36IsId);
}

export function commonsBase36IsIdKeyObject(test: unknown): test is TKeyObject<string> {
	return commonsBaseIsBaseNIdKeyObject(test, commonsBase36IsId);
}

export function commonsBase36IsIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
	return commonsBaseIsBaseNIdOrUndefinedKeyObject(test, commonsBase36IsId);
}

export function commonsBase36IsIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
	return commonsBaseIsBaseNIdArrayKeyObject(test, commonsBase36IsId);
}

export function commonsBase36HasPropertyId(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNId(test, property, commonsBase36IsId);
}

export function commonsBase36HasPropertyIdArray(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArray(test, property, commonsBase36IsId);
}

export function commonsBase36HasPropertyIdKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObject(test, property, commonsBase36IsId);
}

export function commonsBase36HasPropertyIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdOrUndefined(test, property, commonsBase36IsId);
}

export function commonsBase36HasPropertyIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArrayOrUndefined(test, property, commonsBase36IsId);
}

export function commonsBase36HasPropertyIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObjectOrUndefined(test, property, commonsBase36IsId);
}

// base 41

export function commonsBase41DecToChunks(dec: number): number[] {
	return commonsBaseDecToBaseNChunks(dec, 41);
}

export function commonsBase41DigitToBase(digit: number): string {
	return commonsBaseDigitToBaseN(digit, 41);
}

export function commonsBase41BaseToDigit(digit: string): number {
	return commonsBaseBaseNToDigit(digit.toUpperCase(), 41);
}

export function commonsBase41DecToBase(dec: number): string {
	return commonsBaseDecToBaseN(dec, 41);
}

export function commonsBase41BaseToDec(base: string): number {
	return commonsBaseBaseNToDec(base.toUpperCase(), 41);
}

export function commonsBase41GenerateRandomId(): string {
	return commonsBaseGenerateRandomBaseNId(6, 41);
}

export function commonsBase41Is(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE41);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase41IsId(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE41_ID);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase41IsIdArray(test: unknown): test is string[] {
	return commonsBaseIsBaseNIdArray(test, commonsBase41IsId);
}

export function commonsBase41IsIdKeyObject(test: unknown): test is TKeyObject<string> {
	return commonsBaseIsBaseNIdKeyObject(test, commonsBase41IsId);
}

export function commonsBase41IsIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
	return commonsBaseIsBaseNIdOrUndefinedKeyObject(test, commonsBase41IsId);
}

export function commonsBase41IsIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
	return commonsBaseIsBaseNIdArrayKeyObject(test, commonsBase41IsId);
}

export function commonsBase41HasPropertyId(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNId(test, property, commonsBase41IsId);
}

export function commonsBase41HasPropertyIdArray(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArray(test, property, commonsBase41IsId);
}

export function commonsBase41HasPropertyIdKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObject(test, property, commonsBase41IsId);
}

export function commonsBase41HasPropertyIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdOrUndefined(test, property, commonsBase41IsId);
}

export function commonsBase41HasPropertyIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArrayOrUndefined(test, property, commonsBase41IsId);
}

export function commonsBase41HasPropertyIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObjectOrUndefined(test, property, commonsBase41IsId);
}

// base 62

export function commonsBase62DecToChunks(dec: number): number[] {
	return commonsBaseDecToBaseNChunks(dec, 62);
}

export function commonsBase62DigitToBase(digit: number): string {
	return commonsBaseDigitToBaseN(digit, 62);
}

export function commonsBase62BaseToDigit(digit: string): number {
	return commonsBaseBaseNToDigit(digit.toUpperCase(), 62);
}

export function commonsBase62DecToBase(dec: number): string {
	return commonsBaseDecToBaseN(dec, 62);
}

export function commonsBase62BaseToDec(base: string): number {
	return commonsBaseBaseNToDec(base.toUpperCase(), 62);
}

export function commonsBase62GenerateRandomId(): string {
	return commonsBaseGenerateRandomBaseNId(8, 62);
}

export function commonsBase62GenerateLongRandomId(): string {
	return commonsBaseGenerateRandomBaseNId(32, 62);
}

export function commonsBase62Is(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE62);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase62IsId(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE62_ID);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase62IsIdArray(test: unknown): test is string[] {
	return commonsBaseIsBaseNIdArray(test, commonsBase62IsId);
}

export function commonsBase62IsIdKeyObject(test: unknown): test is TKeyObject<string> {
	return commonsBaseIsBaseNIdKeyObject(test, commonsBase62IsId);
}

export function commonsBase62IsIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
	return commonsBaseIsBaseNIdOrUndefinedKeyObject(test, commonsBase62IsId);
}

export function commonsBase62IsIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
	return commonsBaseIsBaseNIdArrayKeyObject(test, commonsBase62IsId);
}

export function commonsBase62HasPropertyId(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNId(test, property, commonsBase62IsId);
}

export function commonsBase62HasPropertyIdArray(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArray(test, property, commonsBase62IsId);
}

export function commonsBase62HasPropertyIdKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObject(test, property, commonsBase62IsId);
}

export function commonsBase62HasPropertyIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdOrUndefined(test, property, commonsBase62IsId);
}

export function commonsBase62HasPropertyIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArrayOrUndefined(test, property, commonsBase62IsId);
}

export function commonsBase62HasPropertyIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObjectOrUndefined(test, property, commonsBase62IsId);
}

export function commonsBase62IsLongId(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_BASE62_LONG_ID);	// ensures a new test each time
	return regex.test(test);
}

export function commonsBase62IsLongIdArray(test: unknown): test is string[] {
	return commonsBaseIsBaseNIdArray(test, commonsBase62IsLongId);
}

export function commonsBase62IsLongIdKeyObject(test: unknown): test is TKeyObject<string> {
	return commonsBaseIsBaseNIdKeyObject(test, commonsBase62IsLongId);
}

export function commonsBase62IsLongIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
	return commonsBaseIsBaseNIdOrUndefinedKeyObject(test, commonsBase62IsLongId);
}

export function commonsBase62IsLongIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
	return commonsBaseIsBaseNIdArrayKeyObject(test, commonsBase62IsLongId);
}

export function commonsBase62HasPropertyLongId(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNId(test, property, commonsBase62IsLongId);
}

export function commonsBase62HasPropertyLongIdArray(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArray(test, property, commonsBase62IsLongId);
}

export function commonsBase62HasPropertyLongIdKeyObject(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObject(test, property, commonsBase62IsLongId);
}

export function commonsBase62HasPropertyLongIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdOrUndefined(test, property, commonsBase62IsLongId);
}

export function commonsBase62HasPropertyLongIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdArrayOrUndefined(test, property, commonsBase62IsLongId);
}

export function commonsBase62HasPropertyLongIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
	return commonsBaseHasPropertyBaseNIdKeyObjectOrUndefined(test, property, commonsBase62IsLongId);
}
