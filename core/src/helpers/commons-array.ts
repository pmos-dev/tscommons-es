import { TArrayWithErrors } from '../types/tarray-with-errors';

import { commonsTypeIsArray, commonsTypeIsError } from './commons-type';
import { commonsNumberRandRange } from './commons-number';

export function commonsArrayRemove<T>(
		array: T[],
		item: T,
		isEqual?: (a: T, b: T) => boolean
): boolean {
	let index: number = -1;

	if (isEqual) {
		for (let i = 0; i < array.length; i++) {
			if (isEqual(item, array[i])) {
				index = i;
				break;
			}
		}
	} else {
		index = array.indexOf(item);
	}
	if (index === -1) return false;
	
	array.splice(index, 1);
	
	return true;
}

export function commonsArrayChunk<T>(src: T[], size: number): T[][] {
	const chunks: T[][] = [];
	for (let i = 0, len = src.length; i < len; i += size) {
		chunks.push(src.slice(i, i + size));
	}
	
	return chunks;
}

export function commonsArrayUnique<T>(
		src: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	// we can't use isTArray, as that only accepts T objects, not E enums etc.
	if (checker) {
		if (!commonsTypeIsArray(src)) throw new Error('Invalid array supplied to commonsArrayUnique');
		for (const item of src) {
			if (!checker(item)) throw new Error('Invalid array item supplied to commonsArrayUnique');
		}
	}
	
	if (!isEqual) {
		return [ ...new Set(src) ];
	}
	
	const uniques: T[] = [];
	for (const item of src) {
		const existing: T|undefined = uniques
				.find((i: T): boolean => isEqual(item, i));
		
		if (!existing) uniques.push(item);
	}
	
	return uniques;
}

export function commonsArrayIntersect<T>(
		as: T[],
		bs: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	// we can't use isTArray, as that only accepts T objects, not E enums etc.
	if (checker) {
		if (!commonsTypeIsArray(as)) throw new Error('Invalid a array supplied to commonsArrayIntersect');
		for (const item of as) {
			if (!checker(item)) throw new Error('Invalid a array item supplied to commonsArrayIntersect');
		}

		if (!commonsTypeIsArray(bs)) throw new Error('Invalid b array supplied to commonsArrayIntersect');
		for (const item of bs) {
			if (!checker(item)) throw new Error('Invalid b array item supplied to commonsArrayIntersect');
		}
	}

	if (!isEqual) {
		const intersection: T[] = as
				.filter((item: T): boolean => bs.includes(item));
				
		return commonsArrayUnique<T>(intersection);	// no checker needed as performed already
	}

	{	// scope
		const intersection: T[] = [];
		for (const item of as) {
			const match: T|undefined = bs
					.find((i: T): boolean => isEqual(item, i));
			
			if (match) intersection.push(item);
		}

		return commonsArrayUnique<T>(
				intersection,
				undefined,
				isEqual
		);	// no checker needed as performed already
	}
}

export function commonsArrayUnion<T>(
		as: T[],
		bs: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	// we can't use isTArray, as that only accepts T objects, not E enums etc.
	if (checker) {
		if (!commonsTypeIsArray(as)) throw new Error('Invalid a array supplied to commonsArrayUnion');
		for (const item of as) {
			if (!checker(item)) throw new Error('Invalid a array item supplied to commonsArrayUnion');
		}

		if (!commonsTypeIsArray(bs)) throw new Error('Invalid b array supplied to commonsArrayUnion');
		for (const item of bs) {
			if (!checker(item)) throw new Error('Invalid b array item supplied to commonsArrayUnion');
		}
	}

	const union: T[] = [ ...as, ...bs ];

	return commonsArrayUnique<T>(union, undefined, isEqual);	// no checker needed as performed already
}

export function commonsArrayDifference<T>(
		as: T[],
		bs: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	// we can't use isTArray, as that only accepts T objects, not E enums etc.
	if (checker) {
		if (!commonsTypeIsArray(as)) throw new Error('Invalid a array supplied to commonsArrayDifference');
		for (const item of as) {
			if (!checker(item)) throw new Error('Invalid a array item supplied to commonsArrayDifference');
		}

		if (!commonsTypeIsArray(bs)) throw new Error('Invalid b array supplied to commonsArrayDifference');
		for (const item of bs) {
			if (!checker(item)) throw new Error('Invalid b array item supplied to commonsArrayDifference');
		}
	}
	
	const union: T[] = commonsArrayUnion<T>(as, bs, undefined, isEqual);
	const intersection: T[] = commonsArrayIntersect<T>(as, bs, undefined, isEqual);
	
	if (!isEqual) {
		const difference: T[] = union
				.filter((item: T): boolean => !intersection.includes(item));
		
		return difference;	// already unique from union
	}
	
	{	// scope
		const difference: T[] = [];
		for (const item of union) {
			const match: T|undefined = intersection
					.find((i: T): boolean => isEqual(item, i));
			
			if (!match) difference.push(item);
		}
		
		return difference;	// already unique from union
	}
}

export function commonsArrayRemoveAll<T>(
		existings: T[],
		toRemoves: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): boolean {
	// we can't use isTArray, as that only accepts T objects, not E enums etc.
	if (checker) {
		if (!commonsTypeIsArray(existings)) throw new Error('Invalid existings array supplied to commonsArrayRemoveAll');
		for (const item of existings) {
			if (!checker(item)) throw new Error('Invalid existings array item supplied to commonsArrayRemoveAll');
		}

		if (!commonsTypeIsArray(toRemoves)) throw new Error('Invalid toRemoves array supplied to commonsArrayRemoveAll');
		for (const item of toRemoves) {
			if (!checker(item)) throw new Error('Invalid toRemoves array item supplied to commonsArrayRemoveAll');
		}
	}
	
	let anyRemoved: boolean = false;
	for (const item of toRemoves) {
		if (commonsArrayRemove(
				existings,
				item,
				isEqual
		)) anyRemoved = true;
	}
	
	return anyRemoved;
}

export function commonsArrayUnionWithout<T>(
		as: T[],
		bs: T[],
		withouts: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	const union: T[] = commonsArrayUnion(
			as,
			bs,
			checker,
			isEqual
	);
	
	commonsArrayRemoveAll(
			union,
			withouts,
			checker,
			isEqual
	);

	return union;
}

// Essentially a one-way difference, i.e. only items in updateds not in original
export function commonsArrayAdded<T>(
		originals: T[],
		news: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	// we can't use isTArray, as that only accepts T objects, not E enums etc.
	if (checker) {
		if (!commonsTypeIsArray(originals)) throw new Error('Invalid originals array supplied to commonsArrayAdded');
		for (const item of originals) {
			if (!checker(item)) throw new Error('Invalid originals array item supplied to commonsArrayAdded');
		}

		if (!commonsTypeIsArray(news)) throw new Error('Invalid news array supplied to commonsArrayAdded');
		for (const item of news) {
			if (!checker(item)) throw new Error('Invalid news array item supplied to commonsArrayAdded');
		}
	}
	
	const addeds: T[] = [];
	for (const item of news) {
		if (!isEqual) {
			if (!originals.includes(item)) addeds.push(item);
		} else {
			const match: T|undefined = originals
					.find((i: T): boolean => isEqual(item, i));
			if (!match) addeds.push(item);
		}
	}
	
	return addeds;
}

export function commonsArrayRemoved<T>(
		originals: T[],
		news: T[],
		checker?: (_: unknown) => _ is T,
		isEqual?: (a: T, b: T) => boolean
): T[] {
	// this turns out to be just an inversion of added
	
	return commonsArrayAdded(news, originals, checker, isEqual);
}

/* Adapted from: https://www.geeksforgeeks.org/print-subsets-given-size-set/ */
function setCombinationsRecurse<T>(
		source: T[],
		setSize: number,
		index: number,
		data: T[],
		i: number,
		out: T[][]
): void {
	// Current combination is ready so add it
	if (index === setSize) {
		out.push(data.slice(0, setSize));
		return;
	}

	// When no more elements are there to put in data[]
	if (i >= source.length) return;

	// current is included, put next at next location
	data[index] = source[i];
	setCombinationsRecurse<T>(source, setSize, index + 1, data, i + 1, out);

	// current is excluded, replace it with next (Note that i+1 is passed, but index is not changed)
	setCombinationsRecurse<T>(source, setSize, index, data, i + 1, out);
}

/* Adapted from: https://www.geeksforgeeks.org/print-subsets-given-size-set/ */
export function commonsArraySetCombinations<T>(
		source: T[],
		setSize: number
): T[][] {
	const out: T[][] = [];

	// A temporary array to store all combination one by one
	const data: T[] = Array(setSize).fill('') as T[];

	// Generate all combination using temporary array, output to out
	setCombinationsRecurse<T>(source, setSize, 0, data, 0, out);

	return out;
}

export function commonsArrayRandomize<T>(array: T[]): void {
	const length: number = array.length;
	
	for (let i = length; i-- > 0;) {
		const j = commonsNumberRandRange(0, length);
		[array[i], array[j]] = [array[j], array[i]];
	}
}

function getMinDistance(order: number[]): number {
	let min: number = order.length;	// just to get the largest possible distance, so min doesn't start with 0
	
	for (let i = 1; i < order.length; i++) {
		const delta: number = Math.abs(order[i] - order[i - 1]);
		min = Math.min(min, delta);
	}
	
	return min;
}

export function commonsArrayShuffle<T>(array: T[], minDistance: number = 2): boolean {
	const length: number = array.length;
	if (length < 3) {
		if (length === 2) commonsArrayRandomize(array);
		return false;
	}
	
	minDistance = Math.min(minDistance, Math.floor(length / 2));
	if (minDistance < 1) {
		commonsArrayRandomize(array);
		return false;
	}
	
	const order: number[] = [];
	for (let i = length, j = 0; i-- > 0;) order.push(j++);
	
	let best: number[]|undefined = order;
	let bestDistance: number = 0;
	
	let ttl: number = Math.max(1000, length);
	while (ttl-- > 0) {
		commonsArrayRandomize(order);
		const distance: number = getMinDistance(order);
		
		if (distance > bestDistance) {
			best = order.slice();
			bestDistance = distance;

			if (bestDistance >= minDistance) break;
		}
	}
	
	const clone: T[] = array.slice();
	
	for (let i = 0; i < length; i++) {
		array[i] = clone[best[i]];
	}
	
	return bestDistance >= minDistance;
}

export function commonsArraySplitMapErrors<T>(array: (T|Error)[]): TArrayWithErrors<T> {
	const errors: Error[] = [];
	const items: T[] = [];

	for (const item of array) {
		try {
			if (item instanceof Error) {
				errors.push(item);
				continue;
			}
		} catch (e) {
			// ignore, probably a Javascript dislike of (e ! class object) instanceof Error
		}

		items.push(item as T);
	}
	
	return {
			errors: errors,
			array: items
	};
}

export function commonsArrayMapWithErrors<S, D>(
		src: S[],
		callback: (item: S) => D|Error
): TArrayWithErrors<D> {
	const results: (D|Error)[] = [];
	
	for (const item of src) {
		try {
			const result: D|Error = callback(item);
			results.push(result);
		} catch (e) {
			if (commonsTypeIsError(e)) {
				results.push(e);
			} else {
				results.push(new Error(e as string));	// assume is a string
			}
		}
	}
	
	return commonsArraySplitMapErrors(results);
}

export function commonsArrayEmpty(array: unknown[]): void {
	if (array.length === 0) return;
	array.splice(0, array.length);
}

export function commonsArrayRemoveUndefineds<T>(array: (T|undefined)[]): T[] {
	return array
			.filter((item: T|undefined): boolean => item !== undefined)
			.map((item: T|undefined): T => item!);
}

export async function commonsArrayRemoveUndefinedsPromise<T>(array: Promise<(T|undefined)>[]): Promise<T[]> {
	return commonsArrayRemoveUndefineds(await Promise.all(array));
}

export function commonsArrayRandomSample<T>(
		array: T[],
		size: number
): T[] {
	// this does it inline removal rather than randomize and slice, as it means the order is maintained
	// it is probably much less efficient
	
	if (size === 0) return [];
	if (size >= array.length) return array;
	
	const clone: T[] = array.slice();
	while (clone.length > size) {
		const index: number = Math.floor(Math.random() * array.length);
		clone.splice(index, 1);
	}
	
	return clone;
}

export function commonsArrayBestItem<T>(
		array: T[],
		sortComparator: (a: T, b: T) => number
): T|undefined {
	if (array.length === 0) return undefined;

	const sorted: T[] = [ ...array ]
			.sort(sortComparator);
	
	return sorted[0];
}

export function commonsArrayRapidSortHead<T>(
		array: T[],
		length: number,
		isLess: (test: T, existing: T) => boolean
): T[] {
	const head: (T|undefined)[] = Array(length).fill(undefined) as undefined[];

	for (const a of array) {
		if (head[length - 1] !== undefined && !isLess(a, head[length - 1]!)) continue;	// greater than tail, so cannot be in head
		
		for (let i = 0; i < length; i++) {
			if (head[i] === undefined || isLess(a, head[i]!)) {
				head.splice(i, 0, a);
				head.pop();
				break;
			}
		}
	}

	const clean: T[] = head
			.filter((nearest: T|undefined): boolean => nearest !== undefined)
			.map((nearest: T|undefined): T => nearest!);

	return clean;
}
