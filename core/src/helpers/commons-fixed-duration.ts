import { COMMONS_REGEX_PATTERN_DURATION_DHIS, COMMONS_REGEX_PATTERN_DURATION_DHISMS, COMMONS_REGEX_PATTERN_DURATION_HISMS } from '../consts/commons-regex';

export function commonsFixedDurationIsdHis(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DURATION_DHIS);
	return regex.test(test);
}

export function commonsFixedDurationIsdHisMs(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DURATION_DHISMS);
	return regex.test(test);
}

export function commonsFixedDurationIsHisMs(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DURATION_HISMS);
	return regex.test(test);
}
