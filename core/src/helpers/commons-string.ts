import { COMMONS_ALPHABET_CHARACTERS, COMMONS_CONSONANT_CHARACTERS, COMMONS_DEFAULT_PUNCTUATION_CHARACTERS, COMMONS_VOWEL_CHARACTERS } from '../consts/commons-chars';

import { commonsTypeIsString } from './commons-type';

// Casing methods

export function commonsStringUcWords(value: string): string {
	// easier to do this via a loop rather than a regex

	let lastWord: boolean = false;
	return value
			.toString()
			.split('')
			.map((c: string): string => {
				const isWord: boolean = /^[A-Z]$/i.test(c);
				try {
					if (!isWord || lastWord) return c;
					return c.toUpperCase();
				} finally {
					lastWord = isWord;
				}
			})
			.join('');
}

export function commonsStringCamelCase(value: string, ucFirst: boolean = true): string {
	return value
			.toString()
			.replace(/([A-Z])/g, ' $1')
			.replace(/[^a-z0-9]/ig, ' ')
			.replace(/[ ]{2,}/, ' ')
			.trim()
			.split(' ')
			.map((s: string): string => s.trim())
			.filter((s: string): boolean => s !== '')
			.map((s: string, index: number): string => {
				if (index === 0 && !ucFirst) return s.toLowerCase();
				if (s.length < 2) return s.toUpperCase();
				return s.substring(0, 1).toUpperCase() + s.substring(1);
			})
			.join('');
}

function delimeterCase(value: string, delimeter: string) {
	return value
			.toString()
			.replace(/([A-Z])/g, ' $1')
			.replace(/[^a-z0-9]/ig, ' ')
			.replace(/[ ]{2,}/, ' ')
			.trim()
			.split(' ')
			.map((s: string): string => s.trim().toLowerCase())
			.filter((s: string): boolean => s !== '')
			.join(delimeter);
}

export function commonsStringSnakeCase(value: string): string {
	return delimeterCase(value, '_');
}

export function commonsStringKebabCase(value: string): string {
	return delimeterCase(value, '-');
}

export function commonsStringDashedCase(value: string): string {
	return commonsStringKebabCase(value);
}

export function commonsStringRegexEscapeString(term: string): string {
	return term
			.replace(/[.]/g, '\\.')
			.replace(/[?]/g, '[?]')
			.replace(/[*]/g, '[*]')
			.replace(/[+]/g, '[+]')
			.replace(/[(]/g, '\\(')
			.replace(/[)]/g, '\\)')
			.replace(/\[/g, '\\[)')
			.replace(/\]/g, '\\]')
			.replace(/[{]/g, '[{]')
			.replace(/[}]/g, '[}]');
}

// Simple character, word and sentence methods

export function commonsStringIsAlphabet(c: string): boolean {
	return c.length === 1
			&& COMMONS_ALPHABET_CHARACTERS.includes(c.toLowerCase());
}

export function commonsStringIsVowel(c: string): boolean {
	return commonsStringIsAlphabet(c)
			&& COMMONS_VOWEL_CHARACTERS.includes(c.toLowerCase());
}

export function commonsStringIsConsonant(c: string): boolean {
	return commonsStringIsAlphabet(c)
			&& COMMONS_CONSONANT_CHARACTERS.includes(c.toLowerCase());
}

export function commonsStringRegexLike(term: string): string {
	return `^${commonsStringRegexEscapeString(term)}$`
			.replace(/%/g, '(?:.|\\s)*');
}

export function commonsStringRtrim(value: string, characters: string = ' \r\n\0'): string {
	const chars: string[] = characters.split('');
	if (chars.length === 0) return value;
	
	while (value.length > 0 && chars.includes(value.substr(value.length - 1, 1))) {
		value = value.substr(0, value.length - 1);
	}
	
	return value;
}

export function commonsStringLtrim(value: string, characters: string = ' \r\n\0'): string {
	const chars: string[] = characters.split('');
	if (chars.length === 0) return value;
	
	while (value.length > 0 && chars.includes(value.substr(0, 1))) {
		value = value.substr(1);
	}
	
	return value;
}

export function commonsStringTrim(value: string, characters: string = ' \r\n\0'): string {
	return commonsStringLtrim(commonsStringRtrim(value, characters), characters);
}

export function commonsStringSplitString(value: string, splitter: string|RegExp = /[\r\n]/g, trim: boolean = true, excludeEmpty: boolean = true): string[] {
	return value
			.split(splitter)
			.map((s: string): string => trim ? s.trim() : s)
			.filter((s: string): boolean => !excludeEmpty || s !== '');
}

// Complex word and sentence methods

export function commonsStringLimitLength(value: string, length: number = 128, soft: boolean = false): string {
	if (value.length <= length) return value;

	let hard: string = value.substr(0, length - 3);	// -2 = make room for ...
	if (!soft) return `${hard}...`;

	const fallback: string = hard;
	
	while (hard.length > 1) {
		if (/[^a-zA-Z0-9]/.test(hard.slice(-1))) return `${hard.substr(0, hard.length - 1)}...`;
		hard = hard.substr(0, hard.length - 1);
	}
	
	return `${fallback}...`;
}

export function commonsStringIsCapitalised(value: string): boolean {
	return /^[A-Z]+$/.test(value.replace(/[^a-z]/i, ''));
}

function capitaliseString(value: string, capitalised: boolean): string {
	return capitalised ? value.toUpperCase() : value;
}

export function commonsStringPluralise(singular: string, quantity: number = 2, autoDetectEndings: boolean = true): string {
	if (singular.length === 0) return '';
	
	if (quantity === 1) return singular;
	if (quantity === 0) return commonsStringPluralise(singular, 2, autoDetectEndings);
	
	const capitalised: boolean = commonsStringIsCapitalised(singular);
	
	const last: string = singular.charAt(singular.length - 1);
	if (!/[a-z]/i.test(last)) {
		// doesn't end with a-z
		return `${singular}${capitaliseString('s', capitalised)}`;
	}
	
	if (autoDetectEndings) {
		const same: string[] = `
			accommodation advice alms aluminum ammends
			baggage barracks binoculars bison bourgeois breadfruit
			cannon caribou cattle chalk chassis chinos clippers clothes clothing cod concrete corps correspondence crossroads
			deer dice doldrums dozen dungarees
			education eggfruit elk eyeglasses
			flour food fruit furniture
			gallows glasses goldfish grapefruit greenfly grouse gymnastics
			haddock halibut headquarters help homework
			ides information insignia
			jackfruit jeans
			kennels knickers knowledge kudos
			leggings lego luggage
			means monkfish moose mullet music
			nailclippers news
			offspring oxygen
			pants passionfruit pike pliers police premises public pyjamas
			reindeer rendezvous
			salmon scenery scissors series shambles sheep shellfish shorts shrimp smithereens species squid staff starfruit sugar swine
			tongs trousers trout tuna tweezers
			wheat whitebait wood
			you
		`
				.replace(/\s+/, ' ')
				.split(' ')
				.map((word: string): string => commonsStringTrim(word))
				.filter((word: string): boolean => word !== '');
		if (same.includes(singular.toLowerCase())) return singular;

		switch (singular.toLowerCase()) {
			case 'cafe':	return capitaliseString('cafes', capitalised);
			case 'child':	return capitaliseString('children', capitalised);
			case 'woman':	return capitaliseString('women', capitalised);
			case 'man':	return capitaliseString('men', capitalised);
			case 'mouse':	return capitaliseString('mice', capitalised);
			case 'goose':	return capitaliseString('geese', capitalised);
			case 'potato':	return capitaliseString('potatoes', capitalised);
			case 'tomato':	return capitaliseString('tomatoes', capitalised);
			case 'fungus':	return capitaliseString('fungae', capitalised);
			
			case 'day':
			case 'monday':
			case 'tuesday':
			case 'wednesday':
			case 'thursday':
			case 'friday':
			case 'saturday':
			case 'sunday':
				return capitaliseString(`${singular.toLowerCase()}s`, capitalised);
		}
		
		if (/(craft|ies)$/.test(singular.toLowerCase())) return singular;

		if (/(ch|x|s|sh)$/i.test(singular)) return `${singular}${capitaliseString('es', capitalised)}`;

		const regex1: RegExpExecArray|null = /^(.+)(f|fe)$/i.exec(singular);
		if (regex1 !== null) return `${regex1[1]}${capitaliseString('ves', capitalised)}`;
		
		const regex2: RegExpExecArray|null = /^(.+[abcdfghjklmnpqrstvwxyz])y$/i.exec(singular);
		if (regex2 !== null) return `${regex2[1]}${capitaliseString('ies', capitalised)}`;
	}
	
	return `${singular}${capitaliseString('s', capitalised)}`;
}

export function commonsStringSingularise(plural: string): string {
	const capitalised: boolean = commonsStringIsCapitalised(plural);
	
	const last: string = plural.charAt(plural.length - 1);
	if (!/[a-z]/i.test(last)) {
		// doesn't end with a-z
		return plural;
	}

	if (plural.toLowerCase().endsWith('us')) return plural;
	if (plural.toLowerCase().endsWith('ess')) return plural;
	if (plural.toLowerCase().endsWith('is')) return plural;
	
	const same: string[] = `
		accommodation advice alms aluminum ammends
		baggage barracks binoculars bison bourgeois breadfruit
		cannon caribou cattle chalk chassis chinos clippers clothes clothing cod concrete corps correspondence crossroads
		deer dice doldrums dozen dungarees
		education eggfruit elk eyeglasses
		flour food fruit furniture
		gallows glasses goldfish grapefruit greenfly grouse gymnastics
		haddock halibut headquarters help homework
		ides information insignia
		jackfruit jeans
		kennels knickers knowledge kudos
		leggings lego luggage
		means monkfish moose mullet music
		nailclippers news
		offspring oxygen
		pants passionfruit pike pliers police premises public pyjamas
		reindeer rendezvous
		salmon scenery scissors series shambles sheep shellfish shorts shrimp smithereens species squid staff starfruit sugar swine
		tongs trousers trout tuna tweezers
		wheat whitebait wood
		you
	`
			.replace(/\s+/, ' ')
			.split(' ')
			.map((word: string): string => commonsStringTrim(word))
			.filter((word: string): boolean => word !== '');
	if (same.includes(plural.toLowerCase())) return plural;

	switch (plural.toLowerCase()) {
		case 'cafes':	return capitaliseString('cafe', capitalised);
		case 'children':	return capitaliseString('child', capitalised);
		case 'women':	return capitaliseString('woman', capitalised);
		case 'men':	return capitaliseString('man', capitalised);
		case 'mice':	return capitaliseString('mouse', capitalised);
		case 'geese':	return capitaliseString('goose', capitalised);
		case 'potatoes':	return capitaliseString('potato', capitalised);
		case 'tomatoes':	return capitaliseString('tomato', capitalised);
		case 'fungae':	return capitaliseString('fungus', capitalised);

		case 'days':
		case 'mondays':
		case 'tuesdays':
		case 'wednesdays':
		case 'thursdays':
		case 'fridays':
		case 'saturdays':
		case 'sundays':
			return capitaliseString(plural.substring(0, plural.length - 1).toLowerCase(), capitalised);
	}
		
	if (/(craft|ies)$/.test(plural.toLowerCase())) return plural;

	if (/(ch|x|s|sh)es$/i.test(plural)) return plural.substring(0, plural.length - 2);

	const regex1: RegExpExecArray|null = /^(.+)ves$/i.exec(plural);
	if (regex1 !== null) return `${regex1[1]}${capitaliseString('f', capitalised)}`;	// not ideal, as it conflates 'f' and 'fe' suffixes
	
	const regex2: RegExpExecArray|null = /^(.+[abcdfghjklmnpqrstvwxyz])ies$/i.exec(plural);
	if (regex2 !== null) return `${regex2[1]}${capitaliseString('y', capitalised)}`;

	if (!plural.toLowerCase().endsWith('s')) return plural;

	return plural.substring(0, plural.length - 1);
}

function noOrNumber(quantity: number, none: boolean = false, capitaliseNoNone: boolean = false): string {
	if (quantity === 0) {
		const wording: string = none ? 'none' : 'no';
		return capitaliseNoNone ? commonsStringUcWords(wording) : wording;
	}
	
	return quantity.toString();
}

export function commonsStringQuantify(singular: string, quantity: number, autoDetectEndings: boolean = true, capitaliseNoNone: boolean = false): string {
	if (singular.length === 0) return noOrNumber(quantity, true, capitaliseNoNone);
	
	const capitalised: boolean = commonsStringIsCapitalised(singular);

	return capitaliseString(`${noOrNumber(quantity, false, capitaliseNoNone)} ${commonsStringPluralise(singular, quantity, autoDetectEndings)}`, capitalised);
}

function isVowelOrY(c: string): boolean {
	return commonsStringIsVowel(c) || c.toLowerCase() === 'y';
}

function isConsonantNotY(c: string): boolean {
	return commonsStringIsConsonant(c) && c.toLowerCase() !== 'y';
}

export function commonsStringSplitWords(sentence: string, allowHyphens: boolean = true, allowApostrophies: boolean = false): string[] {
	if (!allowHyphens) sentence = sentence.replace('-', ' - ');
	if (!allowApostrophies) sentence = sentence.replace(`'`, ` ' `);	// eslint-disable-line @typescript-eslint/quotes
	
	return sentence
			.split(/[^-'A-Za-z]/)
			.map((word: string): string => commonsStringTrim(word))
			.filter((word: string): boolean => word !== '');
}

export function commonsStringInternalRoughSyllables(word: string): string[] {
	// assumes that syllables consist of vowels in the center of each syllable
	
	const chars: string[] = commonsStringTrim(word.toLowerCase()).split('');

	const stack: string[] = [];
	let allowConsume: boolean = false;
	let build: string = '';
	let reset: boolean = true;

	while (true) {
		const c: string|undefined = chars.shift();
		if (c === undefined) break;

		if (!commonsStringIsAlphabet(c)) {	// non-character
			if (build.length > 0) stack.push(build);
			build = '';
			reset = true;

			allowConsume = false;	// don't consume future single consonant
			continue;
		}

		const isVowel: boolean = isVowelOrY(c);

		if (allowConsume && isVowel) {	// double vowel; concatinate with last
			stack.push(`${stack.pop()!}${c}`);

			allowConsume = true;	// consume future single consonant
			continue;
		}

		if (allowConsume && !isVowel) {
			// append to last stacked word
			stack.push(`${stack.pop()!}${c}`);

			allowConsume = false;	// don't consume future single consonant
			continue;
		}

		if (isVowel) {
			if (chars.length === 0 && stack.length > 0 && !reset) {
				// absorb terminal vowels
				stack.push(`${stack.pop()!}${c}`);
				break;
			}

			stack.push(`${build}${c}`);
			build = '';

			allowConsume = true;
			continue;
		}

		build += c;
	}

	if (build !== '') {
		if (build.length > 1) {
			stack.push(build);
		} else {
			stack.push(`${stack.pop()!}${build}`);
		}
	}

	// attempt to make syllables start with consonants where possible
	for (let i = 1; i < stack.length; i++) {
		if (stack[i - 1].length < 2) continue;

		if (isVowelOrY(stack[i].charAt(0)) && isConsonantNotY(stack[i - 1].slice(-1))) {
			stack[i] = `${stack[i - 1].slice(-1)}${stack[i]}`;
			stack[i - 1] = stack[i - 1].slice(0, stack[i - 1].length - 1);
		}
	}

	return stack;
}

export function commonsStringRoughSyllables(wordOrWords: string): string[] {
	// assumes that syllables consist of vowels in the center of each syllable
	
	// it gets complicated trying to cater for multiple words, so we just split and process for each
	const total: string[] = [];
	
	for (const word of commonsStringSplitWords(wordOrWords)) {
		const syllables: string[] = commonsStringInternalRoughSyllables(word);
		for (const syllable of syllables) total.push(syllable);
	}
	
	return total;
}

export function commonsStringSplitSentences(paragraph: string, breakChars: RegExp = /[.!?]/, allowDecimals: boolean = true, breakOnMultipleHyphens: boolean = true): string[] {
	if (breakOnMultipleHyphens) {
		// multiple hyphens count as breaks; single count as hyphenated words
		paragraph = paragraph.replace(/-{2,}/g, '. ');
	}
	
	if (!breakChars.test('\r')) {
		paragraph = paragraph.replace(/\r/, ' ');
	}
	if (!breakChars.test('\n')) {
		paragraph = paragraph.replace(/\n/, ' ');
	}
	
	// We don't use split, as we want to preserve the break characters
	const chars: string[] = commonsStringTrim(paragraph).split('');

	const sentences: string[] = [];
	let build: string = '';
	while (true) {
		const c: string|undefined = chars.shift();
		if (c === undefined) break;
		
		if (breakChars.test(c)) {
			if (build === '' && sentences.length > 0) {
				// concatenate multiple break characters onto the end of the last sentence
				sentences.push(`${sentences.pop()!}${c}`);
				continue;
			}

			if (commonsStringTrim(build) === '') {
				// nothing to push
				build = '';
				continue;
			}
			
			if (
				!allowDecimals
				|| !/[-0-9]/.test(build.slice(-1))
				|| chars.length === 0
				|| !/[0-9]/.test(chars[0])
			) {
				sentences.push(`${commonsStringLtrim(build)}${c}`);
				build = '';
				continue;
			}
		}
		
		build += c;
	}
	if (commonsStringTrim(build) !== '') sentences.push(commonsStringTrim(build));

	return sentences;
}

export function commonsStringFleschKincaidReadingEase(paragraph: string): number {
	const words: string[] = commonsStringSplitWords(paragraph);
	const sentences: string[] = commonsStringSplitSentences(paragraph);
	const syllables: string[] = commonsStringRoughSyllables(paragraph);
	
	return 206.835 - (1.015 * (words.length / sentences.length)) - (84.6 * (syllables.length / words.length));
}

export function commonsStringAutomatedReadabilityIndex(paragraph: string): number {
	const words: string[] = commonsStringSplitWords(paragraph);
	const sentences: string[] = commonsStringSplitSentences(paragraph);
	
	const characters: string[] = [];
	for (const word of words) {
		for (const c of word.split('')) {
			if (commonsStringIsAlphabet(c)) characters.push(c);
		}
	}

	return (4.71 * (characters.length / words.length)) + (0.5 * (words.length / sentences.length)) - 21.43;
}

type TUnicodeCharsOptions = {
		trim: boolean;
		dashes: boolean;
		quotes: boolean;
		backTick: boolean;
		spaces: boolean;
		ellipsis: boolean;
		blank: boolean;
		bullets: boolean;
};

export function commonsStringSanitiseExtendedUnicodeChars(
		src: string,
		options: Partial<TUnicodeCharsOptions> = {}
): string {
	options = {
			trim: true,
			dashes: true,
			quotes: true,
			backTick: true,
			spaces: true,
			ellipsis: true,
			blank: true,
			bullets: true,
			...options
	};

	if (options.trim) src = src.trim();

	if (options.dashes) src = src.replace(/[-\u058a\u05be\u1400\u1806\u2010-\u2015\u2053\u207b\u208b\u2212\u2e17\u2e1a\u2e3a\u2e3b\u2e40\u2e5d\u301c\u3030\u30a0\ufe31\ufe32\ufe58\ufe63\uff0d]|\uD803\udead/g, '-');
	if (options.quotes) src = src.replace(/[\u2018\u2019\u201a\u201b]/g, '\'').replace(/[\u201c-\u201f]/g, '"');
	if (options.backTick) src = src.replace(/`/g, '\'');
	if (options.spaces) src = src.replace(/[\u00a0\u2000-\u200c\u202f\ufeff]/g, ' ');
	if (options.ellipsis) src = src.replace(/\u2026/, '...');
	if (options.blank) src = src.replace(/[\u200d\u2060]/g, '');
	if (options.bullets) src = src.replace(/[\u2022]/g, '*');

	return src;
}

export function commonsStringBuildPunctuationRegexString(chars: string|string[]): string {
	const charsArray: string[] = commonsTypeIsString(chars) ? chars.split('') : [ ...chars ];

	const hyphenIndex: number = charsArray.indexOf('-');
	if (hyphenIndex > -1) {
		charsArray.splice(hyphenIndex, 1);
		charsArray.unshift('-');
	}

	for (let i = charsArray.length; i-- > 0;) {
		if (charsArray[i] === ']') charsArray[i] = '\\]';
		if (charsArray[i] === '\\') charsArray[i] = '\\\\';
	}

	return `[${charsArray.join('')}]`;
}

export enum ECommonsTokeniseHyphenSplit {
		ALL = 'all',	// split on all hyphens
		NONE = 'none',	// never split
		MULTIPLE = 'multiple'	// split on multiple, e.g. She said--because she was minded to--that they would go there later.
}

export type TCommonsTokeniseOptions = {
		sanitiseExtendedUnicodeChars: Partial<Omit<TUnicodeCharsOptions, 'spaces'|'trim'>>;
		hyphenSplit: ECommonsTokeniseHyphenSplit;
		keepNonSpacedPunctuation: boolean;
		returnPunctuationAsWords: boolean;
};

export function commonsStringTokenise(
		text: string,
		puncChars: string|string[] = COMMONS_DEFAULT_PUNCTUATION_CHARACTERS,
		options: Partial<TCommonsTokeniseOptions> = {}
): string[] {
	options = {
			hyphenSplit: ECommonsTokeniseHyphenSplit.ALL,
			keepNonSpacedPunctuation: false,
			returnPunctuationAsWords: true,
			...options
	};
	options.sanitiseExtendedUnicodeChars = {
			dashes: false,
			quotes: false,
			backTick: false,
			ellipsis: false,
			blank: false,
			...options.sanitiseExtendedUnicodeChars
	};

	const PUNC_CHARS_ARRAY: string[] = commonsTypeIsString(puncChars) ? puncChars.split('') : [ ...puncChars ];
	
	switch (options.hyphenSplit) {
		case ECommonsTokeniseHyphenSplit.ALL:
			// add hyphens to the punctuation characters
			PUNC_CHARS_ARRAY.push('-');
			break;
		case ECommonsTokeniseHyphenSplit.MULTIPLE:
			// consider hyphens a non-punctuation character
			// but pre-expand any multiples in the text
			text = text.replace(/-{2,}/g, ' - ');
			break;
		default:
			// consider hyphens a non-punctuation character
			break;
	}

	const regexChars: string = commonsStringBuildPunctuationRegexString(puncChars);

	const ANY_PUNC: RegExp = new RegExp(`(${regexChars})`, 'g');
	const START_PUNC: RegExp = new RegExp(`(^|\\s)(${regexChars})`, 'g');
	const END_PUNC: RegExp = new RegExp(`(${regexChars})($|\\s)`, 'g');

	text = commonsStringSanitiseExtendedUnicodeChars(
			text.replace(/\s/g, ' '),
			{
					...options.sanitiseExtendedUnicodeChars,
					trim: true,
					spaces: true
			}
	);

	if (options.keepNonSpacedPunctuation) {
		text = text
				.replace(START_PUNC, ' $2 ')	// space before punc
				.replace(END_PUNC, ' $1 ');	// space after punc

		// have to correct for words that have multi-punctionation
		const rebuild: string[] = [];
		for (let word of text.split(' ')) {
			const tail: string[] = [];
			while (true) {
				if (word === '') break;

				if (PUNC_CHARS_ARRAY.includes(word[0])) {
					rebuild.push(word[0]);
					word = word.substring(1);
					continue;
				}

				if (PUNC_CHARS_ARRAY.includes(word[word.length - 1])) {
					tail.unshift(word[word.length - 1]);
					word = word.substring(0, word.length - 1);
					continue;
				}

				break;
			}
			
			rebuild.push(word);
			rebuild.push(...tail);
		}

		text = rebuild.join(' ');
	} else {
		text = text
				.replace(ANY_PUNC, ' $1 ');
	}

	return text
			.split(' ')
			.map((s: string): string => s.trim())
			.filter((s: string): boolean => {
				if (s === '') return false;
				if (PUNC_CHARS_ARRAY.includes(s)) return options.returnPunctuationAsWords || false;
				return true;
			});
}

export function commonsStringTokensToSentences(
		tokens: string[],
		terminalPuncChars: string|string[] = '.?!',
		allowDotAfterAbbreviatedWords: string[] = []
): string[][] {
	const TERMINAL_CHARS_ARRAY: string[] = commonsTypeIsString(terminalPuncChars) ? terminalPuncChars.split('') : [ ...terminalPuncChars ];

	const sentences: string[][] = [];
	let current: string[] = [];
	let lastWord: string|undefined;
	while (true) {
		const next: string|undefined = tokens.shift();
		if (!next) break;

		if (TERMINAL_CHARS_ARRAY.includes(next)) {
			if (next === '.' && lastWord && allowDotAfterAbbreviatedWords.includes(lastWord)) {
				// ignore words like Mr. Smith
				continue;
			}

			sentences.push([ ...current, next ]);
			current = [];
			lastWord = undefined;
			continue;
		}

		current.push(next);
		lastWord = next;
	}

	if (current.length > 0) sentences.push(current);

	return sentences;
}

export function commonsStringNormaliseAccents(s: string): string {
	return s.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

export function commonsStringGobblePrefix(
		s: string,
		gobbleUntil: string|RegExp,
		includeUntil: boolean = false
): string {
	if (gobbleUntil instanceof RegExp) {
		const clone: RegExp = new RegExp(gobbleUntil);

		let i: number = s.search(clone);
		if (i === -1) return s;

		if (!includeUntil) {
			const match: RegExpExecArray|null = clone.exec(s.substring(i));
			if (!match) throw new Error('Regex search found a match, but subsequent exec did not match. This may be a regex specific error');

			const matched: string = match[0];
			
			i += matched.length;
		}

		return s.substring(i);
	} else {
		let i: number = s.indexOf(gobbleUntil);
		if (i === -1) return s;

		if (!includeUntil) i += gobbleUntil.length;

		return s.substring(i);
	}
}

export function commonsStringGobbleSuffix(
		s: string,
		gobbleFrom: string|RegExp,
		includeFrom: boolean = false
): string {
	if (gobbleFrom instanceof RegExp) {
		const clone: RegExp = new RegExp(gobbleFrom);

		// this is a nasty inefficient way of doing it, but struggling to come up with a better way

		if (!clone.test(s)) return s;

		let i: number = s.length;
		while (true) {
			if (clone.test(s.substring(i))) break;
			i--;
			if (i < 0) return s;
		}

		const match: RegExpExecArray|null = clone.exec(s);
		if (!match) throw new Error('Test for suffix returned a gt 0 result but then that wasn\'t found in the regex exec. Possibly the regex is malformed?');

		if (includeFrom) i += match[0].length;

		return s.substring(0, i);
	} else {
		let i: number = s.lastIndexOf(gobbleFrom);
		if (i === -1) return s;

		if (includeFrom) i += gobbleFrom.length;

		return s.substring(0, i);
	}
}

export function commonsStringGobblePrefixAndSuffix(
		s: string,
		gobbleUntil: string|RegExp,
		gobbleFrom: string|RegExp,
		includeUntilAndFrom: boolean = false
): string {
	s = commonsStringGobblePrefix(s, gobbleUntil, includeUntilAndFrom);
	s = commonsStringGobbleSuffix(s, gobbleFrom, includeUntilAndFrom);

	return s;
}
