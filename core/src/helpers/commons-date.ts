import { TDateRange } from '../types/tdate-range';

import {
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_TIME_HIS,
		COMMONS_REGEX_PATTERN_DATETIME_DMYHI,
		COMMONS_REGEX_PATTERN_DATE_DMY,
		COMMONS_REGEX_PATTERN_TIME_HI,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED
} from '../consts/commons-regex';

import { CommonsTally } from '../classes/commons-tally';

import {
		commonsNumberFixedDigitRange,
		commonsNumberOrdinalUnit
} from './commons-number';

type TComponents = {
		y: string;
		m: string;
		d: string;
		h: string;
		i: string;
		s: string;
};

function parseIsoString(date: Date): TComponents {
	const match: RegExpMatchArray|null = date.toISOString().match(/^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})(?:\.[0-9]+)?Z$/);
	if (!match) throw new Error('Unable to parse ISO string');

	return {
			y: match[1],
			m: match[2],
			d: match[3],
			h: match[4],
			i: match[5],
			s: match[6]
	};
}

function fromDate(date: Date): TComponents {
	return {
			y: date.getFullYear().toString().padStart(4, '0'),
			m: (date.getMonth() + 1).toString().padStart(2, '0'),
			d: date.getDate().toString().padStart(2, '0'),
			h: date.getHours().toString().padStart(2, '0'),
			i: date.getMinutes().toString().padStart(2, '0'),
			s: date.getSeconds().toString().padStart(2, '0')
	};
}

export function commonsDateTruncateToSecond(date: Date, iso: boolean = false): void {
	if (iso) {
		date.setUTCMilliseconds(0);
	} else {
		date.setMilliseconds(0);
	}
}

export function commonsDateTruncateToMinute(date: Date, iso: boolean = false): void {
	commonsDateTruncateToSecond(date);

	if (iso) {
		date.setUTCSeconds(0);
	} else {
		date.setSeconds(0);
	}
}

export function commonsDateTruncateToHour(date: Date, iso: boolean = false): void {
	commonsDateTruncateToMinute(date);

	if (iso) {
		date.setUTCMinutes(0);
	} else {
		date.setMinutes(0);
	}
}

export function commonsDateTruncateToDate(date: Date, iso: boolean = false): void {
	commonsDateTruncateToHour(date);

	if (iso) {
		date.setUTCHours(0);
	} else {
		date.setHours(0);
	}
}

export function commonsDateQuantise(
		date: Date,
		millis: number,
		direction: 'down'|'up'|'closest'
): void {
	switch (direction) {
		case 'down':
			date.setTime(Math.floor(date.getTime() / millis) * millis);
			break;
		case 'up':
			date.setTime(Math.ceil(date.getTime() / millis) * millis);
			break;
		case 'closest':
			date.setTime(Math.round(date.getTime() / millis) * millis);
			break;
	}
}

export function commonsDateQuantiseRange(
		range: TDateRange,
		millis: number,
		direction: 'down'|'up'|'closest'|'inner'|'outer'
): void {
	switch (direction) {
		case 'down':
		case 'up':
		case 'closest':
			commonsDateQuantise(range.from, millis, direction);
			commonsDateQuantise(range.to, millis, direction);
			break;
		case 'inner':
			commonsDateQuantise(range.from, millis, 'up');
			commonsDateQuantise(range.to, millis, 'down');
			break;
		case 'outer':
			commonsDateQuantise(range.from, millis, 'down');
			commonsDateQuantise(range.to, millis, 'up');
			break;
	}
}

export function commonsDateTallyQuantisedRanges(
		ranges: TDateRange[],
		millis: number
): { date: Date; tally: number }[] {
	const clone: TDateRange[] = [ ...ranges ]
			.map((range: TDateRange): TDateRange => ({
					from: new Date(range.from.getTime()),
					to: new Date(range.to.getTime())
			}));

	const tally: CommonsTally<number> = new CommonsTally<number>();

	for (const range of clone) {
		commonsDateQuantiseRange(range, millis, 'outer');
		if (range.from.getTime() > range.to.getTime()) continue;

		for (let i = range.from.getTime(); i < range.to.getTime(); i += millis) {
			tally.increment(i);
		}
	}

	const array: { key: number; tally: number }[] = tally.asArray();
	if (array.length === 0) return [];

	array
			.sort((a: { key: number }, b: { key: number }): number => {
				if (a.key < b.key) return -1;
				if (a.key > b.key) return 1;
				return 0;
			});
	
	const result: { date: Date; tally: number }[] = [];
	for (let i = array[0].key; i <= array[array.length - 1].key; i += millis) {
		result.push({
				date: new Date(i),
				tally: tally.get(i) || 0
		});
	}

	return result;
}

export function commonsDatePivotNextDay(
		date: Date,
		pivotUntilHour: number,
		iso: boolean = false
): void {
	if (iso) {
		if (date.getUTCHours() < pivotUntilHour) date.setUTCDate(date.getUTCDate() + 1);
	} else {
		if (date.getHours() < pivotUntilHour) date.setDate(date.getDate() + 1);
	}
}

export function commonsDateAutoNextDay(
		prev: Date,
		date: Date,
		iso: boolean = false
): void {
	while (prev.getTime() > date.getTime()) {
		if (iso) {
			date.setUTCDate(date.getDate() + 1);
		} else {
			date.setDate(date.getDate() + 1);
		}
	}
}

export function commonsDateAutoNextDayRange(
		range: TDateRange,
		iso: boolean = false
): void {
	commonsDateAutoNextDay(range.from, range.to, iso);
}

export function commonsDateAutoPivotNextDayRange(
		range: TDateRange,
		pivotUntilHour: number,
		iso: boolean = false
): void {
	commonsDatePivotNextDay(range.from, pivotUntilHour, iso);
	commonsDatePivotNextDay(range.to, pivotUntilHour, iso);
	commonsDateAutoNextDayRange(range, iso);
}

export function commonsDateDateToYmd(date: Date, iso: boolean = false): string {
	const components: TComponents = iso ? parseIsoString(date) : fromDate(date);
	
	return `${components.y}-${components.m}-${components.d}`;
}

export function commonsDateDateTodmY(date: Date, iso: boolean = false): string {
	const components: TComponents = iso ? parseIsoString(date) : fromDate(date);
	
	return `${components.d}/${components.m}/${components.y}`;
}

export function commonsDateDateToHi(date: Date, iso: boolean = false): string {
	const components: TComponents = iso ? parseIsoString(date) : fromDate(date);
	
	return `${components.h}:${components.i}`;
}

export function commonsDateDateToHis(date: Date, iso: boolean = false): string {
	const components: TComponents = iso ? parseIsoString(date) : fromDate(date);
	
	return `${commonsDateDateToHi(date, iso)}:${components.s}`;
}

export function commonsDateDateToYmdHis(date: Date, iso: boolean = false): string {
	return `${commonsDateDateToYmd(date, iso)} ${commonsDateDateToHis(date, iso)}`;
}

export function commonsDateDateTodmYHi(date: Date, iso: boolean = false): string {
	return `${commonsDateDateTodmY(date, iso)} ${commonsDateDateToHi(date, iso)}`;
}

export function commonsDateIsYmdHis(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATETIME_YMDHIS);
	return regex.test(test);
}

export function commonsDateYmdHisToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATETIME_YMDHIS);
	if (match === null) throw new Error('Unable to parse date');
	
	if (iso) {
		return new Date(Date.parse(`${match[1]}-${match[2]}-${match[3]}T${match[4]}:${match[5]}:${match[6]}.000Z`));
	}
	
	const timestamp: Date = new Date();
	
	// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
	// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
	timestamp.setHours(12);	// midday is never affected by daylight saving
	timestamp.setDate(2);	// second is never affected by leap years and the like
	
	timestamp.setFullYear(parseInt(match[1], 10));
	timestamp.setMonth(parseInt(match[2], 10) - 1);
	timestamp.setDate(parseInt(match[3], 10));
	timestamp.setHours(parseInt(match[4], 10));
	timestamp.setMinutes(parseInt(match[5], 10));
	timestamp.setSeconds(parseInt(match[6], 10));
	commonsDateTruncateToSecond(timestamp);

	return timestamp;
}

export function commonsDateIsdmYHi(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATETIME_DMYHI);
	return regex.test(test);
}

export function commonsDatedmYHiToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATETIME_DMYHI);
	if (match === null) throw new Error('Unable to parse date');
	
	if (iso) {
		return new Date(Date.parse(`${match[3]}-${match[2]}-${match[1]}T${match[4]}:${match[5]}:00.000Z`));
	}
	
	const timestamp: Date = new Date();
	
	// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
	// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
	timestamp.setHours(12);	// midday is never affected by daylight saving
	timestamp.setDate(2);	// second is never affected by leap years and the like
	
	timestamp.setFullYear(parseInt(match[3], 10));
	timestamp.setMonth(parseInt(match[2], 10) - 1);
	timestamp.setDate(parseInt(match[1], 10));
	timestamp.setHours(parseInt(match[4], 10));
	timestamp.setMinutes(parseInt(match[5], 10));
	commonsDateTruncateToMinute(timestamp);

	return timestamp;
}

export function commonsDateIsYmd(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATE_YMD);
	return regex.test(test);
}

export function commonsDateIsdmY(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATE_DMY);
	return regex.test(test);
}

export function commonsDateYmdToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATE_YMD);
	if (match === null) throw new Error('Unable to parse date');
	
	if (iso) {
		return new Date(Date.parse(`${match[1]}-${match[2]}-${match[3]}T00:00:00.000Z`));
	}
	
	const timestamp: Date = new Date();
	
	// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
	// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
	timestamp.setHours(12);	// midday is never affected by daylight saving
	timestamp.setDate(2);	// second is never affected by leap years and the like
	
	timestamp.setFullYear(parseInt(match[1], 10));
	timestamp.setMonth(parseInt(match[2], 10) - 1);
	timestamp.setDate(parseInt(match[3], 10));
	commonsDateTruncateToDate(timestamp);

	return timestamp;
}

export function commonsDatedmYToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATE_DMY);
	if (match === null) throw new Error('Unable to parse date');
	
	if (iso) {
		return new Date(Date.parse(`${match[3]}-${match[2]}-${match[1]}T00:00:00.000Z`));
	}
	
	const timestamp: Date = new Date();
	
	// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
	// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
	timestamp.setHours(12);	// midday is never affected by daylight saving
	timestamp.setDate(2);	// second is never affected by leap years and the like
	
	timestamp.setFullYear(parseInt(match[3], 10));
	timestamp.setMonth(parseInt(match[2], 10) - 1);
	timestamp.setDate(parseInt(match[1], 10));
	commonsDateTruncateToDate(timestamp);

	return timestamp;
}

export function commonsDateIsHis(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_TIME_HIS);
	return regex.test(test);
}

export function commonsDateHisToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_TIME_HIS);
	if (match === null) throw new Error('Unable to parse date');
	
	if (iso) {
		return new Date(Date.parse(`1970-01-01T${match[1]}:${match[2]}:${match[3]}.000Z`));
	}
	
	const timestamp: Date = new Date();

	// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
	// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
	timestamp.setHours(12);	// midday is never affected by daylight saving
	timestamp.setDate(2);	// second is never affected by leap years and the like
	
	timestamp.setFullYear(1970);
	timestamp.setMonth(0);
	timestamp.setDate(1);
	timestamp.setHours(parseInt(match[1], 10));
	timestamp.setMinutes(parseInt(match[2], 10));
	timestamp.setSeconds(parseInt(match[3], 10));
	commonsDateTruncateToSecond(timestamp);

	return timestamp;
}

export function commonsDateIsHi(test: string): boolean {
	const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_TIME_HI);
	return regex.test(test);
}

export function commonsDateHiToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_TIME_HI);
	if (match === null) throw new Error('Unable to parse date');
	
	if (iso) {
		return new Date(Date.parse(`1970-01-01T${match[1]}:${match[2]}:00.000Z`));
	}
	
	const timestamp: Date = new Date();

	// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
	// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
	timestamp.setHours(12);	// midday is never affected by daylight saving
	timestamp.setDate(2);	// second is never affected by leap years and the like
	
	timestamp.setFullYear(1970);
	timestamp.setMonth(0);
	timestamp.setDate(1);
	timestamp.setHours(parseInt(match[1], 10));
	timestamp.setMinutes(parseInt(match[2], 10));
	commonsDateTruncateToMinute(timestamp);

	return timestamp;
}

export function commonsDateDateWithoutTime(date: Date, iso: boolean = false): Date {
	return commonsDateYmdToDate(commonsDateDateToYmd(date, iso), iso);
}

export function commonsDateDateWithTime(date: Date, time: Date, iso: boolean = false): Date {
	const ymd: string = commonsDateDateToYmd(date, iso);
	const his: string = commonsDateDateToHis(time, iso);
	
	return commonsDateYmdHisToDate(`${ymd} ${his}`);
}

export function commonsDateTodayTime(time: Date, iso: boolean = false): Date {
	return commonsDateDateWithTime(new Date(), time, iso);
}

export function commonsDateIsSameDate(a: Date, b: Date, iso: boolean = false): boolean {
	return commonsDateDateWithoutTime(a, iso).getTime() === commonsDateDateWithoutTime(b, iso).getTime();
}

export function commonsDateIsToday(test: Date, iso: boolean = false): boolean {
	return commonsDateIsSameDate(test, new Date(), iso);
}

export function commonsDateIsYesterday(test: Date, iso: boolean = false): boolean {
	const now: Date = new Date();
	now.setDate(now.getDate() - 1);
	
	return commonsDateIsSameDate(test, now, iso);
}

export function commonsDateIsTomorrow(test: Date, iso: boolean = false): boolean {
	const now: Date = new Date();
	now.setDate(now.getDate() + 1);
	
	return commonsDateIsSameDate(test, now, iso);
}

export function commonsDateIsLast24Hours(test: Date): boolean {
	const now: Date = new Date();
	const delta: number = now.getTime() - test.getTime();
	
	return delta < (24 * 60 * 60 * 1000);
}

export function commonsDateIsLeapYear(year: number): boolean {
	return new Date(year, 1, 29).getDate() === 29;
}

export function commonsDateListTextMonths(): string[] {
	return 'January,February,March,April,May,June,July,August,September,October,November,December'.split(',');
}

export function commonsDateGetTextMonth(month: number): string {
	if (month < 0 || month > 11) return '';
	
	return commonsDateListTextMonths()[month];
}

export function commonsDateGetNumericMonth(month: string): number {
	return commonsDateListTextMonths()
			.map((m: string): string => m.toUpperCase())
			.map((m: string): string => month.length === 3 ? m.substring(0, 3) : m)
			.indexOf(month.toUpperCase());
}

export function commonsDateListTextDays(): string[] {
	return 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday'.split(',');
}

export function commonsDateGetTextDay(day: number): string {
	if (day < 0 || day > 6) return '';
	
	return commonsDateListTextDays()[day];
}

export function commonsDateGetNumericDay(day: string): number {
	return commonsDateListTextDays()
			.map((m: string): string => m.toUpperCase())
			.map((m: string): string => day.length === 3 ? m.substring(0, 3) : m)
			.indexOf(day.toUpperCase());
}

export function commonsDateGetWeekDate(date: Date, startDay: number = 0): Date {
	if (startDay < 0 || startDay > 6) throw new Error('Trying to get week start for day outside 0-6');
	
	const clone: Date = new Date(date.getTime());
	commonsDateTruncateToDate(clone);
	
	while (clone.getDay() !== startDay) {
		clone.setDate(clone.getDate() - 1);
	}
	
	return clone;
}

export function commonsDateGetNumberOfDaysInMonth(month: number, year: number): number {
	switch (commonsDateGetTextMonth(month)) {
		case 'September':
		case 'April':
		case 'June':
		case 'November':
			return 30;
		case 'February':
			return commonsDateIsLeapYear(year) ? 29 : 28;
		default:
			return 31;
	}
}

export function commonsDateGetDayOfYear(date: Date): number {
	let days: number = 0;
	
	for (let i = 0; i < date.getMonth(); i++) {
		days += commonsDateGetNumberOfDaysInMonth(i, date.getFullYear());
	}
	
	days += date.getDate();
	
	return days;
}

export function commonsDateListTwoDigitDays(days: number): string[] {
	return commonsNumberFixedDigitRange(1, days, 2);
}

export function commonsDateDateToBashTimestamp(date: Date, iso: boolean = false): string {
	const parts: string[] = commonsDateDateTodmY(date, iso).split('/');
	
	return `${parts[0]} ${commonsDateGetTextMonth(date.getMonth()).toUpperCase().substring(0, 3)} ${parts[2]} ${commonsDateDateToHis(date)}`;
}

export function commonsDateDateToCompressed(date: Date, iso: boolean = false): string {
	return commonsDateDateToYmdHis(date, iso).replace(/[-: ]/g, '');
}

export function commonsDateCompressedToDate(date: string, iso: boolean = false): Date {
	const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED);
	if (match === null) throw new Error('Unable to parse compressed date');
	
	return commonsDateYmdHisToDate(`${match[1]}-${match[2]}-${match[3]} ${match[4]}:${match[5]}:${match[6]}`, iso);
}

export function commonsDateBuildDateFromYmdAndOptionals(
		ymd: string,
		hour?: number,
		minute?: number,
		second?: number,
		millisecond?: number,
		iso: boolean = false
): Date {
	const base: Date = commonsDateYmdToDate(ymd, iso);
	
	if (hour !== undefined) base.setHours(hour);
	if (minute !== undefined) base.setMinutes(minute);
	if (second !== undefined) base.setSeconds(second);
	if (millisecond !== undefined) base.setMilliseconds(millisecond);
	
	return base;
}

export function commonsDateGetPastTime(
		date: Date,
		time: Date
): Date {
	const clone: Date = new Date(date.getTime());
	
	clone.setHours(time.getHours());
	clone.setMinutes(time.getMinutes());
	clone.setSeconds(time.getSeconds());
	clone.setMilliseconds(time.getMilliseconds());
	
	if (clone.getTime() > date.getTime()) {
		// timestamp is 'yesterday'
		clone.setDate(clone.getDate() - 1);
	}
	
	return clone;
}

export function commonsDateBuildClosestDateFromTime(
		original: Date,
		hour: number,
		minute: number,
		second?: number,
		millisecond?: number,
		magic12Hours: boolean = false
): Date {
	if (magic12Hours && hour < 13) {
		const magicHour: number = (hour + 12) % 24;
		
		const normalResult: Date = commonsDateBuildClosestDateFromTime(
				original,
				hour,
				minute,
				second,
				millisecond
		);
		const magicResult: Date = commonsDateBuildClosestDateFromTime(
				original,
				magicHour,
				minute,
				second,
				millisecond
		);
		
		const normalDelta: number = Math.abs(original.getTime() - normalResult.getTime());
		const magicDelta: number = Math.abs(original.getTime() - magicResult.getTime());
		
		if (magicDelta < normalDelta) return magicResult;
		return normalResult;
	}
	
	const clone: Date = new Date(original.getTime());
	
	if (hour !== undefined) clone.setHours(hour);
	if (minute !== undefined) clone.setMinutes(minute);
	if (second !== undefined) clone.setSeconds(second);
	if (millisecond !== undefined) clone.setMilliseconds(millisecond);

	clone.setDate(clone.getDate() - 1);
	
	let closest: Date|undefined;
	let minDelta: number|undefined;
	for (let i = -1; i <= 1; i++) {
		const delta: number = Math.abs(clone.getTime() - original.getTime());
		if (minDelta === undefined || delta < minDelta) {
			closest = new Date(clone.getTime());
			minDelta = delta;
		}
		
		clone.setDate(clone.getDate() + 1);
	}
		
	if (!closest) throw new Error('Unable to match closest. This should not be possible');
		
	return closest;
}

// NB, this may not cope with daylight saving changes
export function commonsDateFillEmptyMinutes(
		timestamps: Date[],
		interval: number = 1
): Date[] {
	let min: Date|undefined;
	let max: Date|undefined;
	
	for (const t of timestamps) {
		if (min === undefined || t.getTime() < min.getTime()) min = new Date(t.getTime());
		if (max === undefined || t.getTime() > max.getTime()) max = new Date(t.getTime());
	}
	
	if (min === undefined || max === undefined) return [];

	commonsDateTruncateToMinute(min);
	
	const range: Date[] = [];
	while (min.getTime() <= max.getTime()) {
		range.push(new Date(min.getTime()));
		
		min.setMinutes(min.getMinutes() + interval);
	}
	
	return range;
}

// NB, this may not cope with daylight saving changes
export function commonsDateFillEmptyHours(timestamps: Date[]): Date[] {
	let min: Date|undefined;
	let max: Date|undefined;
	
	for (const t of timestamps) {
		if (min === undefined || t.getTime() < min.getTime()) min = new Date(t.getTime());
		if (max === undefined || t.getTime() > max.getTime()) max = new Date(t.getTime());
	}
	
	if (min === undefined || max === undefined) return [];

	commonsDateTruncateToHour(min);
	
	const range: Date[] = [];
	while (min.getTime() <= max.getTime()) {
		range.push(new Date(min.getTime()));
		
		min.setHours(min.getHours() + 1);
	}
	
	return range;
}

// NB, this may not cope with daylight saving changes
export function commonsDateFillEmptyDays(timestamps: Date[]): Date[] {
	let min: Date|undefined;
	let max: Date|undefined;
	
	for (const t of timestamps) {
		if (min === undefined || t.getTime() < min.getTime()) min = new Date(t.getTime());
		if (max === undefined || t.getTime() > max.getTime()) max = new Date(t.getTime());
	}
	
	if (min === undefined || max === undefined) return [];
	
	commonsDateTruncateToDate(min);
	
	const range: Date[] = [];
	while (min.getTime() <= max.getTime()) {
		range.push(new Date(min.getTime()));
		
		min.setDate(min.getDate() + 1);
	}
	
	return range;
}

export function commonsDateGetDateRangeFromDateArray(dates: Readonly<(Readonly<Date>)[]>): TDateRange {
	if (dates.length === 0) throw new Error('No dates to derive range from');
	
	const min: Date = new Date(dates[0].getTime());
	const max: Date = new Date(dates[0].getTime());
	
	for (const date of dates.slice(1)) {
		if (date.getTime() < min.getTime()) min.setTime(date.getTime());
		if (date.getTime() > max.getTime()) max.setTime(date.getTime());
	}
	
	return {
			from: min,
			to: max
	};
}

export function commonsDateGetDateArrayFromDateRange(range: Readonly<TDateRange>): Date[] {
	const dates: Date[] = [ range.from, range.to ];
	
	return commonsDateFillEmptyDays(dates);
}

export function commonsDateGetHourArrayFromDateRange(range: Readonly<TDateRange>): Date[] {
	const dates: Date[] = [ range.from, range.to ];
	
	return commonsDateFillEmptyHours(dates);
}

export function commonsDatePhpDate(format: string, date?: Date, iso: boolean = false): string {
	if (!date) date = new Date();
	
	const components: TComponents = iso ? parseIsoString(date) : fromDate(date);

	const fixedDate: Date = date;
	return format
			.split('')
			.map((c: string): string => {
				switch (c) {
					case 'd':
						return components.d;
					case 'D':
						return commonsDateGetTextDay(fixedDate.getDay()).substr(0, 3);
					case 'j':
						return parseInt(components.d, 10).toString(10);
					case 'l':
						return commonsDateGetTextDay(fixedDate.getDay());
					case 'S':
						return commonsNumberOrdinalUnit(parseInt(components.d, 10));
					case 'w':
						return fixedDate.getDay().toString(10);
					case 'F':
						return commonsDateGetTextMonth(parseInt(components.m, 10) - 1);
					case 'm':
						return components.m;
					case 'M':
						return commonsDateGetTextMonth(parseInt(components.m, 10) - 1).substr(0, 3);
					case 'n':
						return parseInt(components.m, 10).toString(10);
					case 'Y':
						return components.y;
					case 'y':
						return components.y.substr(2);
					case 'a': {
						const h: number = parseInt(components.h, 10);
						return h < 12 ? 'am' : 'pm';
					}
					case 'A': {
						const h: number = parseInt(components.h, 10);
						return h < 12 ? 'AM' : 'PM';
					}
					case 'g': {
						const h: number = parseInt(components.h, 10) % 12;
						if (h === 0) return '12';
						return h.toString(10);
					}
					case 'G':
						return parseInt(components.h, 10).toString(10);
					case 'h': {
						const h: number = parseInt(components.h, 10) % 12;
						if (h === 0) return '12';
						return h.toString(10).padStart(2, '0');
					}
					case 'H':
						return components.h;
					case 'i':
						return components.i;
					case 's':
						return components.s;
					default:
						return c;
				}
			})
			.join('');
}

export function commonsDateGetDeltaHours(range: TDateRange, inclusive: boolean = false): number {
	const from: Date = new Date(range.from.getTime());
	const to: Date = new Date(range.to.getTime());
	
	commonsDateTruncateToHour(from);
	commonsDateTruncateToHour(to);
	
	// we use this slightly ineffecient way to avoid BST
	let count: number = 0;
	while (from.getTime() < to.getTime()) {
		count++;
		from.setHours(from.getHours() + 1);
	}
	
	return count + (inclusive ? 1 : 0);
}

export function commonsDateGetDeltaDays(range: TDateRange, inclusive: boolean = false, iso: boolean = false): number {
	const from: Date = commonsDateDateWithoutTime(range.from, iso);
	const to: Date = commonsDateDateWithoutTime(range.to, iso);
	
	// we use this slightly ineffecient way to avoid BST
	let count: number = 0;
	while (from.getTime() < to.getTime()) {
		count++;
		from.setDate(from.getDate() + 1);
	}
	
	return count + (inclusive ? 1 : 0);
}

export function commonsDateGetDeltaMonths(range: TDateRange, inclusive: boolean = false, iso: boolean = false): number {
	const from: Date = commonsDateDateWithoutTime(range.from, iso);
	const to: Date = commonsDateDateWithoutTime(range.to, iso);

	from.setDate(1);
	to.setDate(1);
	
	// we use this slightly ineffecient way to avoid BST
	let count: number = 0;
	while (from.getTime() < to.getTime()) {
		count++;
		from.setMonth(from.getMonth() + 1);
	}
	
	return count + (inclusive ? 1 : 0);
}

export function commonsDateGetDeltaYears(range: TDateRange, inclusive: boolean = false, iso: boolean = false): number {
	const from: Date = commonsDateDateWithoutTime(range.from, iso);
	const to: Date = commonsDateDateWithoutTime(range.to, iso);

	from.setDate(1);
	from.setMonth(0);
	to.setDate(1);
	to.setMonth(0);
	
	// we use this slightly ineffecient way to avoid BST
	let count: number = 0;
	while (from.getTime() < to.getTime()) {
		count++;
		from.setFullYear(from.getFullYear() + 1);
	}
	
	return count + (inclusive ? 1 : 0);
}

export function commonsDatePrettyDate(
		date: Date,
		absolute: boolean = false,
		now?: Date,
		includeYear: boolean = true,
		iso: boolean = false
): string {
	if (!now) now = new Date();

	if (!absolute) {	// default enabled
		const clone: Date = new Date(now.getTime());
		
		if (commonsDateIsSameDate(date, clone)) return 'Today';
		
		clone.setDate(now.getDate() - 1);
		if (commonsDateIsSameDate(date, clone)) return 'Yesterday';
		
		clone.setDate(now.getDate() + 1);
		if (commonsDateIsSameDate(date, clone)) return 'Tomorrow';
	}
	
	return commonsDatePhpDate(
			`D d M${includeYear ? ' Y' : ''}`,
			date,
			iso
	);
}

function getDeltaSeconds(
		timestamp: Date,
		future: Date
): number|undefined {
	const delta: number = (future.getTime() - timestamp.getTime()) / 1000;
	
	if (delta < 0) return undefined;
	
	return delta;
}

export function commonsDatePrettyTime(
		time: Date,
		absolute: boolean = false,
		now?: Date,
		iso: boolean = false
): string {
	if (!now) now = new Date();

	if (!absolute) {	// default enabled
		const seconds: number|undefined = getDeltaSeconds(time, now);
		
		if (seconds !== undefined) {
			if (seconds < 60) return 'Just now';

			if (seconds < 120) return '1 min ago';
			
			if (seconds < (10 * 60)) {
				const mins: number = Math.floor(seconds / 60);
				return `${mins} mins ago`;
			}
		}
	}
		
	return commonsDatePhpDate(
			'H:i',
			time,
			iso
	);
}

export function commonsDatePrettyDateTime(
		timestamp: Date,
		absolute: boolean = false,
		now?: Date,
		includeYear: boolean = true,
		iso: boolean = false
): string {
	if (!now) now = new Date();

	if (!absolute) {	// default enabled
		const seconds: number|undefined = getDeltaSeconds(timestamp, now);
		
		if (seconds !== undefined && seconds < (10 * 60)) {
			return commonsDatePrettyTime(
					timestamp,
					false,
					now,
					iso
			);
		}
	}
	
	return `${commonsDatePrettyDate(timestamp, absolute, now, includeYear, iso)} ${commonsDatePrettyTime(timestamp, absolute, now, iso)}`;
}

export function commonsDatePrettyTimeRange(
		range: TDateRange,
		absolute: boolean = true,	// inverse
		now?: Date,
		iso: boolean = false
): string {
	const sFrom: string = commonsDatePrettyTime(
			range.from,
			absolute,
			now,
			iso
	);
	const sTo: string = commonsDatePrettyTime(
			range.to,
			absolute,
			now,
			iso
	);
	
	if (commonsDateIsSameDate(range.from, range.to) && sFrom === sTo) return sFrom;
	
	return `${sFrom} - ${sTo}`;
}

export function commonsDatePrettyDateTimeRange(
		range: TDateRange,
		absolute?: boolean,	// not default value, so can apply the different defaults in prettyDate and prettyTimeRange
		now?: Date,
		includeYear: boolean = true,
		iso: boolean = false
): string {
	const absoluteDate: boolean = absolute === undefined ? false : absolute;
	const absoluteTime: boolean = absolute === undefined ? true : absolute;

	if (commonsDateIsSameDate(range.from, range.to)) {
		return `${commonsDatePrettyDate(range.from, absoluteDate, now, includeYear, iso)} ${commonsDatePrettyTimeRange(range, absoluteTime, now, iso)}`;
	}
	
	return `${commonsDatePrettyDate(range.from, absoluteDate, now, includeYear, iso)} ${commonsDatePrettyTime(range.from, absoluteTime, now, iso)} - ${commonsDatePrettyDate(range.to, absoluteDate, now, includeYear, iso)} ${commonsDatePrettyTime(range.to, absoluteTime, now, iso)}`;
}

export function commonsDateSort(values: Date[]): Date[] {
	return values
			.sort((a: Date, b: Date): number => a.getTime() - b.getTime());
}

export function commonsDateFakeUTCNow(): Date {
	const date: Date = new Date();

	date.setUTCMilliseconds(date.getMilliseconds());
	date.setUTCSeconds(date.getSeconds());
	date.setUTCMinutes(date.getMinutes());
	date.setUTCHours(date.getHours());
	date.setUTCDate(date.getDate());
	date.setUTCMonth(date.getMonth());
	date.setUTCFullYear(date.getFullYear());

	return date;
}
