import { TMapRank } from '../types/tmap-rank';

import { EMapRankDirection } from '../enums/emap-rank-direction';

export function commonsMapRank<T>(
		data: Map<T, number>,
		direction: EMapRankDirection = EMapRankDirection.DESC,
		limit?: number
): TMapRank<T>[] {
	const sortable: TMapRank<T>[] = Array.from(data.keys())
			.map((key: T): TMapRank<T> => ({
					item: key,
					rank: data.get(key) || 0
			}));

	sortable
			.sort((a: TMapRank<T>, b: TMapRank<T>): number => {
				if (a.rank < b.rank) return -1;
				if (a.rank > b.rank) return 1;
				return 0;
			});
	
	if (direction) sortable.reverse();

	if (limit !== undefined) {
		while (sortable.length > limit) sortable.pop();
	}

	return sortable;
}

export function commonsMapRankBest<T>(
		data: Map<T, number>,
		direction: EMapRankDirection = EMapRankDirection.DESC
): T|undefined {
	const mapRank: TMapRank<T>[] = commonsMapRank(data, direction, 1);

	return mapRank.length === 0 ? undefined : mapRank[0].item;
}

// Object.fromEntries is not available until ES2019, which is a bit too recent at the moment
export function commonsMapToObject<K extends string = string, V = unknown>(map: Map<K, V>): { [ key in K ]: V } {
	const object: { [ key: string ]: V } = {};

	for (const [ k, v ] of map.entries()) object[k] = v;

	return object as { [ key in K ]: V };
}

export function commonsObjectToMap<K extends string = string, V = unknown>(object: { [ key: string ]: V }): Map<K, V> {
	return new Map<K, V>(Object.entries(object) as [ K, V ][]);
}
