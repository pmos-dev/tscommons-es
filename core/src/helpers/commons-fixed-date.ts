import { CommonsTally } from '../classes/commons-tally';
import { CommonsFixedDate } from '../classes/commons-fixed-date';
import { CommonsFixedDateRange, TCommonsFixedDateRange } from '../classes/commons-fixed-date-range';
import { CommonsFixedDuration } from '../classes/commons-fixed-duration';

export function commonsFixedDateTallyQuantisedRanges(
		ranges: TCommonsFixedDateRange[],
		quantise: CommonsFixedDuration,
		direction: 'down'|'up'|'closest'|'inner'|'outer' = 'outer'
): { date: CommonsFixedDate; tally: number }[] {
	const tally: CommonsTally<string> = new CommonsTally<string>();

	for (const range of ranges) {
		const r: CommonsFixedDateRange = new CommonsFixedDateRange(range.from.clone, range.to.clone);
		r.quantiseToSeconds(quantise.seconds, direction);

		if (r.isReversed) continue;

		const period: CommonsFixedDate[] = r.quantisedFixedDatesBetween(quantise);
		for (const p of period) tally.increment(p.YmdHis);
	}

	const array: { key: string; tally: number }[] = tally.asArray();
	if (array.length === 0) return [];

	array
			.sort((a: { key: string }, b: { key: string }): number => {
				if (a.key < b.key) return -1;
				if (a.key > b.key) return 1;
				return 0;
			});

	const resultRange: CommonsFixedDateRange = new CommonsFixedDateRange(
			CommonsFixedDate.fromYmdHis(array[0].key),
			CommonsFixedDate.fromYmdHis(array[array.length - 1].key)
	);
	const result: { date: CommonsFixedDate; tally: number }[] = resultRange.quantisedFixedDatesBetween(quantise, true)
			.map((p: CommonsFixedDate): { date: CommonsFixedDate; tally: number } => ({
					date: p.clone,
					tally: tally.get(p.YmdHis) || 0
			}));

	return result;
}

export type TCommonsFixedDateDowLine = [ CommonsFixedDate|undefined, CommonsFixedDate|undefined, CommonsFixedDate|undefined, CommonsFixedDate|undefined, CommonsFixedDate|undefined, CommonsFixedDate|undefined, CommonsFixedDate|undefined ];
export type TCommonsFixedDateDowLineOrMonth = TCommonsFixedDateDowLine|CommonsFixedDate;

export function commonsFixedDateSpreadToCalendar(
		dates: CommonsFixedDate[],
		startDow: number = 0,
		includeMonthRows: boolean = false
): (TCommonsFixedDateDowLine[])|(TCommonsFixedDateDowLineOrMonth[]) {
	const rows: (TCommonsFixedDateDowLine[])|(TCommonsFixedDateDowLineOrMonth[]) = [];
	let row: (CommonsFixedDate|undefined)[] = [];

	for (const date of dates) {
		const truncated: CommonsFixedDate = date.clone;
		truncated.truncateToYmd();

		if (truncated.day === 1) {
			if (row.length > 0) {
				while (row.length < 7) row.push(undefined);
				rows.push(row as TCommonsFixedDateDowLine);
				row = [];
			}

			if (includeMonthRows) {
				(rows as TCommonsFixedDateDowLineOrMonth[]).push(truncated);
			}
		}

		while (row.length < (truncated.dow - startDow)) row.push(undefined);
		if (truncated.dow === startDow) {
			if (row.length > 0) rows.push(row as TCommonsFixedDateDowLine);
			row = [];
		}

		row.push(truncated);
	}

	if (row.length > 0) {
		while (row.length < 7) row.push(undefined);
		rows.push(row as TCommonsFixedDateDowLine);
	}

	return rows;
}
