export function commonsEncodingUnicodeToAscii(unicode: string): string {
	return unicode.replace(/[\u007F-\uFFFF]/g, (c: string): string => '\\u' + `0000${c.charCodeAt(0).toString(16)}`.substr(-4));
}

export function commonsEncodingAsciiToUnicode(ascii: string): string {
	return ascii.replace(/\\u[0-9a-f]{4}/g, (u: string): string => String.fromCharCode(parseInt(u.substr(-4), 16)));
}

export function commonsEncodingAsciiToHex(ascii: string): string {
	return ascii.split('')
			.map((c: string): string => `0${c.charCodeAt(0).toString(16)}`.slice(-2))
			.join('');
}

export function commonsEncodingHexToAscii(hex: string): string {
	if (hex.length < 2) return '';
	return hex.match(/.{2}/g)!
			.map((h: string): string => String.fromCharCode(parseInt(h, 16)))
			.join('');
}

export function commonsEncodingUnicodeToHex(unicode: string): string {
	return commonsEncodingAsciiToHex(commonsEncodingUnicodeToAscii(unicode));
}

export function commonsEncodingHexToUnicode(hex: string): string {
	return commonsEncodingAsciiToUnicode(commonsEncodingHexToAscii(hex));
}
