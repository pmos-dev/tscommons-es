export function commonsMatrixBuild<T>(
		r: number,
		c: number,
		defaultValue?: T
): T[][] {
	const row: T[] = Array<T>(c).fill(defaultValue as any as T);
	
	// we have to do this as a map rather than a fill so that the fill doesn't create references to identical row object
	return Array<unknown[]>(r).fill(undefined as any as unknown[])
			.map((_: unknown): T[] => row.slice());
}

function buildRotatedMatrixHolder<T>(src: T[][]): T[][] {
	return commonsMatrixBuild<T>(src[0].length, src.length);
}

export function commonsMatrixRotateRight<T>(matrix: T[][]): T[][] {
	const dest: T[][] = buildRotatedMatrixHolder(matrix);
	
	for (let sr = matrix.length, dc = 0; sr-- > 0; dc++) {
		for (let sc = 0, dr = 0; sc < matrix[0].length; dr++, sc++) {
			dest[dr][dc] = matrix[sr][sc];
		}
	}
	
	return dest;
}

export function commonsMatrixRotateLeft<T>(matrix: T[][]): T[][] {
	const dest: T[][] = buildRotatedMatrixHolder(matrix);
	
	for (let sr = 0, dc = 0; sr < matrix.length; sr++, dc++) {
		for (let sc = 0, dr = matrix[0].length - 1; sc < matrix[0].length; sc++, dr--) {
			dest[dr][dc] = matrix[sr][sc];
		}
	}
	
	return dest;
}

export function commonsMatrixMirror<T>(matrix: T[][]): T[][] {
	const dest: T[][] = buildRotatedMatrixHolder(matrix);
	
	for (let r = matrix.length; r-- > 0;) {
		for (let c = matrix[0].length; c-- > 0;) {
			dest[c][r] = matrix[r][c];
		}
	}
	
	return dest;
}

export function commonsMatrixFlip<T>(
		matrix: T[][],
		horizontal: boolean,
		vertical: boolean
): T[][] {
	const dest: T[][] = commonsMatrixBuild(matrix.length, matrix[0].length);
	
	for (
		let sr = matrix.length,
			dr = vertical ? 0 : (matrix.length - 1);
		sr-- > 0;
		dr += vertical ? 1 : -1
	) {
		for (
			let sc = matrix[0].length,
				dc = horizontal ? 0 : (matrix[0].length - 1);
			sc-- > 0;
			dc += horizontal ? 1 : -1
		) {
			dest[dr][dc] = matrix[sr][sc];
		}
	}
	
	return dest;
}
