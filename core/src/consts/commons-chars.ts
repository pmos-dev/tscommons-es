export const COMMONS_DEFAULT_PUNCTUATION_CHARACTERS: string = '.?!,;:()[]{}"';

export const COMMONS_ALPHABET_CHARACTERS: string[] = 'abcdefghijklmnopqrstuvwxyz'.split('');
export const COMMONS_VOWEL_CHARACTERS: string[] = 'aeiou'.split('');
export const COMMONS_CONSONANT_CHARACTERS: string[] = COMMONS_ALPHABET_CHARACTERS
		.filter((c: string): boolean => !COMMONS_VOWEL_CHARACTERS.includes(c));
