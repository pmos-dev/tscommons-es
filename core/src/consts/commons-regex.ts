export const COMMONS_REGEX_PATTERN_NUMERIC: RegExp = /^-?[0-9]{1,10}(\.[0-9]{1,64})?$/;
export const COMMONS_REGEX_PATTERN_INT: RegExp = /^-?[0-9]{1,10}$/;
export const COMMONS_REGEX_PATTERN_ID: RegExp = /^[0-9]{1,8}$/;
export const COMMONS_REGEX_PATTERN_DIGIT: RegExp = /^[0-9]$/;
	
export const COMMONS_REGEX_PATTERN_ID_CHAR: RegExp = /^[a-z0-9'#]$/i;
export const COMMONS_REGEX_PATTERN_ID_NAME: RegExp = /^[a-z0-9()'#]([-a-z0-9()'_#. ]{0,62}[-a-z0-9()'_#.])?$/i;
export const COMMONS_REGEX_PATTERN_LETTER: RegExp = /^[A-Z]$/;

export const COMMONS_REGEX_PATTERN_LETTERDIGIT: RegExp = /^[A-Z0-9]$/;
	
export const COMMONS_REGEX_PATTERN_TEXT_255: RegExp = /^.{1,255}$/;

export const COMMONS_REGEX_PATTERN_MD5: RegExp = /^[a-f0-9]{32}$/i;
export const COMMONS_REGEX_PATTERN_SHA224: RegExp = /^[a-f0-9]{56}$/i;
export const COMMONS_REGEX_PATTERN_SHA256: RegExp = /^[a-f0-9]{64}$/i;
export const COMMONS_REGEX_PATTERN_SHA512: RegExp = /^[a-f0-9]{128}$/i;
export const COMMONS_REGEX_PATTERN_BASE36: RegExp = /^[0-9A-Z]+$/i;	// this can be case insensitive because the MySQL = is itself case insensitive!
export const COMMONS_REGEX_PATTERN_BASE36_ID: RegExp = /^[0-9A-Z]{10}$/i;	// this can be case insensitive because the MySQL = is itself case insensitive!
export const COMMONS_REGEX_PATTERN_BASE41: RegExp = /^[0-9A-Za-e]+$/;
export const COMMONS_REGEX_PATTERN_BASE41_ID: RegExp = /^[0-9A-Za-e]{6}$/;
export const COMMONS_REGEX_PATTERN_BASE62: RegExp = /^[0-9A-Za-z]+$/;
export const COMMONS_REGEX_PATTERN_BASE62_ID: RegExp = /^[0-9A-Za-z]{8}$/;
export const COMMONS_REGEX_PATTERN_BASE62_LONG_ID: RegExp = /^[0-9A-Za-z]{32}$/;
	
export const COMMONS_REGEX_PATTERN_HEXCOLOR: RegExp = /^[a-f0-9]{6}$/i;
export const COMMONS_REGEX_PATTERN_CSS_COLOR: RegExp = /^#([0-9a-f]{3}){1,2}$/i;
export const COMMONS_REGEX_PATTERN_RGB_COLOR: RegExp = /^#([0-9a-f]{2}){3}$/i;
export const COMMONS_REGEX_PATTERN_RGBA_COLOR: RegExp = /^#([0-9a-f]{2}){4}$/i;

export const COMMONS_REGEX_PATTERN_DATE_DMY: RegExp = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
export const COMMONS_REGEX_PATTERN_DATETIME_DMYHI: RegExp = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4}) ([0-9]{2}):([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_TIME_HI: RegExp = /^([0-9]{2}):([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_DATE_YMD: RegExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_DATETIME_YMDHIS: RegExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_TIME_HIS: RegExp = /^([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED: RegExp = /^([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_DATE_ECMA: RegExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})(\.[0-9]{3})?[A-Z]$/;

export const COMMONS_REGEX_PATTERN_DURATION_DHIS: RegExp = /^([0-9]{1,5}):([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
export const COMMONS_REGEX_PATTERN_DURATION_DHISMS: RegExp = /^([0-9]{1,5}):([0-9]{2}):([0-9]{2}):([0-9]{2})\.([0-9]{1,8})$/;
export const COMMONS_REGEX_PATTERN_DURATION_HISMS: RegExp = /^([0-9]{2}):([0-9]{2}):([0-9]{2})\.([0-9]{1,8})$/;

export const COMMONS_REGEX_PATTERN_EMAIL: RegExp = /^[-a-z0-9._%+']{1,64}@([-a-z0-9]{1,64}\.)*[-a-z]{2,12}$/i;
export const COMMONS_REGEX_PATTERN_PHONE: RegExp = /^[+()0-9#*]?[- +()0-9#*]*[0-9#*]$/;

export const COMMONS_REGEX_PATTERN_URL: RegExp = /^http(s)?:\/\/([a-z0-9][-a-z0-9]{0,62})(\.[a-z0-9][-a-z0-9]{0,62}){0,32}(:[0-9]+)?(\/[-a-z0-9\/._?&=!~*'():#\[\]@$+,;%]{0,940})?$/i;
export const COMMONS_REGEX_PATTERN_IP: RegExp = /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/i;

// tslint:disable-next-line:variable-name
export const COMMONS_REGEX_PATTERN_IPv4: RegExp = /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/i;
// tslint:disable-next-line:variable-name
export const COMMONS_REGEX_PATTERN_IPv6: RegExp = /^((([0-9a-f]{1,4}:){7,7}[0-9a-f]{1,4})|(([0-9a-f]{1,4}:){1,7}:)|(([0-9a-f]{1,4}:){1,6}:[0-9a-f]{1,4})|(([0-9a-f]{1,4}:){1,5}(:[0-9a-f]{1,4}){1,2})|(([0-9a-f]{1,4}:){1,4}(:[0-9a-f]{1,4}){1,3})|(([0-9a-f]{1,4}:){1,3}(:[0-9a-f]{1,4}){1,4})|(([0-9a-f]{1,4}:){1,2}(:[0-9a-f]{1,4}){1,5})|([0-9a-f]{1,4}:((:[0-9a-f]{1,4}){1,6}))|(:((:[0-9a-f]{1,4}){1,7}|:)))$/i;
