export type TArrayWithErrors<T> = {
		errors: Error[];
		array: T[];
};
