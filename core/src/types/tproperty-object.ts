export type TPropertyObject<T = any> = {
		[ name: string ]: T;
};
