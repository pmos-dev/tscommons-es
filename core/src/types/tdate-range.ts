import { commonsTypeHasPropertyDate } from '../helpers/commons-type';

export type TDateRange = {
		from: Date;
		to: Date;
};

export function isTDateRange(test: unknown): test is TDateRange {
	if (!commonsTypeHasPropertyDate(test, 'from')) return false;
	if (!commonsTypeHasPropertyDate(test, 'to')) return false;
	
	return true;
}
