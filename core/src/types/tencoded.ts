import { TPrimative, TPrimativeArray, TPrimativeObject } from './tprimative';

export type TEncoded = TPrimative|TPrimativeArray|TPrimativeObject|TEncodedArray|TEncodedObject|null;

export type TEncodedArray = TEncoded[];

// Annoyingly using TPropertyObject<TEncoded> causes a circular reference complaint, even though TS should allow circular types.
// Perhaps the circularity needs to occur in the same file?
export type TEncodedObject = {
		[ name: string ]: TEncoded;
};
