import { TPrimative } from './tprimative';

export type TDefined = TPrimative|{ [ key: string ]: unknown }|null;
