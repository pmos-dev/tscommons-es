export type TEnumObject<E extends string, T> = {
		[key in E]: T;
};
