import { commonsTypeIsT, commonsTypeIsTArray } from '../helpers/commons-type';

export type TTOrTArray<T> = T|T[];

export function isTOrTArray<T>(
		test: unknown,
		isT: (t: unknown) => t is T
): test is TTOrTArray<T> {
	if (commonsTypeIsT<T>(test, isT)) return true;
	if (commonsTypeIsTArray<T>(test, isT)) return true;

	return false;
}
