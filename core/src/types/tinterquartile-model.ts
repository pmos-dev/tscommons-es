import { commonsTypeHasPropertyNumber } from '../helpers/commons-type';

export type TInterquartileModel = {
		min: number;
		q1: number;
		median: number;
		q3: number;
		max: number;
};

export function isTInterquartileModel(test: unknown): test is TInterquartileModel {
	if (!commonsTypeHasPropertyNumber(test, 'min')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'q1')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'median')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'q3')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'max')) return false;

	return true;
}

export type TInterquartileHoursModel = TInterquartileModel & {
		hour: number;
};

export function isTInterquartileHoursModel(test: unknown): test is TInterquartileHoursModel {
	if (!isTInterquartileModel(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'hour')) return false;

	return true;
}

export type TInterquartileDowsModel = TInterquartileModel & {
		dow: number;
};

export function isTInterquartileDowsModel(test: unknown): test is TInterquartileDowsModel {
	if (!isTInterquartileModel(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'dow')) return false;

	return true;
}
