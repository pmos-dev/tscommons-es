export type TMapRank<T> = {
		item: T;
		rank: number;
};
