import { TPropertyObject } from './tproperty-object';

export type TPrimative = string|number|boolean;

export type TPrimativeArray = TPrimative[];

export type TPrimativeObject = TPropertyObject<TPrimative>;
