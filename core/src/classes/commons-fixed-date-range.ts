import { commonsTypeHasPropertyT, commonsTypeIsObject, commonsTypeIsString } from '../helpers/commons-type';

import { CommonsFixedDate, TCommonsFixedDateObject, TCommonsFixedYmdHisObject, isTCommonsFixedDateObject, isTCommonsFixedYmdHisObject } from './commons-fixed-date';
import { CommonsFixedDuration } from './commons-fixed-duration';

export type TCommonsFixedDateRange = {
		from: CommonsFixedDate;
		to: CommonsFixedDate;
}

export class CommonsFixedDateRange {
	public get __internalIdentifier(): string {
		return '__CommonsFixedDateRange';
	}

	private fixedFrom!: CommonsFixedDate;
	private fixedTo!: CommonsFixedDate;

	constructor(
			from: CommonsFixedDate,
			to: CommonsFixedDate
	) {
		this.from = from;
		this.to = to;
	}

	public static is(test: unknown): test is CommonsFixedDateRange {
		if (!commonsTypeIsObject(test)) return false;

		try {
			const identify: unknown = test['__internalIdentifier'];
			if (!commonsTypeIsString(identify)) return false;

			return identify === '__CommonsFixedDateRange';
		} catch (e) {
			return false;
		}
	}

	public get clone(): CommonsFixedDateRange {
		const clone: CommonsFixedDateRange = new CommonsFixedDateRange(
				this.fixedFrom.clone,
				this.fixedTo.clone
		);

		return clone;
	}

	public static fromRange(range: TCommonsFixedDateRange): CommonsFixedDateRange {
		return new CommonsFixedDateRange(range.from.clone, range.to.clone);
	}

	public get from(): CommonsFixedDate {
		return this.fixedFrom;
	}
	public set from(date: CommonsFixedDate) {
		this.fixedFrom = date.clone;
	}

	public get to(): CommonsFixedDate {
		return this.fixedTo;
	}
	public set to(date: CommonsFixedDate) {
		this.fixedTo = date.clone;
	}

	public get range(): TCommonsFixedDateRange {
		return {
				from: this.from,
				to: this.to
		};
	}
	public set range(range: TCommonsFixedDateRange) {
		this.from = range.from;
		this.to = range.to;
	}

	public get YmdHisObject(): { from: TCommonsFixedYmdHisObject; to: TCommonsFixedYmdHisObject } {
		return {
				from: this.from.YmdHisObject,
				to: this.to.YmdHisObject
		};
	}
	public set YmdHisObject(range: { from: TCommonsFixedYmdHisObject; to: TCommonsFixedYmdHisObject }) {
		// have to use fixedFrom/To so it isn't cloned

		this.fixedFrom.YmdHisObject = range.from;
		this.fixedTo.YmdHisObject = range.to;
	}

	public get object(): { from: TCommonsFixedDateObject; to: TCommonsFixedDateObject } {
		return {
				from: this.from.object,
				to: this.to.object
		};
	}
	public set object(range: { from: TCommonsFixedDateObject; to: TCommonsFixedDateObject }) {
		// have to use fixedFrom/To so it isn't cloned
		
		this.fixedFrom.object = range.from;
		this.fixedTo.object = range.to;
	}

	public get YmdHisJsonString(): string {
		return JSON.stringify(this.YmdHisObject);
	}
	public set YmdHisJsonString(s: string) {
		const parsed: unknown = JSON.parse(s);

		if (!commonsTypeHasPropertyT<TCommonsFixedYmdHisObject>(parsed, 'from', isTCommonsFixedYmdHisObject)) throw new Error('Invalid TFixedYmdHisObject from date range');
		if (!commonsTypeHasPropertyT<TCommonsFixedYmdHisObject>(parsed, 'to', isTCommonsFixedYmdHisObject)) throw new Error('Invalid TFixedYmdHisObject to date range');

		this.YmdHisObject = parsed as { from: TCommonsFixedYmdHisObject; to: TCommonsFixedYmdHisObject };
	}

	public get jsonString(): string {
		return JSON.stringify(this.object);
	}
	public set jsonString(s: string) {
		const parsed: unknown = JSON.parse(s);

		if (!commonsTypeHasPropertyT<TCommonsFixedDateObject>(parsed, 'from', isTCommonsFixedDateObject)) throw new Error('Invalid TFixedDateObject from date range');
		if (!commonsTypeHasPropertyT<TCommonsFixedDateObject>(parsed, 'to', isTCommonsFixedDateObject)) throw new Error('Invalid TFixedDateObject to date range');

		this.object = parsed as { from: TCommonsFixedDateObject; to: TCommonsFixedDateObject };
	}

	public get prettyWithoutYear(): string {
		if (this.fixedFrom.Ymd === this.fixedTo.Ymd) {
			// same day
			return `${this.fixedFrom.prettyWithoutYear} ${this.fixedFrom.Hi} - ${this.fixedTo.Hi}`;
		}

		return `${this.fixedFrom.prettyWithoutYear} ${this.fixedFrom.Hi} - ${this.fixedTo.prettyWithoutYear} ${this.fixedTo.Hi}`;
	}

	public get pretty(): string {
		if (this.fixedFrom.Ymd === this.fixedTo.Ymd) {
			// same day
			return `${this.fixedFrom.pretty} ${this.fixedFrom.Hi} - ${this.fixedTo.Hi}`;
		}

		return `${this.fixedFrom.pretty} ${this.fixedFrom.Hi} - ${this.fixedTo.pretty} ${this.fixedTo.Hi}`;
	}

	public get prettyShortWithoutYear(): string {
		if (this.fixedFrom.Ymd === this.fixedTo.Ymd) {
			// same day
			return `${this.fixedFrom.prettyShortWithoutYear} ${this.fixedFrom.Hi} - ${this.fixedTo.Hi}`;
		}

		return `${this.fixedFrom.prettyShortWithoutYear} ${this.fixedFrom.Hi} - ${this.fixedTo.prettyShortWithoutYear} ${this.fixedTo.Hi}`;
	}

	public get prettyShort(): string {
		if (this.fixedFrom.Ymd === this.fixedTo.Ymd) {
			// same day
			return `${this.fixedFrom.prettyShort} ${this.fixedFrom.Hi} - ${this.fixedTo.Hi}`;
		}

		return `${this.fixedFrom.prettyShort} ${this.fixedFrom.Hi} - ${this.fixedTo.prettyShort} ${this.fixedTo.Hi}`;
	}

	public get delta(): number {
		return this.fixedFrom.deltaTo(this.fixedTo);
	}

	public get deltaAbs(): number {
		return Math.abs(this.delta);
	}

	public get seconds(): number {
		return this.fixedFrom.secondsTo(this.fixedTo);
	}
	public get secondsAbs(): number {
		return Math.abs(this.seconds);
	}

	public get minutes(): number {
		return this.fixedFrom.minutesTo(this.fixedTo);
	}
	public get minutesAbs(): number {
		return Math.abs(this.minutes);
	}

	public get hours(): number {
		return this.fixedFrom.hoursTo(this.fixedTo);
	}
	public get hoursAbs(): number {
		return Math.abs(this.hours);
	}

	public get days(): number {
		return this.fixedFrom.daysTo(this.fixedTo);
	}
	public get daysAbs(): number {
		return Math.abs(this.days);
	}

	public get isReversed(): boolean {
		return this.delta < 0;
	}

	public get durationAbs(): CommonsFixedDuration {
		const millis: number = this.deltaAbs;
		return CommonsFixedDuration.fromMillis(millis);
	}

	public isEqual(): boolean {
		return this.delta === 0;
	}

	public isEqualYmd(): boolean {
		return this.fixedFrom.Ymd === this.fixedTo.Ymd;
	}

	public isEqualHisMs(): boolean {
		return this.fixedFrom.HisMs === this.fixedTo.HisMs;
	}

	public isEqualHis(): boolean {
		return this.fixedFrom.His === this.fixedTo.His;
	}

	public isEqualHi(): boolean {
		return this.fixedFrom.Hi === this.fixedTo.Hi;
	}

	public isEqualYmdHis(): boolean {
		return this.fixedFrom.YmdHis === this.fixedTo.YmdHis;
	}

	public quantiseToSeconds(
			seconds: number,
			direction: 'down'|'up'|'closest'|'inner'|'outer'
	): void {
		switch (direction) {
			case 'down':
			case 'up':
			case 'closest':
				this.from.quantiseToSeconds(seconds, direction);
				this.to.quantiseToSeconds(seconds, direction);
				break;
			case 'inner':
				this.from.quantiseToSeconds(seconds, 'up');
				this.to.quantiseToSeconds(seconds, 'down');
				break;
			case 'outer':
				this.from.quantiseToSeconds(seconds, 'down');
				this.to.quantiseToSeconds(seconds, 'up');
				break;
		}
	}

	public quantiseToMinutes(
			minutes: number,
			direction: 'down'|'up'|'closest'|'inner'|'outer'
	): void {
		this.quantiseToSeconds(minutes * 60, direction);
	}

	public quantiseToHours(
			hours: number,
			direction: 'down'|'up'|'closest'|'inner'|'outer'
	): void {
		this.quantiseToMinutes(hours * 60, direction);
	}

	public quantiseToDays(
			days: number,
			direction: 'down'|'up'|'closest'|'inner'|'outer'
	): void {
		this.quantiseToHours(days * 24, direction);
	}

	public autoNextDay(): void {
		this.to.autoNextDay(this.from);
	}

	public autoPivotNextDay(pivotUntilHour: number): void {
		this.from.pivotNextDay(pivotUntilHour);
		this.to.pivotNextDay(pivotUntilHour);
		this.autoNextDay();
	}

	public quantisedFixedDatesBetween(
			quantise: CommonsFixedDuration,
			inclusive: boolean = false
	): CommonsFixedDate[] {
		const now: CommonsFixedDate = this.from.clone;
		
		const items: CommonsFixedDate[] = [];
		while (true) {
			if (now.compareTo(this.to) === 1) break;
			if (!inclusive && now.compareTo(this.to) === 0) break;

			items.push(now.clone);

			now.milli += quantise.millis;
		}

		return items;
	}
}
