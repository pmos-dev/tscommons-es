import { CommonsJoinTable, TCommonsJoin } from './commons-join-table';

export class CommonsTimestampJoinTable<
		A,
		B,
		DataT extends {
				timestamp: Date;
				[ key: string ]: unknown;
		} = {
				timestamp: Date;
		}
> extends CommonsJoinTable<
		A,
		B,
		DataT
> {
	public purgeOlderThan(threshold: Date): TCommonsJoin<A, B, DataT>[] {
		const expireds: TCommonsJoin<A, B, DataT>[] = this.joins
				.filter((join: TCommonsJoin<A, B, DataT>): boolean => join.data.timestamp.getTime() < threshold.getTime());
		
		for (const expired of expireds) {
			this.unjoin(expired.a, expired.b);
		}

		return [ ...expireds ];
	}

	public getTimestamp(a: A, b: B): Date|undefined {
		const data: DataT|undefined = this.getJoinData(a, b);
		if (!data) return undefined;

		return data.timestamp;
	}

	public touch(a: A, b: B): boolean {
		const timestamp: Date|undefined = this.getTimestamp(a, b);
		if (!timestamp) return false;

		timestamp.setTime(new Date().getTime());

		return true;
	}
}
