import { commonsTypeHasPropertyNumber, commonsTypeIsNumberKeyObject, commonsTypeIsObject, commonsTypeIsString } from '../helpers/commons-type';

import {
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		COMMONS_REGEX_PATTERN_DATE_DMY,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_TIME_HI,
		COMMONS_REGEX_PATTERN_TIME_HIS
} from '../consts/commons-regex';

function splitDigits(s: string): number[] {
	return s
			.replace(/[-: ./]/g, ',')
			.split(',')
			.map((d: string): number => parseInt(d, 10));
}

const MONTHS: string[] = 'January,February,March,April,May,June,July,August,September,October,November,December'.split(',');
const DAYS: string[] = 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday'.split(',');

export type TCommonsFixedYmdHisObject = {
		year: number;
		month: number;
		day: number;
		hour: number;
		minute: number;
		second: number;
};
export function isTCommonsFixedYmdHisObject(test: unknown): test is TCommonsFixedYmdHisObject {
	if (!commonsTypeIsNumberKeyObject(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'year')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'month')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'day')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'hour')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'minute')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'second')) return false;

	return true;
}

export type TCommonsFixedDateObject = TCommonsFixedYmdHisObject & {
		milli: number;
};
export function isTCommonsFixedDateObject(test: unknown): test is TCommonsFixedDateObject {
	if (!isTCommonsFixedYmdHisObject(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'milli')) return false;

	return true;
}

export class CommonsFixedDate {
	public get __internalIdentifier(): string {
		return '__CommonsFixedDate';
	}

	private y: number;
	private m: number;
	private d: number;
	private h: number;
	private i: number;
	private s: number;
	private ms: number;

	private internalDateCalcs: Date;

	constructor() {
		const now: Date = new Date();
		this.y = now.getFullYear();
		this.m = now.getMonth() + 1;
		this.d = now.getDate();
		this.h = now.getHours();
		this.i = now.getMinutes();
		this.s = now.getSeconds();
		this.ms = now.getMilliseconds();

		this.internalDateCalcs = now;
	}

	public static is(test: unknown): test is CommonsFixedDate {
		if (!commonsTypeIsObject(test)) return false;

		try {
			const identify: unknown = test['__internalIdentifier'];
			if (!commonsTypeIsString(identify)) return false;

			return identify === '__CommonsFixedDate';
		} catch (e) {
			return false;
		}
	}

	public static fromYmdHisMs(value: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.YmdHisMs = value;

		return fixed;
	}

	public static fromYmdHis(value: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.YmdHis = value;

		return fixed;
	}

	public static fromYmd(value: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.zYmd = value;

		return fixed;
	}

	public static fromHisMs(value: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.zHisMs = value;

		return fixed;
	}

	public static fromHis(value: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.zHis = value;

		return fixed;
	}

	public static fromHi(value: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.zHi = value;

		return fixed;
	}

	public static fromUTCDate(date: Date): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.UTCDate = date;

		return fixed;
	}

	public static fromObject(date: TCommonsFixedDateObject): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.object = date;

		return fixed;
	}

	public static fromYmdHisObject(date: TCommonsFixedYmdHisObject): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.YmdHisObject = date;

		return fixed;
	}

	public static fromJsonString(date: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.jsonString = date;

		return fixed;
	}

	public static fromYmdHisJsonString(date: string): CommonsFixedDate {
		const fixed: CommonsFixedDate = new CommonsFixedDate();
		fixed.YmdHisJsonString = date;

		return fixed;
	}

	public get clone(): CommonsFixedDate {
		const clone: CommonsFixedDate = new CommonsFixedDate();
		clone.YmdHisMs = this.YmdHisMs;

		return clone;
	}

	private refactor(): void {
		while (this.ms >= 1000) {
			this.s++;
			this.ms -= 1000;
		}
		while (this.ms < 0) {
			this.s--;
			this.ms += 1000;
		}

		while (this.s >= 60) {
			this.i++;
			this.s -= 60;
		}
		while (this.s < 0) {
			this.i--;
			this.s += 60;
		}

		while (this.i >= 60) {
			this.h++;
			this.i -= 60;
		}
		while (this.i < 0) {
			this.h--;
			this.i += 60;
		}

		while (this.h >= 24) {
			this.d++;
			this.h -= 24;
		}
		while (this.h < 0) {
			this.d--;
			this.h += 24;
		}

		const date: Date = this.internalDateCalcs;
		date.setTime(0);
		date.setUTCFullYear(this.y, this.m - 1, this.d);
		this.y = date.getUTCFullYear();
		this.m = date.getUTCMonth() + 1;
		this.d = date.getUTCDate();
	}

	public get year(): number {
		return this.y;
	}
	public set year(value: number) {
		this.y = Math.trunc(value);
		this.refactor();
	}

	public get month(): number {
		return this.m;
	}
	public set month(value: number) {
		this.m = Math.trunc(value);
		this.refactor();
	}

	public get monthString(): string {
		return MONTHS[this.m - 1];
	}

	public get monthShortString(): string {
		return this.monthString.substring(0, 3);
	}

	public get day(): number {
		return this.d;
	}
	public set day(value: number) {
		this.d = Math.trunc(value);
		this.refactor();
	}

	public get dow(): number {
		const date: Date = this.internalDateCalcs;
		date.setTime(0);
		date.setUTCFullYear(2000, 0, 1);

		date.setUTCHours(12);
		date.setUTCFullYear(this.y);
		date.setUTCMonth(this.m - 1);
		date.setUTCDate(this.d);

		return date.getUTCDay();
	}
	// no setter

	public get dowString(): string {
		return DAYS[this.dow];
	}

	public get dowShortString(): string {
		return this.dowString.substring(0, 3);
	}

	public get hour(): number {
		return this.h;
	}
	public set hour(value: number) {
		this.h = Math.trunc(value);
		this.refactor();
	}

	public get minute(): number {
		return this.i;
	}
	public set minute(value: number) {
		this.i = Math.trunc(value);
		this.refactor();
	}

	public get second(): number {
		return this.s;
	}
	public set second(value: number) {
		this.s = Math.trunc(value);
		this.refactor();
	}

	public get milli(): number {
		return this.ms;
	}
	public set milli(value: number) {
		this.ms = Math.trunc(value);
		this.refactor();
	}

	public get Ymd(): string {
		return `${this.y.toString(10).padStart(4, '0')}-${this.m.toString(10).padStart(2, '0')}-${this.d.toString(10).padStart(2, '0')}`;
	}
	public set Ymd(Ymd: string) {
		if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(Ymd)) throw new Error('Invalid Ymd string');

		const digits: number[] = splitDigits(Ymd);
		
		this.y = digits[0];
		this.m = digits[1];
		this.d = digits[2];

		this.refactor();
	}
	public set zYmd(Ymd: string) {
		this.Ymd = Ymd;

		this.h = 0;
		this.i = 0;
		this.s = 0;
		this.ms = 0;

		this.refactor();
	}

	public get dmY(): string {
		const parts: string[] = this.Ymd.split('-');
		
		return `${parts[2]}/${parts[1]}/${parts[0]}`;
	}
	public set dmY(dmY: string) {
		if (!COMMONS_REGEX_PATTERN_DATE_DMY.test(dmY)) throw new Error('Invalid dmY string');

		const parts: string[] = dmY.split('/');

		this.Ymd = `${parts[2]}-${parts[1]}-${parts[0]}`;
	}
	public set zdmY(dmY: string) {
		this.h = 0;
		this.i = 0;
		this.s = 0;
		this.ms = 0;

		this.dmY = dmY;
	}

	public get Hi(): string {
		return `${this.h.toString(10).padStart(2, '0')}:${this.i.toString(10).padStart(2, '0')}`;
	}
	public set Hi(Hi: string) {
		if (!COMMONS_REGEX_PATTERN_TIME_HI.test(Hi)) throw new Error('Invalid Hi string');

		const digits: number[] = splitDigits(Hi);
		
		this.h = digits[0];
		this.i = digits[1];
		this.s = 0;
		this.ms = 0;

		this.refactor();
	}
	public set zHi(Hi: string) {
		this.y = 1970;
		this.m = 1;
		this.d = 1;

		this.Hi = Hi;
	}

	public get His(): string {
		return `${this.Hi}:${this.s.toString(10).padStart(2, '0')}`;
	}
	public set His(His: string) {
		if (!COMMONS_REGEX_PATTERN_TIME_HIS.test(His)) throw new Error('Invalid His string');

		const parts: string[] = His.split(':');

		this.Hi = `${parts[0]}:${parts[1]}`;

		const digits: number[] = splitDigits(parts[2]);

		this.s = digits[0];

		this.refactor();
	}
	public set zHis(His: string) {
		this.y = 1970;
		this.m = 1;
		this.d = 1;

		this.His = His;
	}

	public get HisMs(): string {
		return `${this.His}.${this.ms.toString(10)}`;
	}
	public set HisMs(HisMs: string) {
		const parts: string[] = HisMs.split('.');
		if (parts.length === 1) parts.push('0');
		if (parts.length !== 2) throw new Error('Invalid HisMs');

		if (!/^[0-9]+$/.test(parts[1])) throw new Error('Invalid Ms for HisMs');

		this.His = parts[0];

		this.ms = parseInt(parts[1], 10);

		this.refactor();
	}
	public set zHisMs(HisMs: string) {
		this.y = 1970;
		this.m = 1;
		this.d = 1;

		this.HisMs = HisMs;
	}

	public get YmdHis(): string {
		return `${this.Ymd} ${this.His}`;
	}
	public set YmdHis(YmdHis: string) {
		if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(YmdHis)) throw new Error('Invalid YmdHis string');

		const parts: string[] = YmdHis.split(' ');
		if (parts.length !== 2) throw new Error('Invalid YmdHis');

		this.zYmd = parts[0];
		this.His = parts[1];
	}

	public get YmdHisMs(): string {
		return `${this.Ymd} ${this.HisMs}`;
	}
	public set YmdHisMs(YmdHisMs: string) {
		const parts: string[] = YmdHisMs.split('.');
		if (parts.length === 1) parts.push('0');
		if (parts.length !== 2) throw new Error('Invalid YmdHisMs');

		if (!/^[0-9]+$/.test(parts[1])) throw new Error('Invalid Ms for YmdHisMs');

		this.YmdHis = parts[0];
		this.ms = parseInt(parts[1], 10);
		
		this.refactor();
	}

	public get dmYHi(): string {
		return `${this.dmY} ${this.Hi}`;
	}
	public set dmYHi(dmYHi: string) {
		const parts: string[] = dmYHi.split(' ');
		if (parts.length !== 2) throw new Error('Invalid dmYHi');

		this.zdmY = parts[0];
		this.Hi = parts[1];
	}

	public get dmYHis(): string {
		return `${this.dmY} ${this.His}`;
	}
	public set dmYHis(dmYHis: string) {
		const parts: string[] = dmYHis.split(' ');
		if (parts.length !== 2) throw new Error('Invalid dmYHis string');

		this.dmY = parts[0];
		this.His = parts[1];
	}

	public get UTCDate(): Date {
		this.refactor();

		const date: Date = this.internalDateCalcs;
		date.setTime(0);
		date.setUTCFullYear(this.y, this.m - 1, this.d);
		date.setUTCHours(this.h, this.i, this.s, this.ms);

		return date;
	}
	public set UTCDate(date: Date) {
		this.y = date.getUTCFullYear();
		this.m = date.getUTCMonth() + 1;
		this.d = date.getUTCDate();
		this.h = date.getUTCHours();
		this.i = date.getUTCMinutes();
		this.s = date.getUTCSeconds();
		this.ms = date.getUTCMilliseconds();

		this.refactor();
	}

	public parseLocalDate(date: Date): void {
		this.y = date.getFullYear();
		this.m = date.getMonth() + 1;
		this.d = date.getDate();
		this.h = date.getHours();
		this.i = date.getMinutes();
		this.s = date.getSeconds();
		this.ms = date.getMilliseconds();

		this.refactor();
	}

	public get prettyWithoutYear(): string {
		return `${this.dowString} ${this.day.toString(10).padStart(2, '0')} ${this.monthString}`;
	}

	public get pretty(): string {
		return `${this.prettyWithoutYear} ${this.year.toString(10).padStart(4, '0')}`;
	}

	public get prettyShortWithoutYear(): string {
		return `${this.dowShortString} ${this.day.toString(10).padStart(2, '0')} ${this.monthShortString}`;
	}

	public get prettyShort(): string {
		return `${this.prettyShortWithoutYear} ${this.year.toString(10).padStart(4, '0')}`;
	}

	public deltaTo(date: CommonsFixedDate, absolute: boolean = false): number {
		const d1: number = this.UTCDate.getTime();
		const d2: number = date.UTCDate.getTime();

		const delta: number = d2 - d1;

		if (absolute) return Math.abs(delta);
		return delta;
	}

	public secondsTo(date: CommonsFixedDate, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / 1000);
	}

	public minutesTo(date: CommonsFixedDate, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / (1000 * 60));
	}

	public hoursTo(date: CommonsFixedDate, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / (1000 * 60 * 60));
	}

	public daysTo(date: CommonsFixedDate, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / (1000 * 60 * 60 * 24));
	}

	public compareTo(date: CommonsFixedDate): number {
		const delta: number = this.deltaTo(date);

		if (delta > 0) return -1;
		if (delta < 0) return 1;
		return 0;
	}

	public isEqual(date: CommonsFixedDate): boolean {
		return this.compareTo(date) === 0;
	}

	public isEqualYmd(date: CommonsFixedDate): boolean {
		return this.year === date.year
				&& this.month === date.month
				&& this.day === date.day;
	}

	public isEqualHis(date: CommonsFixedDate): boolean {
		return this.hour === date.hour
				&& this.minute === date.minute
				&& this.second === date.second;
	}

	public isEqualHisMs(date: CommonsFixedDate): boolean {
		return this.isEqualHis(date)
				&& this.milli === date.milli;
	}

	public isEqualYmdHis(date: CommonsFixedDate): boolean {
		return this.isEqualYmd(date) && this.isEqualHis(date);
	}

	public isEqualYmdHisMs(date: CommonsFixedDate): boolean {
		return this.isEqualYmd(date) && this.isEqualHisMs(date);
	}

	public truncateToSecond(): void {
		this.ms = 0;
	}

	public truncateToMinute(): void {
		this.truncateToSecond();
		this.s = 0;
	}

	public truncateToHour(): void {
		this.truncateToMinute();
		this.i = 0;
	}
	
	public truncateToYmd(): void {
		this.zYmd = this.Ymd;
	}

	public toString(): string {
		return this.YmdHisMs;
	}

	public get YmdHisObject(): TCommonsFixedYmdHisObject {
		return {
				year: this.year,
				month: this.month,
				day: this.day,
				hour: this.hour,
				minute: this.minute,
				second: this.second
		};
	}
	public set YmdHisObject(object: TCommonsFixedYmdHisObject) {
		this.y = object.year;
		this.m = object.month;
		this.d = object.day;
		this.h = object.hour;
		this.i = object.minute;
		this.s = object.second;
		this.ms = 0;

		this.refactor();
	}

	public get object(): TCommonsFixedDateObject {
		return {
				...this.YmdHisObject,
				milli: this.milli
		};
	}
	public set object(object: TCommonsFixedDateObject) {
		this.YmdHisObject = object;
		this.milli = object.milli;

		this.refactor();
	}

	public get YmdHisJsonString(): string {
		return JSON.stringify(this.YmdHisObject);
	}
	public set YmdHisJsonString(s: string) {
		const parsed: unknown = JSON.parse(s);
		if (!isTCommonsFixedYmdHisObject(parsed)) throw new Error('Invalid TFixedYmdHisObject');

		this.YmdHisObject = parsed;
	}

	public get jsonString(): string {
		return JSON.stringify(this.object);
	}
	public set jsonString(s: string) {
		const parsed: unknown = JSON.parse(s);
		if (!isTCommonsFixedDateObject(parsed)) throw new Error('Invalid TFixedDateObject');

		this.object = parsed;
	}

	public quantiseToSeconds(
			seconds: number,
			direction: 'down'|'up'|'closest'
	): void {
		// we have to use the UTCDate.getTime() here as we might want to span >60 periods

		const clone: CommonsFixedDate = this.clone;
		clone.truncateToSecond();

		const date: Date = clone.UTCDate;
		let dateMillis: number = date.getTime();

		const periodMillis: number = seconds *= 1000;

		switch (direction) {
			case 'down':
				dateMillis = Math.floor(dateMillis / periodMillis) * periodMillis;
				break;
			case 'up':
				dateMillis = Math.ceil(dateMillis / periodMillis) * periodMillis;
				break;
			case 'closest':
				dateMillis = Math.round(dateMillis / periodMillis) * periodMillis;
				break;
		}

		this.UTCDate = new Date(dateMillis);
	}

	public quantiseToMinutes(
			minutes: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.quantiseToSeconds(minutes * 60, direction);
		this.truncateToMinute();
	}

	public quantiseToHours(
			hours: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.quantiseToMinutes(hours * 60, direction);
		this.truncateToHour();
	}

	public quantiseToDays(
			days: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.quantiseToHours(days * 24, direction);
		this.truncateToYmd();
	}

	public pivotNextDay(
			pivotUntilHour: number
	): void {
		if (this.hour < pivotUntilHour) this.day++;
	}

	public autoNextDay(prev: CommonsFixedDate): void {
		while (prev.compareTo(this) === 1) {
			this.day++;
		}
	}
}
