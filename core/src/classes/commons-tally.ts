export type TCommonsTallyEntry<K> = { key: K; tally: number };

export class CommonsTally<K> {
	private internalMap: Map<K, number> = new Map<K, number>();
	private originalKeys: K[] = [];

	constructor(
			keys: K[] = [],
			private isEqual: (a: K, b: K) => boolean = (a: K, b: K) => a === b
	) {
		this.originalKeys = keys.slice();

		this.reset();
	}

	public reset(): void {
		this.internalMap.clear();

		for (const k of this.originalKeys) this.internalMap.set(k, 0);
	}

	public get keys(): K[] {
		return Array.from(this.internalMap.keys());
	}

	private useExistingKeyIfAvailable(key: K): K {
		for (const k of this.internalMap.keys()) {
			if (this.isEqual(k, key)) return k;
		}

		return key;
	}

	public add(
			k: K,
			tally: number|undefined
	): void {
		if (tally === undefined) return;

		const match: K = this.useExistingKeyIfAvailable(k);

		if (!this.internalMap.has(match)) this.internalMap.set(match, tally);
		else this.internalMap.set(match, this.internalMap.get(match)! + tally);
	}

	public increment(k: K): void {
		this.add(k, 1);
	}

	public addForMap(data: Map<K, number>): void {
		for (const k of data.keys()) {
			this.add(k, data.get(k));
		}
	}

	public enumerateArray(array: K[]): void {
		for (const k of array) this.increment(k);
	}
	
	public get size(): number {
		return this.internalMap.size;
	}
	
	public get total(): number {
		let total: number = 0;
		for (const k of this.internalMap.keys()) {
			total += this.internalMap.get(k)!;
		}

		return total;
	}

	public get(key: K): number|undefined {
		return this.internalMap.get(key);
	}

	public asMap(
			includeZeros: boolean = true
	): Map<K, number> {
		const clone: Map<K, number> = new Map<K, number>();

		for (const k of this.internalMap.keys()) {
			const value: number = this.internalMap.get(k)!;

			if (value > 0 || includeZeros) clone.set(k, value);
		}

		return clone;
	}

	public asStringKeyObject(
			serialize: (k: K) => string,
			includeZeros: boolean = true
	): { [ k: string ]: number } {
		const clone: Map<K, number> = this.asMap(includeZeros);

		const object: { [ k: string ]: number } = {};
		for (const k of clone.keys()) {
			object[serialize(k)] = clone.get(k)!;
		}

		return object;
	}

	public asNumberKeyObject(
			serialize: (k: K) => number,
			includeZeros: boolean = true
	): { [ k: string ]: number } {
		const clone: Map<K, number> = this.asMap(includeZeros);

		const object: { [ k: number ]: number } = {};
		for (const k of clone.keys()) {
			object[serialize(k)] = clone.get(k)!;
		}

		return object;
	}

	public asArray(): TCommonsTallyEntry<K>[] {
		const array: TCommonsTallyEntry<K>[] = [];

		for (const k of this.internalMap.keys()) {
			array.push({
					key: k,
					tally: this.internalMap.get(k)!
			});
		}

		return array;
	}

	public asSortedArray(dec: boolean = false): TCommonsTallyEntry<K>[] {
		const array: TCommonsTallyEntry<K>[] = this.asArray();

		array
				.sort((a: { tally: number }, b: { tally: number }): number => {
					if (a.tally < b.tally) return -1;
					if (a.tally > b.tally) return 1;
					return 0;
				});
		if (dec) array.reverse();
		
		return array;
	}

	public top(limit: number): TCommonsTallyEntry<K>[] {
		return this.asSortedArray(true).slice(0, limit);
	}

	public topKeys(limit: number): K[] {
		return this.top(limit)
				.map((entry: TCommonsTallyEntry<K>): K => entry.key);
	}

	public get winner(): K|undefined {
		const tops: K[] = this.topKeys(1);
		if (tops.length === 0) return undefined;

		return tops[0];
	}
}
