import { commonsBase62GenerateRandomId } from '../helpers/commons-base';

import { CommonsCache, TCommonsCache } from './commons-cache';

type TCursor<
		CursorIdT,
		CacheIdT
> = {
		cursorId: CursorIdT;
		cacheId: CacheIdT;
		timestamp: Date;
		length: number;
		index: number;
};

export type TCommonsCursorWalk<D> = {
		block: D[];
		index: number;
		remaining: number;
}

export class CommonsCursor<
		D,
		T extends { items: D[] },
		CursorIdT,
		CacheIdT
> {
	private cursors: TCursor<CursorIdT, CacheIdT>[] = [];

	constructor(
			private cache: CommonsCache<T, CacheIdT>,
			private generateRandomId: () => CursorIdT,
			private maxAgeInMillis?: number
	) {}

	private get(cursorId: CursorIdT): TCursor<CursorIdT, CacheIdT>|undefined {
		this.checkForAndExpire();
		
		const existing: TCursor<CursorIdT, CacheIdT>|undefined = this.cursors
				.find((cursor: TCursor<CursorIdT, CacheIdT>): boolean => cursor.cursorId === cursorId);
		if (!existing) return undefined;
		
		if (!this.cache.has(existing.cacheId)) {
			this.delete(cursorId);
			return this.get(cursorId);
		}

		return existing;
	}

	public generateForCache(cacheId: CacheIdT): CursorIdT|undefined {
		this.checkForAndExpire();
		
		const cached: TCommonsCache<T, CacheIdT>|undefined = this.cache.getRawCacheById(cacheId);
		if (!cached) return undefined;

		const cursorId: CursorIdT = this.generateRandomId();

		const cursor: TCursor<CursorIdT, CacheIdT> = {
				cursorId: cursorId,
				cacheId: cacheId,
				timestamp: new Date(cached.timestamp.getTime()),
				length: cached.data.items.length,
				index: 0
		};

		this.cursors.push(cursor);

		return cursorId;
	}

	public next(
			cursorId: CursorIdT,
			size: number
	): TCommonsCursorWalk<D> {
		this.checkForAndExpire();
		
		const existing: TCursor<CursorIdT, CacheIdT>|undefined = this.cursors
				.find((cursor: TCursor<CursorIdT, CacheIdT>): boolean => cursor.cursorId === cursorId);

		if (!existing) {
			return {
					block: [],
					index: 0,
					remaining: 0
			};
		}
		
		const cache: T|undefined = this.cache.getById(existing.cacheId);
		if (!cache) {
			this.delete(cursorId);
			return this.next(cursorId, -1);	// the size doesn't matter
		}

		const block: D[] = cache.items.slice(existing.index, Math.min(existing.index + size, existing.length));

		existing.index = Math.max(0, Math.min(existing.index + size, existing.length));

		return {
				block: block,
				index: existing.index,
				remaining: existing.length - existing.index
		};
	}

	public skip(
			cursorId: CursorIdT,
			size: number
	): boolean {
		const existing: TCursor<CursorIdT, CacheIdT>|undefined = this.get(cursorId);
		if (!existing) return false;
		
		existing.index = Math.max(0, Math.min(existing.index + size, existing.length));

		return true;
	}

	public rewind(
			cursorId: CursorIdT,
			size: number
	): boolean {
		return this.skip(cursorId, -size);
	}

	private delete(cursorId: CursorIdT): boolean {
		const existing: TCursor<CursorIdT, CacheIdT>|undefined = this.get(cursorId);
		if (!existing) return false;

		this.cursors.splice(this.cursors.indexOf(existing), 1);
		return true;
	}

	public close(cursorId: CursorIdT): void {
		this.delete(cursorId);
	}

	public getExpiration(cursorId: CursorIdT): Date|undefined {
		if (!this.maxAgeInMillis) return;

		const existing: TCursor<CursorIdT, CacheIdT>|undefined = this.get(cursorId);
		if (!existing) return undefined;

		const expires: Date = new Date(existing.timestamp.getTime());
		expires.setTime(expires.getTime() + this.maxAgeInMillis);

		return expires;
	}

	private checkForAndExpire(): void {
		if (!this.maxAgeInMillis) return;
		
		const threshold: Date = new Date();
		threshold.setTime(threshold.getTime() - this.maxAgeInMillis);

		this.cursors = this.cursors
				.filter((cursor: TCursor<CursorIdT, CacheIdT>): boolean => cursor.timestamp.getTime() > threshold.getTime());
	}
}

export class CommonsUidCursor<
		D,
		T extends { items: D[] }
> extends CommonsCursor<
		D,
		T,
		string,
		string
> {
	constructor(
			cache: CommonsCache<T, string>,
			maxAgeInMillis?: number
	) {
		super(
				cache,
				(): string => commonsBase62GenerateRandomId(),
				maxAgeInMillis
		);
	}
}
