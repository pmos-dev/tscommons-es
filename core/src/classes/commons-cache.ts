import { commonsBase62GenerateRandomId } from '../helpers/commons-base';

export type TCommonsCache<
		DataT,
		IdT
> = {
		id: IdT;
		timestamp: Date;
		data: DataT;
};

export class CommonsCache<
		DataT,
		IdT
> {
	private caches: TCommonsCache<DataT, IdT>[];

	constructor(
			private generateRandomId: () => IdT,
			private maxAgeInMillis?: number
	) {
		this.caches = [];
	}

	public getRawCacheById(id: IdT): TCommonsCache<DataT, IdT>|undefined {
		this.checkForAndExpire();

		const existing: TCommonsCache<DataT, IdT>|undefined = this.caches
				.find((store: Pick<TCommonsCache<unknown, IdT>, 'id'>): boolean => store.id === id);

		return existing;
	}

	public has(id: IdT): boolean {
		this.checkForAndExpire();

		return this.getRawCacheById(id) !== undefined;
	}

	protected getIdByData(
			data: DataT,
			isEqual: (a: DataT, b: DataT) => boolean
	): IdT|undefined {
		const existing: TCommonsCache<DataT, IdT>|undefined = this.caches
				.find((store: Pick<TCommonsCache<DataT, unknown>, 'data'>): boolean => isEqual(data, store.data));
		if (!existing) return undefined;

		return existing.id;
	}

	public getById(id: IdT): DataT|undefined {
		const match: TCommonsCache<DataT, IdT>|undefined = this.getRawCacheById(id);
		if (!match) return undefined;

		return match.data;
	}

	public touchForId(
			id: IdT,
			date?: Date
	): boolean {
		const existing: TCommonsCache<DataT, IdT>|undefined = this.getRawCacheById(id);
		if (!existing) return false;

		existing.timestamp = date ? new Date(date.getTime()) : new Date();
		return true;
	}

	public storeForId(
			id: IdT,
			data: DataT
	): void {
		this.checkForAndExpire();
		
		const existing: TCommonsCache<DataT, IdT>|undefined = this.getRawCacheById(id);

		if (existing) {
			existing.data = { ...data };
			existing.timestamp = new Date();
		} else {
			this.caches.push({
					id: id,
					timestamp: new Date(),
					data: { ...data }
			});
		}
	}

	public storeForData(
			data: DataT,
			isEqual: (a: DataT, b: DataT) => boolean
	): IdT {
		this.checkForAndExpire();
		
		let existing: TCommonsCache<DataT, IdT>|undefined;
		if (isEqual) {
			const id: IdT|undefined = this.getIdByData(
					data,
					isEqual
			);
			if (id) existing = this.getRawCacheById(id);
		}

		if (existing) {
			existing.data = { ...data };
			existing.timestamp = new Date();

			return existing.id;
		} else {
			const id: IdT = this.generateRandomId();

			this.caches.push({
					id: id,
					timestamp: new Date(),
					data: { ...data }
			});

			return id;
		}
	}

	public deleteForId(id: IdT): boolean {
		const existing: TCommonsCache<DataT, IdT>|undefined = this.getRawCacheById(id);
		if (!existing) return false;

		this.caches.splice(this.caches.indexOf(existing), 1);

		this.checkForAndExpire();

		return true;
	}

	public getExpiration(cursorId: IdT): Date|undefined {
		if (!this.maxAgeInMillis) return undefined;

		this.checkForAndExpire();

		const existing: TCommonsCache<DataT, IdT>|undefined = this.getRawCacheById(cursorId);
		if (!existing) return undefined;

		const expires: Date = new Date(existing.timestamp.getTime());
		expires.setTime(expires.getTime() + this.maxAgeInMillis);

		return expires;
	}

	private checkForAndExpire(): void {
		if (!this.maxAgeInMillis) return;

		const threshold: Date = new Date();
		threshold.setTime(threshold.getTime() - this.maxAgeInMillis);

		this.caches = this.caches
				.filter((store: Pick<TCommonsCache<unknown, unknown>, 'timestamp'>): boolean => store.timestamp.getTime() > threshold.getTime());
	}

	public wipe(): void {
		this.caches = [];
	}
}

export class CommonsUidCache<DataT> extends CommonsCache<DataT, string> {
	constructor(
			maxAgeInMillis?: number
	) {
		super(
				(): string => commonsBase62GenerateRandomId(),
				maxAgeInMillis
		);
	}
}
