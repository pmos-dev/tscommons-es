import { commonsArrayRemove } from '../helpers/commons-array';

export type TCommonsJoin<
		A,
		B,
		DataT extends { [ key: string ]: unknown }
> = {
		a: A;
		b: B;
		data: DataT;
};

export class CommonsJoinTable<
		A,
		B,
		DataT extends { [ key: string ]: unknown } = { [ key: string ]: never }
> {
	private internalJoins: TCommonsJoin<A, B, DataT>[] = [];
	protected get joins(): TCommonsJoin<A, B, DataT>[] {
		return this.internalJoins;
	}

	public export(): TCommonsJoin<A, B, DataT>[] {
		return [ ...this.joins ];
	}

	public get as(): A[] {
		return this.joins
				.map((join: TCommonsJoin<A, B, DataT>): A => join.a);
	}

	public get bs(): B[] {
		return this.joins
				.map((join: TCommonsJoin<A, B, DataT>): B => join.b);
	}

	public get size(): number {
		return this.joins.length;
	}

	constructor(
			private isEqualA: (a1: A, a2: A) => boolean = (a1: A, a2: A): boolean => a1 === a2,
			private isEqualB: (b1: B, b2: B) => boolean = (b1: B, b2: B): boolean => b1 === b2
	) {}

	public reset() {
		this.internalJoins = [];
	}

	private getExisting(a: A, b: B): TCommonsJoin<A, B, DataT>|undefined {
		return this.internalJoins
				.find((j: TCommonsJoin<A, B, DataT>): boolean => this.isEqualA(a, j.a) && this.isEqualB(b, j.b));
	}

	public join(
			a: A,
			b: B,
			data: DataT
	): boolean {
		const existing: TCommonsJoin<A, B, DataT>|undefined = this.getExisting(a, b);
		
		if (existing) {
			Object.assign(existing, data);
			return false;
		}

		this.internalJoins.push({
				a: a,
				b: b,
				data: data
		});

		return true;
	}

	public unjoin(a: A, b: B): boolean {
		const existing: TCommonsJoin<A, B, DataT>|undefined = this.getExisting(a, b);
		if (!existing) return false;

		commonsArrayRemove(
				this.internalJoins,
				existing,
				(j1: TCommonsJoin<A, B, DataT>, j2: TCommonsJoin<A, B, DataT>): boolean => this.isEqualA(j1.a, j2.a) && this.isEqualB(j1.b, j2.b)
		);

		return true;
	}

	public listBsByA(a: A): B[] {
		return this.internalJoins
				.filter((j: TCommonsJoin<A, B, DataT>): boolean => this.isEqualA(a, j.a))
				.map((j: TCommonsJoin<A, B, DataT>): B => j.b);
	}

	public listAsByB(b: B): A[] {
		return this.internalJoins
				.filter((j: TCommonsJoin<A, B, DataT>): boolean => this.isEqualB(b, j.b))
				.map((j: TCommonsJoin<A, B, DataT>): A => j.a);
	}

	public getJoinData(a: A, b: B): DataT|undefined {
		const existing: TCommonsJoin<A, B, DataT>|undefined = this.getExisting(a, b);
		if (!existing) return undefined;

		return existing.data;
	}
}
