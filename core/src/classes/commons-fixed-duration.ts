import { commonsTypeHasPropertyNumber, commonsTypeIsNumberKeyObject, commonsTypeIsObject, commonsTypeIsString } from '../helpers/commons-type';

import { COMMONS_REGEX_PATTERN_TIME_HI, COMMONS_REGEX_PATTERN_TIME_HIS } from '../consts/commons-regex';

export type TCommonsFixeddHisObject = {
		day: number;
		hour: number;
		minute: number;
		second: number;
};
export function isTCommonsFixeddHisObject(test: unknown): test is TCommonsFixeddHisObject {
	if (!commonsTypeIsNumberKeyObject(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'day')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'hour')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'minute')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'second')) return false;

	return true;
}

export type TCommonsFixedDurationObject = TCommonsFixeddHisObject & {
		milli: number;
};
export function isTCommonsFixedDurationObject(test: unknown): test is TCommonsFixedDurationObject {
	if (!isTCommonsFixeddHisObject(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'milli')) return false;

	return true;
}

function splitDigits(s: string): number[] {
	return s
			.replace(/[-: ./]/g, ',')
			.split(',')
			.map((d: string): number => parseInt(d, 10));
}

export class CommonsFixedDuration {
	public get __internalIdentifier(): string {
		return '__CommonsFixedDuration';
	}

	private d: number;
	private h: number;
	private i: number;
	private s: number;
	private ms: number;

	constructor() {
		this.d = 0;
		this.h = 0;
		this.i = 0;
		this.s = 0;
		this.ms = 0;
	}

	public static is(test: unknown): test is CommonsFixedDuration {
		if (!commonsTypeIsObject(test)) return false;

		try {
			const identify: unknown = test['__internalIdentifier'];
			if (!commonsTypeIsString(identify)) return false;

			return identify === '__CommonsFixedDuration';
		} catch (e) {
			return false;
		}
	}

	public deltaTo(date: CommonsFixedDuration, absolute: boolean = false): number {
		const d1: number = this.UTCDate.getTime();
		const d2: number = date.UTCDate.getTime();

		const delta: number = d2 - d1;

		if (absolute) return Math.abs(delta);
		return delta;
	}

	public secondsTo(date: CommonsFixedDuration, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / 1000);
	}

	public minutesTo(date: CommonsFixedDuration, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / (1000 * 60));
	}

	public hoursTo(date: CommonsFixedDuration, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / (1000 * 60 * 60));
	}

	public daysTo(date: CommonsFixedDuration, absolute: boolean = false): number {
		const delta: number = this.deltaTo(date, absolute);

		return Math.floor(delta / (1000 * 60 * 60 * 24));
	}

	public compareTo(date: CommonsFixedDuration): number {
		const delta: number = this.deltaTo(date);

		if (delta > 0) return -1;
		if (delta < 0) return 1;
		return 0;
	}

	public isEqual(date: CommonsFixedDuration): boolean {
		return this.compareTo(date) === 0;
	}

	public isEquald(date: CommonsFixedDuration): boolean {
		return this.day === date.day;
	}

	public isEqualHis(date: CommonsFixedDuration): boolean {
		return this.hour === date.hour
				&& this.minute === date.minute
				&& this.second === date.second;
	}

	public isEqualHisMs(date: CommonsFixedDuration): boolean {
		return this.isEqualHis(date)
				&& this.milli === date.milli;
	}

	public isEqualdHis(date: CommonsFixedDuration): boolean {
		return this.isEquald(date) && this.isEqualHis(date);
	}

	public isEqualdHisMs(date: CommonsFixedDuration): boolean {
		return this.isEquald(date) && this.isEqualHisMs(date);
	}

	public static fromMillis(millis: number): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.millis = millis;

		return fixed;
	}

	public static fromdHisMs(value: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.dHisMs = value;

		return fixed;
	}

	public static fromdHis(value: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.dHis = value;

		return fixed;
	}

	public static fromHisMs(value: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.HisMs = value;

		return fixed;
	}

	public static fromHis(value: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.His = value;

		return fixed;
	}

	public static fromHi(value: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.Hi = value;

		return fixed;
	}

	public static fromUTCDate(date: Date): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.UTCDate = date;

		return fixed;
	}

	public static fromObject(date: TCommonsFixedDurationObject): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.object = date;

		return fixed;
	}

	public static fromdHisObject(date: TCommonsFixeddHisObject): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.dHisObject = date;

		return fixed;
	}

	public static fromJsonString(date: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.jsonString = date;

		return fixed;
	}

	public static fromdHisJsonString(date: string): CommonsFixedDuration {
		const fixed: CommonsFixedDuration = new CommonsFixedDuration();
		fixed.dHisJsonString = date;

		return fixed;
	}

	public get clone(): CommonsFixedDuration {
		const clone: CommonsFixedDuration = new CommonsFixedDuration();
		clone.dHisMs = this.dHisMs;

		return clone;
	}

	private refactor(): void {
		while (this.ms >= 1000) {
			this.s++;
			this.ms -= 1000;
		}
		while (this.ms < 0) {
			this.s--;
			this.ms += 1000;
		}

		while (this.s >= 60) {
			this.i++;
			this.s -= 60;
		}
		while (this.s < 0) {
			this.i--;
			this.s += 60;
		}

		while (this.i >= 60) {
			this.h++;
			this.i -= 60;
		}
		while (this.i < 0) {
			this.h--;
			this.i += 60;
		}

		while (this.h >= 24) {
			this.d++;
			this.h -= 24;
		}
		while (this.h < 0) {
			this.d--;
			this.h += 24;
		}
	}

	public get day(): number {
		return this.d;
	}
	public set day(value: number) {
		this.d = value;
		this.refactor();
	}

	public get hour(): number {
		return this.h;
	}
	public set hour(value: number) {
		this.h = value;
		this.refactor();
	}

	public get minute(): number {
		return this.i;
	}
	public set minute(value: number) {
		this.i = value;
		this.refactor();
	}

	public get second(): number {
		return this.s;
	}
	public set second(value: number) {
		this.s = value;
		this.refactor();
	}

	public get milli(): number {
		return this.ms;
	}
	public set milli(value: number) {
		this.ms = value;
		this.refactor();
	}

	public get millis(): number {
		let d: number = this.d;
		d *= 24;

		d += this.h;
		d *= 60;

		d += this.i;
		d *= 60;

		d += this.s;
		d *= 1000;

		d += this.ms;

		return d;
	}
	public set millis(d: number) {
		this.ms = d % 1000;
		d = Math.floor(d / 1000);

		this.s = d % 60;
		d = Math.floor(d / 60);

		this.i = d % 60;
		d = Math.floor(d / 60);

		this.h = d % 24;
		d = Math.floor(d / 24);

		this.d = d;

		this.refactor();
	}

	public get seconds(): number {
		return this.millis / 1000;
	}
	public set seconds(d: number) {
		this.millis = d * 1000;
	}

	public get minutes(): number {
		return this.seconds / 60;
	}
	public set minutes(d: number) {
		this.seconds = d * 60;
	}

	public get hours(): number {
		return this.minutes / 60;
	}
	public set hours(d: number) {
		this.minutes = d * 60;
	}

	public get days(): number {
		return this.hours / 24;
	}
	public set days(d: number) {
		this.hours = d * 24;
	}

	public get Hi(): string {
		return `${this.h.toString(10).padStart(2, '0')}:${this.i.toString(10).padStart(2, '0')}`;
	}
	public set Hi(Hi: string) {
		if (!COMMONS_REGEX_PATTERN_TIME_HI.test(Hi)) throw new Error('Invalid Hi string');

		const digits: number[] = splitDigits(Hi);
		
		this.d = 0;
		this.h = digits[0];
		this.i = digits[1];
		this.s = 0;
		this.ms = 0;

		this.refactor();
	}

	public get His(): string {
		return `${this.Hi}:${this.s.toString(10).padStart(2, '0')}`;
	}
	public set His(His: string) {
		if (!COMMONS_REGEX_PATTERN_TIME_HIS.test(His)) throw new Error('Invalid His string');

		const parts: string[] = His.split(':');

		this.Hi = `${parts[0]}:${parts[1]}`;

		const digits: number[] = splitDigits(parts[2]);

		this.s = digits[0];

		this.refactor();
	}

	public get HisMs(): string {
		return `${this.His}.${this.ms.toString(10)}`;
	}
	public set HisMs(HisMs: string) {
		const parts: string[] = HisMs.split('.');
		if (parts.length === 1) parts.push('0');
		if (parts.length !== 2) throw new Error('Invalid HisMs');

		if (!/^[0-9]+$/.test(parts[1])) throw new Error('Invalid Ms for HisMs');

		this.His = parts[0];

		this.ms = parseInt(parts[1], 10);

		this.refactor();
	}

	public get dHis(): string {
		return `${this.d.toString(10)}:${this.His}`;
	}
	public set dHis(dHis: string) {
		const parts: string[] = dHis.split(':');
		if (parts.length !== 4) throw new Error('Invalid dHis');

		this.His = `${parts[1]}:${parts[2]}:${parts[3]}`;
		this.d = parseInt(parts[0], 10);
	}

	public get dHisMs(): string {
		return `${this.d.toString(10)}:${this.HisMs}`;
	}
	public set dHisMs(dHisMs: string) {
		const parts: string[] = dHisMs.split(':');
		if (parts.length !== 4) throw new Error('Invalid dHis');

		const sms: string[] = parts[3].split('.');
		if (sms.length < 2) sms.push('0');

		this.His = `${parts[1]}:${parts[2]}:${sms[0]}`;
		this.d = parseInt(parts[0], 10);
		this.ms = parseInt(sms[1], 10);
		
		this.refactor();
	}

	public get UTCDate(): Date {
		this.refactor();

		const date: Date = new Date(0);
		date.setUTCDate(this.d + 1);
		date.setUTCHours(this.h);
		date.setUTCMinutes(this.i);
		date.setUTCSeconds(this.s);
		date.setUTCMilliseconds(this.ms);

		return date;
	}
	public set UTCDate(date: Date) {
		this.d = date.getUTCDate() - 1;
		this.h = date.getUTCHours();
		this.i = date.getUTCMinutes();
		this.s = date.getUTCSeconds();
		this.ms = date.getUTCMilliseconds();

		this.refactor();
	}

	public get pretty(): string {
		if (this.d > 4) return `${this.d.toString(10)} days`;

		if (this.d > 0) return `${this.d.toString(10)} days, ${this.h.toString(10)} hours`;

		if (this.h > 6) return `${this.h.toString(10)} hours`;

		if (this.h > 0) return `${this.h.toString(10)} hours, ${this.i.toString(10)} minutes`;

		if (this.i > 4) return `${this.i.toString(10)} minutes`;

		if (this.i > 0) return `${this.i.toString(10)} minutes, ${this.s.toString(10)} seconds`;

		return `${this.s.toString(10)} seconds`;
	}

	public toString(): string {
		return this.dHisMs;
	}

	public get dHisObject(): TCommonsFixeddHisObject {
		return {
				day: this.day,
				hour: this.hour,
				minute: this.minute,
				second: this.second
		};
	}
	public set dHisObject(object: TCommonsFixeddHisObject) {
		this.d = object.day;
		this.h = object.hour;
		this.i = object.minute;
		this.s = object.second;
		this.ms = 0;

		this.refactor();
	}

	public get object(): TCommonsFixedDurationObject {
		return {
				...this.dHisObject,
				milli: this.milli
		};
	}
	public set object(object: TCommonsFixedDurationObject) {
		this.dHisObject = object;
		this.milli = object.milli;

		this.refactor();
	}

	public get dHisJsonString(): string {
		return JSON.stringify(this.dHisObject);
	}
	public set dHisJsonString(s: string) {
		const parsed: unknown = JSON.parse(s);
		if (!isTCommonsFixeddHisObject(parsed)) throw new Error('Invalid TFixeddHisObject');

		this.dHisObject = parsed;
	}

	public get jsonString(): string {
		return JSON.stringify(this.object);
	}
	public set jsonString(s: string) {
		const parsed: unknown = JSON.parse(s);
		if (!isTCommonsFixedDurationObject(parsed)) throw new Error('Invalid TFixedDurationObject');

		this.object = parsed;
	}

	public truncateToSecond(): void {
		this.ms = 0;
	}
	
	public truncateToMinute(): void {
		this.truncateToSecond();
		this.s = 0;
	}

	public truncateToHour(): void {
		this.truncateToMinute();
		this.i = 0;
	}

	public truncateToDay(): void {
		this.truncateToHour();
		this.h = 0;
	}

	public quantiseToSeconds(
			seconds: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.truncateToSecond();

		const periodMillis: number = seconds *= 1000;

		switch (direction) {
			case 'down':
				this.millis = Math.floor(this.millis / periodMillis) * periodMillis;
				break;
			case 'up':
				this.millis = Math.ceil(this.millis / periodMillis) * periodMillis;
				break;
			case 'closest':
				this.millis = Math.round(this.millis / periodMillis) * periodMillis;
				break;
		}
	}

	public quantiseToMinutes(
			minutes: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.quantiseToSeconds(minutes * 60, direction);
		this.truncateToMinute();
	}

	public quantiseToHours(
			hours: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.quantiseToMinutes(hours * 60, direction);
		this.truncateToHour();
	}

	public quantiseToDays(
			days: number,
			direction: 'down'|'up'|'closest'
	): void {
		this.quantiseToHours(days * 24, direction);
		this.truncateToDay();
	}
}
