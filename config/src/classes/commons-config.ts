import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyDate
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';

export class CommonsConfig {
	public static hasProperty(config: TPropertyObject, key: string): boolean {
		return commonsTypeHasProperty(config, key);
	}
	
	public static getObject(config: TPropertyObject, key: string, defaultValue?: TPropertyObject): TPropertyObject {
		if (!CommonsConfig.hasProperty(config, key)) {
			if (defaultValue !== undefined) return defaultValue;
			throw new Error(`No such key: ${key}`);
		}

		if (!commonsTypeHasPropertyObject(config, key)) throw new Error('Key value is not an object');

		return config[key] as TPropertyObject;
	}
	
	public static getString(config: TPropertyObject, key: string, defaultValue?: string): string {
		if (!CommonsConfig.hasProperty(config, key)) {
			if (defaultValue !== undefined) return defaultValue;
			throw new Error(`No such key: ${key}`);
		}

		if (!commonsTypeHasPropertyString(config, key)) throw new Error('Key value is not a string');

		return config[key] as string;
	}
	
	public static getNumber(config: TPropertyObject, key: string, defaultValue?: number): number {
		if (!CommonsConfig.hasProperty(config, key)) {
			if (defaultValue !== undefined) return defaultValue;
			throw new Error(`No such key: ${key}`);
		}

		if (!commonsTypeHasPropertyNumber(config, key)) throw new Error('Key value is not a number');

		return config[key] as number;
	}
	
	public static getBoolean(config: TPropertyObject, key: string, defaultValue?: boolean): boolean {
		if (!CommonsConfig.hasProperty(config, key)) {
			if (defaultValue !== undefined) return defaultValue;
			throw new Error(`No such key: ${key}`);
		}

		if (!commonsTypeHasPropertyBoolean(config, key)) throw new Error('Key value is not a boolean');

		return config[key] ? true : false;
	}
	
	public static getDate(config: TPropertyObject, key: string, defaultValue?: Date): Date {
		if (!CommonsConfig.hasProperty(config, key)) {
			if (defaultValue !== undefined) return defaultValue;
			throw new Error(`No such key: ${key}`);
		}

		if (!commonsTypeHasPropertyDate(config, key)) throw new Error('Key value is not a Date');

		return config[key] as Date;
	}
	
	public static setObject(config: TPropertyObject, key: string, data: TPropertyObject): void {
		config[key] = data;
	}
	
	public static setString(config: TPropertyObject, key: string, data: string): void {
		config[key] = data;
	}
	
	public static setNumber(config: TPropertyObject, key: string, data: number): void {
		config[key] = data;
	}
	
	public static setBoolean(config: TPropertyObject, key: string, data: boolean): void {
		config[key] = data;
	}
	
	public static setDate(config: TPropertyObject, key: string, data: Date): void {
		config[key] = data;
	}

	constructor(
			private config: TPropertyObject
	) {}
	
	protected setRawObject(config: TPropertyObject): void {
		this.config = config;
	}
	
	protected getRawObject(): TPropertyObject {
		return this.config;
	}

	public cloneConfig(src: CommonsConfig): void {
		this.setRawObject(src.getRawObject());
	}

	public hasProperty(key: string): boolean {
		return CommonsConfig.hasProperty(this.config, key);
	}
	
	public getObject(key: string, defaultValue?: TPropertyObject): TPropertyObject {
		return CommonsConfig.getObject(this.config, key, defaultValue);
	}
	
	public getObjectAsConfig(key: string, allowEmpty: boolean = false): TPropertyObject {
		const object: TPropertyObject = this.getObject(key, allowEmpty ? {} : undefined);
		return new CommonsConfig(object);
	}
	
	public getString(key: string, defaultValue?: string): string {
		return CommonsConfig.getString(this.config, key, defaultValue);
	}
	
	public getNumber(key: string, defaultValue?: number): number {
		return CommonsConfig.getNumber(this.config, key, defaultValue);
	}
	
	public getBoolean(key: string, defaultValue?: boolean): boolean {
		return CommonsConfig.getBoolean(this.config, key, defaultValue);
	}
	
	public getDate(key: string, defaultValue?: Date): Date {
		return CommonsConfig.getDate(this.config, key, defaultValue);
	}
	
	public setObject(key: string, data: TPropertyObject): void {
		return CommonsConfig.setObject(this.config, key, data);
	}
	
	public setString(key: string, data: string): void {
		return CommonsConfig.setString(this.config, key, data);
	}
	
	public setNumber(key: string, data: number): void {
		return CommonsConfig.setNumber(this.config, key, data);
	}
	
	public setBoolean(key: string, data: boolean): void {
		return CommonsConfig.setBoolean(this.config, key, data);
	}
	
	public setDate(key: string, data: Date): void {
		return CommonsConfig.setDate(this.config, key, data);
	}
}
