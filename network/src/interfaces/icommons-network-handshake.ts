import { commonsBase62HasPropertyId } from 'tscommons-es-core';

import { TChannel, TDeviceId } from './icommons-network-packet';

export interface ICommonsNetworkHandshake {
		channel: TChannel;
		deviceId: TDeviceId;
		[ key: string ]: unknown;
}

export function isICommonsNetworkHandshake(test: unknown): test is ICommonsNetworkHandshake {
	if (!commonsBase62HasPropertyId(test, 'channel')) return false;
	if (!commonsBase62HasPropertyId(test, 'deviceId')) return false;

	return true;
}
