import {
		commonsBase62HasPropertyId,
		commonsBase62HasPropertyIdArrayOrUndefined,
		commonsBase62HasPropertyIdOrUndefined,
		commonsTypeHasProperty,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyDateOrUndefined,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyString,
		TEncoded
} from 'tscommons-es-core';

import { ETransmissionMethod, isETransmissionMethod } from '../enums/etransmission-method';

export const REGEX_PATTERN_NS = '^[A-Za-z](-?[0-9A-Za-z]+)*$';
export const REGEX_PATTERN_COMMAND = '^[A-Za-z](-?[0-9A-Za-z]+)*$';

export type TChannel = string;
export type TDeviceId = string;

export interface ICommonsNetworkPacket {
		id: string;
		method: ETransmissionMethod;
		channel: TChannel;
		destination?: TDeviceId;
		destinations?: TDeviceId[];
		source: TDeviceId;
		ignoreOwn?: boolean;
		timestamp: Date;
		expiry?: Date;
		ns: string;
		command: string;
		data?: TEncoded;
		replay?: boolean;
}

export function isICommonsNetworkPacket(test: unknown): test is ICommonsNetworkPacket {
	if (!commonsBase62HasPropertyId(test, 'id')) return false;
	if (!commonsTypeHasPropertyEnum<ETransmissionMethod>(test, 'method', isETransmissionMethod)) return false;
	if (!commonsBase62HasPropertyId(test, 'channel')) return false;
	if (!commonsBase62HasPropertyId(test, 'source')) return false;
	if (!commonsTypeHasPropertyString(test, 'ns')) return false;

	if (!commonsBase62HasPropertyIdOrUndefined(test, 'destination')) return false;
	if (!commonsBase62HasPropertyIdArrayOrUndefined(test, 'destinations')) return false;

	if (!commonsTypeHasPropertyDate(test, 'timestamp')) return false;
	if (!commonsTypeHasPropertyDateOrUndefined(test, 'expiry')) return false;

	switch (test.method) {
		case ETransmissionMethod.BROADCAST:
			if (commonsTypeHasProperty(test, 'destination') || commonsTypeHasProperty(test, 'destinations')) return false;
			break;
		case ETransmissionMethod.DIRECT:
			if (!commonsTypeHasProperty(test, 'destination') || commonsTypeHasProperty(test, 'destinations')) return false;
			break;
		case ETransmissionMethod.MULTICAST:
			if (commonsTypeHasProperty(test, 'destination') || !commonsTypeHasProperty(test, 'destinations')) return false;
			break;
	}
	
	const regexNs: RegExp = new RegExp(REGEX_PATTERN_NS);
	if (!regexNs.test(test.ns as string)) return false;
	
	const regexCommand: RegExp = new RegExp(REGEX_PATTERN_COMMAND);
	if (!regexCommand.test(test.command as string)) return false;

	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'ignoreOwn')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'replay')) return false;
	
	return true;
}
