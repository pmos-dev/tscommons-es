import {
		commonsBase62HasPropertyId,
		commonsBase62IsId,
		commonsDateCompressedToDate,
		commonsDateDateToCompressed,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringOrUndefined,
		TEncoded
} from 'tscommons-es-core';

import { ICommonsNetworkPacket, REGEX_PATTERN_COMMAND, REGEX_PATTERN_NS, TChannel, TDeviceId } from '../interfaces/icommons-network-packet';

import { ETransmissionMethod } from '../enums/etransmission-method';

export type TCommonsNetworkPacketCompressed = {
		i: string;
		m: string;
		c: TChannel;
		d?: string;
		s: TDeviceId;
		o?: number;
		t: string;
		e?: string;
		n: string;
		x: string;
		z?: TEncoded;
		r?: number;
};

export function isTCommonsNetworkPacketCompressed(test: unknown): test is TCommonsNetworkPacketCompressed {
	if (!commonsBase62HasPropertyId(test, 'i')) return false;
	if (!commonsTypeHasPropertyString(test, 'm') || ![ 'b', 'd', 'm' ].includes(test.m as string)) return false;
	if (!commonsBase62HasPropertyId(test, 'c')) return false;
	if (!commonsBase62HasPropertyId(test, 's')) return false;
	if (!commonsTypeHasPropertyString(test, 't')) return false;
	if (!commonsTypeHasPropertyString(test, 'n')) return false;

	switch (test.m) {
		case 'b':
			break;
		case 'd':
			if (!commonsBase62HasPropertyId(test, 'd')) return false;
			break;
		case 'm':
			if (!commonsTypeHasPropertyString(test, 'd')) return false;
			const destinations: string[] = (test.d as string).split(',');
			for (const destination of destinations) {
				if (!commonsBase62IsId(destination)) return false;
			}
			break;
	}

	if (!commonsTypeHasPropertyStringOrUndefined(test, 'e')) return false;

	const regexNs: RegExp = new RegExp(REGEX_PATTERN_NS);
	if (!regexNs.test(test.n as string)) return false;
	
	const regexCommand: RegExp = new RegExp(REGEX_PATTERN_COMMAND);
	if (!regexCommand.test(test.x as string)) return false;

	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'r')) return false;
	
	return true;
}

export function compressPacket(packet: ICommonsNetworkPacket): TCommonsNetworkPacketCompressed {
	const compressed: TCommonsNetworkPacketCompressed = {
			i: packet.id,
			m: '',
			c: packet.channel,
			s: packet.source,
			t: commonsDateDateToCompressed(packet.timestamp, true),
			n: packet.ns,
			x: packet.command
	};
	
	switch (packet.method) {
		case ETransmissionMethod.BROADCAST:
			compressed.m = 'b';
			break;
		case ETransmissionMethod.DIRECT:
			compressed.m = 'd';
			compressed.d = packet.destination;
			break;
		case ETransmissionMethod.MULTICAST:
			compressed.m = 'm';
			compressed.d = packet.destinations!.join(',');
			break;
	}
	
	if (packet.ignoreOwn) compressed.o = 1;
	if (packet.expiry) compressed.e = commonsDateDateToCompressed(packet.expiry, true);
	
	if (packet.data !== undefined) compressed.z = packet.data;
	
	if (packet.replay !== undefined) compressed.r = packet.replay ? 1 : 0;
	
	return compressed;
}

export function decompressPacket(compressed: TCommonsNetworkPacketCompressed): ICommonsNetworkPacket {
	const packet: ICommonsNetworkPacket = {
			id: compressed.i,
			method: ETransmissionMethod.BROADCAST,
			channel: compressed.c,
			source: compressed.s,
			timestamp: commonsDateCompressedToDate(compressed.t, true),
			ns: compressed.n,
			command: compressed.x
	};
	
	switch (compressed.m) {
		case 'b':
			packet.method = ETransmissionMethod.BROADCAST;
			break;
		case 'd':
			packet.method = ETransmissionMethod.DIRECT;
			packet.destination = compressed.d;
			break;
		case 'm':
			packet.method = ETransmissionMethod.MULTICAST;
			packet.destinations = compressed.d!.split(',');
			break;
	}

	if (compressed.o === 1) packet.ignoreOwn = true;
	if (compressed.e) packet.expiry = commonsDateCompressedToDate(compressed.e, true);

	if (compressed.z !== undefined) packet.data = compressed.z;
	
	if (compressed.r !== undefined) packet.replay = compressed.r === 1;
	
	return packet;
}
