import { commonsTypeIsString } from 'tscommons-es-core';

export enum ETransmissionMethod {
		BROADCAST = 'broadcast',
		DIRECT = 'direct',
		MULTICAST = 'multicast'
}

export function isETransmissionMethod(test: unknown): test is ETransmissionMethod {
	if (!commonsTypeIsString(test)) return false;

	switch (test) {
		case ETransmissionMethod.BROADCAST:
		case ETransmissionMethod.DIRECT:
		case ETransmissionMethod.MULTICAST:
			return true;
		default:
			return false;
	}
}
