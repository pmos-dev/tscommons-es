import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyT
} from 'tscommons-es-core';

export interface ICommonsSession<T> {
		sid: string;
		
		start: Date;
		last: Date;
		
		data: T;
}

export function isICommonsSession<T>(
		test: unknown,
		isT: (t: unknown) => t is T
): test is ICommonsSession<T> {
	if (!commonsTypeHasPropertyString(test, 'sid')) return false;
	
	if (!commonsTypeHasPropertyDate(test, 'start')) return false;
	if (!commonsTypeHasPropertyDate(test, 'last')) return false;

	if (!commonsTypeHasPropertyT<T>(test, 'data', isT)) return false;
	
	return true;
}
