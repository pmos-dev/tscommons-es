import { commonsTypeHasPropertyString, commonsTypeHasPropertyBoolean } from 'tscommons-es-core';

export interface ICommonsUser {
		username: string;
		enabled: boolean;
}

export function isICommonsUser(
		test: unknown
): test is ICommonsUser {
	if (!commonsTypeHasPropertyString(test, 'username')) return false;
	if (!commonsTypeHasPropertyBoolean(test, 'enabled')) return false;
	
	return true;
}
