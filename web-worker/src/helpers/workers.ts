import { TEncodedObject, TPropertyObject, commonsTypeDecodePropertyObject, commonsTypeEncodePropertyObject } from 'tscommons-es-core';

export function commonsWebWorkerWrapAsPromise<
		ParamsT extends TPropertyObject = TPropertyObject,
		ReturnT extends TPropertyObject = TPropertyObject
>(
		createWorker: () => Worker,
		params: ParamsT,
		serialiseParams: (_: ParamsT) => TEncodedObject = (p: ParamsT): TEncodedObject => commonsTypeEncodePropertyObject(p),
		unserialiseReturn: (_: TEncodedObject) => ReturnT = (serialised: TEncodedObject): ReturnT => commonsTypeDecodePropertyObject(serialised) as ReturnT,
		index?: string,
		activeWorkers?: Map<string, Worker>,
		debugContext?: string
): Promise<ReturnT> {
	if (index && activeWorkers) {
		const activeWorker: Worker|undefined = activeWorkers.get(index);
		if (activeWorker) {
			if (debugContext) console.log(`Aborting active worker ${debugContext || ''}`);
			activeWorker.terminate();
			activeWorkers.delete(index);
		}
	}

	const serialisedParams: TEncodedObject = serialiseParams(params);

	return new Promise<ReturnT>((resolve: (result: ReturnT) => void, error: (e: Error) => void): void => {
		const closure: Worker = createWorker();
		
		if (index && activeWorkers) activeWorkers.set(index, closure);

		closure.onmessage = ({ data }) => {
			const serialisedReturn: TEncodedObject = data as TEncodedObject;
			const unserialisedReturn: ReturnT = unserialiseReturn(serialisedReturn);

			if (index && activeWorkers) {
				const activeWorker: Worker|undefined = activeWorkers.get(index);
				if (activeWorker && activeWorker !== closure) {
					error(new Error('Worker previously terminated'));
					return;
				}

				activeWorkers.delete(index);
				if (debugContext) console.log(`CONCLUDED ${debugContext || ''}`);
			}

			resolve(unserialisedReturn);
		};

		closure.postMessage(serialisedParams);
	});
};
