import { commonsArrayShuffle } from 'tscommons-es-core';

import { TCommonsAlgorithmSearchNode } from '../types/tsearch-node';

export type TCommonsAlgorithmInternalNode<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> = NodeT & {
		parent: TCommonsAlgorithmInternalNode<ActionT, NodeT>|undefined;
}

export function commonsAlgorithmDeInternalise<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>,
		InternalNodeT extends TCommonsAlgorithmInternalNode<ActionT, NodeT>
>(node: InternalNodeT): NodeT {
	const clone: NodeT = { ...node };
	delete clone['parent'];

	return clone;
}

export abstract class CommonsAlgorithmSearch<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> {
	protected frontier: TCommonsAlgorithmInternalNode<ActionT, NodeT>[] = [];
	private explored: Omit<TCommonsAlgorithmInternalNode<ActionT, NodeT>, 'action'|'parent'>[] = [];

	constructor(
			private actions: ActionT[],
			private shuffle: boolean = true
	) {}

	protected abstract get next(): TCommonsAlgorithmInternalNode<ActionT, NodeT>|undefined;

	protected abstract isEqual(
			a: Omit<NodeT, 'action'>,
			b: Omit<NodeT, 'action'>
	): boolean;
	
	protected abstract isGoal(node: NodeT): boolean;

	protected abstract applyActionIfPossible(
			node: NodeT,
			action: ActionT
	): NodeT|undefined;

	private inArray(
			search: Omit<TCommonsAlgorithmInternalNode<ActionT, NodeT>, 'action'|'parent'>,
			array: Omit<TCommonsAlgorithmInternalNode<ActionT, NodeT>, 'action'|'parent'>[]
	) {
		for (const n of array) if (this.isEqual(search as Omit<NodeT, 'action'>, n as Omit<NodeT, 'action'>)) return true;

		return false;
	}

	private inFrontier(node: TCommonsAlgorithmInternalNode<ActionT, NodeT>): boolean {
		return this.inArray(node, this.frontier);
	}

	private inExplored(node: TCommonsAlgorithmInternalNode<ActionT, NodeT>): boolean {
		return this.inArray(node, this.explored);
	}

	protected transition(
			node: TCommonsAlgorithmInternalNode<ActionT, NodeT>,
			action: ActionT
	): TCommonsAlgorithmInternalNode<ActionT, NodeT>|undefined {
		const applied: NodeT|undefined = this.applyActionIfPossible(node, action);
		if (!applied) return undefined;

		return {
				...applied,
				parent: node,
				action: action
		};
	}

	protected transitions(node: TCommonsAlgorithmInternalNode<ActionT, NodeT>): TCommonsAlgorithmInternalNode<ActionT, NodeT>[] {
		const potentials: TCommonsAlgorithmInternalNode<ActionT, NodeT>[] = [];

		const actions: ActionT[] = [ ...this.actions ];
		if (this.shuffle) commonsArrayShuffle(actions);

		for (const action of actions) {
			const attempt: TCommonsAlgorithmInternalNode<ActionT, NodeT>|undefined = this.transition(node, action);
			if (!attempt) continue;

			potentials.push({
					...attempt,
					action: action,
					parent: node
			});
		}

		return potentials;
	}

	public computeRoute(
			start: Omit<NodeT, 'action'>,
			progressCallback?: (frontierLength: number, exploredLength: number) => void,
			progressBlockSize: number = 10
	): NodeT[]|undefined {
		this.frontier.push({
				...start,
				parent: undefined,
				action: undefined
		} as TCommonsAlgorithmInternalNode<ActionT, NodeT>);

		let next: TCommonsAlgorithmInternalNode<ActionT, NodeT>|undefined;

		let i: number = 0;
		while (true) {
			if (progressCallback) {
				if ((i++ % progressBlockSize) === 0) progressCallback(this.frontier.length, this.explored.length);
			}

			next = this.next;
			if (!next) return undefined;

			if (this.isGoal(next)) break;

			// destructuring seems to be more efficient than clone and delete
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const { parent, action, ...clone } = { ...next, action: undefined, parent: undefined };
			this.explored.push(clone);

			for (const n of this.transitions(next)) {
				if (this.inFrontier(n)) continue;
				if (this.inExplored(n)) continue;

				this.frontier.push(n);
			}
		}

		if (!next) return undefined;

		// traverse backwards
		const route: NodeT[] = [];
		let nextAction: ActionT|undefined;

		while (next) {
			const clone: NodeT = commonsAlgorithmDeInternalise<
					ActionT,
					NodeT,
					TCommonsAlgorithmInternalNode<ActionT, NodeT>
			>(next);
			
			route.push({
					...clone,
					action: nextAction
			});

			nextAction = next.action;
			next = next.parent;
		}

		return route.reverse();
	}

	public listExplored(): NodeT[] {
		return [ ...this.explored ]
				.map((e: TCommonsAlgorithmInternalNode<ActionT, NodeT>): NodeT => commonsAlgorithmDeInternalise<
						ActionT,
						NodeT,
						TCommonsAlgorithmInternalNode<ActionT, NodeT>
				>(e));
	}
}
