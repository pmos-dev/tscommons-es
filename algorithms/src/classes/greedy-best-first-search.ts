import { TCommonsAlgorithmSearchNode } from '../types/tsearch-node';

import { CommonsAlgorithmSearch, TCommonsAlgorithmInternalNode, commonsAlgorithmDeInternalise } from './search';

export abstract class CommonsAlgorithmGreedyBestFirstSearch<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> extends CommonsAlgorithmSearch<ActionT, NodeT> {
	protected abstract heuristic(node: NodeT): number;

	protected get next(): TCommonsAlgorithmInternalNode<ActionT, NodeT> | undefined {
		if (this.frontier.length === 0) return undefined;

		let bestScore: number|undefined;
		let bestIndex: number|undefined;

		for (let i = 0; i < this.frontier.length; i++) {
			const score: number = this.heuristic(commonsAlgorithmDeInternalise(this.frontier[i]));

			if (bestScore === undefined || score < bestScore) {
				bestScore = score;
				bestIndex = i;
			}
		}

		if (bestIndex === undefined) return undefined;

		return this.frontier.splice(bestIndex, 1)[0];
	}
}
