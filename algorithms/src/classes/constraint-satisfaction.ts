import { commonsArrayRemove, commonsArrayRemoveAll, commonsArrayShuffle, commonsBase62GenerateRandomId } from 'tscommons-es-core';

export abstract class CommonsAlgorithmConstraintSatisfactionNode<DomainT> {
	private internalUid: string;

	private saved: DomainT[]|undefined;

	constructor(
			private internalDomain: DomainT[],
			private isDomainValueEqual?: (a: DomainT, b: DomainT) => boolean
	) {
		this.internalUid = commonsBase62GenerateRandomId();
	}

	public saveDomains(): void {
		if (this.saved) throw new Error('Save collision');
		this.saved = [ ...this.internalDomain ];
	}

	public revertDomains(): void {
		if (!this.saved) throw new Error('No save to revert to');
		this.internalDomain = this.saved;	// not clone
		this.saved = undefined;
	}

	public get domain(): DomainT[] {
		return [ ...this.internalDomain ];
	}
	public set domain(values: DomainT[]) {
		this.internalDomain = [ ...values ];
	}

	public removeFromDomain(value: DomainT): void {
		commonsArrayRemove(
				this.internalDomain,
				value,
				this.isDomainValueEqual
		);
	}

	public get uid(): string {
		return this.internalUid;
	}

	public isEqual(b: CommonsAlgorithmConstraintSatisfactionNode<DomainT>): boolean {
		return this.internalUid === b.internalUid;
	}

	public abstract applyUnaryConsistency(): void;
}

export class CommonsAlgorithmConstraintSatisfactionEdge<
		DomainT,
		NodeT extends CommonsAlgorithmConstraintSatisfactionNode<DomainT>,
		EdgeDataT
> {
	constructor(
			private xNode: NodeT,
			private yNode: NodeT,
			private edgeData?: EdgeDataT
	) {}

	public get x(): NodeT {
		return this.xNode;
	}

	public get y(): NodeT {
		return this.yNode;
	}

	public get data(): EdgeDataT|undefined {
		return this.edgeData;
	}

	public isEqual(edge: CommonsAlgorithmConstraintSatisfactionEdge<DomainT, NodeT, EdgeDataT>): boolean {
		return edge.xNode.isEqual(this.xNode) && edge.yNode.isEqual(this.yNode);
	}
}

export abstract class CommonsAlgorithmConstraintSatisfactionGraph<
		DomainT,
		NodeT extends CommonsAlgorithmConstraintSatisfactionNode<DomainT>,
		EdgeDataT = void
> {
	private internalNodes: NodeT[] = [];
	private internalEdges: CommonsAlgorithmConstraintSatisfactionEdge<
			DomainT,
			NodeT,
			EdgeDataT
	>[] = [];

	public get nodes(): NodeT[] {
		return [ ...this.internalNodes ];
	}

	public get edges(): CommonsAlgorithmConstraintSatisfactionEdge<
			DomainT,
			NodeT,
			EdgeDataT
	>[] {
		return [ ...this.internalEdges ];
	}

	public addNode(node: NodeT): NodeT {
		const existing: NodeT|undefined = this.internalNodes
				.find((n: NodeT): boolean => n.isEqual(node));
		if (existing) return existing;

		this.internalNodes.push(node);
		return node;
	}

	public removeNode(node: NodeT): boolean {
		const existing: NodeT|undefined = this.internalNodes
				.find((n: NodeT): boolean => n.isEqual(node));
		if (!existing) return false;

		commonsArrayRemove(this.internalNodes, existing);

		const edgeMatches: CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		>[] = this.internalEdges
				.filter((edge: CommonsAlgorithmConstraintSatisfactionEdge<
						DomainT,
						NodeT,
						EdgeDataT
				>): boolean => edge.x.uid === existing.uid || edge.y.uid === existing.uid);

		commonsArrayRemoveAll(this.internalEdges, edgeMatches);

		return true;
	}

	public join(
			a: NodeT,
			b: NodeT,
			data?: EdgeDataT
	): CommonsAlgorithmConstraintSatisfactionEdge<DomainT, NodeT, EdgeDataT> {
		const existingA: NodeT|undefined = this.internalNodes
				.find((n: NodeT): boolean => n.isEqual(a));
		if (!existingA) throw new Error('Node A has not been added yet');

		const existingB: NodeT|undefined = this.internalNodes
				.find((n: NodeT): boolean => n.isEqual(b));
		if (!existingB) throw new Error('Node B has not been added yet');

		if (existingA.uid === existingB.uid) throw new Error('Nodes cannot be equal');

		const edge: CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		> = new CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		>(existingA, existingB, data);

		const existing: CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		>|undefined = this.internalEdges
				.find((e: CommonsAlgorithmConstraintSatisfactionEdge<
						DomainT,
						NodeT,
						EdgeDataT
				>): boolean => e.isEqual(edge));
		if (existing) return existing;

		this.internalEdges.push(edge);

		return edge;
	}

	public unjoin(a: NodeT, b: NodeT): void {
		const existingA: NodeT|undefined = this.internalNodes
				.find((n: NodeT): boolean => n.isEqual(a));
		if (!existingA) throw new Error('Node A has not been added yet');

		const existingB: NodeT|undefined = this.internalNodes
				.find((n: NodeT): boolean => n.isEqual(b));
		if (!existingB) throw new Error('Node B has not been added yet');

		if (existingA.uid === existingB.uid) throw new Error('Nodes cannot be equal');

		const edge: CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		>|undefined = this.internalEdges
				.find((e: CommonsAlgorithmConstraintSatisfactionEdge<
						DomainT,
						NodeT,
						EdgeDataT
				>): boolean => e.x.uid === a.uid && e.y.uid === b.uid);
		if (edge) commonsArrayRemove(this.internalEdges, edge);

		this.unjoin(b, a);
	}

	private listEdges(x: NodeT): CommonsAlgorithmConstraintSatisfactionEdge<
			DomainT,
			NodeT,
			EdgeDataT
	>[] {
		return this.internalEdges
				.filter((e: CommonsAlgorithmConstraintSatisfactionEdge<
						DomainT,
						NodeT,
						EdgeDataT
				>): boolean => e.x.isEqual(x));
	}

	public listRemoteNodes(x: NodeT): NodeT[] {
		return this.listEdges(x)
				.map((e: CommonsAlgorithmConstraintSatisfactionEdge<
						DomainT,
						NodeT,
						EdgeDataT
				>): NodeT => e.y);
	}

	protected abstract satisfies(
			xValue: DomainT,
			yValue: DomainT,
			edge: CommonsAlgorithmConstraintSatisfactionEdge<
					DomainT,
					NodeT,
					EdgeDataT
			>|undefined
	): boolean;

	private revise(x: NodeT, y: NodeT): boolean {
		let isRevised: boolean = false;

		const edge: CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		>|undefined = this.internalEdges
				.find((e: CommonsAlgorithmConstraintSatisfactionEdge<
						DomainT,
						NodeT,
						EdgeDataT
				>): boolean => e.x.isEqual(x) && e.y.isEqual(y));

		for (const xValue of x.domain) {
			let satisfied: boolean = false;
			for (const yValue of y.domain) {

				if (this.satisfies(
						xValue,
						yValue,
						edge
				)) {
					satisfied = true;
					break;
				}
			}

			if (!satisfied) {
				x.removeFromDomain(xValue);
				isRevised = true;
			}
		}

		return isRevised;
	}

	public applyUnaryConsistency(): void {
		for (const node of this.internalNodes) {
			node.applyUnaryConsistency();
		}
	}

	public applyAc3(): boolean {
		const edges: CommonsAlgorithmConstraintSatisfactionEdge<
				DomainT,
				NodeT,
				EdgeDataT
		>[] = [ ...this.internalEdges ];

		while (edges.length > 0) {
			const edge: CommonsAlgorithmConstraintSatisfactionEdge<
					DomainT,
					NodeT,
					EdgeDataT
			>|undefined = edges.shift();
			if (!edge) throw new Error('Empty edges array. Should not be possible');

			if (this.revise(edge.x, edge.y)) {
				if (edge.x.domain.length === 0) return false;

				const zs: NodeT[] = this.listRemoteNodes(edge.x);
				commonsArrayRemove(zs, edge.y);

				for (const z of zs) {
					const zEdge: CommonsAlgorithmConstraintSatisfactionEdge<
							DomainT,
							NodeT,
							EdgeDataT
					> = new CommonsAlgorithmConstraintSatisfactionEdge<
							DomainT,
							NodeT,
							EdgeDataT
					>(z, edge.x);
					edges.push(zEdge);
				}
			}
		}

		return true;
	}

	public saveSnapshot(): void {
		for (const node of this.nodes) node.saveDomains();
	}

	public revertSnapshot(): void {
		for (const node of this.nodes) node.revertDomains();
	}

	public snapshotForInference(): Map<string, DomainT[]> {
		const map: Map<string, DomainT[]> = new Map<string, DomainT[]>();
		for (const node of this.nodes) {
			map.set(node.uid, [ ...node.domain ]);
		}

		return map;
	}

	public isUnsolvable(): boolean {
		for (const node of this.nodes) {
			if (node.domain.length === 0) return true;
		}

		return false;
	}

	public isComplete(): boolean {
		if (!this.isConsistent()) return false;

		for (const node of this.nodes) {
			if (node.domain.length > 1) return false;
		}

		return true;
	}

	public isConsistent(): boolean {
		return true;
	}

	public get mostConnecteds(): NodeT[] {
		type TNodeWithEdgeCount = {
				node: NodeT;
				edgeCount: number;
		};

		const clone: TNodeWithEdgeCount[] = this.nodes
				.map((node: NodeT): TNodeWithEdgeCount => ({
						node: node,
						edgeCount: this.listEdges(node).length
				}));
		if (clone.length === 0) return [];

		clone
				.sort((a: TNodeWithEdgeCount, b: TNodeWithEdgeCount): number => {
					if (a.edgeCount < b.edgeCount) return -1;
					if (a.edgeCount > b.edgeCount) return 1;
					return 0;
				})
				.reverse();
		
		const best: TNodeWithEdgeCount = clone[0];

		const bests: TNodeWithEdgeCount[] = clone
				.filter((c: TNodeWithEdgeCount): boolean => c.edgeCount === best.edgeCount);
		
		return bests
				.map((c: TNodeWithEdgeCount): NodeT => c.node);
	}

	public get mostConnected(): NodeT|undefined {
		const choices: NodeT[] = this.mostConnecteds;
		if (choices.length === 0) return undefined;

		return choices[Math.floor(Math.random() * choices.length)];
	}

	public get smallestNonSingularDomains(): NodeT[] {
		type TNodeWithDomainCount = {
				node: NodeT;
				domainCount: number;
		};

		const clone: TNodeWithDomainCount[] = this.nodes
				.map((node: NodeT): TNodeWithDomainCount => ({
						node: node,
						domainCount: node.domain.length
				}))
				.filter((node: TNodeWithDomainCount): boolean => node.domainCount > 1);
		if (clone.length === 0) return [];

		clone
				.sort((a: TNodeWithDomainCount, b: TNodeWithDomainCount): number => {
					if (a.domainCount < b.domainCount) return -1;
					if (a.domainCount > b.domainCount) return 1;
					return 0;
				});
		
		const best: TNodeWithDomainCount = clone[0];

		const bests: TNodeWithDomainCount[] = clone
				.filter((c: TNodeWithDomainCount): boolean => c.domainCount === best.domainCount);
		
		return bests
				.map((c: TNodeWithDomainCount): NodeT => c.node);
	}

	public get smallestNonSingularDomain(): NodeT|undefined {
		const choices: NodeT[] = this.smallestNonSingularDomains;
		if (choices.length === 0) return undefined;

		return choices[Math.floor(Math.random() * choices.length)];
	}

	public applyBacktrack(
			chooseNextNode: () => NodeT|undefined = (): NodeT|undefined => this.smallestNonSingularDomain
	): boolean {
		if (this.isComplete()) return true;
		if (this.isUnsolvable()) return false;

		const choice: NodeT|undefined = chooseNextNode();
		if (!choice) return false;

		const available: DomainT[] = [ ...choice.domain ];
		commonsArrayShuffle(available);
		for (const value of available) {
			this.saveSnapshot();

			choice.domain = [ value ];

			if (this.isConsistent()) {
				const valid: boolean = this.applyAc3();
				const map: Map<string, DomainT[]> = this.snapshotForInference();

				this.revertSnapshot();

				const preInference: Map<string, DomainT[]> = this.snapshotForInference();

				if (valid) {
					// inference
					for (const node of this.nodes) {
						const match: DomainT[]|undefined = map.get(node.uid);
						if (match) node.domain = [ ...match ];
					}

					choice.domain = [ value ];

					const recurse: boolean = this.applyBacktrack();
					if (recurse) return true;
				}

				choice.removeFromDomain(value);
				for (const node of this.nodes) {
					const match: DomainT[]|undefined = preInference.get(node.uid);
					if (!match) throw new Error('No preInference to revert to');
					node.domain = match;
				}
			} else {
				this.revertSnapshot();
				choice.removeFromDomain(value);
			}
		}

		return false;
	}
}
