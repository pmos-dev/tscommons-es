import { TCommonsAlgorithmSearchNode } from '../types/tsearch-node';

import { CommonsAlgorithmGreedyBestFirstSearch as CommonsAlgorithmGreedyBestFirstSearch } from './greedy-best-first-search';
import { TCommonsAlgorithmInternalNode, commonsAlgorithmDeInternalise } from './search';

export type TCommonsAlgorithmInternalStepCostNode<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> = TCommonsAlgorithmInternalNode<ActionT, NodeT> & {
		stepCost: number;
}

type TStepCostInternalNode<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> = TCommonsAlgorithmInternalNode<ActionT, NodeT> & {
		stepCost: number;
}

function commonsAlgorithmDeInternaliseStepCost<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>,
		InternalNodeT extends TStepCostInternalNode<ActionT, NodeT>
>(node: InternalNodeT): NodeT {
	const clone: NodeT = commonsAlgorithmDeInternalise(node);
	delete clone['stepCost'];

	return clone;
}

export abstract class CommonsAlgorithmAStarSearch<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> extends CommonsAlgorithmGreedyBestFirstSearch<ActionT, NodeT> {
	protected abstract greedyBestFirstHeuristic(node: NodeT): number;

	protected heuristic(node: NodeT): number {
		return (node as Pick<TStepCostInternalNode<ActionT, NodeT>, 'stepCost'>).stepCost + this.greedyBestFirstHeuristic(node);
	}

	protected stepCost(_node: NodeT): number {
		return 1;
	}

	protected override transition(
			node: TStepCostInternalNode<ActionT, NodeT>,
			action: ActionT
	): TStepCostInternalNode<ActionT, NodeT>|undefined {
		const applied: TStepCostInternalNode<ActionT, NodeT>|undefined = super.transition(node, action) as TStepCostInternalNode<ActionT, NodeT>|undefined;
		if (!applied) return undefined;

		const stepCost: number = this.stepCost(commonsAlgorithmDeInternaliseStepCost(applied));

		return {
				...applied,
				stepCost: node.stepCost + stepCost
		};
	}

	public override computeRoute(
			start: Omit<NodeT, 'action'>,
			progressCallback?: (frontierLength: number, exploredLength: number) => void,
			progressBlockSize: number = 10
	): NodeT[]|undefined {
		return super.computeRoute(
				{
						...start,
						stepCost: 0
				} as Omit<NodeT, 'action'> & Pick<TCommonsAlgorithmInternalStepCostNode<ActionT, NodeT>, 'stepCost'>,
				progressCallback,
				progressBlockSize
		);
	}
}
