import { TCommonsAlgorithmSearchNode } from '../types/tsearch-node';

import { CommonsAlgorithmSearch, TCommonsAlgorithmInternalNode } from './search';

export abstract class CommonsAlgorithmDepthFirstSearch<
		ActionT,
		NodeT extends TCommonsAlgorithmSearchNode<ActionT>
> extends CommonsAlgorithmSearch<ActionT, NodeT> {
	protected get next(): TCommonsAlgorithmInternalNode<ActionT, NodeT> | undefined {
		return this.frontier.pop();
	}
}
