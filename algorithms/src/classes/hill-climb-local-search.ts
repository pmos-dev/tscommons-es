import { TCommonsAlgorithmStateWithScore } from '../types/tstart-with-score';

import { ECommonsAlgorithmOrientation } from '../enums/elocal-search-orientation';

import { CommonsAlgorithmLocalSearch } from './local-search';

export abstract class CommonsAlgorithmHillClimbLocalSearch<StateT> extends CommonsAlgorithmLocalSearch<StateT> {
	constructor(
			protected orientation: ECommonsAlgorithmOrientation
	) {
		super();
	}

	protected abstract chooseFromValids(valids: TCommonsAlgorithmStateWithScore<StateT>[]): TCommonsAlgorithmStateWithScore<StateT>|undefined;

	protected override choose(state: TCommonsAlgorithmStateWithScore<StateT>, potentials: TCommonsAlgorithmStateWithScore<StateT>[]): TCommonsAlgorithmStateWithScore<StateT>|undefined {
		const orientationClosure: ECommonsAlgorithmOrientation = this.orientation;

		const valids: TCommonsAlgorithmStateWithScore<StateT>[] = potentials
				.filter((potential: TCommonsAlgorithmStateWithScore<StateT>): boolean => {
					switch (orientationClosure) {
						case ECommonsAlgorithmOrientation.MAXIMUM:
							return potential.score > state.score;
						case ECommonsAlgorithmOrientation.MINIMUM:
							return potential.score < state.score;
					}
				});

		const choice: TCommonsAlgorithmStateWithScore<StateT>|undefined = this.chooseFromValids(valids);
		if (!choice) return undefined;

		return choice;
	}
}

export abstract class CommonsAlgorithmSteepestHillClimbLocalSearch<StateT> extends CommonsAlgorithmHillClimbLocalSearch<StateT> {
	protected chooseFromValids(valids: TCommonsAlgorithmStateWithScore<StateT>[]): TCommonsAlgorithmStateWithScore<StateT>|undefined {
		if (valids.length === 0) return undefined;

		valids
				.sort((a: TCommonsAlgorithmStateWithScore<StateT>, b: TCommonsAlgorithmStateWithScore<StateT>): number => {
					if (a.score < b.score) return -1;
					if (a.score > b.score) return 1;
					return 0;
				});
		
		switch (this.orientation) {
			case ECommonsAlgorithmOrientation.MAXIMUM:
				return valids[valids.length - 1];
			case ECommonsAlgorithmOrientation.MINIMUM:
				return valids[0];
		}
	}
}

export abstract class CommonsAlgorithmStochasticHillClimbLocalSearch<StateT> extends CommonsAlgorithmHillClimbLocalSearch<StateT> {
	protected chooseFromValids(valids: TCommonsAlgorithmStateWithScore<StateT>[]): TCommonsAlgorithmStateWithScore<StateT>|undefined {
		if (valids.length === 0) return undefined;
		
		return valids[Math.floor(Math.random() * valids.length)];
	}
}

export abstract class CommonsAlgorithmFirstChoiceHillClimbLocalSearch<StateT> extends CommonsAlgorithmHillClimbLocalSearch<StateT> {
	protected chooseFromValids(valids: TCommonsAlgorithmStateWithScore<StateT>[]): TCommonsAlgorithmStateWithScore<StateT>|undefined {
		if (valids.length === 0) return undefined;
		
		return valids[0];
	}
}
