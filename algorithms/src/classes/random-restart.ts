import { TCommonsAlgorithmStateWithScore } from '../types/tstart-with-score';

import { ECommonsAlgorithmOrientation } from '../enums/elocal-search-orientation';

import { CommonsAlgorithmLocalSearch } from './local-search';

export abstract class CommonsAlgorithmRandomRestartSearch<
		StateT,
		SearchT extends CommonsAlgorithmLocalSearch<StateT>
> {
	constructor(
			private orientation: ECommonsAlgorithmOrientation
	) {}

	protected abstract constructLocalSearch(orientation: ECommonsAlgorithmOrientation): SearchT;

	protected abstract generateInitial(): StateT;

	protected abstract choose(potentials: TCommonsAlgorithmStateWithScore<StateT>[], orientation: ECommonsAlgorithmOrientation): TCommonsAlgorithmStateWithScore<StateT>|undefined;

	public computeOptimial(max: number): TCommonsAlgorithmStateWithScore<StateT>|undefined {
		const optimals: TCommonsAlgorithmStateWithScore<StateT>[] = [];

		for (let i = max; i-- > 0;) {
			const search: SearchT = this.constructLocalSearch(this.orientation);
			const initial: StateT = this.generateInitial();

			const optimal: TCommonsAlgorithmStateWithScore<StateT> = search.computeOptimial(initial);
			optimals.push(optimal);
		}

		return this.choose(optimals, this.orientation);
	}
}
