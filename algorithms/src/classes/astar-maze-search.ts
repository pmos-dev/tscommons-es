import { ECommonsAlgorithmMazeHeuristic } from '../enums/emaze-heuristic';

import { TCommonsAlgorithmSearchDimensionMazeCell, TCommonsAlgorithmSearchDimensionMazeNode } from '../types/tmaze-search-node';

import { CommonsAlgorithmAStarSearch } from './astar-search';

type TDimensionWeightedMazeCell<DimT extends number[]> = TCommonsAlgorithmSearchDimensionMazeCell<DimT> & {
		weight: number;
};

function dimensionCacheIndex<DimT extends number[]>(coord: DimT): string {
	let index: string = '';
	for (const n of coord) index += n.toString() + ':';

	return index;
}

function isDimensionValueEqual<DimT extends number[]>(a: DimT, b: DimT): boolean {
	for (let i = a.length; i-- > 0;) {
		if (a[i] !== b[i]) return false;
	}

	return true;
}

export class CommonsAlgorithmAStarMazeSearch<
		DimT extends number[],
		CellT extends TCommonsAlgorithmSearchDimensionMazeCell<DimT>
> extends CommonsAlgorithmAStarSearch<DimT, TCommonsAlgorithmSearchDimensionMazeNode<DimT>> {
	private weightedCells: (CellT & TDimensionWeightedMazeCell<DimT>)[] = [];
	private weightedCellsCache: Map<string, (CellT & TDimensionWeightedMazeCell<DimT>)> = new Map<string, (CellT & TDimensionWeightedMazeCell<DimT>)>();

	private min: DimT;
	private max: DimT;

	constructor(
			cells: CellT[],
			allActions: DimT[],
			private goal: DimT,
			private heuristicMethod: ECommonsAlgorithmMazeHeuristic
	) {
		super(
				allActions,
				true
		);

		let min: DimT|undefined;
		let max: DimT|undefined;

		for (const cell of cells) {
			if (!min) {
				min = [ ...cell.coord ] as DimT;
			} else {
				for (let i = cell.coord.length; i-- > 0;) min[i] = Math.min(min[i], cell.coord[i]);
			}

			if (!max) {
				max = [ ...cell.coord ] as DimT;
			} else {
				for (let i = cell.coord.length; i-- > 0;) max[i] = Math.max(max[i], cell.coord[i]);
			}
		}

		if (!min || !max) throw new Error('No min/max determinable');

		this.min = min;
		this.max = max;

		for (const cell of cells) {
			const ds: number[] = cell.coord
					.map((t: number, i: number): number => t - goal[i]);
			
			let weight: number|undefined;
			switch (this.heuristicMethod) {
				case ECommonsAlgorithmMazeHeuristic.MANHATTEN:
					weight = 0;
					for (let i = ds.length; i-- > 0;) weight += Math.abs(ds[i]);
					break;
				case ECommonsAlgorithmMazeHeuristic.PYTHAGORAS:
					weight = 0;
					for (let i = ds.length; i-- > 0;) weight += Math.pow(ds[i], 2);
					weight = Math.sqrt(weight);
					break;
			}

			this.weightedCells.push({
					...cell,
					coord: [ ...cell.coord ] as DimT,
					visitable: cell.visitable,
					costToVisit: cell.costToVisit,
					permittedActions: cell.permittedActions,
					weight: weight
			});
		}

		for (const cell of this.weightedCells) {
			this.weightedCellsCache.set(dimensionCacheIndex(cell.coord), cell);
		}
	}

	protected override isGoal(node: TCommonsAlgorithmSearchDimensionMazeNode<DimT>): boolean {
		return isDimensionValueEqual(node.coord, this.goal);
	}

	private getCache(coord: DimT): TDimensionWeightedMazeCell<DimT>|undefined {
		const index: string = dimensionCacheIndex<DimT>(coord);

		return this.weightedCellsCache.get(index);
	}

	protected override greedyBestFirstHeuristic(node: TCommonsAlgorithmSearchDimensionMazeNode<DimT>): number {
		const cell: TDimensionWeightedMazeCell<DimT>|undefined = this.getCache(node.coord);
		if (!cell) throw new Error('No such cell exists');

		return cell.weight;
	}

	protected override stepCost(node: TCommonsAlgorithmSearchDimensionMazeNode<DimT>): number {
		const cell: TDimensionWeightedMazeCell<DimT>|undefined = this.getCache(node.coord);
		if (!cell) throw new Error('No such cell exists');

		return cell.costToVisit === undefined ? 1 : cell.costToVisit;
	}

	protected override isEqual(a: TCommonsAlgorithmSearchDimensionMazeNode<DimT>, b: TCommonsAlgorithmSearchDimensionMazeNode<DimT>): boolean {
		return isDimensionValueEqual(a.coord, b.coord);
	}
	
	protected override applyActionIfPossible(node: TCommonsAlgorithmSearchDimensionMazeNode<DimT> , action: DimT): (TCommonsAlgorithmSearchDimensionMazeNode<DimT> )|undefined {
		const coord: DimT = [ ...node.coord ] as DimT;

		const current: TCommonsAlgorithmSearchDimensionMazeCell<DimT>|undefined = this.getCache(coord);
		if (!current) return undefined;

		const available: DimT|undefined = (current.permittedActions || [])
				.find((permitted: DimT): boolean => {
					for (let i = permitted.length; i-- > 0;) {
						if (permitted[i] !== action[i]) return false;
					}
					return true;
				});
		if (!available) return undefined;

		for (let i = coord.length; i-- > 0;) {
			coord[i] += available[i];
			
			if (coord[i] <= this.min[i]) return undefined;
			if (coord[i] >= this.max[i]) return undefined;
		}

		const cell: TCommonsAlgorithmSearchDimensionMazeCell<DimT>|undefined = this.getCache(coord);
		if (!cell) return undefined;
		if (!cell.visitable) return undefined;

		return {
				action: available,
				coord: cell.coord
		};
	}
}
