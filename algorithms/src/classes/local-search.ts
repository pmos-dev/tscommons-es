import { TCommonsAlgorithmStateWithScore } from '../types/tstart-with-score';

export abstract class CommonsAlgorithmLocalSearch<StateT> {
	protected abstract score(state: StateT): number;

	protected abstract neighbours(state: StateT): StateT[];

	protected abstract choose(state: TCommonsAlgorithmStateWithScore<StateT>, potentials: TCommonsAlgorithmStateWithScore<StateT>[]): TCommonsAlgorithmStateWithScore<StateT>|undefined;

	public computeNext(current: TCommonsAlgorithmStateWithScore<StateT>): TCommonsAlgorithmStateWithScore<StateT>|undefined {
		const neighbours: StateT[] = this.neighbours(current.state);

		const potentials: TCommonsAlgorithmStateWithScore<StateT>[] = neighbours
				.map((s: StateT): TCommonsAlgorithmStateWithScore<StateT> => ({
						state: s,
						score: this.score(s)
				}));
		if (potentials.length === 0) return undefined;
		
		const choice: TCommonsAlgorithmStateWithScore<StateT>|undefined = this.choose(current, potentials);
		if (!choice) return undefined;

		return choice;
	}

	public computeOptimial(initial: StateT): TCommonsAlgorithmStateWithScore<StateT> {
		let current: TCommonsAlgorithmStateWithScore<StateT> = {
				state: initial,
				score: this.score(initial)
		};

		while (true) {
			const next: TCommonsAlgorithmStateWithScore<StateT>|undefined = this.computeNext(current);
			if (!next) return current;

			current = next;
		}
	}
}
