import { ECommonsAlgorithmBinaryPlayer } from '../enums/ebinary-player';

export type TCommonsAlgorithmBinaryWinnerMiniMaxPlayer = ECommonsAlgorithmBinaryPlayer;

type TUtility = -1|1|0;

export type TCommonsAlgorithmBinaryPlay<ActionT> = {
		player: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer;
		action: ActionT;
};

export abstract class CommonsAlgorithmBinaryWinnerMiniMax<
		ActionT,
		StateT
> {
	constructor(
			private state: StateT
	) {}

	protected abstract player(state: StateT): TCommonsAlgorithmBinaryWinnerMiniMaxPlayer;

	protected abstract actions(state: StateT, player: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer): ActionT[];

	protected abstract clone(state: StateT): StateT;

	protected abstract applyAction(state: StateT, player: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer, action: ActionT): StateT;

	private result(state: StateT, player: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer, action: ActionT): StateT {
		const clone: StateT = this.clone(state);

		this.applyAction(clone, player, action);

		return clone;
	}

	protected abstract isWon(state: StateT, player: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer): boolean;
	protected abstract isDraw(state: StateT): boolean;

	private winner(state: StateT): TCommonsAlgorithmBinaryWinnerMiniMaxPlayer|undefined {
		for (const player of [ ECommonsAlgorithmBinaryPlayer.A, ECommonsAlgorithmBinaryPlayer.B ]) {
			if (this.isWon(state, player)) return player;
		}

		return undefined;
	}

	private isTerminal(state: StateT): boolean {
		if (this.isDraw(state)) return true;
		if (this.winner(state) !== undefined) return true;

		return false;
	}

	private utility(state: StateT): TUtility {
		const won: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer|undefined = this.winner(state);

		if (won === ECommonsAlgorithmBinaryPlayer.A) return -1;
		if (won === ECommonsAlgorithmBinaryPlayer.B) return 1;
		return 0;
	}

	private minimaxMax(state: StateT): TUtility {
		if (this.isTerminal(state)) return this.utility(state);

		const currentPlayer: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer = this.player(state);

		let v: TUtility|undefined;

		for (const action of this.actions(state, currentPlayer)) {
			const applied: StateT = this.result(state, currentPlayer, action);
			const score: TUtility = this.minimaxMin(applied);

			v = v === undefined ? score : (Math.max(v, score) as TUtility);
		}

		return v || 0;
	}

	private minimaxMin(state: StateT): TUtility {
		if (this.isTerminal(state)) return this.utility(state);

		const currentPlayer: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer = this.player(state);

		let v: TUtility|undefined;

		for (const action of this.actions(state, currentPlayer)) {
			const applied: StateT = this.result(state, currentPlayer, action);
			const score: TUtility = this.minimaxMax(applied);

			v = v === undefined ? score : (Math.min(v, score) as TUtility);
		}

		return v || 0;
	}

	private minimax(state: StateT): TCommonsAlgorithmBinaryPlay<ActionT>|undefined {
		if (this.isTerminal(state)) return undefined;

		const currentPlayer: TCommonsAlgorithmBinaryWinnerMiniMaxPlayer = this.player(state);

		const possibilities: [ ActionT, TUtility ][] = [];

		let best: TUtility|undefined;

		for (const action of this.actions(state, currentPlayer)) {
			const test: StateT = this.result(state, currentPlayer, action);

			if (currentPlayer === ECommonsAlgorithmBinaryPlayer.B) {
				const v: TUtility = this.minimaxMin(test);
				possibilities.push([ action, v ]);

				best = best === undefined ? v : (Math.max(best, v) as TUtility);
			} else if (currentPlayer === ECommonsAlgorithmBinaryPlayer.A) {
				const v: TUtility = this.minimaxMax(test);
				possibilities.push([ action, v ]);

				best = best === undefined ? v : (Math.min(best, v) as TUtility);
			} else {
				throw new Error('Invalid player');
			}
		}

		if (possibilities.length === 0) return undefined;

		const bestPossibility: [ ActionT, TUtility ] = possibilities
				.find((p: [ ActionT, TUtility ]): boolean => p[1] === best)!;

		return {
				player: currentPlayer,
				action: bestPossibility[0]
		};
	}

	public get currentState(): StateT {
		return this.clone(this.state);
	}

	public next(): TCommonsAlgorithmBinaryPlay<ActionT>|undefined {
		const play: TCommonsAlgorithmBinaryPlay<ActionT>|undefined = this.minimax(this.state);
		if (play === undefined) return undefined;

		this.state = this.result(this.state, play.player, play.action);

		return play;
	}
}
