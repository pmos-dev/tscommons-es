export type TCommonsAlgorithmStateWithScore<StateT> = {
		state: StateT;
		score: number;
};
