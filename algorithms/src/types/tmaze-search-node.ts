import { TCommonsAlgorithmSearchNode } from './tsearch-node';

export type TCommonsAlgorithmSearchDimensionMazeNode<DimT extends number[]> = TCommonsAlgorithmSearchNode<DimT> & {
		coord: DimT;
};

export type TCommonsAlgorithmSearchDimensionMazeCell<DimT extends number[]> = {
		coord: DimT;
		visitable: boolean;
		costToVisit?: number;
		permittedActions?: DimT[];
};
