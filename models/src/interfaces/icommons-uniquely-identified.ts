import { commonsTypeIsPropertyObject, commonsTypeHasPropertyString, commonsBase62IsId, commonsBase62IsLongId } from 'tscommons-es-core';

export interface ICommonsUniquelyIdentified {
		uid: string;
}

export function isICommonsUniquelyIdentified(test: unknown): test is ICommonsUniquelyIdentified {
	if (!commonsTypeIsPropertyObject(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'uid')) return false;
	
	return true;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsUniquelyIdentifiedBase62 extends ICommonsUniquelyIdentified {}

export function isICommonsUniquelyIdentifiedBase62(test: unknown): test is ICommonsUniquelyIdentifiedBase62 {
	if (!isICommonsUniquelyIdentified(test)) return false;

	if (!commonsBase62IsId(test.uid)) return false;
	
	return true;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsUniquelyIdentifiedBase62Long extends ICommonsUniquelyIdentified {}

export function isICommonsUniquelyIdentifiedBase62Long(test: unknown): test is ICommonsUniquelyIdentifiedBase62Long {
	if (!isICommonsUniquelyIdentified(test)) return false;

	if (!commonsBase62IsLongId(test.uid)) return false;
	
	return true;
}
