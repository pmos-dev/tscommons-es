import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsModel, isICommonsModel } from './icommons-model';

export interface ICommonsFirstClass extends ICommonsModel {
		id: number;
}

export function isICommonsFirstClass(test: unknown): test is ICommonsFirstClass {
	if (!isICommonsModel(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'id')) return false;

	return true;
}
