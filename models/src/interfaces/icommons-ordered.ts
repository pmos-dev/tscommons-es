import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsFirstClass, isICommonsFirstClass } from './icommons-first-class';

export interface ICommonsOrdered extends ICommonsFirstClass {
		ordered: number;
}

export function isICommonsOrdered(test: unknown): test is ICommonsOrdered {
	if (!isICommonsFirstClass(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'ordered')) return false;
	
	return true;
}
