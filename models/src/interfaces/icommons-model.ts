import {
		CommonsFixedDate,
		CommonsFixedDuration,
		commonsTypeIsDate,
		commonsTypeIsPrimative,
		commonsTypeIsPropertyObject,
		commonsTypeIsStringArray,
		TPrimative
} from 'tscommons-es-core';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsModel {
		[ key: string ]: TPrimative|string[]|Date|CommonsFixedDate|CommonsFixedDuration|undefined;
}

export function isICommonsModel(test: unknown): test is ICommonsModel {
	if (!commonsTypeIsPropertyObject(test)) return false;

	for (const key of Object.keys(test)) {
		if (test[key] === undefined) continue;
		if (commonsTypeIsDate(test[key])) continue;
		if (commonsTypeIsStringArray(test[key])) continue;
		if (CommonsFixedDate.is(test[key])) continue;
		
		if (!commonsTypeIsPrimative(test[key])) return false;
	}

	return true;
}
