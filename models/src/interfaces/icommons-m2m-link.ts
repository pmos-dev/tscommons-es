import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsFirstClass } from './icommons-first-class';
import { ICommonsModel, isICommonsModel } from './icommons-model';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsM2MLink<
		_A extends ICommonsFirstClass,	// eslint-disable-line @typescript-eslint/no-unused-vars
		_B extends ICommonsFirstClass	// eslint-disable-line @typescript-eslint/no-unused-vars
> extends ICommonsModel {
		// can't define the a and b fields as we don't know what they will be called
}

export function isICommonsM2MLink<
		A extends ICommonsFirstClass,
		B extends ICommonsFirstClass
>(test: unknown, aFieldName: string, bFieldName: string): test is ICommonsM2MLink<A, B> {
	if (!isICommonsModel(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, aFieldName)) return false;
	if (!commonsTypeHasPropertyNumber(test, bFieldName)) return false;
	
	return true;
}
