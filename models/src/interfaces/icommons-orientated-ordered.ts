import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsFirstClass } from './icommons-first-class';
import { ICommonsSecondClass, isICommonsSecondClass } from './icommons-second-class';

export interface ICommonsOrientatedOrdered<
		P extends ICommonsFirstClass
> extends ICommonsSecondClass<P> {
		ordered: number;
}

export function isICommonsOrientatedOrdered<
		P extends ICommonsFirstClass
>(test: unknown, firstClassFieldName: string): test is ICommonsOrientatedOrdered<P> {
	if (!isICommonsSecondClass<P>(test, firstClassFieldName)) return false;

	if (!commonsTypeHasPropertyNumber(test, 'ordered')) return false;
	
	return true;
}
