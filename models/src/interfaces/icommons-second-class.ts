import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsFirstClass, isICommonsFirstClass } from './icommons-first-class';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsSecondClass<
		_P extends ICommonsFirstClass	// eslint-disable-line @typescript-eslint/no-unused-vars
> extends ICommonsFirstClass {
		// can't define the firstClassField as we don't know what it will be called
}

export function isICommonsSecondClass<
		P extends ICommonsFirstClass
>(test: unknown, firstClassFieldName: string): test is ICommonsSecondClass<P> {
	if (!isICommonsFirstClass(test)) return false;

	if (!commonsTypeHasPropertyNumber(test, firstClassFieldName)) return false;
	
	return true;
}
