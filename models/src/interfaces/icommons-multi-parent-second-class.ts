import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { ICommonsFirstClass, isICommonsFirstClass } from './icommons-first-class';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsMultiParentSecondClass extends ICommonsFirstClass {
		// can't define the firstClassField as we don't know what it will be called
}

export function isICommonsMultiParentSecondClass(test: unknown, firstClassFieldNames: string[]): test is ICommonsMultiParentSecondClass {
	if (!isICommonsFirstClass(test)) return false;

	for (const firstClassFieldName of firstClassFieldNames) {
		if (!commonsTypeHasPropertyNumber(test, firstClassFieldName)) return false;
	}
	
	return true;
}
