import { ICommonsFirstClass } from '../interfaces/icommons-first-class';

import { CommonsGenericFieldModelCache } from './generic-field-model-cache';

export class CommonsFirstClassCache<
		ModelT extends ICommonsFirstClass
> extends CommonsGenericFieldModelCache<
		ModelT,
		number
> {
	constructor(
			getRemote: (id: number) => Promise<ModelT|undefined>,
			listRemoteIds?: () => Promise<number[]>,
			listRemotes?: () => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		super(
				'id',
				getRemote,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);
	}
}
