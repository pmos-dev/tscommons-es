import { ICommonsFirstClass } from '../interfaces/icommons-first-class';
import { ICommonsSecondClass } from '../interfaces/icommons-second-class';

import { CommonsGenericSecondClassFieldModelCache } from './generic-second-class-field-model-cache';

export class CommonsSecondClassCache<
		ModelT extends ICommonsSecondClass<ParentT>,
		ParentT extends ICommonsFirstClass
> extends CommonsGenericSecondClassFieldModelCache<
		ModelT,
		ParentT,
		number
> {
	constructor(
			getRemote: (parent: ParentT, id: number) => Promise<ModelT|undefined>,
			listRemoteIds?: (parent: ParentT) => Promise<number[]>,
			listRemotes?: (parent: ParentT) => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		super(
				'id',
				getRemote,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);
	}
}
