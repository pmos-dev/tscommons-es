import { CommonsCache } from 'tscommons-es-core';

export abstract class CommonsGenericFieldModelCache<
		ModelT,
		IdT
> {
	private getCache: CommonsCache<ModelT, IdT>;
	private listCache: CommonsCache<IdT[], 1>;
	
	constructor(
			private idField: string,
			private getRemote: (id: IdT) => Promise<ModelT|undefined>,
			private listRemoteIds?: () => Promise<IdT[]>,
			private listRemotes?: () => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		if (!this.listRemoteIds && !this.listRemotes) throw new Error('Either listRemoteIds or listRemotes must be specified');

		// This cache is linked to the Model IDs, so generation of new ids is not applicable.

		this.getCache = new CommonsCache<ModelT, IdT>(
				(): IdT => { throw new Error('NA'); },	// eslint-disable-line brace-style
				maxAgeInMillis
		);
		this.listCache = new CommonsCache<IdT[], 1>(
				(): 1 => { throw new Error('NA'); },	// eslint-disable-line brace-style
				maxAgeInMillis
		);
	}

	public async get(id: IdT): Promise<ModelT|undefined> {
		const match: ModelT|undefined = this.getCache.getById(id);
		if (match) return match;

		const remote: ModelT|undefined = await this.getRemote(id);

		this.listCache.deleteForId(1);

		if (remote) this.getCache.storeForId(id, remote);

		return remote;
	}

	public getFromCacheOnly(id: IdT): ModelT|undefined {
		return this.getCache.getById(id);
	}

	// Note, this may interfere with sorting
	public appendToCacheOnly(
			data: ModelT,
			resetListIfNew: boolean = false
	): void {
		if (!this.getCache.has(data[this.idField] as IdT) || !this.listCache.getById(1) || !this.listCache.getById(1)!.includes(data[this.idField] as IdT)) {
			// new entry

			if (resetListIfNew) {
				this.listCache.deleteForId(1);
			} else {
				const existing: IdT[]|undefined = this.listCache.getById(1);
				if (existing) existing.push(data[this.idField] as IdT);
			}
		}

		this.getCache.storeForId(data[this.idField] as IdT, data);
	}

	public async list(
			allowExpiredRefresh: boolean = true
	): Promise<ModelT[]> {
		let ids: IdT[]|undefined = this.listCache.getById(1);

		if (!ids) {
			if (this.listRemotes) {
				const remotes: ModelT[] = await this.listRemotes();
				
				this.getCache.wipe();
				for (const remote of remotes) {
					this.getCache.storeForId(remote[this.idField] as IdT, remote);
				}

				ids = remotes
						.map((remote: ModelT): IdT => remote[this.idField] as IdT);
			}
			if (this.listRemoteIds) {
				ids = await this.listRemoteIds();
			}

			this.listCache.storeForId(1, ids!);
		}

		const promises: Promise<ModelT|undefined>[] = ids!
				.map((id: IdT): Promise<ModelT|undefined> => this.get(id));

		const items: (ModelT|undefined)[] = await Promise.all(promises);

		// undefineds shouldn't occur. If they do, the list ID might be invalid or expired
		const rebuild: ModelT[] = [];
		for (const item of items) {
			if (!item) {
				this.listCache.wipe();
				if (allowExpiredRefresh) return await this.list(false);
			} else {
				rebuild.push(item);
			}
		}

		return rebuild;
	}

	public clear(id: IdT): void {
		this.getCache.deleteForId(id);
		this.listCache.deleteForId(1);
	}

	public wipe(): void {
		this.getCache.wipe();
		this.listCache.deleteForId(1);	// same as wipe as there's only '1' entry
	}
}
