import { CommonsCache } from 'tscommons-es-core';

import { ICommonsFirstClass } from '../interfaces/icommons-first-class';

enum EPopulated {
		NO = 'no',
		PARTIAL = 'partial',
		FULL = 'full'
}

type TCache<ModelT, IdT> = {
		populated: EPopulated;
		ids: IdT[];
		cache: CommonsCache<ModelT, IdT>;
};

export abstract class CommonsGenericSecondClassFieldModelCache<
		ModelT,
		ParentT extends ICommonsFirstClass,
		IdT
> {
	private caches: Map<number, TCache<ModelT, IdT>>;
	
	constructor(
			private idField: string,
			private getRemote: (parent: ParentT, id: IdT) => Promise<ModelT|undefined>,
			private listRemoteIds?: (parent: ParentT) => Promise<IdT[]>,
			private listRemotes?: (parent: ParentT) => Promise<ModelT[]>,
			private maxAgeInMillis?: number
	) {
		if (!this.listRemoteIds && !this.listRemotes) throw new Error('Either listRemoteIds or listRemotes must be specified');

		this.caches = new Map<number, TCache<ModelT, IdT>>();
	}

	private ensureCacheForParent(
			parent: ParentT
	): TCache<ModelT, IdT> {
		const existing: TCache<ModelT, IdT>|undefined = this.caches.get(parent.id);
		if (existing) return existing;

		// This cache is linked to the Model IDs, so generation of new ids is not applicable.

		this.caches.set(
				parent.id,
				{
						cache: new CommonsCache<ModelT, IdT>(
								(): IdT => { throw new Error('NA'); },	// eslint-disable-line brace-style
								this.maxAgeInMillis
						),
						ids: [],
						populated: EPopulated.NO
				}
		);

		return this.ensureCacheForParent(parent);
	}

	public async get(
			parent: ParentT,
			id: IdT
	): Promise<ModelT|undefined> {
		const cache: TCache<ModelT, IdT> = this.ensureCacheForParent(parent);

		const match: ModelT|undefined = cache.cache.getById(id);
		if (match) return match;

		const remote: ModelT|undefined = await this.getRemote(parent, id);
		if (remote) cache.cache.storeForId(id, remote);

		// if no original cache match, then the list is probably out of date, so can't be FULL
		cache.populated = EPopulated.PARTIAL;

		return remote;
	}

	public getFromCacheOnly(
			parent: ParentT,
			id: IdT
	): ModelT|undefined {
		const cache: TCache<ModelT, IdT> = this.ensureCacheForParent(parent);

		return cache.cache.getById(id);
	}

	// Note, this may interfere with sorting
	public appendToCacheOnly(
			parent: ParentT,
			data: ModelT,
			resetListIfNew: boolean = false
	): void {
		const cache: TCache<ModelT, IdT> = this.ensureCacheForParent(parent);

		if (!cache.cache.has(data[this.idField] as IdT) || !cache.ids.includes(data[this.idField] as IdT)) {
			// new entry

			if (resetListIfNew) {
				cache.ids = [];
				cache.populated = EPopulated.PARTIAL;
			} else {
				cache.ids.push(data[this.idField] as IdT);
				// leave FULL or PARTIAL as existing
			}
		} else {
			// allow the list of IDs to be kept if the entry already exists
		}

		cache.cache.storeForId(data[this.idField] as IdT, data);
	}

	public async list(
			parent: ParentT,
			allowExpiredRefresh: boolean = true
	): Promise<ModelT[]> {
		const cache: TCache<ModelT, IdT> = this.ensureCacheForParent(parent);

		if (cache.populated === EPopulated.FULL) {
			const items: (ModelT|undefined)[] = cache.ids
					.map((id: IdT): ModelT|undefined => cache.cache.getById(id));

			// undefineds shouldn't occur. If they do, the list ID might be invalid or expired
			const rebuild: ModelT[] = [];
			for (const item of items) {
				if (!item) {
					cache.ids = [];
					cache.populated = EPopulated.NO;

					if (allowExpiredRefresh) return await this.list(parent, allowExpiredRefresh);
					
					return [];
				} else {
					rebuild.push(item);
				}
			}

			return rebuild;
		}

		if (this.listRemotes) {
			const remotes: ModelT[] = await this.listRemotes(parent);

			const ids: IdT[] = remotes
					.map((remote: ModelT): IdT => remote[this.idField] as IdT);

			cache.cache.wipe();
			for (const remote of remotes) {
				cache.cache.storeForId(remote[this.idField] as IdT, remote);
			}

			cache.ids = [ ...ids ];
			cache.populated = EPopulated.FULL;

			return this.list(parent, false);
		}

		if (this.listRemoteIds) {
			const ids: IdT[] = await this.listRemoteIds(parent);

			const promises: Promise<ModelT|undefined>[] = ids
					.map((id: IdT): Promise<ModelT|undefined> => this.get(parent, id));

			const items: (ModelT|undefined)[] = await Promise.all(promises);

			cache.ids = items
					.filter((item: ModelT|undefined): boolean => item !== undefined)
					.map((item: ModelT|undefined): IdT => item![this.idField] as IdT);
			cache.populated = EPopulated.FULL;

			return this.list(parent, false);
		}

		throw new Error('Cannot get here');
	}

	public clear(parent: ParentT, id: IdT): void {
		const cache: TCache<ModelT, IdT> = this.ensureCacheForParent(parent);

		cache.cache.deleteForId(id);
		cache.populated = EPopulated.PARTIAL;
		// PARTIAL means no need to splice the id, as list only accepts FULL
	}

	public wipe(parent: ParentT): void {
		this.caches.delete(parent.id);
	}

	public wipeAll(): void {
		this.caches.clear();
	}
}
