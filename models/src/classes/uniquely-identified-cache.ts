import { ICommonsUniquelyIdentified } from '../interfaces/icommons-uniquely-identified';
import { ICommonsFirstClass } from '../interfaces/icommons-first-class';

import { CommonsFirstClassCache } from './first-class-cache';
import { CommonsSecondClassCache } from './second-class-cache';
import { CommonsGenericFieldModelCache } from './generic-field-model-cache';

export class CommonsUniquelyIdentifiedCache<
		ModelT extends ICommonsUniquelyIdentified
> extends CommonsGenericFieldModelCache<
		ModelT,
		string
> {
	constructor(
			getRemote: (id: string) => Promise<ModelT|undefined>,
			maxAgeInMillis?: number
	) {
		super(
				'id',
				getRemote,
				(): Promise<string[]> => { throw new Error('listRemoteIds is not applicable for CommonsUniquelyIdentifiedCache'); },	// eslint-disable-line brace-style
				(): Promise<ModelT[]> => { throw new Error('listRemotes is not applicable for CommonsUniquelyIdentifiedCache'); },	// eslint-disable-line brace-style
				maxAgeInMillis
		);
	}
}

export class CommonsUniquelyIdentifiedFirstClassCache<
		ModelT extends ICommonsFirstClass & ICommonsUniquelyIdentified
> {
	private idCache: CommonsFirstClassCache<ModelT>;
	private uidCache: CommonsUniquelyIdentifiedCache<ModelT>;

	constructor(
			getRemoteById: (id: number) => Promise<ModelT|undefined>,
			getRemoteByUid: (uid: string) => Promise<ModelT|undefined>,
			listRemoteIds?: () => Promise<number[]>,
			listRemotes?: () => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		this.idCache = new CommonsFirstClassCache<ModelT>(
				getRemoteById,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);
		this.uidCache = new CommonsUniquelyIdentifiedCache<ModelT>(
				getRemoteByUid,
				maxAgeInMillis
		);
	}

	public async getById(id: number): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.idCache.get(id);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByUid(uid: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.uidCache.get(uid);

		// sync
		if (item) this.idCache.appendToCacheOnly(item, false);

		return item;
	}

	public getFromCacheOnlyById(id: number): ModelT|undefined {
		const item: ModelT|undefined = this.idCache.getFromCacheOnly(id);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public getFromCacheOnlyByUid(uid: string): ModelT|undefined {
		const item: ModelT|undefined = this.uidCache.getFromCacheOnly(uid);

		// sync
		if (item) this.idCache.appendToCacheOnly(item, false);

		return item;
	}

	// Note, this may interfere with sorting
	public appendToCacheOnly(
			data: ModelT,
			resetListIfNew: boolean = false
	): void {
		this.idCache.appendToCacheOnly(data, resetListIfNew);
		this.uidCache.appendToCacheOnly(data, resetListIfNew);
	}

	public async list(): Promise<ModelT[]> {
		const items: ModelT[] = await this.idCache.list();

		// sync
		for (const item of items) this.uidCache.appendToCacheOnly(item, false);

		return items;
	}

	public clearById(id: number): void {
		const existing: ModelT|undefined = this.idCache.getFromCacheOnly(id);

		if (existing) this.uidCache.clear(existing.uid);

		this.idCache.clear(id);
	}

	public clearByUid(uid: string): void {
		const existing: ModelT|undefined = this.uidCache.getFromCacheOnly(uid);

		if (existing) this.idCache.clear(existing.id);

		this.uidCache.clear(uid);
	}

	public wipe(): void {
		this.idCache.wipe();
		this.uidCache.wipe();
	}
}

export class CommonsUniquelyIdentifiedSecondClassCache<
		ModelT extends ICommonsFirstClass & ICommonsUniquelyIdentified,
		ParentT extends ICommonsFirstClass
> {
	private idCache: CommonsSecondClassCache<ModelT, ParentT>;
	private uidCache: CommonsUniquelyIdentifiedCache<ModelT>;

	constructor(
			getRemoteById: (parent: ParentT, id: number) => Promise<ModelT|undefined>,
			getRemoteByUid: (uid: string) => Promise<ModelT|undefined>,
			listRemoteIds?: (parent: ParentT) => Promise<number[]>,
			listRemotes?: (parent: ParentT) => Promise<ModelT[]>,
			maxAgeInMillis?: number
	) {
		this.idCache = new CommonsSecondClassCache<ModelT, ParentT>(
				getRemoteById,
				listRemoteIds,
				listRemotes,
				maxAgeInMillis
		);
		this.uidCache = new CommonsUniquelyIdentifiedCache<ModelT>(
				getRemoteByUid,
				maxAgeInMillis
		);
	}

	public async getById(
			parent: ParentT,
			id: number
	): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.idCache.get(parent, id);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public async getByUid(uid: string): Promise<ModelT|undefined> {
		const item: ModelT|undefined = await this.uidCache.get(uid);

		// not safe to sync up

		return item;
	}

	public getFromCacheOnlyById(
			parent: ParentT,
			id: number
	): ModelT|undefined {
		const item: ModelT|undefined = this.idCache.getFromCacheOnly(parent, id);

		// sync
		if (item) this.uidCache.appendToCacheOnly(item, false);

		return item;
	}

	public getFromCacheOnlyByUid(uid: string): ModelT|undefined {
		const item: ModelT|undefined = this.uidCache.getFromCacheOnly(uid);

		// not safe to sync up

		return item;
	}

	// Note, this may interfere with sorting
	public appendToCacheOnly(
			parent: ParentT,
			data: ModelT,
			resetListIfNew: boolean = false
	): void {
		this.idCache.appendToCacheOnly(parent, data, resetListIfNew);
		this.uidCache.appendToCacheOnly(data, resetListIfNew);
	}

	public async list(parent: ParentT): Promise<ModelT[]> {
		const items: ModelT[] = await this.idCache.list(parent);

		// sync
		for (const item of items) this.uidCache.appendToCacheOnly(item, false);

		return items;
	}

	public clearById(parent: ParentT, id: number): void {
		const existing: ModelT|undefined = this.idCache.getFromCacheOnly(parent, id);

		if (existing) this.uidCache.clear(existing.uid);

		this.idCache.clear(parent, id);
	}

	public clearByUid(
			uid: string,
			parentField?: string
	): void {
		const existing: ModelT|undefined = this.uidCache.getFromCacheOnly(uid);

		if (existing && parentField) {
			// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
			const parent: ParentT = {
					id: existing[parentField]
			} as ParentT;

			this.idCache.clear(parent, existing.id);
		}

		this.uidCache.clear(uid);
	}

	public wipe(parent: ParentT): void {
		this.idCache.wipe(parent);
		this.uidCache.wipe();
	}

	public wipeAll(): void {
		this.idCache.wipeAll();
		this.uidCache.wipe();
	}
}
