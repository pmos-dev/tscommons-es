import { Observable } from 'rxjs';

import { CommonsHttpError } from 'tscommons-es-http';

export type TCommonsStreamableRestObservable<T> = {
		dataStream: Observable<T>;
		outcome: Promise<true|CommonsHttpError>;
};
