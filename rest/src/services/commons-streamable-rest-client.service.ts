import { Subject, Subscription } from 'rxjs';

import { TEncoded, TEncodedObject } from 'tscommons-es-core';
import { CommonsHttpError, CommonsStreamableHttpClientService, ECommonsHttpContentType, ECommonsHttpResponseDataType, ICommonsStreamableHttpClientImplementation, TCommonsHttpRequestOptions, TCommonsStreamableHttpObservable } from 'tscommons-es-http';

import { TCommonsStreamableRestObservable } from '../types/tcommons-streamable-rest-observable';

function handleTypedDataStream<T extends TEncoded = TEncoded>(
		stream: TCommonsStreamableHttpObservable,
		parseDataStream: (data: string|Uint8Array) => T,
		dataStreamErrorCallback: (e: Error) => void
): TCommonsStreamableRestObservable<T> {
	const typedDataStream: Subject<T> = new Subject<T>();

	const subscription: Subscription = stream.dataStream
			.subscribe((data: string|Uint8Array): void => {
				try {
					const typed: T = parseDataStream(data);
					typedDataStream.next(typed);
				} catch (e) {
					dataStreamErrorCallback(e as Error);
				}
			});

	const promise: Promise<true|CommonsHttpError> = new Promise<true|CommonsHttpError>((resolve: (outcome: true|CommonsHttpError) => void, reject: (e: Error) => void): void => {
		void (async (): Promise<void> => {
			try {
				const outcome: true|CommonsHttpError = await stream.outcome;
				resolve(outcome);
			} catch (e) {
				reject(e as Error);
			} finally {
				subscription.unsubscribe();
			}
		})();
	});

	return {
			dataStream: typedDataStream,
			outcome: promise
	};

}

export class CommonsStreamableRestClientService extends CommonsStreamableHttpClientService {
	constructor(
			implementation: ICommonsStreamableHttpClientImplementation,
			private rootUrl: string,
			private contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation);
	}

	public headRest<
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Pick<TCommonsStreamableRestObservable<undefined>, 'outcome'> {
		const stream: TCommonsStreamableHttpObservable = this.head<P, H>(
				`${this.rootUrl}${script}`,
				params,
				headers,
				options
		);

		return {
				outcome: stream.outcome
		};
	}
	
	public getRest<
			T extends TEncoded = TEncoded,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			parseDataStream: (data: string|Uint8Array) => T,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {},
			dataStreamErrorCallback: (e: Error) => void = (_e: Error): void => {
				// do nothing
			}
	): TCommonsStreamableRestObservable<T> {
		const stream: TCommonsStreamableHttpObservable = this.get<P, H>(
				`${this.rootUrl}${script}`,
				params,
				headers,
				{
						...options,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		);

		return handleTypedDataStream(
				stream,
				parseDataStream,
				dataStreamErrorCallback
		);
	}
	
	public postRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			parseDataStream: (data: string|Uint8Array) => T,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {},
			dataStreamErrorCallback: (e: Error) => void = (_e: Error): void => {
				// do nothing
			}
	): TCommonsStreamableRestObservable<T> {
		const stream: TCommonsStreamableHttpObservable = this.post<B, P, H>(
				`${this.rootUrl}${script}`,
				body,
				params,
				headers,
				{
						...options,
						bodyDataEncoding: options.bodyDataEncoding || this.contentType,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		);

		return handleTypedDataStream(
				stream,
				parseDataStream,
				dataStreamErrorCallback
		);
	}
	
	public putRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			parseDataStream: (data: string|Uint8Array) => T,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {},
			dataStreamErrorCallback: (e: Error) => void = (_e: Error): void => {
				// do nothing
			}
	): TCommonsStreamableRestObservable<T> {
		const stream: TCommonsStreamableHttpObservable = this.put<B, P, H>(
				`${this.rootUrl}${script}`,
				body,
				params,
				headers,
				{
						...options,
						bodyDataEncoding: options.bodyDataEncoding || this.contentType,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		);

		return handleTypedDataStream(
				stream,
				parseDataStream,
				dataStreamErrorCallback
		);
	}
	
	public patchRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			parseDataStream: (data: string|Uint8Array) => T,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {},
			dataStreamErrorCallback: (e: Error) => void = (_e: Error): void => {
				// do nothing
			}
	): TCommonsStreamableRestObservable<T> {
		const stream: TCommonsStreamableHttpObservable = this.patch<B, P, H>(
				`${this.rootUrl}${script}`,
				body,
				params,
				headers,
				{
						...options,
						bodyDataEncoding: options.bodyDataEncoding || this.contentType,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		);

		return handleTypedDataStream(
				stream,
				parseDataStream,
				dataStreamErrorCallback
		);
	}
	
	public deleteRest<
			T extends TEncoded = TEncoded,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			parseDataStream: (data: string|Uint8Array) => T,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {},
			dataStreamErrorCallback: (e: Error) => void = (_e: Error): void => {
				// do nothing
			}
	): TCommonsStreamableRestObservable<T|undefined> {
		const stream: TCommonsStreamableHttpObservable = this.delete<P, H>(
				`${this.rootUrl}${script}`,
				params,
				headers,
				{
						...options,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		);

		return handleTypedDataStream(
				stream,
				parseDataStream,
				dataStreamErrorCallback
		);
	}
}
