import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsHttpClientImplementation } from 'tscommons-es-http';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { ECommonsHttpContentType } from 'tscommons-es-http';

import { CommonsSessionRestClientService } from './commons-session-rest-client.service';

export class CommonsSimpleSinglePwSessionRestClientService extends CommonsSessionRestClientService {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			authRestCall: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, authRestCall, contentType);
	}
	
	public async authenticate(
			password: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		this.setSession(undefined);
		
		const result: unknown = await this.postRest<
				{ sid: string }
		>(
				this.authRestCall,
				{
						pw: password
				},
				undefined,
				undefined,
				options
		);
		if (!commonsTypeHasPropertyString(result, 'sid')) return false;
		
		this.setSession(result.sid as string);
		
		return true;
	}
}
