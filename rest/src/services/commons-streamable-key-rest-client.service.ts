import { ICommonsStreamableHttpClientImplementation } from 'tscommons-es-http';
import { ECommonsHttpContentType } from 'tscommons-es-http';

import { ECommonsAuthorizationMethod } from '../enums/ecommons-authorization-method';

import { CommonsStreamableAuthorizedRestClientService } from './commons-streamable-authorized-rest-client.service';

export class CommonsStreamableKeyRestClientService extends CommonsStreamableAuthorizedRestClientService {
	constructor(
			implementation: ICommonsStreamableHttpClientImplementation,
			rootUrl: string,
			authKey: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, contentType);
		
		this.setAuthorization(
				ECommonsAuthorizationMethod.KEY,
				authKey
		);
	}
}
