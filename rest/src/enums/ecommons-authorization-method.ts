import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsAuthorizationMethod {
		KEY = 'key',
		SESSION = 'session',
		BEARER = 'bearer'
}

export function toECommonsAuthorizationMethod(type: string): ECommonsAuthorizationMethod|undefined {
	switch (type) {
		case ECommonsAuthorizationMethod.KEY.toString():
			return ECommonsAuthorizationMethod.KEY;
		case ECommonsAuthorizationMethod.SESSION.toString():
			return ECommonsAuthorizationMethod.SESSION;
		case ECommonsAuthorizationMethod.BEARER.toString():
			return ECommonsAuthorizationMethod.BEARER;
	}
	return undefined;
}

export function fromECommonsAuthorizationMethod(type: ECommonsAuthorizationMethod): string {
	switch (type) {
		case ECommonsAuthorizationMethod.KEY:
			return ECommonsAuthorizationMethod.KEY.toString();
		case ECommonsAuthorizationMethod.SESSION:
			return ECommonsAuthorizationMethod.SESSION.toString();
		case ECommonsAuthorizationMethod.BEARER:
			return ECommonsAuthorizationMethod.BEARER.toString();
	}
	
	throw new Error('Unknown ECommonsAuthorizationMethod');
}

export function isECommonsAuthorizationMethod(test: unknown): test is ECommonsAuthorizationMethod {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsAuthorizationMethod(test) !== undefined;
}

export function keyToECommonsAuthorizationMethod(key: string): ECommonsAuthorizationMethod {
	switch (key) {
		case 'KEY':
			return ECommonsAuthorizationMethod.KEY;
		case 'SESSION':
			return ECommonsAuthorizationMethod.SESSION;
		case 'BEARER':
			return ECommonsAuthorizationMethod.BEARER;
	}
	
	throw new Error(`Unable to obtain ECommonsAuthorizationMethod for key: ${key}`);
}

export const ECOMMONS_AUTHORIZATION_METHODS: ECommonsAuthorizationMethod[] = Object.keys(ECommonsAuthorizationMethod)
		.map((e: string): ECommonsAuthorizationMethod => keyToECommonsAuthorizationMethod(e));
