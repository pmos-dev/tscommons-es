import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

import { IRgb, IRgba } from '../interfaces/irgb';
import { IHsl, IHsla } from '../interfaces/ihsl';
import { IHsv, IHsva } from '../interfaces/ihsv';
import { IYiq, IYiqa } from '../interfaces/iyiq';
import { ILab, ILaba } from '../interfaces/ilab';
import { IYuv, IYuva } from '../interfaces/iyuv';
import { IYCbCr, IYCbCra } from '../interfaces/iycbcr';

import { ERgba } from '../enums/ergba';
import { EHsla } from '../enums/ehsla';
import { EHsva } from '../enums/ehsva';
import { EYiqa } from '../enums/eyiqa';
import { ELaba } from '../enums/elaba';
import { EYuva } from '../enums/eyuva';
import { EYCbCra } from '../enums/eycbcra';

export function commonsColorRgbToHsl(rgb: IRgb|IRgba): IHsl|IHsla {
	const clone: IRgb = {
			[ERgba.RED]: rgb[ERgba.RED],
			[ERgba.GREEN]: rgb[ERgba.GREEN],
			[ERgba.BLUE]: rgb[ERgba.BLUE]
	};
	
	clone[ERgba.RED] /= 255;
	clone[ERgba.GREEN] /= 255;
	clone[ERgba.BLUE] /= 255;

	const max: number = Math.max(clone[ERgba.RED], clone[ERgba.GREEN], clone[ERgba.BLUE]);
	const min: number = Math.min(clone[ERgba.RED], clone[ERgba.GREEN], clone[ERgba.BLUE]);
	let h: number = 0;
	let s: number = 0;
	const l: number = (max + min) / 2;

	if (max === min) {
		h = s = 0; // achromatic
	} else {
		const d: number = max - min;
		s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
		switch (max){
			case clone[ERgba.RED]: h = (clone[ERgba.GREEN] - clone[ERgba.BLUE]) / d + (clone[ERgba.GREEN] < clone[ERgba.BLUE] ? 6 : 0); break;
			case clone[ERgba.GREEN]: h = (clone[ERgba.BLUE] - clone[ERgba.RED]) / d + 2; break;
			case clone[ERgba.BLUE]: h = (clone[ERgba.RED] - clone[ERgba.GREEN]) / d + 4; break;
		}
		h /= 6;
	}

	const hsl: IHsl = {
			[EHsla.HUE]: h * 360,
			[EHsla.SATURATION]: s,
			[EHsla.LIGHTNESS]: l
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(rgb, ERgba.ALPHA)) {
		const rgba: IRgba = rgb as IRgba;
		const hsla: IHsla = hsl as IHsla;
		
		hsla[EHsla.ALPHA] = rgba[ERgba.ALPHA];
		
		return hsla;
	}
	
	return hsl;
}

export function commonsColorRgbToHex(rgb: IRgb, hash: boolean = true): string {
	return `${hash ? '#' : ''}${[
			Math.floor(rgb[ERgba.RED]).toString(16).padStart(2, '0'),
			Math.floor(rgb[ERgba.GREEN]).toString(16).padStart(2, '0'),
			Math.floor(rgb[ERgba.BLUE]).toString(16).padStart(2, '0')
	]
			.join('')}`;
}

export function commonsColorHexToRgb(hex: string): IRgb {
	const regex: RegExp = /^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i;
	
	const match: RegExpExecArray|null = regex.exec(hex);
	if (!match) throw new Error('Invalid hex color');
	
	return {
			[ ERgba.RED ]: parseInt(match[1], 16),
			[ ERgba.GREEN ]: parseInt(match[2], 16),
			[ ERgba.BLUE ]: parseInt(match[3], 16)
	};
}

function hueToRgb(p: number, q: number, t: number): number {
	if (t < 0) t += 1;
	if (t > 1) t -= 1;
	if (t < 1 / 6) return p + (q - p) * 6 * t;
	if (t < 1 / 2) return q;
	if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
	
	return p;
}

export function commonsColorHslToRgb(hsl: IHsl|IHsla): IRgb|IRgba {
	const rgb: IRgb = {
			[ERgba.RED]: 0,
			[ERgba.GREEN]: 0,
			[ERgba.BLUE]: 0
	};
	
	const h: number = hsl[EHsla.HUE] / 360;

	if (hsl[EHsla.SATURATION] === 0) {
		rgb[ERgba.RED] = rgb[ERgba.GREEN] = rgb[ERgba.BLUE] = hsl[EHsla.LIGHTNESS];
	} else {
		const q: number = hsl[EHsla.LIGHTNESS] < 0.5 ? hsl[EHsla.LIGHTNESS] * (1 + hsl[EHsla.SATURATION]) : hsl[EHsla.LIGHTNESS] + hsl[EHsla.SATURATION] - hsl[EHsla.LIGHTNESS] * hsl[EHsla.SATURATION];
		const p: number = 2 * hsl[EHsla.LIGHTNESS] - q;
		rgb[ERgba.RED] = hueToRgb(p, q, h + 1 / 3);
		rgb[ERgba.GREEN] = hueToRgb(p, q, h);
		rgb[ERgba.BLUE] = hueToRgb(p, q, h - 1 / 3);
	}

	rgb[ERgba.RED] = Math.round(rgb[ERgba.RED] * 255);
	rgb[ERgba.GREEN] = Math.round(rgb[ERgba.GREEN] * 255);
	rgb[ERgba.BLUE] = Math.round(rgb[ERgba.BLUE] * 255);
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(hsl, EHsla.ALPHA)) {
		const hsla: IHsla = hsl as IHsla;
		const rgba: IRgba = rgb as IRgba;
		
		rgba[ERgba.ALPHA] = hsla[EHsla.ALPHA];
		return rgba;
	}

	return rgb;
}

export function commonsColorHsvToHsl(hsv: IHsv|IHsva): IHsl|IHsla {
	const l: number = hsv[EHsva.VALUE] * (1 - (hsv[EHsva.SATURATION] / 2));
	
	const hsl: IHsl = {
			[EHsla.HUE]: hsv[EHsva.HUE],
			[EHsla.SATURATION]: (l === 0 || l === 1) ? 0 : ((hsv[EHsva.VALUE] - l) / Math.min(l, 1 - l)),
			[EHsla.LIGHTNESS]: l
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(hsv, EHsva.ALPHA)) {
		const hsva: IHsva = hsv as IHsva;
		const hsla: IHsla = hsl as IHsla;
		
		hsla[EHsla.ALPHA] = hsva[EHsva.ALPHA];
		return hsla;
	}

	return hsl;
}

export function commonsColorHslToHsv(hsl: IHsl|IHsla): IHsv|IHsva {
	const v: number = hsl[EHsla.LIGHTNESS] + (hsl[EHsla.SATURATION] * Math.min(hsl[EHsla.LIGHTNESS], 1 - hsl[EHsla.LIGHTNESS]));
	
	const hsv: IHsv = {
			[EHsva.HUE]: hsl[EHsva.HUE],
			[EHsva.SATURATION]: v === 0 ? 0 : (2 * (1 - (hsl[EHsla.LIGHTNESS] / v))),
			[EHsva.VALUE]: v
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(hsl, EHsla.ALPHA)) {
		const hsla: IHsla = hsl as IHsla;
		const hsva: IHsva = hsv as IHsva;
		
		hsva[EHsva.ALPHA] = hsla[EHsla.ALPHA];
		return hsva;
	}

	return hsv;
}

export function commonsColorHsvToRgb(hsv: IHsv|IHsva): IRgb|IRgba {
	const hsl: IHsl|IHsla = commonsColorHsvToHsl(hsv);
	return commonsColorHslToRgb(hsl);
}

export function commonsColorRgbToHsv(rgb: IRgb|IRgba): IHsv|IHsva {
	const hsl: IHsl|IHsla = commonsColorRgbToHsl(rgb);
	return commonsColorHslToHsv(hsl);
}

export function commonsColorRgbToYiq(rgb: IRgb|IRgba): IYiq|IYiqa {
	// adapted from https://codepen.io/konsumer/pen/emMGRb

	const y: number = ((0.299 * rgb[ERgba.RED]) + ( 0.587 * rgb[ERgba.GREEN]) + ( 0.114 * rgb[ERgba.BLUE])) / 255;
	const i: number = ((0.596 * rgb[ERgba.RED]) + (-0.275 * rgb[ERgba.GREEN]) + (-0.321 * rgb[ERgba.BLUE])) / 255;
	const q: number = ((0.212 * rgb[ERgba.RED]) + (-0.523 * rgb[ERgba.GREEN]) + ( 0.311 * rgb[ERgba.BLUE])) / 255;

	const yiq: IYiq = {
			[ EYiqa.Y ]: y,
			[ EYiqa.I ]: i,
			[ EYiqa.Q ]: q
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(rgb, ERgba.ALPHA)) {
		const rgba: IRgba = rgb as IRgba;
		const yiqa: IYiqa = yiq as IYiqa;
		
		yiqa[EYiqa.ALPHA] = rgba[ERgba.ALPHA];
		return yiqa;
	}

	return yiq;
}

export function commonsColorYiqToRgb(yiq: IYiq|IYiqa): IRgb|IRgba {
	// adapted from https://codepen.io/konsumer/pen/emMGRb

	const r: number = (yiq[EYiqa.Y] + (0.956 * yiq[EYiqa.I]) + ( 0.621 * yiq[EYiqa.Q])) * 255;
	const g: number = (yiq[EYiqa.Y] + (-0.272 * yiq[EYiqa.I]) + (-0.647 * yiq[EYiqa.Q])) * 255;
	const b: number = (yiq[EYiqa.Y] + (-1.105 * yiq[EYiqa.I]) + ( 1.702 * yiq[EYiqa.Q])) * 255;
	
	const rgb: IRgb = {
			[ERgba.RED]: Math.max(0, Math.min(255, r)),
			[ERgba.GREEN]: Math.max(0, Math.min(255, g)),
			[ERgba.BLUE]: Math.max(0, Math.min(255, b))
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(yiq, EYiqa.ALPHA)) {
		const yiqa: IYiqa = yiq as IYiqa;
		const rgba: IRgba = rgb as IRgba;
		
		rgba[ERgba.ALPHA] = yiqa[EYiqa.ALPHA];
		return rgba;
	}

	return rgb;
}

// Adapted from:
// https://github.com/colormine/colormine/blob/master/colormine/src/main/org/colormine/colorspace/ColorSpaceConverter.java

type TXyz = {
		x: number;
		y: number;
		z: number;
};

function pivotRgb(n: number ): number {
	return (n > 0.04045 ? Math.pow((n + 0.055) / 1.055, 2.4) : n / 12.92) * 100;
}
	
function pivotXyz(n: number): number {
	const i: number = Math.cbrt(n);
	return n > 0.008856 ? i : 7.787 * n + 16 / 116;
}
	
function rgbToXyz(rgb: IRgb): TXyz {
	const r: number = pivotRgb(rgb[ERgba.RED] / 255);
	const g: number = pivotRgb(rgb[ERgba.GREEN] / 255);
	const b: number = pivotRgb(rgb[ERgba.BLUE] / 255);

	// Observer. = 2°, Illuminant = D65
	return {
			x: r * 0.4124 + g * 0.3576 + b * 0.1805,
			y: r * 0.2126 + g * 0.7152 + b * 0.0722,
			z: r * 0.0193 + g * 0.1192 + b * 0.9505
	};
}

export function commonsColorRgbToLab(rgb: IRgb|IRgba): ILab|ILaba {
	const xyz: TXyz = rgbToXyz(rgb);

	const REF_X: number = 95.047; // Observer= 2°, Illuminant= D65
	const REF_Y: number = 100;
	const REF_Z: number = 108.883;

	const x: number = pivotXyz(xyz.x / REF_X);
	const y: number = pivotXyz(xyz.y / REF_Y);
	const z: number = pivotXyz(xyz.z / REF_Z);

	const lab: ILab = {
			// tslint:disable:object-literal-sort-keys
			[ELaba.L]: 116 * y - 16,
			[ELaba.A]: 500 * (x - y),
			[ELaba.B]: 200 * (y - z)
			// tslint:enable:object-literal-sort-keys
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(rgb, ERgba.ALPHA)) {
		const rgba: IRgba = rgb as IRgba;
		const laba: ILaba = lab as ILaba;
		
		laba[ELaba.ALPHA] = rgba[ERgba.ALPHA];
		return laba;
	}

	return lab;
}

export function commonsColorRgbToYuv(rgb: IRgb|IRgba): IYuv|IYuva {
	// adapted from:
	// https://github.com/SimonWaldherr/ColorConverter.js/blob/master/colorconverter.js
	
	const y: number = Math.round(0.299 * rgb[ERgba.RED] + 0.587 * rgb[ERgba.GREEN] + 0.114 * rgb[ERgba.BLUE]);
	const u: number = Math.round((((rgb[ERgba.BLUE] - y) * 0.493) + 111) / 222 * 255);
	const v: number = Math.round((((rgb[ERgba.RED] - y) * 0.877) + 155) / 312 * 255);

	const yuv: IYuv = {
			// tslint:disable:object-literal-sort-keys
			[EYuva.Y]: y,
			[EYuva.U]: u,
			[EYuva.V]: v
			// tslint:enable:object-literal-sort-keys
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(rgb, ERgba.ALPHA)) {
		const rgba: IRgba = rgb as IRgba;
		const yuva: IYuva = yuv as IYuva;
		
		yuva[EYuva.ALPHA] = rgba[ERgba.ALPHA];
		return yuva;
	}

	return yuv;
}

export function commonsColorRgbToYCbCr(rgb: IRgb|IRgba): IYCbCr|IYCbCra {
	// adapted from:
	// http://www.openspc2.org/reibun/javascript/convert/011/
	
	const y: number = 0.299 * rgb[ERgba.RED] + 0.587 * rgb[ERgba.GREEN] + 0.114 * rgb[ERgba.BLUE];
	const cb: number = rgb[ERgba.BLUE] - y;
	const cr: number = rgb[ERgba.RED] - y;
	
	const ycbcr: IYCbCr = {
			// tslint:disable:object-literal-sort-keys
			[EYCbCra.Y]: y,
			[EYCbCra.CB]: cb,
			[EYCbCra.CR]: cr
			// tslint:enable:object-literal-sort-keys
	};
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(rgb, ERgba.ALPHA)) {
		const rgba: IRgba = rgb as IRgba;
		const ycbcra: IYCbCra = ycbcr as IYCbCra;
		
		ycbcra[EYCbCra.ALPHA] = rgba[ERgba.ALPHA];
		return ycbcra;
	}

	return ycbcr;
}

export function commonsColorBlendRgbs(
		rgbA: IRgb|IRgba,
		rgbB: IRgb|IRgba,
		weight: number,
		invertHueWheelDirection: boolean = false
): IRgb|IRgba {
	let hue: number = 0;
	
	const hslA: IHsl|IHsla = commonsColorRgbToHsl(rgbA);
	const hslB: IHsl|IHsla = commonsColorRgbToHsl(rgbB);
	
	if (invertHueWheelDirection) {
		// hue has to be calculated inverted round the colorwheel rather than directly
		
		if (hslA[EHsla.HUE] < rgbB[EHsla.HUE]) {
			const delta: number = hslA[EHsla.HUE] + (1 - hslB[EHsla.HUE]);
			hue = hslB[EHsla.HUE] + (delta * weight);
			if (hue > 1) hue -= 1;
		} else {
			const delta: number = hslB[EHsla.HUE] + (1 - hslA[EHsla.HUE]);
			hue = hslA[EHsla.HUE] + (delta * weight);
			if (hue > 1) hue -= 1;
		}
	} else {
		hue = (hslB[EHsla.HUE] * weight) + (hslA[EHsla.HUE] * (1 - weight));
	}
	
	const blend: IHsl = {
			[ EHsla.HUE ]: hue,
			[ EHsla.SATURATION ]: (hslB[EHsla.SATURATION] * weight) + (hslA[EHsla.SATURATION] * (1 - weight)),
			[ EHsla.LIGHTNESS ]: (hslB[EHsla.LIGHTNESS] * weight) + (hslA[EHsla.LIGHTNESS] * (1 - weight))
	};
	
	const rgb: IRgb = commonsColorHslToRgb(blend);
	
	// pass-through for alpha channel
	if (commonsTypeHasPropertyNumber(rgb, ERgba.ALPHA)) {
		const aa: IRgba = rgbA as IRgba;
		const ba: IRgba = rgbA as IRgba;
		const rgba: IRgba = rgb as IRgba;
		
		rgba[EHsla.ALPHA] = (ba[EHsla.ALPHA] * weight) + (aa[EHsla.ALPHA] * (1 - weight));
		return rgba;
	}

	return rgb;
}

type TOmniColorspace = IRgb|IRgba
		|IHsl|IHsla
		|IHsv|IHsva
		|IYiq|IYiqa
		|IYuv|IYuva
		|IYCbCr|IYCbCra
		|ILab|ILaba;

function peg(value: number, max: number, min: number = 0) {
	const delta: number = max - min;
	value = (value - min) / delta;
	
	return Math.max(0, Math.min(1, value));
}

export function commonsColorColorspaceToRatios<T extends TOmniColorspace>(value: T): T {
	const ratios: T = { ...value };

	if (
		commonsTypeHasPropertyNumber(ratios, ERgba.RED)
			&& commonsTypeHasPropertyNumber(ratios, ERgba.GREEN)
			&& commonsTypeHasPropertyNumber(ratios, ERgba.BLUE)
	) {
		ratios[ERgba.RED] = peg(ratios[ERgba.RED], 255);
		ratios[ERgba.GREEN] = peg(ratios[ERgba.GREEN], 255);
		ratios[ERgba.BLUE] = peg(ratios[ERgba.BLUE], 255);
	}
	
	if (
		commonsTypeHasPropertyNumber(ratios, EHsla.HUE)
			&& commonsTypeHasPropertyNumber(ratios, EHsla.SATURATION)
			&& commonsTypeHasPropertyNumber(ratios, EHsla.LIGHTNESS)
	) {
		ratios[EHsla.HUE] = peg(ratios[EHsla.HUE], 360);
	}
	
	if (
		commonsTypeHasPropertyNumber(ratios, EHsva.HUE)
			&& commonsTypeHasPropertyNumber(ratios, EHsva.SATURATION)
			&& commonsTypeHasPropertyNumber(ratios, EHsva.VALUE)
	) {
		ratios[EHsva.HUE] = peg(ratios[EHsva.HUE], 360);
	}
	
	if (
		commonsTypeHasPropertyNumber(ratios, EYiqa.Y)
			&& commonsTypeHasPropertyNumber(ratios, EYiqa.I)
			&& commonsTypeHasPropertyNumber(ratios, EYiqa.Q)
	) {
		ratios[EYiqa.I] = peg(ratios[EYiqa.I], 1, -1);
		ratios[EYiqa.Q] = peg(ratios[EYiqa.Q], 1, -1);
	}
	
	if (
		commonsTypeHasPropertyNumber(ratios, EYuva.Y)
			&& commonsTypeHasPropertyNumber(ratios, EYuva.U)
			&& commonsTypeHasPropertyNumber(ratios, EYuva.V)
	) {
		ratios[EYuva.Y] = peg(ratios[EYuva.Y], 255);
		ratios[EYuva.U] = peg(ratios[EYuva.U], 255);
		ratios[EYuva.V] = peg(ratios[EYuva.V], 255);
	}
	
	if (
		commonsTypeHasPropertyNumber(ratios, EYCbCra.Y)
			&& commonsTypeHasPropertyNumber(ratios, EYCbCra.CB)
			&& commonsTypeHasPropertyNumber(ratios, EYCbCra.CR)
	) {
		ratios[EYCbCra.Y] = peg(ratios[EYCbCra.Y], 255);
		ratios[EYCbCra.CB] = peg(ratios[EYCbCra.CB], 255, -255);
		ratios[EYCbCra.CR] = peg(ratios[EYCbCra.CR], 255, -255);
	}
	
	if (
		commonsTypeHasPropertyNumber(ratios, ELaba.L)
			&& commonsTypeHasPropertyNumber(ratios, ELaba.A)
			&& commonsTypeHasPropertyNumber(ratios, ELaba.B)
	) {
		ratios[ELaba.L] = peg(ratios[ELaba.L], 100);
		ratios[ELaba.A] = peg(ratios[ELaba.A], 255, -255);
		ratios[ELaba.B] = peg(ratios[ELaba.B], 255, -255);
	}
	
	// alpha is always assumed to be 0..255
	if (commonsTypeHasPropertyNumber(ratios, ERgba.ALPHA)) {
		ratios[ERgba.ALPHA] = peg(ratios[ERgba.ALPHA], 255, 0);
	}
	
	return ratios;
}

export function commonsColorRelativeLuminance(rgb: IRgb): number {
	const rs: number = rgb[ERgba.RED] / 255;
	const gs: number = rgb[ERgba.GREEN] / 255;
	const bs: number = rgb[ERgba.BLUE] / 255;

	const r: number = rs < 0.03928 ? (rs / 12.92) : Math.pow(((rs + 0.055) / 1.055), 2.4);
	const g: number = gs < 0.03928 ? (gs / 12.92) : Math.pow(((gs + 0.055) / 1.055), 2.4);
	const b: number = bs < 0.03928 ? (bs / 12.92) : Math.pow(((bs + 0.055) / 1.055), 2.4);

	return (0.2126 * r) + (0.7152 * g) + (0.0722 * b);
}
