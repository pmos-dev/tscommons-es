export type TXy = {
		x: number;
		y: number;
};

export type TXyz = TXy & {
		z: number;
};
