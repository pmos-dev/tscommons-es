import { TXy } from './txy';

export type TBoundary = TXy & {
		width: number;
		height: number;
};
