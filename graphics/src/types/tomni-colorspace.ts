import { IRgb, IRgba } from '../interfaces/irgb';
import { IHsl, IHsla } from '../interfaces/ihsl';
import { IHsv, IHsva } from '../interfaces/ihsv';
import { IYiq, IYiqa } from '../interfaces/iyiq';
import { ILab, ILaba } from '../interfaces/ilab';
import { IYuv, IYuva } from '../interfaces/iyuv';
import { IYCbCr, IYCbCra } from '../interfaces/iycbcr';

export type TOmniColorspace = IRgb|IRgba
		|IHsl|IHsla
		|IHsv|IHsva
		|IYiq|IYiqa
		|IYuv|IYuva
		|IYCbCr|IYCbCra
		|ILab|ILaba;
