import { EYuva } from '../enums/eyuva';

import { IColorspace } from './icolorspace';

export interface IYuv extends IColorspace {
		[EYuva.Y]: number;
		[EYuva.U]: number;
		[EYuva.V]: number;
}

export interface IYuva extends IYuv {
		[EYuva.ALPHA]: number;
}
