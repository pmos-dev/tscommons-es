export interface IDimensions {
		x: number;
		y: number;
		width: number;
		height: number;
}
