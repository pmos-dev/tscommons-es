import { EHsva } from '../enums/ehsva';

import { IColorspace } from './icolorspace';

export interface IHsv extends IColorspace {
		[EHsva.HUE]: number;
		[EHsva.SATURATION]: number;
		[EHsva.VALUE]: number;
}

export interface IHsva extends IHsv {
		[EHsva.ALPHA]: number;
}
