import { ELaba } from '../enums/elaba';

import { IColorspace } from './icolorspace';

export interface ILab extends IColorspace {
		[ELaba.L]: number;
		[ELaba.A]: number;
		[ELaba.B]: number;
}

export interface ILaba extends ILab {
		[ELaba.ALPHA]: number;
}
