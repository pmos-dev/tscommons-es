import { EYiqa } from '../enums/eyiqa';

import { IColorspace } from './icolorspace';

export interface IYiq extends IColorspace {
		[EYiqa.Y]: number;
		[EYiqa.I]: number;
		[EYiqa.Q]: number;
}

export interface IYiqa extends IYiq {
		[EYiqa.ALPHA]: number;
}
