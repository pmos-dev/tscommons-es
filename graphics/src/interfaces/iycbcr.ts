import { EYCbCra } from '../enums/eycbcra';

import { IColorspace } from './icolorspace';

export interface IYCbCr extends IColorspace {
		[EYCbCra.Y]: number;
		[EYCbCra.CB]: number;
		[EYCbCra.CR]: number;
}

export interface IYCbCra extends IYCbCr {
		[EYCbCra.ALPHA]: number;
}
