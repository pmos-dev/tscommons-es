export enum EYCbCra {
		Y = 'y',
		CB = 'cb',
		CR = 'cr',
		ALPHA = 'alpha'
}
