import {
		commonsTypeIsObject,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

export interface ICommonsXy {
		x: number;
		y: number;
		radius?: number;
}

export function isICommonsXy(test: unknown): test is ICommonsXy {
	if (!commonsTypeIsObject(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'x')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'y')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'radius')) return false;

	return true;
}
