import {
		commonsTypeIsObject,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

export interface ICommonsGeographic {
		latitude: number;
		longitude: number;
		radius?: number;
}

export function isICommonsGeographic(test: unknown): test is ICommonsGeographic {
	if (!commonsTypeIsObject(test)) return false;
	
	if (!commonsTypeHasPropertyNumber(test, 'latitude')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'longitude')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'radius')) return false;

	return true;
}
