import { commonsTypeIsString } from 'tscommons-es-core';

// NB, this is about DataField.type, i.e. the types of Fields [data field rows, uppercase F] that are supported can can be created/updated/deleted through adamantine
// It is not about possible model field types, i.e. the fields [lower case f] of models within the databases
// The main difference is that STRINGARRAY doesn't exist here, as it is considered the same as enum
// It is confusing as there is considerable overlap between the values, even though they mean two different things!

export enum ECommonsAdamantineFieldType {
		BOOLEAN = 'boolean',
		DATETIME = 'datetime',
		DATE = 'date',
		EMAIL = 'email',
		ENUM = 'enum',
		FLOAT = 'float',
		HEXRGB = 'hexrgb',
		INT = 'int',
		SMALLINT = 'smallint',
		STRING = 'string',
		TEXT = 'text',
		TIME = 'time',
		TINYINT = 'tinyint',
		URL = 'url',
		DURATION = 'duration'
}

export function toECommonsAdamantineFieldType(type: string): ECommonsAdamantineFieldType|undefined {
	switch (type) {
		case ECommonsAdamantineFieldType.BOOLEAN.toString():
			return ECommonsAdamantineFieldType.BOOLEAN;
		case ECommonsAdamantineFieldType.DATETIME.toString():
			return ECommonsAdamantineFieldType.DATETIME;
		case ECommonsAdamantineFieldType.DATE.toString():
			return ECommonsAdamantineFieldType.DATE;
		case ECommonsAdamantineFieldType.EMAIL.toString():
			return ECommonsAdamantineFieldType.EMAIL;
		case ECommonsAdamantineFieldType.ENUM.toString():
			return ECommonsAdamantineFieldType.ENUM;
		case ECommonsAdamantineFieldType.FLOAT.toString():
			return ECommonsAdamantineFieldType.FLOAT;
		case ECommonsAdamantineFieldType.HEXRGB.toString():
			return ECommonsAdamantineFieldType.HEXRGB;
		case ECommonsAdamantineFieldType.INT.toString():
			return ECommonsAdamantineFieldType.INT;
		case ECommonsAdamantineFieldType.SMALLINT.toString():
			return ECommonsAdamantineFieldType.SMALLINT;
		case ECommonsAdamantineFieldType.STRING.toString():
			return ECommonsAdamantineFieldType.STRING;
		case ECommonsAdamantineFieldType.TEXT.toString():
			return ECommonsAdamantineFieldType.TEXT;
		case ECommonsAdamantineFieldType.TIME.toString():
			return ECommonsAdamantineFieldType.TIME;
		case ECommonsAdamantineFieldType.TINYINT.toString():
			return ECommonsAdamantineFieldType.TINYINT;
		case ECommonsAdamantineFieldType.URL.toString():
			return ECommonsAdamantineFieldType.URL;
		case ECommonsAdamantineFieldType.DURATION.toString():
			return ECommonsAdamantineFieldType.DURATION;
	}
	return undefined;
}

export function fromECommonsAdamantineFieldType(type: ECommonsAdamantineFieldType): string {
	switch (type) {
		case ECommonsAdamantineFieldType.BOOLEAN:
			return ECommonsAdamantineFieldType.BOOLEAN.toString();
		case ECommonsAdamantineFieldType.DATETIME:
			return ECommonsAdamantineFieldType.DATETIME.toString();
		case ECommonsAdamantineFieldType.DATE:
			return ECommonsAdamantineFieldType.DATE.toString();
		case ECommonsAdamantineFieldType.EMAIL:
			return ECommonsAdamantineFieldType.EMAIL.toString();
		case ECommonsAdamantineFieldType.ENUM:
			return ECommonsAdamantineFieldType.ENUM.toString();
		case ECommonsAdamantineFieldType.FLOAT:
			return ECommonsAdamantineFieldType.FLOAT.toString();
		case ECommonsAdamantineFieldType.HEXRGB:
			return ECommonsAdamantineFieldType.HEXRGB.toString();
		case ECommonsAdamantineFieldType.INT:
			return ECommonsAdamantineFieldType.INT.toString();
		case ECommonsAdamantineFieldType.SMALLINT:
			return ECommonsAdamantineFieldType.SMALLINT.toString();
		case ECommonsAdamantineFieldType.STRING:
			return ECommonsAdamantineFieldType.STRING.toString();
		case ECommonsAdamantineFieldType.TEXT:
			return ECommonsAdamantineFieldType.TEXT.toString();
		case ECommonsAdamantineFieldType.TIME:
			return ECommonsAdamantineFieldType.TIME.toString();
		case ECommonsAdamantineFieldType.TINYINT:
			return ECommonsAdamantineFieldType.TINYINT.toString();
		case ECommonsAdamantineFieldType.URL:
			return ECommonsAdamantineFieldType.URL.toString();
		case ECommonsAdamantineFieldType.DURATION:
			return ECommonsAdamantineFieldType.DURATION.toString();
	}
	
	throw new Error('Unknown ECommonsAdamantineFieldType');
}

export function isECommonsAdamantineFieldType(test: unknown): test is ECommonsAdamantineFieldType {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsAdamantineFieldType(test) !== undefined;
}

export function keyToECommonsAdamantineFieldType(key: string): ECommonsAdamantineFieldType {
	switch (key) {
		case 'BOOLEAN':
			return ECommonsAdamantineFieldType.BOOLEAN;
		case 'DATETIME':
			return ECommonsAdamantineFieldType.DATETIME;
		case 'DATE':
			return ECommonsAdamantineFieldType.DATE;
		case 'EMAIL':
			return ECommonsAdamantineFieldType.EMAIL;
		case 'ENUM':
			return ECommonsAdamantineFieldType.ENUM;
		case 'FLOAT':
			return ECommonsAdamantineFieldType.FLOAT;
		case 'HEXRGB':
			return ECommonsAdamantineFieldType.HEXRGB;
		case 'INT':
			return ECommonsAdamantineFieldType.INT;
		case 'SMALLINT':
			return ECommonsAdamantineFieldType.SMALLINT;
		case 'STRING':
			return ECommonsAdamantineFieldType.STRING;
		case 'TEXT':
			return ECommonsAdamantineFieldType.TEXT;
		case 'TIME':
			return ECommonsAdamantineFieldType.TIME;
		case 'TINYINT':
			return ECommonsAdamantineFieldType.TINYINT;
		case 'URL':
			return ECommonsAdamantineFieldType.URL;
		case 'DURATION':
			return ECommonsAdamantineFieldType.DURATION;
	}
	
	throw new Error(`Unable to obtain ECommonsAdamantineFieldType for key: ${key}`);
}

export const ECOMMONS_ADAMANTINE_FIELD_TYPES: ECommonsAdamantineFieldType[] = Object.keys(ECommonsAdamantineFieldType)
		.map((e: string): ECommonsAdamantineFieldType => keyToECommonsAdamantineFieldType(e));
