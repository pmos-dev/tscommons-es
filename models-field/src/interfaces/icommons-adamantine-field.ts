import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyStringArrayOrUndefined,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyDateOrUndefined,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyFixedDate,
		commonsTypeHasPropertyFixedDuration,
		CommonsFixedDuration,
		CommonsFixedDate
} from 'tscommons-es-core';

import { ICommonsModel } from 'tscommons-es-models';

import { ECommonsAdamantineFieldType, isECommonsAdamantineFieldType } from '../enums/ecommons-adamantine-field-type';

export interface ICommonsAdamantineField extends ICommonsModel {
		name: string;
		type: ECommonsAdamantineFieldType;
		optional: boolean;
		description?: string;
		options?: string[];
		multiple?: boolean;
		defaultValue?: number|string|boolean|Date|CommonsFixedDate|CommonsFixedDuration|string[]|undefined;
		alpha?: boolean;
}

export function isICommonsAdamantineField(
		test: unknown
): test is ICommonsAdamantineField {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineFieldType>(test, 'type', isECommonsAdamantineFieldType)) return false;
	if (!commonsTypeHasPropertyBoolean(test, 'optional')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'description')) return false;
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, 'options')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'multiple')) return false;

	if (
		!commonsTypeHasPropertyNumberOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyStringOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyBooleanOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyDateOrUndefined(test, 'defaultValue')
		&& !commonsTypeHasPropertyFixedDate(test, 'defaultValue')
		&& !commonsTypeHasPropertyFixedDuration(test, 'defaultValue')
		&& !commonsTypeHasPropertyStringArrayOrUndefined(test, 'defaultValue')
	) return false;

	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'alpha')) return false;

	return true;
}
