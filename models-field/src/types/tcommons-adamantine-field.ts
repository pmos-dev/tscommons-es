import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyEnum,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyStringArrayOrUndefined,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasProperty,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyStringArray,
		commonsTypeIsDate,
		commonsDateDateToYmdHis,
		commonsTypeIsFixedDate,
		commonsTypeIsFixedDuration,
		commonsTypeIsString,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		CommonsFixedDate,
		commonsTypeAttemptNumber,
		CommonsFixedDuration
} from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models';

import { ICommonsAdamantineField } from '../interfaces/icommons-adamantine-field';

import { ECommonsAdamantineFieldType, isECommonsAdamantineFieldType } from '../enums/ecommons-adamantine-field-type';

// Can't use Omit any more
export type TCommonsAdamantineField = {
		name: string;
		type: ECommonsAdamantineFieldType;
		optional: boolean;
} & ICommonsModel;

type TCommonsAdamantineFieldWithDescription = TCommonsAdamantineField & {
		description: string;
};
type TCommonsAdamantineFieldWithOptions = TCommonsAdamantineField & {
		options: string[];
};
type TCommonsAdamantineFieldWithAlpha = TCommonsAdamantineField & {
		alpha: boolean;
};
type TCommonsAdamantineFieldWithMultiple = TCommonsAdamantineField & {
		multiple: boolean;
};
type TCommonsAdamantineFieldWithDefaultValue = TCommonsAdamantineField & {
		defaultValue: number|boolean|string|string[]|null;
};

export function isTCommonsAdamantineField(
		test: unknown
): test is TCommonsAdamantineField {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsAdamantineFieldType>(test, 'type', isECommonsAdamantineFieldType)) return false;
	if (!commonsTypeHasPropertyBoolean(test, 'optional')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'description')) return false;
	if (!commonsTypeHasPropertyStringArrayOrUndefined(test, 'options')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'alpha')) return false;
	
	if (commonsTypeHasProperty(test, 'defaultValue')) {
		if (
			!commonsTypeHasPropertyNumber(test, 'defaultValue')
			&& !commonsTypeHasPropertyBoolean(test, 'defaultValue')
			&& !commonsTypeHasPropertyString(test, 'defaultValue')
			&& !commonsTypeHasPropertyStringArray(test, 'defaultValue')
			&& test['defaultValue'] !== null
		) return false;
	}
	
	return true;
}

export function encodeICommonsAdamantineField(data: ICommonsAdamantineField): TCommonsAdamantineField {
	const encoded: TCommonsAdamantineField = {
			name: data.name,
			type: data.type,
			optional: data.optional
	};
	
	if (data.description !== undefined) {
		(encoded as TCommonsAdamantineFieldWithDescription).description = data.description;
	}
	
	if (data.options !== undefined) {
		(encoded as TCommonsAdamantineFieldWithOptions).options = data.options;
	}
	
	if (data.alpha !== undefined) {
		(encoded as TCommonsAdamantineFieldWithAlpha).alpha = data.alpha;
	}

	if (data.multiple !== undefined) {
		(encoded as TCommonsAdamantineFieldWithMultiple).multiple = data.multiple;
	}

	if (data.defaultValue !== undefined) {
		// the fixed*_ is not ideal, but no other easy way to do it

		if (commonsTypeIsDate(data.defaultValue)) {
			(encoded as TCommonsAdamantineFieldWithDefaultValue).defaultValue = commonsDateDateToYmdHis(data.defaultValue, true);
		} else if (commonsTypeIsFixedDate(data.defaultValue)) {
			(encoded as TCommonsAdamantineFieldWithDefaultValue).defaultValue = `fixeddate_${data.defaultValue.YmdHis}`;
		} else if (commonsTypeIsFixedDuration(data.defaultValue)) {
			(encoded as TCommonsAdamantineFieldWithDefaultValue).defaultValue = `fixedduration_${data.defaultValue.millis.toString(10)}`;
		} else {
			(encoded as TCommonsAdamantineFieldWithDefaultValue).defaultValue = data.defaultValue;
		}
	}
	
	return encoded;
}

export function decodeICommonsAdamantineField(encoded: TCommonsAdamantineField): ICommonsAdamantineField {
	const decoded: ICommonsAdamantineField = {
			name: encoded.name,
			type: encoded.type,
			optional: encoded.optional
	};
	
	if ((encoded as TCommonsAdamantineFieldWithDescription).description !== undefined) decoded.description = (encoded as TCommonsAdamantineFieldWithDescription).description;
	if ((encoded as TCommonsAdamantineFieldWithOptions).options !== undefined) decoded.options = (encoded as TCommonsAdamantineFieldWithOptions).options;
	if ((encoded as TCommonsAdamantineFieldWithAlpha).alpha !== undefined) decoded.alpha = (encoded as TCommonsAdamantineFieldWithAlpha).alpha;
	if ((encoded as TCommonsAdamantineFieldWithMultiple).multiple !== undefined) decoded.multiple = (encoded as TCommonsAdamantineFieldWithMultiple).multiple;

	if ((encoded as TCommonsAdamantineFieldWithDefaultValue).defaultValue !== undefined) {
		let value: unknown = (encoded as TCommonsAdamantineFieldWithDefaultValue).defaultValue;

		if (commonsTypeIsString(value)) {
			// the fixed*_ is not ideal, but no other easy way to do it
		
			if (value.startsWith('fixeddate_')) {
				const parts: string[] = value.split('_');
				if (parts.length === 2 && COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(parts[1])) {
					decoded.defaultValue = CommonsFixedDate.fromYmdHis(parts[1]);
				}
			} else if (value.startsWith('fixedduration_')) {
				const parts: string[] = value.split('_');
				if (parts.length === 2) {
					const millis: number|undefined = commonsTypeAttemptNumber(parts[1]);
					if (millis !== undefined) decoded.defaultValue = CommonsFixedDuration.fromMillis(millis);
				}
			} else {
				decoded.defaultValue = value;
			}
		} else {
			if (value === null) value = undefined;

			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			decoded.defaultValue = value as any;
		}
	}

	return decoded;
}
